#!/bin/sh

PM2_SERVER_NAME="clicky-server"

cd /opt/clicky-balls

sudo tar -xzf client-build.tar.gz -C client/
sudo tar -xzf server-build.tar.gz -C server/
sudo rm client-build.tar.gz server-build.tar.gz

cd server

sudo pm2 delete -s $PM2_SERVER_NAME || ':'
sudo pm2 start dist/server.js --name $PM2_SERVER_NAME
sudo pm2 save
sudo pm2 status
