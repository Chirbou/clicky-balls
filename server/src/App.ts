import express, { Application } from 'express'
import expressStaticGzip from 'express-static-gzip'
import { Server } from 'http'
import cors, { CorsOptions } from 'cors'
import path from 'path'
import { NetworkEventHandler } from './core/NetworkEventHandler'
import { cspExpress } from './core/Csp'

const PORT = 3333

export const CORS_ORIGIN = ['localhost:8080', 'localhost:3333']
export class App {
  server: Application = express()
  httpServer!: Server

  corsOptions: CorsOptions = {
    // origin: 'localhost:8080',
    origin: CORS_ORIGIN,
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

  run() {
    this.httpServer = this.server.listen(PORT, 'localhost', () => {
      console.log(`server listening on http://localhost:${PORT}`)
    })
    this.server.use(cors())
    this.server.use(
      expressStaticGzip(path.resolve(__dirname, '../../client/build/'), {})
    )
    this.server.use(
      '*',
      expressStaticGzip(path.resolve(__dirname, '../../client/build/'), {})
    )
    this.server.use(cspExpress)
    NetworkEventHandler.init(this.httpServer)
  }
}
