import { Colors } from '../game/Colors'
import { Game } from '../game/Game'
import { allAdjectives } from '../resources/adjectives'
import { allNouns } from '../resources/nouns'
import { Room } from '../game/rooms/Room'
import { Player } from '../game/player/Player'
import { FactionNames } from '../game/FactionNames'
import { GameObjectType } from '../game/gameobjects/GameObjectType'
import { IFactionAffinity } from '../game/interfaces/IFactionAffinity'
import { IGameObject } from '../game/interfaces/IGameObject'

export interface Coordinate {
  x: number;
  y: number;
}

export class Utils {
  static isInRadius(
    aoePosition: Coordinate,
    targetPosition: Coordinate,
    aoeRadius: number
  ): boolean {
    // (X-a)²+(Y-b)² R²
    return (
      Math.pow(targetPosition.x - aoePosition.x, 2) +
      Math.pow(targetPosition.y - aoePosition.y, 2) <
      Math.pow(aoeRadius, 2)
    )
  }

  static getRandomCoordinates(): Coordinate {
    return {
      x: Math.round(Math.random() * 800),
      y: Math.round(Math.random() * 600),
    }
  }

  static computeDamage(damage: number, def: number) {
    //return Math.round(damage - (damage * 2 * Math.atan(def / 100)) / Math.PI)
    return Math.round(damage - def)
  }

  static isInSquare(
    squareCenter?: Coordinate,
    squareSize?: number,
    dot?: Coordinate
  ): boolean {
    if (!squareCenter || !dot || (!squareSize && squareSize !== 0))
      return false
    return (
      Utils.isInRow(squareCenter.x, squareSize, dot.x) &&
      Utils.isInRow(squareCenter.y, squareSize, dot.y)
    )
  }

  static isInRow(center: number, size: number, x: number): boolean {
    return center - size / 2 <= x && center + size / 2 >= x
  }

  static getAOETargets(
    room: Room,
    position: Coordinate,
    aoeRadius: number
  ): Player[] {
    const players = room.players
    const hitByAOEPlayers = []
    for (const player of players) {
      if (Utils.isInRadius(position, player.position, aoeRadius)) {
        hitByAOEPlayers.push(player)
      }
    }
    return hitByAOEPlayers
  }

  static getRandomColor(): string {
    const rdm = Math.floor(Math.random() * (9 - 1 + 1) + 1)
    const moreRdm = `${Math.floor(Math.random() * (9 - 1 + 1) + 1)}${Math.floor(
      Math.random() * (9 - 1 + 1) + 1
    )}`

    let thatGoodColor = ''
    switch (rdm) {
      case 1:
        thatGoodColor = `rgb(113,${moreRdm},113)`
        break

      case 2:
        thatGoodColor = `rgb(13,${moreRdm},13)`
        break

      case 3:
        thatGoodColor = `rgb(113,${moreRdm},13)`
        break

      case 4:
        thatGoodColor = `rgb(13,${moreRdm},113)`

        break

      case 5:
        thatGoodColor = `rgb(${moreRdm},13,113)`

        break

      case 6:
        thatGoodColor = `rgb(${moreRdm},113,13)`

        break

      case 7:
        thatGoodColor = `rgb(${moreRdm},113,113)`

        break

      case 8:
        thatGoodColor = `rgb(113,113,${moreRdm})`

        break

      case 9:
        thatGoodColor = `rgb(13,113,${moreRdm})`
        break
    }

    return thatGoodColor
  }

  static getColorFromFaction(faction: FactionNames): string {
    let color = ''
    switch (faction) {
      case FactionNames.CONSERVATIST:
        color = Colors.CONSERVATISTS_COLOR
        break
      case FactionNames.PUNK:
        color = Colors.PUNKS_COLOR
        break
      case FactionNames.TECHNOPHILE:
        color = Colors.TECHNOPHILES_COLOR
        break
    }

    return color
  }

  static generateName() {
    return `${allAdjectives[Math.ceil(Math.random() * (allAdjectives.length - 1))]
      } ${allNouns[Math.ceil(Math.random() * (allNouns.length - 1))]}`
  }
  static getRandomInt(max: number) {
    return Math.floor(Math.random() * max)
  }
  static randomIntFromInterval(min: number, max: number) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  static getRandomPositionFromIntervalInBounds(
    position: Coordinate,
    offset: number,
    avoidPosition?: Coordinate,
    avoidOffset?: number,
    bounds: Coordinate = { x: Game.GAMEZONE_WIDTH, y: Game.GAMEZONE_HEIGHT },
  ): Coordinate {
    const minPosition = {
      x: Math.max(position.x - offset, 0),
      y: Math.max(position.y - offset, 0),
    }
    const maxPosition = {
      x: Math.min(position.x + offset, bounds.x),
      y: Math.min(position.y + offset, bounds.y),
    }
    let randomPosition: Coordinate
    let index = 0
    do {
      randomPosition = {
        x: Math.floor(Math.random() * (maxPosition.x - minPosition.x)) + minPosition.x,
        y: Math.floor(Math.random() * (maxPosition.y - minPosition.y)) + minPosition.y,
      }
      index++
    } while (
      avoidPosition &&
      avoidOffset &&
      this.isPositionWithinDistance(randomPosition, avoidPosition, avoidOffset) &&
      index < 10
    )
    return randomPosition
  }

  static isPositionWithinDistance(
    position1: Coordinate,
    position2: Coordinate,
    distance: number,
  ): boolean {
    const dx = position1.x - position2.x
    const dy = position1.y - position2.y
    const distanceSquared = dx * dx + dy * dy
    return distanceSquared <= distance * distance
  }

  //   static getRandomPositionFromIntervalInBounds(
  //   position: Coordinate,
  //   randomAmount: number,
  //   bounds = { x: Game.GAMEZONE_WIDTH, y: Game.GAMEZONE_HEIGHT }
  // ): Coordinate {
  //   let x = Utils.randomIntFromInterval(
  //     position.x - randomAmount,
  //     position.x + randomAmount
  //   )
  //   let y = Utils.randomIntFromInterval(
  //     position.y - randomAmount,
  //     position.y + randomAmount
  //   )
  //   if (x < 0) {
  //     x = 0
  //   } else if (x > bounds.x) {
  //     x = bounds.x
  //   }
  //   if (y < 0) {
  //     y = 0
  //   } else if (y > bounds.y) {
  //     y = bounds.y
  //   }
  //   return { x, y }
  // }

  static getDistance(position1: Coordinate, position2: Coordinate) {
    const x1 = position1.x
    const x2 = position2.x
    const y1 = position1.y
    const y2 = position2.y
    const res =
      (x1 - x2) * (x1 - x2) +
      (y1 - y2) * (y1 - y2) +
      ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))
    return res
  }

  static getGameObjectFromId(
    id: string,
    type: GameObjectType
  ): IGameObject | null {
    for (const room of Game.rooms) {
      const gameObject = room.gameObjects.get(type)?.find((g) => g.id === id)
      if (gameObject) {
        return gameObject
      }
    }
    return null
  }

  // static isColliding(go1: IGameObject, go2: IGameObject): boolean {
  //   return (
  //     Utils.isInSquare(go1.position, go1.hitbox, go2.position) ||
  //     Utils.isInSquare(go2.position, go2.hitbox, go1.position)
  //   )
  // }
  static isColliding(go1: IGameObject, go2: IGameObject): boolean {
    // calculate the coordinates of the top-left and bottom-right corners of the bounding boxes
    const go1TopLeft = {
      x: go1.position.x - go1.hitbox / 2,
      y: go1.position.y - go1.hitbox / 2
    }
    const go1BottomRight = {
      x: go1.position.x + go1.hitbox / 2,
      y: go1.position.y + go1.hitbox / 2
    }
    const go2TopLeft = {
      x: go2.position.x - go2.hitbox / 2,
      y: go2.position.y - go2.hitbox / 2
    }
    const go2BottomRight = {
      x: go2.position.x + go2.hitbox / 2,
      y: go2.position.y + go2.hitbox / 2
    }

    // check if the bounding boxes overlap
    return (
      go1TopLeft.x <= go2BottomRight.x &&
      go1BottomRight.x >= go2TopLeft.x &&
      go1TopLeft.y <= go2BottomRight.y &&
      go1BottomRight.y >= go2TopLeft.y
    )
  }

  static findGameObjectByTypeAndFaction(
    type: GameObjectType,
    faction: FactionNames
  ) {
    for (let i = 0; i < Game.rooms.length; i++) {
      const gameObject = (
        Game.rooms[i].gameObjects.get(type) as IFactionAffinity[]
      )?.find((o) => o.faction === faction)

      if (gameObject) {
        return gameObject
      }
    }
    return false
  }
  static findGameObjectByGameObjectType(gameObjectType: GameObjectType) {
    const gameObjects = []
    for (let i = 0; i < Game.rooms.length; i++) {
      const gameObjectsFromRoom = Game.rooms[i].gameObjects.get(gameObjectType)
      if (gameObjectsFromRoom) {
        gameObjects.push(...gameObjectsFromRoom)
      }
    }
    return gameObjects
  }

  static getRandomEmoji() {
    const emojiTab = [
      '🙈',
      '🙉',
      '🙊',
      '🦟',
      '🦗',
      '🕷',
      '🦂',
      '🐢',
      '🐍',
      '🦎',
      '🦖',
      '🦕',
      '🐙',
      '🦑',
      '🦐',
      '🦞',
      '🦀',
      '🐡',
      '🐠',
      '🐟',
      '🐬',
      '🐳',
      '🐋',
      '🦈',
      '🐊',
      '🐅',
      '🐆',
      '🦓',
      '🦍',
      '🦧',
      '🐶',
      '🐱',
      '🐭',
      '🐹',
      '🐰',
      '🦊',
      '🐻',
      '🐼',
      '🐨',
      '🐯',
      '🦁',
      '🐮',
      '🐷',
      '🐽',
      '🐸',
      '🐒',
      '🐔',
      '🐧',
      '🐦',
      '🐤',
      '🐣',
      '🐥',
      '🦆',
      '🦅',
      '🦉',
      '🦇',
      '🐺',
      '🐗',
      '🐴',
      '🦄',
      '🤑',
      '🤠',
      '😈',
      '👿',
      '👹',
      '👺',
      '🤡',
      '💩',
      '👻',
      '💀',
      '☠️',
      '👽',
      '👾',
      '🤖',
      '🎃',
      '😺',
      '🤯',
      '😳',
      '🥵',
      '🥶',
      '😱',
      '🤘',
      '🤙',
      '🖕',
      '👀',
      '👁',
      '👅',
      '👄',
      '🧠',
      '💅',
      '🤳',
      '🦜',
      '🦢',
      '🦩',
      '🕊',
      '🐇',
      '🦝',
      '🦨',
      '🦦',
      '🦥',
      '🐁',
      '🐀',
      '🐿',
      '🦔',
      '🐾',
      '🐲',
      '🍆',
      '🥒',
      '🍑',
      '🧂',
      '🐉',
    ]
    const randomNumber = this.getRandomInt(emojiTab.length)
    return emojiTab[randomNumber]
  }
  static separateUnits(num: number) {
    const unitsTabNumber: number[] = num
      .toString()
      .split('')
      .map((u) => parseInt(u))
    let multiplier = 1

    for (let i = unitsTabNumber.length - 1; i >= 0; i--) {
      unitsTabNumber[i] *= multiplier
      multiplier *= 10
    }
    return unitsTabNumber
  }
  static compareOnZindex( a:IGameObject, b:IGameObject ) {
    if ( a.zIndex > b.zIndex ){
      return -1;
    }
    if ( a.zIndex < b.zIndex ){
      return 1;
    }
    return 0;
  }
}
