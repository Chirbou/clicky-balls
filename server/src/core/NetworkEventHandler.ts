import io, { Socket } from 'socket.io'
import { Server } from 'http'
import { Game } from '../game/Game'
import { GameActions, GameEvents } from '../game/GameEventActions'
import { ClassNames } from '../game/classes/ClassNames'
import { skillsDef } from '../game/skills/core/skillsDef'
import { PlayerSelectionData } from '../game/interfaces/IPlayer'
import { classesDef } from '../game/classes/classesDef'
import { Player } from '../game/player/Player'
import { DEFAULT_CLASS_LOCK_ARCHETYPE } from '../game/classes/classeLock'

const SOCKET_CORS_ORIGIN = '*'

export class NetworkEventHandler {
  protected static server: io.Server

  static init(server: Server) {
    NetworkEventHandler.server = new io.Server(server, {
      cors: {
        origin: SOCKET_CORS_ORIGIN,
        methods: ['GET', 'POST'],
      },
    })
    NetworkEventHandler.listenEvents()
  }

  static listenEvents() {
    Game.start()

    NetworkEventHandler.server.on(GameActions.CONNECT, (socket: Socket) => {

      socket.emit(GameEvents.CONNECTED, {classesDef, classesLocks:DEFAULT_CLASS_LOCK_ARCHETYPE})

      socket.on(GameActions.PLAYER_SELECTION_READY, (selectionData: PlayerSelectionData) => {
        Game.playerSelectionReady(socket, selectionData)
      })

      socket.on(GameActions.DISCONNECT, () => {
        Game.playerLeave(socket.id)
      })

      skillsDef.forEach((skill) => {
        socket.on(skill.name, ({ idTarget, position }) => {
          Game.handleSkill(skill.name, socket.id, idTarget, position)
        })
      })

      socket.on(GameActions.UPDATE_CANON, (angle) => {
        Game.updateAngle(socket.id, angle)
      })

      socket.on(GameActions.SELECT_LVL_UP, (className: ClassNames) => {
        Game.selectLvlUp(socket.id, className)
      })

      socket.on(GameActions.REVIVE, () => {
        Game.revivePlayer(socket.id)
      })

      socket.on(GameActions.DEVOLVE, () => {
        Game.devolvePlayer(socket.id)
      })

      socket.on(GameActions.EMOTE, (emote: string) => {
        Game.emote(socket.id, emote)
      })
    })
  }

  static broadcast(eventName: GameEvents, data: unknown = null) {
    try {
      NetworkEventHandler.server.emit(eventName, data)
    } catch (e) {
      console.log(e)
      console.error('Could not broadcast !', data)
    }
  }
}
