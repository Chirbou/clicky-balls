function* infinite() {
  let index = 0

  while (true) {
    yield index++
  }
}

const generator = infinite()

export const getUUID = (): string => {
  return `${generator.next().value}`
}