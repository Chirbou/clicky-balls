import {
  ExpressCSPParams,
  SELF,
  expressCspHeader,
  EVAL,
  INLINE,
} from 'express-csp-header'

export const cspParams: ExpressCSPParams = {
  directives: {
    'default-src': [SELF,'data:'],
    'img-src': [SELF,'data:'],
    'script-src': [SELF, EVAL, 'data:', 'http://37.187.75.123', '37.187.75.123'],
    'style-src': [SELF, INLINE, 'data:',],
    'font-src': [SELF, 'data:'],
    'connect-src': [SELF,'data:'],
    'object-src': [SELF, 'data:'],
  },
}

export const cspExpress = expressCspHeader(cspParams)
