import { dropGold } from '../gameobjects/Gold'
import { IHasHealthPoint, IHasHealthPointData } from './IHasHealthPoint'

export interface IPlunderable extends IHasHealthPoint {
  goldAmount: number,
  wasRecentlyBroken: boolean,
  unplunderableDuration: number,
}

export interface IPlunderableData extends IHasHealthPointData {
  goldAmount: number,
  wasRecentlyBroken: boolean,
  unplunderableDuration: number,
}

export const loseHalfGold = (gameObject: IPlunderable) => {
  gameObject.goldAmount = Math.round(gameObject.goldAmount / 2)
  dropGold(gameObject.goldAmount, gameObject.room, gameObject.position)
  setRecentlyBroken(gameObject, true)
  return gameObject.goldAmount
}

export const loseAllGold = (gameObject: IPlunderable) => {
  dropGold(gameObject.goldAmount, gameObject.room, gameObject.position)
  gameObject.goldAmount = 0
}

export const setRecentlyBroken = (gameObject: IPlunderable, wasRecentlyBroken: boolean) => {
  gameObject.wasRecentlyBroken = wasRecentlyBroken
}
