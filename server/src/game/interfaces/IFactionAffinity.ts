import { FactionAffinity } from '../FactionAffinity'
import { FactionNames } from '../FactionNames'
import { IGameObject, IGameObjectData } from './IGameObject'

export interface IFactionAffinity extends IGameObject {
  faction: FactionNames
  factionAffinity?: FactionAffinity
  color?: string
  initFactionAffinity?: () => void
  updateFaction?: (faction: FactionNames) => void
}

export const isFactionnable = (gameObject: IGameObject | IFactionAffinity | undefined) => {
  return (gameObject as IFactionAffinity)?.faction !== undefined
}

export interface IFactionAffinityData extends IGameObjectData {
  faction: FactionNames
  factionAffinity?: FactionAffinity
  color?: string
}
  
export const defaultUpdateFaction = (gameObject: IFactionAffinity, faction: FactionNames) => {
  gameObject.faction = faction
}