import { Utils } from '../../core/Utils'
import { Game } from '../Game'
import { GameEvents } from '../GameEventActions'
import { ScoreShiftData } from '../ScoreShiftData'
import { ScoreShiftType } from '../skills/core/Skill'
import { SkillNames } from '../skills/core/SkillNames'
import { IGameObject, IGameObjectData } from './IGameObject'


// TODO faire des gameObject qui peuvent être Disabled

export interface ICanBeDisabled extends IGameObject {
  
}

export interface ICanBeDisabledData extends IGameObjectData {
  disabled: boolean
}


