import { IGameObject } from './IGameObject'

export interface ITriggerable extends IGameObject {
  cooldownBeforeActivation: number
  trigger(gameObject: IGameObject): void
  cancel(gameObject: IGameObject): void
}