import { ScoreLandings } from '../classes/Classe'
import { ClassLockArchetype, getAvailableClasses } from '../classes/classeLock'
import { classesDef } from '../classes/classesDef'
import { ClassNames } from '../classes/ClassNames'
import { GameEvents } from '../GameEventActions'
import { IGameObject, IGameObjectData } from './IGameObject'
import { IHasHealthPoint } from './IHasHealthPoint'

export interface IEvolveable extends IGameObject {
  currentClass: ClassNames
  level: number
  canEvolve: boolean
  scoreToEvolve?: ScoreLandings 
  scoreToDevolve?: ScoreLandings
  levelUp(className: ClassNames): void
  levelDown(className: ClassNames | undefined): void
  checkLevelUp(): void
  checkLevelDown(): void
}

export interface IEvolveableData extends IGameObjectData {
  currentClass: ClassNames
  level: number
  canEvolve: boolean
  scoreToEvolve?: ScoreLandings 
  scoreToDevolve?: ScoreLandings
}

export const defaultCheckLvlUp = (evolvable: IEvolveable & IHasHealthPoint, classLocks: ClassLockArchetype[]) => {
  const currentClass = classesDef.find((c) => c.name === evolvable.currentClass)
  const scoreToLvlUp = currentClass?.stats.scoreToEvolve
  
  if (!evolvable.canEvolve && scoreToLvlUp && evolvable.hp >= scoreToLvlUp) {
    evolvable.canEvolve = true
    evolvable.emitGameEvent(GameEvents.CAN_EVOLVE, getAvailableClasses(evolvable.currentClass, classLocks))
  }
}

