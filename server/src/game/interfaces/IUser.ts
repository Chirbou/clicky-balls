import { Socket } from 'socket.io'
import { ClassLockArchetype } from '../classes/classeLock'

export interface IUser {
  socket: Socket
  classesLocks: ClassLockArchetype[]
}

export interface IUserData {
  classesLocks: ClassLockArchetype[]
}