import { StatsShift, ClasseStats, Classe } from '../classes/Classe'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Player } from '../player/Player'
import { PlayerSkillData } from '../skills/core/PlayerSkillData'
import { SkillFeedbackData } from '../skills/core/SkillFeedbackData'
import { SkillNames } from '../skills/core/SkillNames'
import { SkillResults } from '../skills/core/SkillResults'
import { IAlterable, IAlterableData } from './IAlterable'
import { IEvolveable, IEvolveableData } from './IEvolveable'
import { IGameObject, UNSTACKABLE_GAME_OBJECTS, IGameObjectData } from './IGameObject'
import { IFactionAffinity, IFactionAffinityData } from './IFactionAffinity'
import { IUser, IUserData } from './IUser'
import { IHasHealthPoint, IHasHealthPointData } from './IHasHealthPoint'
import { Game } from '../Game'
import { GameEvents } from '../GameEventActions'
import { Flag } from '../gameobjects/Flag'
import { Utils } from '../../core/Utils'
import { dropGold, Gold } from '../gameobjects/Gold'
import { DEFAULT_CLASS } from '../classes/classesDef'
import { ClassNames } from '../classes/ClassNames'
import { toRand } from '../rooms/PortalPositions'
import { PlayerResume } from '../player/PlayerResume'
import { FactionNames } from '../FactionNames'
import { ClassLockArchetype } from '../classes/classeLock'

export const BASE_SCORE = 100
export const BASE_LEVEL = 1

export interface IPlayerData extends IGameObjectData, IUserData, IEvolveableData, IAlterableData, IHasHealthPointData, IFactionAffinityData {
  emoji: string
  atk: number
  def: number
  heal: number
  angle: number
  tpStackMax: number
  ownedGold: number
  totalGold: number
  goldStackMax: number
  inventory: IGameObjectData[]
  dead: boolean
  martyrProtectorId?: string
  gluedPlayersIds?: string[]
  contaminedPlayersIds?: string[]
  suckingPlayersIds: string[]
  sufferedDamage: number
  gameZoneWidth: number
  gameZoneHeight: number
  allPlayerCounter: number
  playersResume: PlayerResume[]
}

export interface IPlayer extends IGameObject, IUser, IEvolveable, IAlterable, IHasHealthPoint, IFactionAffinity {
  emoji: string
  atk: number
  def: number
  heal: number
  angle: number
  tpStackMax: number
  ownedGold: number
  totalGold: number
  goldStackMax: number
  inventory: IGameObject[]
  dead: boolean
  playerSkillData: Map <SkillNames, PlayerSkillData>
  timeout?: NodeJS.Timeout
  martyrProtector?: Player
  gluedPlayers: Player[] | undefined
  contaminedPlayers: Player[] | undefined
  suckingPlayers: Player[] | undefined
  sufferedDamage: number
  inventoryContains(type: GameObjectType): void
  dropExcessGold(newGoldStackMax: number): void
  revive(): void
  die(): void
  checkDeath(): void
  getPlayerSkillData(skillName: SkillNames): void
  shiftStats(statsShift: StatsShift, type: 'add' | 'sub'): void
  setClasseStats(classeStats: ClasseStats): void
  resetSkillOnTimeout(playerSkillData?: PlayerSkillData): void
  handleSkillFeedback(feedback: SkillFeedbackData, result: SkillResults): void
  initStats(classe: Classe | undefined): void
  setPlayerSkillData(skillNames: SkillNames[]): void
  syncPlayerSkillData(): void
  syncAllPlayers(): void
}

export const isPlayer = (gameObject: IGameObject | IPlayer | undefined) => {
  return (gameObject as IPlayer)?.playerSkillData !== undefined
}

export const defaultDie = (player: IPlayer) => {
  player.level = 1
  player.atk = 0
  player.def = 0
  player.heal = 0
  player.dead = true
  player.hp = 0
  const room = player.room

  if(player.ownedGold > 0) {
    player.dropExcessGold(0)
    player.ownedGold = 0
  }

  const flag = player.inventory.find(g => GameObjectType.FLAG === g.type) as Flag
  if (flag && room) {
    flag.position = {
      x: player.position.x,
      y: player.position.y
    }

    let tryCount = 0
    let isColliding = false

    do {
      tryCount++

      isColliding = !!room.getObjectsByTypes(UNSTACKABLE_GAME_OBJECTS).find(g =>
        g.id !== flag.id && g.type !== GameObjectType.FLAGBASE && Utils.isColliding(flag, g)
      )

      if (isColliding) {
        flag.position.x = Math.floor(1100 * Math.random())
        flag.position.y = Math.floor(700 * Math.random())
      }
    } while (isColliding && tryCount < 10)
    flag.dropFlag(player.room, player.position, null)
    player.inventory = player.inventory.filter(g => GameObjectType.FLAG !== g.type)
  }
  room?.emitGameEvent(GameEvents.ROOM_UPDATE, room.normalize())
}

export const defaultRevive = (player: IPlayer) => {
  player.hp = BASE_SCORE
  player.dead = false
  player.position = toRand()
  player.currentClass = ClassNames.PECNO
  player.gluedPlayers = []
  player.contaminedPlayers = []
  player.level = BASE_LEVEL
  player.martyrProtector = undefined
  player.sufferedDamage = 0
  player.disabled = false
  player.inventory = []
  player.initStats(DEFAULT_CLASS)
  player.room.emitGameEvent(
    GameEvents.PLAYER_UPDATE,
    player?.normalize()
  )
}

export const defaultDropExcessGold = (player: IPlayer, newGoldStackMax: number) => {
  const room = player.room
  if (!room) return

  if(player.ownedGold > newGoldStackMax){
    dropGold((player.ownedGold - newGoldStackMax), room, player.position)
    player.ownedGold = newGoldStackMax
    room.emitGameEvent(GameEvents.ROOM_UPDATE, room.normalize())
  }
}

export interface PlayerSelectionData {
  name: string
  emoji: string
  faction: FactionNames
  classesLocks : ClassLockArchetype[]
}
