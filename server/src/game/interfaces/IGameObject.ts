import { Room } from '../rooms/Room'
import { Coordinate } from '../../core/Utils'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Player } from '../player/Player'

export interface IGameObject {
  id: string
  name: string
  type: GameObjectType
  disabled: boolean
  position: Coordinate
  room: Room
  roomId: string
  zIndex: number
  size: number
  hitbox: number
  removeFromRoom(): void
  normalize(): IGameObjectData
  emitGameEvent(gameEvent: GameEvents, data: unknown): void
  trigger(player: Player): void
}

export interface IGameObjectData {
  id: string
  name: string
  type: GameObjectType
  disabled: boolean
  position: Coordinate
  roomId: string
  zIndex: number
  size: number
  hitbox: number
}

export const UNSTACKABLE_GAME_OBJECTS: GameObjectType[] = [
  GameObjectType.POWER_UP,
  GameObjectType.FLAG,
  GameObjectType.PORTAL,
  GameObjectType.FLAGBASE,
  GameObjectType.GOLD_DEPOSIT,
  GameObjectType.GOLD_MINE
]

export const defaultRemoveFromRoom = (gameObject: IGameObject) => {
  gameObject.room.gameObjects.set(
    gameObject.type,
    gameObject.room.gameObjects.get(gameObject.type)?.filter(g => g.id !== gameObject.id) || []
  )

  gameObject.room.emitGameEvent(GameEvents.ROOM_UPDATE, gameObject.room.normalize())
}

export const defaultEmitGameEvent = (gameObject: IGameObject, gameEvent: GameEvents, data: unknown) => {
  gameObject.room.emitGameEvent(gameEvent, data)
}
