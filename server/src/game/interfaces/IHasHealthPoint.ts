import { Utils } from '../../core/Utils'
import { Game } from '../Game'
import { GameEvents } from '../GameEventActions'
import { ScoreShiftData } from '../ScoreShiftData'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Attitude, Tower } from '../gameobjects/Tower'
import { ScoreShiftType } from '../skills/core/Skill'
import { SkillNames } from '../skills/core/SkillNames'
import { IGameObject, IGameObjectData } from './IGameObject'

export interface IHasHealthPoint extends IGameObject {
  hp: number
  maxHp: number
  hit(player: IGameObject, skillName: SkillNames): void
  takeDamage(damage: number): number
  takeHeal(heal: number): number
  takeRawDamage(damage: number): number
  handleScoreShiftData(
    playerSource: IHasHealthPoint,
    playerTarget: IHasHealthPoint | undefined,
    value: number | undefined,
    scoreshiftType: ScoreShiftType,
    isCritic: boolean,
  ): void

}

export interface IHasHealthPointData extends IGameObjectData {
  hp: number
  maxHp: number
}

export const defaultTakeHeal = (gameObject: IHasHealthPoint, heal: number) => {
  heal = Math.abs(heal)
  if (gameObject.disabled) return 0
  if (gameObject.hp > 0) {
    gameObject.hp += heal
    return heal
  } else {
    return 0
  }
}

export const defaultTakeDamage = (gameObject: IHasHealthPoint, damage: number): number => {

  damage = Math.max(Utils.computeDamage(Math.abs(damage), 0), 1)
  if (gameObject.disabled) return 0
  const towers = gameObject.room.getObjectsByType(GameObjectType.TOWER) as Tower[]
  if (towers.length) {
    towers.map(tower => tower.triggerAttackMode())
  }
  if (gameObject.hp > 0) {
    return gameObject.takeRawDamage(damage)
  } else {
    return 0
  }
}

export const defaultHandleScoreShiftData = (
  playerSource: IHasHealthPoint,
  playerTarget: IHasHealthPoint | undefined,
  value: number | undefined,
  scoreshiftType: ScoreShiftType,
  isCritic: boolean,
  skill?: SkillNames
) => {
  const roomSource = playerSource.room
  const roomTarget = playerTarget?.room
  const eventValue: ScoreShiftData = {
    idSource: playerSource.id,
    idTarget: playerTarget?.id,
    isCritic: isCritic,
    isSuccessful: true,
    scoreSource: playerSource.hp,
    scoreTarget: playerTarget?.hp,
    skill: skill ? skill : SkillNames.DPS_ATK,
    type: scoreshiftType,
    value: value,
  }

  if (roomTarget && roomSource !== roomTarget) {
    roomSource.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
    roomTarget.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
  } else {
    roomSource.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
  }
}

export const defaultTakeRawDamage = (gameObject: IHasHealthPoint, damage: number): number => {
  if (gameObject.hp > 0) {
    gameObject.hp -= Math.round(damage)
    return damage
  } else {
    return 0
  }
}

export const isAnHealthGameObject = (obj: IGameObject): obj is IHasHealthPoint => {
  return 'id' in obj && 'hp' in obj && 'maxHp' in obj
}
