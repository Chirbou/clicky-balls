import { Alteration, AlterationNames, AlterationData } from '../alterations/Alteration'
import { Player } from '../player/Player'
import { IFactionAffinity, isFactionnable } from './IFactionAffinity'
import { IGameObject } from './IGameObject'
import { IHasHealthPoint } from './IHasHealthPoint'

export interface IAlterable extends IFactionAffinity, IHasHealthPoint{
  alterations: Alteration[]
  suckingPlayers: Player[] | undefined
  applyAlteration(alterationName: AlterationNames, cooldownDuration: number, source?:IGameObject): void
  applyAlterationForever(alterationName: AlterationNames): void
  removeAlteration(alterationName: AlterationNames): void
  removeAllAlterations(): void
  hasAlteration(alterationName: AlterationNames): boolean
  getAlteration(alterationName: AlterationNames): Alteration | undefined
}

export interface IAlterableData {
  alterations: AlterationData[]
}

export const isAlterable = (obj: IGameObject): obj is IAlterable => {
  return 'alterations' in obj
}

export const defaultHasAlteration = (gameObject: IAlterable, alterationName: AlterationNames): boolean => {
  return !!gameObject.alterations.find(
    (alteration) => alteration.name === alterationName
  )
}

export const defaultGetAlteration = (
  gameObject: IAlterable, alterationName: AlterationNames
): Alteration | undefined => {
  return gameObject.alterations.find(
    (alteration) => alteration.name === alterationName
  )
}
