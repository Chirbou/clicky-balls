import { SkillNames } from './skills/core/SkillNames'
import { AlterationNames } from './alterations/Alteration'
export interface ScoreShiftData {
  idSource?: string
  idTarget?: string
  scoreSource?: number,
  scoreTarget?: number,
  type?: 'damage' | 'heal'
  value?: number
  isCritic?: boolean
  isSuccessful?: boolean
  skill?: SkillNames | AlterationNames | undefined
}
