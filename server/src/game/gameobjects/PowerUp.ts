import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { AlterationNames } from '../alterations/Alteration'
import { GameEvents } from '../GameEventActions'
import { SkillNames } from '../skills/core/SkillNames'
import { defaultEmitGameEvent, defaultRemoveFromRoom, IGameObject, IGameObjectData } from '../interfaces/IGameObject'

export interface PowerUpData extends IGameObjectData {
  buffDuration: number
}

export enum PowerUpType {
  BUFF_HEAL = 'BUFF_HEAL',
  BUFF_DAMAGE = 'BUFF_DAMAGE',
}

export interface PowerUpEffect {
  type: PowerUpType,
  power: number,
  duration: number
}

export class PowerUp implements IGameObject {
  id: string = getUUID()
  type = GameObjectType.POWER_UP
  position: Coordinate
  size = 50
  hitbox = 50
  room: Room
  roomId: string
  effects: PowerUpEffect[] = []
  buffDuration = 30000
  name = ''
  disabled = false
  zIndex = 0

  constructor(room: Room, position: Coordinate = { x: 0, y: 0 }) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.room.addGameObject(this.type, this)
  }

  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }

  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  appear() {

  }

  disappear() {

  }

  hitBy(player: Player) {

  }

  pickupBy(player: Player) {

  }

  trigger(player: Player) {
    const isPlayerAlreadyBuffed = player.hasAlteration(AlterationNames.BUFF_ATK)
      || player.hasAlteration(AlterationNames.BUFF_HEAL)

    if (isPlayerAlreadyBuffed) return

    if (Utils.getRandomInt(2) === 0) {
      player.applyAlteration(AlterationNames.BUFF_ATK, this.buffDuration)
    } else {
      player.applyAlteration(AlterationNames.BUFF_HEAL, this.buffDuration)
    }
    this.removeFromRoom()
    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }

  cancel(player: Player) { }

  hit(player: Player, skillName: SkillNames) {

  }

  normalize(): PowerUpData {
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      size: this.size,
      hitbox: this.hitbox,
      buffDuration: this.buffDuration,
      name: this.name,
      disabled: this.disabled,
      roomId: this.roomId,
      zIndex: this.zIndex
    }
  }
}
