import { Player } from '../player/Player'
import { IGameObjectData } from '../interfaces/IGameObject'
import { IHasHealthPointData } from '../interfaces/IHasHealthPoint'
import { IFactionAffinityData } from '../interfaces/IFactionAffinity'
import { Tower } from './Tower'
import { GameEvents } from '../GameEventActions'

export interface TowerData extends IGameObjectData, IHasHealthPointData, IFactionAffinityData {
  cooldown: number | null
  amount: number
  destroyed: boolean
  orientation: number
}

export const DEFAULT_TOWER_MAXHP = 1000

export class PlayerTower extends Tower {
  holderId: string
  lifespan: number
  timeout: NodeJS.Timeout

  constructor(playerSource: Player, shootRate: number, damage: number, lifespan: number, maxHp: number) {
    super(playerSource.room, playerSource.faction, playerSource.position, maxHp, shootRate, damage)
    this.lifespan = lifespan
    this.holderId = playerSource.id

    this.timeout = setTimeout(() => {
      this.destroy()
    }, this.lifespan)
  }
  destroy(): void {
    this.destroyed = true
    this.room.removeGameObject(this.type,this)
    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }
}
