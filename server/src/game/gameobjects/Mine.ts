import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { FactionNames } from '../FactionNames'
import { IGameObject, IGameObjectData } from '../interfaces/IGameObject'
import { Game } from '../Game'
import { ScoreShiftType } from '../skills/core/Skill'
import { SkillNames } from '../skills/core/SkillNames'
import {
  defaultHandleScoreShiftData,
  defaultTakeDamage,
  defaultTakeHeal,
  defaultTakeRawDamage,
  IHasHealthPoint,
  IHasHealthPointData,
} from '../interfaces/IHasHealthPoint'
import { IFactionAffinity, IFactionAffinityData } from '../interfaces/IFactionAffinity'

export interface MineData extends IGameObjectData, IHasHealthPointData, IFactionAffinityData {
  ownerId: string | null
  cooldown: number | null
}

export class Mine implements IGameObject, IHasHealthPoint, IFactionAffinity {
  type = GameObjectType.MINE
  id = getUUID()
  position: Coordinate
  size = 50
  hitbox = 100
  ownerId: string | null
  cooldownBeforeActivation = 3000
  faction: FactionNames
  triggerable = false
  INITIALIZATION_DURATION = 4000
  aoeRadius = 200
  damage = 50
  owner: Player
  hp = 0
  maxHp = 0
  name = ''
  disabled = false
  room: Room
  roomId: string
  zIndex = 0
  lifespan = 60 * 1000

  constructor(
    room: Room,
    faction: FactionNames,
    position: Coordinate = { x: 0, y: 0 },
    ownerId: string | null
  ) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.faction = faction
    this.ownerId = ownerId
    this.owner = Game.players.find((p) => p.id === this.ownerId) as Player
    this.size = this.owner.size
    this.hitbox = this.owner.size * 2
    if (ownerId) {
      this.triggerable = false

      setTimeout(() => {
        this.triggerable = true
      }, this.INITIALIZATION_DURATION)
    }
    this.room.addGameObject(this.type, this)

    setTimeout(() => {
      this.trigger()
    }, this.lifespan)
  }

  takeDamage(damage: number): number {
    return defaultTakeDamage(this, damage)
  }
  takeHeal(heal: number): number {
    return defaultTakeHeal(this, heal)
  }
  takeRawDamage(damage: number): number {
    return defaultTakeRawDamage(this, damage)
  }
  handleScoreShiftData(
    playerSource: IHasHealthPoint,
    playerTarget: IHasHealthPoint | undefined,
    value: number | undefined,
    scoreshiftType: ScoreShiftType,
    isCritic: boolean
  ): void {
    return defaultHandleScoreShiftData(playerSource, playerTarget, value, scoreshiftType, isCritic)
  }

  removeFromRoom(): void {
    throw new Error('Method not implemented.')
  }
  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    throw new Error('Method not implemented.')
  }
  hit(player: Player, skillName: SkillNames): void { }

  trigger(player?: Player) {
    if (this.triggerable) {
      const roomPlayers = this.room.players
      const targets = Utils.getAOETargets(
        this.room,
        this.position,
        this.aoeRadius
      )
      
      roomPlayers.forEach((p) => {
        p.emitGameEvent(GameEvents.GAMEOBJECT_ON, { type: GameObjectType.MINE, id: this.id })
      })
      targets.forEach((p) => {
        p.takeDamage(this.damage)
        if (this.owner)
          this.handleScoreShiftData(this.owner, p, this.damage, ScoreShiftType.DAMAGE, false)
      })

      this.remove()
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
    }
  }

  cancel(player: Player) { }

  remove() {
    this.owner.inventory = this.owner.inventory.filter((o) => o.id !== this.id)
    this.room.gameObjects.set(
      this.type,
      this.room.gameObjects.get(this.type)?.filter((g) => g.id !== this.id) || []
    )

    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }

  normalize(): MineData {
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      size: this.size,
      hitbox: this.hitbox,
      ownerId: this.ownerId,
      faction: this.faction,
      cooldown: this.cooldownBeforeActivation,
      disabled: this.disabled,
      hp: this.hp,
      maxHp: this.maxHp,
      name: this.name,
      roomId: this.roomId,
      zIndex: this.zIndex,
    }
  }
}
