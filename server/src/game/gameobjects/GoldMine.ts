import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from '../rooms/Room'
import { GameObjectType } from './GameObjectType'
import { SkillNames } from '../skills/core/SkillNames'
import { dropGold, Gold } from './Gold'
import { defaultHandleScoreShiftData, defaultTakeDamage, defaultTakeHeal, defaultTakeRawDamage, IHasHealthPoint, IHasHealthPointData } from '../interfaces/IHasHealthPoint'
import { defaultEmitGameEvent, defaultRemoveFromRoom, IGameObject, IGameObjectData } from '../interfaces/IGameObject'
import { GameEvents } from '../GameEventActions'
import { ScoreShiftType } from '../skills/core/Skill'
import { loseAllGold, IPlunderable, IPlunderableData } from '../interfaces/IPlunderable'
import { Game } from '../Game'

export interface GoldMineData extends IPlunderableData, IGameObjectData, IHasHealthPointData {
  type: GameObjectType
  position: Coordinate
  size: number
  cooldown: number | null
  id: string
}

export class GoldMine implements IPlunderable, IGameObject, IHasHealthPoint {
  id: string = getUUID()
  type: GameObjectType = GameObjectType.GOLD_MINE
  position: Coordinate
  size = 100
  hitbox = 100
  zIndex = 1
  cooldownBeforeActivation = 0
  goldAmount = 100
  hp = 100
  hitCount = 0
  name = ''
  disabled = false
  room: Room
  roomId: string
  maxHp = 0
  wasRecentlyBroken = false
  unplunderableDuration = 15000

  constructor(room: Room, maxHp: number, position: Coordinate = { x: 0, y: 0 }) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.hp = maxHp
    this.maxHp = maxHp
    this.goldAmount = 5000
    this.room.addGameObject(this.type, this)
    Game.totalGoldInGame+=this.goldAmount
  }

  takeDamage(damage: number): number {
    if((this.hp - damage)  <= 0){
      this.hp = 0
      damage = this.hp
    }
    return defaultTakeDamage(this, damage)
  }

  takeHeal(heal: number): number {
    return defaultTakeHeal(this, 0)
  }

  takeRawDamage(damage: number): number {
    if (Math.random() < 0.1) {
      const gold = Math.floor(damage * 5 + Math.random() * 20 * damage)
      dropGold(gold, this.room, this.position, this.size + 40, this.position, this.size)
      Game.totalGoldInGame+=gold
    }

    return defaultTakeRawDamage(this, damage)
  }

  handleScoreShiftData(playerSource: IHasHealthPoint, playerTarget: IHasHealthPoint | undefined, value: number | undefined, scoreshiftType: ScoreShiftType, isCritic: boolean): void {
    defaultHandleScoreShiftData(playerSource, playerTarget, value, scoreshiftType, isCritic)
  }

  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }
  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  hit(player: Player, skillName: SkillNames) {

    if (this.hp <= 0) {
      loseAllGold(this)
      //this.remove()
    }
  }

  trigger(player: Player) {

  }

  cancel(player: Player) {

  }

  normalize(): GoldMineData {
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      size: this.size,
      hitbox: this.hitbox,
      cooldown: this.cooldownBeforeActivation,
      goldAmount: this.goldAmount,
      hp: this.hp,
      roomId: this.roomId,
      zIndex: this.zIndex,
      disabled: this.disabled,
      maxHp: this.maxHp,
      name: this.name,
      wasRecentlyBroken: this.wasRecentlyBroken,
      unplunderableDuration: this.unplunderableDuration,
    }
  }

  remove(): void {
    this.room.gameObjects.set(
      GameObjectType.GOLD_MINE,
      this.room.gameObjects.get(this.type)?.filter(g => g.id !== this.id) || []
    )
  }
}
