import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { FactionNames } from '../FactionNames'
import { IGameObject, defaultRemoveFromRoom, defaultEmitGameEvent, IGameObjectData } from '../interfaces/IGameObject'
import { defaultHandleScoreShiftData, defaultTakeDamage, defaultTakeHeal, defaultTakeRawDamage, IHasHealthPoint, IHasHealthPointData } from '../interfaces/IHasHealthPoint'
import { Game } from '../Game'
import { ScoreShiftType } from '../skills/core/Skill'
import { SkillNames } from '../skills/core/SkillNames'
import { IFactionAffinity, IFactionAffinityData } from '../interfaces/IFactionAffinity'
import { FactionAffinity } from '../FactionAffinity'

export interface TotemData extends IGameObjectData, IHasHealthPointData, IFactionAffinityData {
  ownerId?: string
  cooldown: number
  amount: number
}

export class Totem implements IGameObject, IHasHealthPoint, IFactionAffinity {
  id: string = getUUID()
  type: GameObjectType = GameObjectType.TOTEM
  position: Coordinate
  size = 50
  hitbox = 50
  ownerId?: string
  room: Room
  roomId: string
  cooldownBeforeActivation = 3000
  faction: FactionNames
  INITIALIZATION_DURATION = 4000
  aoeRadius = 200
  heal = 10
  timeBetweenIteration = 1000
  iteration = 0
  iterationMax = 30
  owner: Player
  interval: NodeJS.Timeout
  zIndex = 0
  name = 'Totem'
  disabled = false
  hp = 100
  maxHp = 150
  amount = 5

  triggerable = false

  constructor(room: Room, faction: FactionNames, position: Coordinate = { x: 0, y: 0 }, ownerId?: string) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.faction = faction
    this.ownerId = ownerId
    this.owner = Game.players.find(p => p.id === this.ownerId) as Player
    this.interval = setInterval(() => { this.act() }, this.timeBetweenIteration)

    setTimeout(() => {
      this.triggerable = true
    }, this.INITIALIZATION_DURATION)
    this.room.addGameObject(this.type, this)
  }

  hit(player: IGameObject, skillName: SkillNames): void {
    if (this.hp <= 0) {
      this.remove()
    }
  }

  factionAffinity?: FactionAffinity | undefined
  color?: string | undefined

  takeDamage(damage: number): number {
    return defaultTakeDamage(this, damage)
  }

  takeHeal(heal: number): number {
    return defaultTakeHeal(this, heal)
  }

  takeRawDamage(damage: number): number {
    return defaultTakeRawDamage(this, damage)
  }

  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }

  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
    this.room.emitGameEvent(gameEvent, data)
  }

  trigger(player: Player) {
    if (this.triggerable && player.faction !== this.faction) {
      this.remove()
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
    }
  }

  handleScoreShiftData(
    playerSource: IHasHealthPoint,
    playerTarget: IHasHealthPoint | undefined,
    value: number | undefined,
    scoreshiftType: ScoreShiftType,
    isCritic: boolean
  ): void {
    defaultHandleScoreShiftData(playerSource, playerTarget, value, scoreshiftType, isCritic)
  }

  remove() {
    if (this.interval) {
      clearInterval(this.interval)
    }
    this.owner.inventory = this.owner.inventory.filter(o => o.id !== this.id)
    this.room.gameObjects.set(
      this.type,
      this.room.gameObjects.get(this.type)?.filter(g => g.id !== this.id) || []
    )

    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }

  normalize(): TotemData {
    if (this.hp > this.maxHp) {
      this.hp = this.maxHp
    }
    return {
      ownerId: this.ownerId,
      type: this.type,
      id: this.id,
      position: this.position,
      faction: this.faction,
      size: this.size,
      hitbox: this.hitbox,
      cooldown: this.cooldownBeforeActivation,
      amount: 5,
      hp: this.hp,
      disabled: this.disabled,
      maxHp: this.maxHp,
      name: this.name,
      roomId: this.roomId,
      zIndex: this.zIndex,
    }
  }

  act() {
    if (this.iteration < this.iterationMax) {
      const targets = Utils.getAOETargets(this.room, this.position, this.aoeRadius)
      targets.forEach((p) => {
        if (p.faction === this.faction) {
          p.takeHeal(this.heal)
          this.handleScoreShiftData(p, p, this.heal, ScoreShiftType.HEAL, false)
        } else {
          p.takeDamage(this.heal)
          this.handleScoreShiftData(p, p, this.heal, ScoreShiftType.DAMAGE, false)

        }
        p.emitGameEvent(GameEvents.GAMEOBJECT_ON, { type: GameObjectType.TOTEM, id: this.id })
      })
      this.iteration++
    }
    else {
      this.remove()
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
    }
  }
}
