import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameEvents } from '../GameEventActions'
import { ClassNames } from '../classes/ClassNames'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { FactionNames } from '../FactionNames'
import { FlagBase } from './FlagBase'
import { NetworkEventHandler } from '../../core/NetworkEventHandler'
import { SkillNames } from '../skills/core/SkillNames'
import { AlterationNames } from '../alterations/Alteration'
import { isPlayer } from '../interfaces/IPlayer'
import { IGameObject, defaultEmitGameEvent, IGameObjectData, defaultRemoveFromRoom } from '../interfaces/IGameObject'
import { IFactionAffinity, IFactionAffinityData } from '../interfaces/IFactionAffinity'
import { Game } from '../Game'
import { flagDataStats } from '../GameSelectionData'

export interface FlagData extends IGameObjectData, IFactionAffinityData {
  holderId: string | null
  cooldown: number | null
}

export class Flag implements IGameObject, IFactionAffinity {
  id = getUUID()
  type = GameObjectType.FLAG
  position: Coordinate
  size = 50
  hitbox = 50
  room: Room
  roomId: string
  cooldownBeforeActivation = 3000
  faction: FactionNames
  name = ''
  disabled = false
  zIndex = 0
  lastFlagBaseId: string
  takeable = true
  LOCK_TIME_AFTER_PUTTING_FLAG = 20000
  holderId: string | null

  constructor(room: Room, faction: FactionNames, position: Coordinate = { x: 0, y: 0 }, holderId: string | null, lastFlagBaseId: string) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.faction = faction
    this.holderId = holderId
    this.lastFlagBaseId = lastFlagBaseId
    if (holderId && this.isHolderAFlagBase(holderId)) {
      this.takeable = false
      setTimeout(() => {
        this.takeable = true
      }, this.LOCK_TIME_AFTER_PUTTING_FLAG)
    }
    this.room.addGameObject(this.type, this)
  }

  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }

  hit(player: Player, skillName: SkillNames): void {

  }

  trigger(player: Player) {
    let flagBase: FlagBase | undefined = undefined
    if (this.holderId) {
      flagBase = Game.getGameObjectById(this.holderId) as FlagBase
    }
    if (this.isFlagTakeable(player) &&
      ((flagBase && player.faction !== flagBase.faction) ||
        this.holderId === null ||
        player.currentClass === ClassNames.ROGUE)
    ) {
      player.emitGameEvent(GameEvents.GAMEOBJECT_ON, { type: GameObjectType.FLAG, id: this.id })

      player.timeout = setTimeout(() => {
        if (this.isFlagTakeable(player) && Utils.isColliding(player, this) && !player.hasAlteration(AlterationNames.DEAD)) {
          const flagBase = this.room.gameObjects
            .get(GameObjectType.FLAGBASE)
            ?.find(g => g.id === this.holderId) as FlagBase
          player.inventory.push(this)
          this.holderId = player.id
          if (flagBase) {
            flagBase.hasFlag = false
          }
          const normalizedPlayer = player.normalize()
          this.removeFromRoom()
          this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
          this.room.emitGameEvent(GameEvents.PLAYER_UPDATE, normalizedPlayer)
          NetworkEventHandler.broadcast(GameEvents.FLAG_ANNOUNCE, {
            player: normalizedPlayer,
            text: ' a volé le drapeau des ',
            factionOfFlag: this.faction
          })

        }
      }, this.cooldownBeforeActivation)
    } else {
      player.emitGameEvent(GameEvents.GAMEOBJECT_MISS, { type: GameObjectType.FLAG, coords: this.position, id: this.id })
    }
  }

  cancel(player: Player) {
    if (player.timeout) {
      clearTimeout(player.timeout)
      player.emitGameEvent(GameEvents.GAMEOBJECT_OFF, { type: GameObjectType.FLAG, id: this.id })
    }
  }

  normalize(): FlagData {
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      size: this.size,
      hitbox: this.hitbox,
      holderId: this.holderId,
      faction: this.faction,
      cooldown: this.cooldownBeforeActivation,
      zIndex: this.zIndex,
      disabled: this.disabled,
      name: this.name,
      roomId: this.roomId,
    }
  }
  playerGetFlag(player: Player) {
    this.holderId = player.id
  }

  isHolderAFlagBase(id: string) {
    const flagBase = Game.getGameObjectById(id) as FlagBase
    return flagBase && flagBase.type === GameObjectType.FLAGBASE
  }

  isFlagTakeable(player: Player) {
    return (this.holderId === null || this.isHolderAFlagBase(this.holderId)) &&
      !player.inventoryContains(GameObjectType.FLAG) && this.takeable
  }

  respawn() {
    // remove flag from its potential current flagbase
    if (this.holderId && this.isHolderAFlagBase(this.holderId)) {
      const currentFlagBase = Game.getGameObjectById(this.holderId) as FlagBase
      if (currentFlagBase) {
        currentFlagBase.hasFlag = false
      }
    }

    // make it spawn in its initial flagbase
    const flagBase = Utils.findGameObjectByTypeAndFaction(GameObjectType.FLAGBASE, this.faction) as FlagBase
    if (flagBase) {
      flagBase.hasFlag = true
      this.holderId = flagBase.id
      this.position = flagBase.position
      this.takeable = true
      const flagBaseRoom = Game.rooms.find(r => flagBase.roomId === r.id)
      if (flagBaseRoom) {
        this.removeFromRoom()
        this.room = flagBaseRoom
        flagBaseRoom.addGameObject(this.type, this)
        flagBaseRoom.emitGameEvent(GameEvents.ROOM_UPDATE, flagBaseRoom.normalize())
      }
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
    }
  }
  
  getResume(){
    const faction = this.faction;
    let baseFaction = FactionNames.NOFACTION
    let freeOwnerFaction = FactionNames.NOFACTION
    if(!this.holderId){
      return {
        faction : faction,
        inBase : false,
      }
    }
    const inBase = this.isHolderAFlagBase(this.holderId)
    if(this.isHolderAFlagBase(this.holderId)){
      const flagBaseId = this.holderId
      const flagBase = Utils.findGameObjectByGameObjectType(GameObjectType.FLAGBASE).find(o => o.id === flagBaseId) as FlagBase
      baseFaction = flagBase.faction
    }else if (isPlayer(Game.players.find(p => p.id === this.holderId))) {      
      const owner = Game.players.find(p => p.id === this.holderId) as Player
      freeOwnerFaction = owner.faction
    }
    return {
      faction : faction,
      inBase : inBase,
      baseFaction : baseFaction,
      freeOwnerFaction: freeOwnerFaction
    }
  }
  dropFlag(room: Room, position: Coordinate = { x: 0, y: 0 }, holderId: string | null) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.holderId = holderId
    if (holderId && this.isHolderAFlagBase(holderId)) {
      this.takeable = false
      setTimeout(() => {
        this.takeable = true
      }, this.LOCK_TIME_AFTER_PUTTING_FLAG)
    }
    this.room.addGameObject(this.type, this)
  }
}
