import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { FactionNames } from '../FactionNames'
import { Flag } from './Flag'
import { Affinity } from '../FactionAffinity'
import { Game } from '../Game'
import { NetworkEventHandler } from '../../core/NetworkEventHandler'
import { SkillNames } from '../skills/core/SkillNames'
import { GoldDeposit } from './GoldDeposit'
import { defaultEmitGameEvent, defaultRemoveFromRoom, IGameObject, IGameObjectData } from '../interfaces/IGameObject'
import { IFactionAffinity, IFactionAffinityData } from '../interfaces/IFactionAffinity'

export interface FlagBaseData extends IGameObjectData, IFactionAffinityData {
  hasFlag: boolean
  cooldown: number | null
}

export class FlagBase implements IGameObject, IFactionAffinity {
  id: string = getUUID()
  name = ''
  type = GameObjectType.FLAGBASE
  disabled = false
  position: Coordinate
  size = 100
  hitbox = 100
  zIndex = 1
  cooldownBeforeActivation = 3000
  cooldownPassiveIncome = 10000
  passiveIncome = 50
  faction: FactionNames
  room: Room
  roomId: string

  passiveGoldIncome?: NodeJS.Timeout
  hasFlag: boolean
  goldDeposit?: GoldDeposit

  constructor(room: Room, faction: FactionNames, position: Coordinate = { x: 0, y: 0 }, hasFlagInitially: boolean) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.faction = faction
    this.hasFlag = false

    if (hasFlagInitially) {
      new Flag(room, faction, position, this.id, this.id)
      this.hasFlag = true
    }

    this._handlePassiveGoldIncome()
    this.room.addGameObject(this.type, this)
  }

  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }

  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  hit(player: Player, skillName: SkillNames): void {
  }

  trigger(player: Player) {
    if (!this.hasFlag && player.inventoryContains(GameObjectType.FLAG)) {
      player.emitGameEvent(GameEvents.GAMEOBJECT_ON, { type: GameObjectType.FLAGBASE, id: this.id, })
      player.timeout = setTimeout(() => {
        if (Utils.isColliding(player, this)) {
          this.handlePutFlag(player)
        }
      }, this.cooldownBeforeActivation)
    }
  }

  cancel(player: Player) {
    if (player.timeout) {
      clearTimeout(player.timeout)
      player.emitGameEvent(GameEvents.GAMEOBJECT_OFF, { type: GameObjectType.FLAGBASE, id: this.id })
    }
  }

  handlePutFlag(player: Player) {
    const flag = player.inventory.find(g => g.type === GameObjectType.FLAG) as Flag
    if (flag) {
      this.hasFlag = true
      flag.holderId = this.id
      flag.dropFlag(this.room, this.position, this.id)
      flag.takeable = false
      setTimeout(() => {
        flag.takeable = true
      }, flag.LOCK_TIME_AFTER_PUTTING_FLAG)
      // TODO gros soucis ne supprime pas le flag 
      player.inventory = player.inventory.filter(g => g.type !== GameObjectType.FLAG)
      

      Affinity.updateAffinity(player, this.faction, true, 400)

      const normalizedPlayer = player.normalize()
      NetworkEventHandler.broadcast(GameEvents.FLAG_ANNOUNCE, {
        player: normalizedPlayer,
        text: ' a déposé le drapeau des ',
        factionOfFlag: flag.faction,
      })

      this._handlePutFlagEffect(flag)

      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
      this.room.emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())

    }
  }

  _handlePassiveGoldIncome() {
    this.passiveGoldIncome = setInterval(() => {
      if (!this.hasFlag) return
      const depositUltime = Utils
        .findGameObjectByTypeAndFaction(GameObjectType.GOLD_DEPOSIT, this.faction) as GoldDeposit

      if (depositUltime) {
        depositUltime.addGold(this.passiveIncome)
        Game.totalGoldInGame += this.passiveIncome
      }
    }, this.cooldownPassiveIncome)
  }

  normalize(): FlagBaseData {
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      size: this.size,
      hitbox: this.hitbox,
      hasFlag: this.hasFlag,
      faction: this.faction,
      cooldown: this.cooldownBeforeActivation,
      disabled: this.disabled,
      name: this.name,
      roomId: this.roomId,
      zIndex: this.zIndex,
    }
  }

  _handlePutFlagEffect(flag: Flag) {
    const lastFlagBase = Utils.getGameObjectFromId(flag.lastFlagBaseId, GameObjectType.FLAGBASE) as IFactionAffinity

    if (lastFlagBase && this.faction !== flag.faction && this.faction !== lastFlagBase.faction) {
      const debuffed = Game.players.filter(p => p.faction === flag.faction)
      const buffed = Game.players.filter(p => p.faction === this.faction)
      buffed.forEach(p => { p.takeHeal(400) })
      debuffed.forEach(p => { p.takeDamage(100) })
      Game.rooms.forEach((room) => room.emitGameEvent(GameEvents.ROOM_UPDATE, room.normalize()))
    } else {
      const debuffed = Game.players.filter(p => p.faction === flag.faction)
      debuffed.forEach(p => { p.takeHeal(200) })
      Game.rooms.forEach((room) => room.emitGameEvent(GameEvents.ROOM_UPDATE, room.normalize()))
    }
  }

  remove(): void {

  }
}
