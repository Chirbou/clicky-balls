import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { SkillNames } from '../skills/core/SkillNames'
import { defaultEmitGameEvent, defaultRemoveFromRoom, IGameObject, IGameObjectData } from '../interfaces/IGameObject'
import { AlterationNames } from '../alterations/Alteration'

export enum GoldValue {
  MASSIVE = 100,
  CHONKY = 50,
  BIG = 20,
  NORMAL = 10,
  SMALL = 5,
  TINY = 1,
}

export const getRandomGoldValue = () => {
  const values = Object.values(GoldValue)
  const randomIndex = Math.floor(Math.random() * values.length)
  return values[randomIndex] as GoldValue
}

export interface GoldData extends IGameObjectData {
  cooldown: number | null
  value: GoldValue
}

export const computeGoldLifespan = () => {
  return Math.floor(45000 + Math.random() * 90000)
}

const GOLD_POSITION_OFFSET = 40

export const dropGold = (
  amount: number,
  room: Room,
  position: Coordinate,
  offset: number = GOLD_POSITION_OFFSET,
  avoid?: Coordinate,
  avoidOffset?: number
) => {
  if (amount <= 0) return

  const goldAmountDividedByUnits = Utils.separateUnits(amount)
  // 3698 -> ['3000', '600', '90', '8']
  const MapAmountToGold: { amount: number, gold: GoldValue }[] = [
    { amount: 500, gold: GoldValue.MASSIVE },
    { amount: 250, gold: GoldValue.CHONKY },
    { amount: 100, gold: GoldValue.BIG },
    { amount: 40, gold: GoldValue.NORMAL },
    { amount: 10, gold: GoldValue.SMALL },
    { amount: 1, gold: GoldValue.TINY }
  ]

  for (let i = 0; i < goldAmountDividedByUnits.length; i++) {
    const goldAmountToPop = goldAmountDividedByUnits[i]
    for (const amountToGold of MapAmountToGold) {
      if (goldAmountToPop >= amountToGold.amount) {
        const toDropCoinsCount = goldAmountToPop / amountToGold.gold
        for (let i = 0; i < toDropCoinsCount; i++) {
          const randomPosition = Utils.getRandomPositionFromIntervalInBounds(position, offset, avoid, avoidOffset)
          new Gold(room, randomPosition, amountToGold.gold)
        }
        break
      }
    }
  }
}

export class Gold implements IGameObject {
  id: string = getUUID()
  position: Coordinate
  type: GameObjectType = GameObjectType.GOLD
  size = 20
  hitbox = 20
  room: Room
  roomId: string
  cooldownBeforeActivation = 3000
  cooldownBeforeElimination = computeGoldLifespan()
  triggerable = false
  name = ''
  disabled = false
  zIndex = 0
  value: GoldValue

  constructor(room: Room, position: Coordinate = { x: 0, y: 0 }, value: GoldValue = GoldValue.TINY) {
    this.room = room
    this.position = position
    this.room = room
    this.roomId = room.id
    this.value = value
    this.triggerable = true
    
    this.room.addGameObject(this.type, this)
    setTimeout(() => {
      this.triggerForCollidingPlayers()
    }, 500);
  }
  triggerForCollidingPlayers() {
    const collidingPlayers: Player[] = []

    this.room.players.forEach(player => {

      if (
        Utils.isColliding(this, player) &&
        !player.hasAlteration(AlterationNames.DEAD) &&
        player.ownedGold + this.value <= player.goldStackMax
      ) {

        collidingPlayers.push(player)

      }
    });

     if (collidingPlayers.length >= 1) {
      collidingPlayers.sort(Utils.compareOnZindex)
      this.trigger(collidingPlayers[0])
    }
    
  }
  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }

  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  hit(player: Player, skillName: SkillNames): void {

  }

  trigger(player: Player) {
    if (this.triggerable) {
      if (player.ownedGold >= player.goldStackMax) {
        return player.emitGameEvent(GameEvents.GAMEOBJECT_MISS,
          { type: GameObjectType.GOLD, coords: player.position, id: this.id }
        )
      }

      if ((player.ownedGold + this.value) > player.goldStackMax) {
        const earned = player.goldStackMax - player.ownedGold
        const excess = (player.ownedGold + this.value) - player.goldStackMax
        dropGold(excess, this.room, this.position, Math.random() * 50)
        player.ownedGold += earned
      } else {
        player.ownedGold += this.value
      }
      player.emitGameEvent(GameEvents.GAMEOBJECT_ON, { type: this.type, id: this.id })
      this.removeFromRoom()
    }
  }

  cancel(player: Player) { }

  normalize(): GoldData {
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      size: this.size,
      hitbox: this.hitbox,
      cooldown: this.cooldownBeforeActivation,
      name: this.name,
      disabled: this.disabled,
      roomId: this.roomId,
      zIndex: this.zIndex,
      value: this.value,
    }
  }
}
