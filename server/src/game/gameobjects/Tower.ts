import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { FactionNames } from '../FactionNames'
import { ScoreShiftType, Skill } from '../skills/core/Skill'
import { SkillNames } from '../skills/core/SkillNames'
import { Alteration, AlterationData, AlterationNames } from '../alterations/Alteration'
import {
  defaultEmitGameEvent,
  defaultRemoveFromRoom,
  IGameObject,
  IGameObjectData,
} from '../interfaces/IGameObject'
import {
  defaultHandleScoreShiftData,
  defaultTakeDamage,
  defaultTakeRawDamage,
  IHasHealthPoint,
  IHasHealthPointData,
} from '../interfaces/IHasHealthPoint'
import {
  IFactionAffinity,
  IFactionAffinityData,
} from '../interfaces/IFactionAffinity'
import { IAlterable, defaultGetAlteration, defaultHasAlteration } from '../interfaces/IAlterable'
import { alterationsDef } from '../alterations/alterationsDefs'
import { Flag } from './Flag'

export interface TowerData
  extends IGameObjectData,
  IHasHealthPointData,
  IFactionAffinityData {
  cooldown: number | null;
  amount: number;
  attitude: Attitude;
  destroyed: boolean;
  orientation: number;
  alterations: AlterationData[]
}

export const DEFAULT_TOWER_MAXHP = 1000
export const DEFAULT_SHOOT_RATE = 1000
export const DEFAULT_DAMAGE = 10
export enum Attitude {
  HEAL = 'HEAL',
  ATTACK = 'ATTACK',
}
export enum Temperament {
  WATCHER = 'WATCHER',
  GUARDIAN = 'GUARDIAN',
}
export class Tower implements IGameObject, IHasHealthPoint, IFactionAffinity, IAlterable {
  id: string = getUUID()
  position: Coordinate
  size = 75
  hitbox = 75
  cooldownBeforeActivation = 3000
  faction: FactionNames
  triggerable = false
  INITIALIZATION_DURATION = 4000
  aoeRadius = 200
  damage = 5
  interval: NodeJS.Timeout
  hp: number
  maxHp = 700
  amount = 25
  name = ''
  type = GameObjectType.TOWER
  disabled = false
  room: Room
  roomId: string
  zIndex = 0
  destroyed = false
  destroyedCooldown = 60 * 1000
  orientation = 0
  shootRate = 1000
  alterations: Alteration[]
  suckingPlayers: Player[] | undefined
  attitude = Attitude.HEAL
  temperament = Temperament.WATCHER
  attackCount = 0
  constructor(
    currentRoom: Room,
    faction: FactionNames,
    position: Coordinate = { x: 0, y: 0 },
    maxHp: number,
    shootRate: number,
    damage: number,
    temperament?: Temperament,
    attitude?: Attitude
  ) {
    this.room = currentRoom
    this.roomId = currentRoom.id
    this.position = position
    this.faction = faction
    this.maxHp = maxHp
    this.hp = Math.floor(maxHp * 0.75)
    this.shootRate = shootRate
    this.damage = damage
    this.alterations = []
    this.temperament = temperament ?? Temperament.WATCHER
    this.attitude = attitude ? attitude : Attitude.HEAL
    this.interval = setInterval(() => {

      this.act()

    }, this.shootRate)

    setTimeout(() => {
      this.triggerable = true
    }, this.INITIALIZATION_DURATION)
    this.room.addGameObject(this.type, this)
  }
  applyAlteration(alterationName: AlterationNames, cooldownDuration: number, source?: IGameObject) {
    const alteration = alterationsDef.get(alterationName)
    alteration?.apply(this, cooldownDuration, source)
  }

  applyAlterationForever(alterationName: AlterationNames) {
    const alteration = alterationsDef.get(alterationName)
    alteration?.applyForever(this)
  }

  removeAlteration(alterationName: AlterationNames) {
    const alteration = alterationsDef.get(alterationName)
    alteration?.removeAlteration(this)
  }
  removeAllAlterations() {
    for (const alteration of this.alterations) {
      this.removeAlteration(alteration.name)
    }
  }
  hasAlteration(alterationName: AlterationNames): boolean {
    return defaultHasAlteration(this, alterationName)
  }
  getAlteration(alterationName: AlterationNames): Alteration | undefined {
    return defaultGetAlteration(this, alterationName)
  }

  takeDamage(damage: number): number {

    this.attitude = Attitude.ATTACK

    return defaultTakeDamage(this, damage)
  }

  takeHeal(heal: number): number {
    if (this.temperament === Temperament.WATCHER) {
      this.attitude = Attitude.HEAL
    }
    heal = Math.abs(heal)
    for (const alteration of this.alterations) {
      heal = alteration.computeHeal(heal, this)
    }
    this.hp += heal
    this.destroyed = false
    if (this.hp > 100 && this.disabled) {
      this.disabled = false
    }
    return heal
  }

  takeRawDamage(damage: number): number {
    return defaultTakeRawDamage(this, damage)
  }

  handleScoreShiftData(
    playerSource: IHasHealthPoint,
    playerTarget: IHasHealthPoint | undefined,
    value: number | undefined,
    scoreshiftType: ScoreShiftType,
    isCritic: boolean
  ): void {
    const skill = scoreshiftType === ScoreShiftType.DAMAGE ? SkillNames.DPS_ATK : SkillNames.HEALER_HEAL
    defaultHandleScoreShiftData(
      playerSource,
      playerTarget,
      value,
      scoreshiftType,
      isCritic,
      skill
    )
  }

  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }
  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  hit(player: Player, skillName: SkillNames): void {
    if (this.destroyed) return

    this.orientation = computeOrientation(
      this.position,
      player.position
    )

    if (this.hp <= 0) {
      this.hp = 0
      this.destroy()
    }
  }

  destroy(): void {
    this.destroyed = true
    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }

  trigger(player: Player) { }

  cancel(player: Player) { }

  refresh() { }
  triggerAttackMode() {
    this.attitude = Attitude.ATTACK
    this.attackCount = 0
  }
  remove() {
    if (this.interval) clearInterval(this.interval)
    this.room.gameObjects.set(
      this.type,
      this.room.gameObjects.get(this.type)?.filter((g) => g.id !== this.id) ||
      []
    )

    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }

  normalize(): TowerData {
    if (this.hp > this.maxHp) {
      this.hp = this.maxHp
    }
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      faction: this.faction,
      size: this.size,
      attitude: this.attitude,
      hitbox: this.hitbox,
      cooldown: this.cooldownBeforeActivation,
      amount: this.amount,
      hp: this.hp,
      disabled: this.disabled,
      maxHp: this.maxHp,
      name: this.name,
      roomId: this.roomId,
      zIndex: this.zIndex,
      destroyed: this.destroyed,
      orientation: this.orientation,
      alterations: this.alterations.map(alteration => alteration.normalize()),
    }
  }
  getTargetableRoomPlayers() {
    const roomPlayers = this.room.players.filter((roomPlayer) => {
      return (
        !roomPlayer.hasAlteration(AlterationNames.DEAD) &&
        !roomPlayer.hasAlteration(AlterationNames.STEALTH) &&
        !roomPlayer.hasAlteration(AlterationNames.INVISIBLE) &&
        !roomPlayer.hasAlteration(AlterationNames.INVINCIBLE)
      )
    })
    return roomPlayers
  }
  //TODO Refactor : Optimiser l'étude de l'inventaire : Mission trouver si un joueur de la room possède le flag de la faction
  didAPlayerGetFactionFlag(roomPlayers: Player[]) {
    let res = false
    const flaggedPlayers = roomPlayers.filter(player => player.inventoryContains(GameObjectType.FLAG))
    for (let i = 0; i < flaggedPlayers.length; i++) {
      const flag = flaggedPlayers[i].inventory.find(obj => obj.type === GameObjectType.FLAG) as Flag
      if (flag.faction === this.faction) {
        res = true
        break
      }
    }
    return res
  }
  act() {

    //Pour définir l'attitude : Voir selon Tempérament, puis selon les personnes présentes, puis selon l'atittude
    // switch selectAttitude()
    this.handleAutoAttackState()

    if (this.destroyed) return

    const roomPlayers = this.getTargetableRoomPlayers()

    if (roomPlayers.length === 0) return

    switch (this.temperament) {
      case Temperament.WATCHER:
        this.selectWatcherAttitude(roomPlayers)
        break
      case Temperament.GUARDIAN:
        this.selectGuardianAttitude(roomPlayers)
        break

      default:
        this.selectWatcherAttitude(roomPlayers)
        break
    }
    this.attackOrHeal(roomPlayers)
    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }

  selectGuardianAttitude(roomPlayers: Player[]) {
    const nearestEnnemy = this.getNearestPlayerByFaction(roomPlayers, this.faction, false)
    this.attitude = nearestEnnemy ? Attitude.ATTACK : Attitude.HEAL
  }

  selectWatcherAttitude(roomPlayers: Player[]) {
    if (this.didAPlayerGetFactionFlag(roomPlayers)) {
      this.attitude = Attitude.ATTACK
    }
  }

  handleAutoAttackState() {
    if (this.attitude === Attitude.ATTACK && this.attackCount < 8) {
      this.attackCount++
    } else {
      this.attitude = Attitude.HEAL
      this.attackCount = 0
    }
  }

  attack(player: Player) {
    player.takeDamage(this.damage)
    this.handleScoreShiftData(
      this,
      player,
      this.damage,
      ScoreShiftType.DAMAGE,
      false
    )
    this.orientation = computeOrientation(this.position, player.position)
  }

  heal(player: Player) {
    player.takeHeal(this.damage)
    this.handleScoreShiftData(
      this,
      player,
      this.damage,
      ScoreShiftType.HEAL,
      false
    )
    this.orientation = computeOrientation(this.position, player.position)
  }
  getNearestPlayerByFaction(roomPlayers: Player[], faction: FactionNames, lookingForAlly: boolean) {
    let nearestPlayer: Player | undefined = undefined
    let nearest: number | undefined = undefined
    for (let i = 0; i < roomPlayers.length; i++) {
      // TODO REFACTOR
      const currentPlayerDistance = Utils.getDistance(
        this.position,
        roomPlayers[i].position
      )
      if (lookingForAlly) {
        if (
          (!nearest || currentPlayerDistance < nearest)
          && roomPlayers[i].faction === faction
        ) {
          nearest = currentPlayerDistance
          nearestPlayer = roomPlayers[i]
        }
      } else {
        if ((!nearest || currentPlayerDistance < nearest) && roomPlayers[i].faction !== faction) {
          nearest = currentPlayerDistance
          nearestPlayer = roomPlayers[i]
        }
      }
    }
    return nearestPlayer
  }
  getNearestHealablePlayersByFaction(roomPlayers: Player[]) {
    const healableRoomPlayers = roomPlayers.filter(player => player.hp < player.maxHp)
    return this.getNearestPlayerByFaction(healableRoomPlayers, this.faction, true)
  }

  attackOrHeal(roomPlayers: Player[]) {
    const nearestEnnemy = this.getNearestPlayerByFaction(roomPlayers, this.faction, false)
    let nearestHealableAlly
    switch (this.attitude) {
      case Attitude.ATTACK:
        if (nearestEnnemy) {
          this.attack(nearestEnnemy)
        }
        break
      case Attitude.HEAL:
        nearestHealableAlly = this.getNearestHealablePlayersByFaction(roomPlayers)
        if (nearestHealableAlly) {
          this.heal(nearestHealableAlly)
        }
        break

      default:
        break
    }
  }
}
const computeOrientation = (p1: Coordinate, p2: Coordinate) => {
  return (Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180) / Math.PI
}
