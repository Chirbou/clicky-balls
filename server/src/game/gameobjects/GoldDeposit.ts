import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Player } from '../player/Player'
import { Room } from './../rooms/Room'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from './../gameobjects/GameObjectType'
import { FactionNames } from '../FactionNames'
import { Affinity } from '../FactionAffinity'
import { Game } from '../Game'
import { SkillNames } from '../skills/core/SkillNames'
import { defaultEmitGameEvent, defaultRemoveFromRoom, IGameObject, IGameObjectData } from '../interfaces/IGameObject'
import { IFactionAffinity, IFactionAffinityData } from '../interfaces/IFactionAffinity'
import { defaultHandleScoreShiftData, defaultTakeDamage, defaultTakeHeal, defaultTakeRawDamage, IHasHealthPoint, IHasHealthPointData } from '../interfaces/IHasHealthPoint'
import { ScoreShiftType } from '../skills/core/Skill'
import { dropGold, Gold } from './Gold'
import { loseHalfGold, IPlunderable, IPlunderableData, setRecentlyBroken } from '../interfaces/IPlunderable'

export const DEFAULT_GOLD_DEPOSIT_MAXHP = 3000
export const DEFAULT_AUTOHEAL_DURATION = 5000
export const DEFAULT_AUTOHEAL_AMOUNT = 10

export interface GoldDepositData extends IPlunderableData, IGameObjectData, IHasHealthPointData, IFactionAffinityData {
  cooldown: number | null
}

export class GoldDeposit implements IPlunderable, IGameObject, IHasHealthPoint, IFactionAffinity {
  id: string = getUUID()
  type: GameObjectType = GameObjectType.GOLD_DEPOSIT
  position: Coordinate
  size = 100
  hitbox = 100
  zIndex = 1
  cooldownBeforeActivation = 3000
  faction: FactionNames
  room: Room
  disabled = false
  name = ''
  roomId: string
  goldAmount = 0
  hp: number
  maxHp = DEFAULT_GOLD_DEPOSIT_MAXHP
  wasRecentlyBroken = false
  unplunderableDuration = 30000

  constructor(room: Room, faction: FactionNames, position: Coordinate = { x: 0, y: 0 }) {
    this.room = room
    this.roomId = room.id
    this.position = position
    this.faction = faction
    this.room.addGameObject(this.type, this)
    this.hp = Math.floor(this.maxHp * 0.75)

    setInterval(() => {
      this.takeHeal(DEFAULT_AUTOHEAL_AMOUNT)
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())

    }, DEFAULT_AUTOHEAL_DURATION)
  }
  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }
  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  hit(player: Player, skillName: SkillNames): void {
    if (this.hp <= 0 && !this.wasRecentlyBroken) {
      this.hp = 0
      loseHalfGold(this)
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
      this.handleRespawn()
    }
  }

  handleRespawn() {
    setTimeout(() => {
      setRecentlyBroken(this, false)
      this.takeHeal(DEFAULT_GOLD_DEPOSIT_MAXHP / 3)
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
    }, this.unplunderableDuration)
  }

  trigger(player: Player) {
    if (player.ownedGold > 0) {
      player.emitGameEvent(GameEvents.GAMEOBJECT_ON, { type: this.type, id: this.id, })
      if (player.timeout) clearTimeout(player.timeout)
      player.timeout = setTimeout(() => {
        if (Utils.isColliding(player, this)) {
          this.handlePutGold(player)
        }
      }, this.cooldownBeforeActivation)
    }
  }

  cancel(player: Player) {
    if (player.timeout) {
      clearTimeout(player.timeout)
      player.emitGameEvent(GameEvents.GAMEOBJECT_OFF, { type: this.type, id: this.id })
    }
  }

  takeDamage(damage: number): number {
    return defaultTakeDamage(this, damage)
  }

  takeHeal(heal: number): number {
    heal = Math.abs(heal)
    this.hp += heal
    if (this.hp > 100 && this.disabled) {
      this.disabled = false
    }
    return heal
  }

  takeRawDamage(damage: number): number {
    if (Math.random() < damage / 50) {
      const gold = Math.floor(damage / 4 + Math.random() * damage)
      this.goldAmount -= gold
      dropGold(gold, this.room, this.position, this.size + 20, this.position, this.size)
    }

    return defaultTakeRawDamage(this, damage)
  }

  handleScoreShiftData(playerSource: IHasHealthPoint, playerTarget: IHasHealthPoint | undefined, value: number | undefined, scoreshiftType: ScoreShiftType, isCritic: boolean): void {
    defaultHandleScoreShiftData(playerSource, playerTarget, value, scoreshiftType, isCritic)
  }

  handlePutGold(player: Player) {
    if (this.hp > (this.maxHp / 4)) {
      const goldAmount = player.ownedGold
      player.ownedGold = 0
      player.totalGold = player.totalGold + goldAmount
      this.addGold(goldAmount)
      player.emitGameEvent(
        GameEvents.GAMEOBJECT_COMPLETE, { type: this.type, id: this.id, goldAmount: goldAmount }
      )

      switch (this.faction) {
        case FactionNames.PUNK:
          player.factionAffinity.punk = player.factionAffinity.punk + (goldAmount * 4)
          player.factionAffinity.conservatist = player.factionAffinity.conservatist - goldAmount
          player.factionAffinity.technophile = player.factionAffinity.technophile - goldAmount

          if (player.factionAffinity.punk > 1000) {
            player.factionAffinity.punk = 1000
          }
          if (player.factionAffinity.conservatist < 0) {
            player.factionAffinity.conservatist = 0
          }
          if (player.factionAffinity.technophile < 0) {
            player.factionAffinity.technophile = 0
          }
          break
        case FactionNames.CONSERVATIST:
          player.factionAffinity.conservatist = player.factionAffinity.conservatist + + (goldAmount * 4)
          player.factionAffinity.punk = player.factionAffinity.punk - goldAmount
          player.factionAffinity.technophile = player.factionAffinity.technophile - goldAmount
          if (player.factionAffinity.conservatist > 1000) {
            player.factionAffinity.conservatist = 1000
          }
          if (player.factionAffinity.punk < 0) {
            player.factionAffinity.punk = 0
          }
          if (player.factionAffinity.technophile < 0) {
            player.factionAffinity.technophile = 0
          }
          break
        case FactionNames.TECHNOPHILE:
          player.factionAffinity.technophile = player.factionAffinity.technophile + + (goldAmount * 4)
          player.factionAffinity.conservatist = player.factionAffinity.conservatist - goldAmount
          player.factionAffinity.punk = player.factionAffinity.punk - goldAmount
          if (player.factionAffinity.technophile > 1000) {
            player.factionAffinity.technophile = 1000
          }
          if (player.factionAffinity.conservatist < 0) {
            player.factionAffinity.conservatist = 0
          }
          if (player.factionAffinity.punk < 0) {
            player.factionAffinity.punk = 0
          }
          break
      }

      player.faction = Affinity.getAffinity(player.factionAffinity)
      Affinity.getColorFromAffinity(player)
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
      this.room.emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())
    } else {
      player.emitGameEvent(GameEvents.GAMEOBJECT_MISS, { type: GameObjectType.GOLD_DEPOSIT, coords: this.position, id: this.id })
    }
  }

  normalize(): GoldDepositData {
    if (this.hp > this.maxHp) {
      this.hp = this.maxHp
    }
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      faction: this.faction,
      size: this.size,
      hitbox: this.hitbox,
      cooldown: this.cooldownBeforeActivation,
      goldAmount: this.goldAmount,
      hp: this.hp,
      disabled: this.disabled,
      maxHp: this.maxHp,
      name: this.name,
      roomId: this.roomId,
      zIndex: this.zIndex,
      wasRecentlyBroken: this.wasRecentlyBroken,
      unplunderableDuration: this.unplunderableDuration,
    }
  }

  addGold(quantity: number) {
    // TODO Refactor les etats du goldDeposit, quand il ne peut pas recevoir de Gold, et l'annoncer
    if (this.hp > (this.maxHp / 4)) {
      this.goldAmount += quantity
      this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
    }
  }

}
