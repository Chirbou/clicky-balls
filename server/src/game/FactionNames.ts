export enum FactionNames {
    CONSERVATIST = 'conservatist',
    PUNK = 'punk',
    TECHNOPHILE = 'technophile',
    NOFACTION = 'nofaction'
  }
  