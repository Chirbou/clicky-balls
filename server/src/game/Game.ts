import { Socket } from 'socket.io/dist/socket'
import { AlterationNames } from './alterations/Alteration'
import { ClassNames } from './classes/ClassNames'
import { SkillNames } from './skills/core/SkillNames'
import { skillsDef } from './skills/core/skillsDef'
import { GameEvents } from './GameEventActions'
import { NetworkEventHandler } from '../core/NetworkEventHandler'
import { Player } from './player/Player'
import { PlayerResume } from './player/PlayerResume'
import { Coordinate, Utils } from '../core/Utils'
import { Room } from './rooms/Room'
import { RoomTypes } from './rooms/RoomTypes'
import { Affinity } from './FactionAffinity'
import { FactionNames } from './FactionNames'
import { classesDef } from './classes/classesDef'
import { GoldDeposit } from './gameobjects/GoldDeposit'
import { GameObjectType } from './gameobjects/GameObjectType'
import { IGameObject } from './interfaces/IGameObject'
import { IHasHealthPoint } from './interfaces/IHasHealthPoint'
import { IPlayerData, PlayerSelectionData } from './interfaces/IPlayer'
import { Timer, TimerConstants } from './Timer'
import { Flag } from './gameobjects/Flag'
import { FactionAndFlagData, FactionData, GameSelectionData } from './GameSelectionData'
import { World } from './rooms/World'
import { ScoreLandings } from './classes/Classe'

export const resetFactionTotalGold = () => {
  return {
    [FactionNames.PUNK]: {
      gold: 0
    }, [FactionNames.CONSERVATIST]: {
      gold: 0
    }, [FactionNames.TECHNOPHILE]: {
      gold: 0
    }, [FactionNames.NOFACTION]: {
      gold: 0
    },
  }
}

export class Game {
  static players: Player[] = []
  static rooms: Room[] = []
  static GAMEZONE_WIDTH = 1200
  static GAMEZONE_HEIGHT = 800
  // TODO Rename les var factionTotalGold wtf
  static factionTotalGoldEver = {
    [FactionNames.PUNK]: {
      gold: 0
    }, [FactionNames.CONSERVATIST]: {
      gold: 0
    }, [FactionNames.TECHNOPHILE]: {
      gold: 0
    }, [FactionNames.NOFACTION]: {
      gold: 0
    },
  }
  static factionTotalGold = resetFactionTotalGold()
  static totalGoldInGame = 0
  static timeStampStart = Date.now()
  static timer: Timer = new Timer(Date.now())

  static start() {
    const world = new World()

    const goldDeposits = Game.rooms
      .map(r => r.gameObjects.get(GameObjectType.GOLD_DEPOSIT) as GoldDeposit[])
      .filter(gd => gd && gd?.length)
      .reduce((arr, gd) => [...arr, ...gd], [])

    const punkGoldDeposits = goldDeposits.filter(gd => gd.faction === FactionNames.PUNK)
    const conservatistGoldDeposits = goldDeposits.filter(gd => gd.faction === FactionNames.CONSERVATIST)
    const technophileGoldDeposits = goldDeposits.filter(gd => gd.faction === FactionNames.TECHNOPHILE)

    const punkFlag = Utils.findGameObjectByTypeAndFaction(GameObjectType.FLAG, FactionNames.PUNK) as Flag
    const conservatistFlag = Utils.findGameObjectByTypeAndFaction(GameObjectType.FLAG, FactionNames.CONSERVATIST) as Flag
    const technophileFlag = Utils.findGameObjectByTypeAndFaction(GameObjectType.FLAG, FactionNames.TECHNOPHILE) as Flag

    setInterval(() => {

      this.factionTotalGold.punk.gold = punkGoldDeposits.reduce((acc, gd) => acc + gd.goldAmount, 0)
      this.factionTotalGold.conservatist.gold = conservatistGoldDeposits.reduce((acc, gd) => acc + gd.goldAmount, 0)
      this.factionTotalGold.technophile.gold = technophileGoldDeposits.reduce((acc, gd) => acc + gd.goldAmount, 0)

      NetworkEventHandler.broadcast(GameEvents.GAME_STATE_SELECTION, {
        factions: [
          {
            name: FactionNames.PUNK,
            goldAmount: this.factionTotalGold.punk.gold,
            playerCount: Game.players.filter(p => p.faction === FactionNames.PUNK).length,
          },
          {
            name: FactionNames.CONSERVATIST,
            goldAmount: this.factionTotalGold.conservatist.gold,
            playerCount: Game.players.filter(p => p.faction === FactionNames.CONSERVATIST).length,
          },
          {
            name: FactionNames.TECHNOPHILE,
            goldAmount: this.factionTotalGold.technophile.gold,
            playerCount: Game.players.filter(p => p.faction === FactionNames.TECHNOPHILE).length,
          },
        ],
        timer: Game.timer.timeStampStart,
      } as GameSelectionData)


      //Pour la gestion de GameStatsPanel :
      NetworkEventHandler.broadcast(GameEvents.GAME_STATE_PANEL, [
        {
          faction: FactionNames.PUNK,
          goldAmount: punkGoldDeposits.reduce((acc, gd) => acc + gd.goldAmount, 0),
          playerCount: Game.players.filter(p => p.faction === FactionNames.PUNK).length,
          flagData: punkFlag.getResume()
        },
        {
          faction: FactionNames.CONSERVATIST,
          goldAmount: conservatistGoldDeposits.reduce((acc, gd) => acc + gd.goldAmount, 0),
          playerCount: Game.players.filter(p => p.faction === FactionNames.CONSERVATIST).length,
          flagData: conservatistFlag.getResume()
        },
        {
          faction: FactionNames.TECHNOPHILE,
          goldAmount: technophileGoldDeposits.reduce((acc, gd) => acc + gd.goldAmount, 0),
          playerCount: Game.players.filter(p => p.faction === FactionNames.TECHNOPHILE).length,
          flagData: technophileFlag.getResume()
        },
        {
          faction: FactionNames.NOFACTION,
          goldAmount: this.totalGoldInGame,
          playerCount: Game.players.length,
        },
      ] as FactionAndFlagData[])
    }, 1000)
  }

  static getNormalizedPlayers(): IPlayerData[] {
    return Game.players.map((p) => p.normalize())
  }

  static newPlayerJoin(socket: Socket, selectionData: PlayerSelectionData): Player {
    const position = Utils.getRandomCoordinates()
    const player = new Player(socket, position, selectionData.name, selectionData.emoji, selectionData.faction,selectionData.classesLocks)
    return player
  }

  static playerSelectionReady(socket: Socket, selectionData: PlayerSelectionData): void {
    const player = Game.newPlayerJoin(socket, selectionData)

    // NOTE - Code has to be in this order otherwise it breaks
    Game.players.push(player)
    
    player.emitGameEvent(GameEvents.START_GAME, {
      id: player.id,
      users: Game.getNormalizedPlayers(),
      timeStampStart: Game.timeStampStart,
      timerDuration: TimerConstants.GAME_DURATION,
    })
    // END NOTE

    player.enterStartRoom()
    NetworkEventHandler.broadcast(GameEvents.PLAYER_JOIN, player.normalize())
    Game.rooms[0].emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())
  }

  static playerLeave(id: string): void {
    Game.players = Game.players.filter((p) => p.id !== id)

    for (const room of Game.rooms) {
      const player = room.players.find((p) => p.id === id)
      if (player) {
        if (!player.dead) {
          player.die()
        } else if (player._rebornTimeout) {
          clearTimeout(player._rebornTimeout)
        }
        room.playerLeaves(player)
        break
      }
    }
    NetworkEventHandler.broadcast(GameEvents.PLAYER_LEAVE, id)
  }

  static getRoomFromName(name: RoomTypes) {
    const rooms = Game.rooms.filter((r) => r.type === name)
    if (rooms.length > 0) {
      return rooms[0]
    } else {
      return Game.rooms[0]
    }
  }

  static handleSkill(
    skillName: SkillNames,
    idPlayerSource: string,
    idTarget: string,
    position: Coordinate
  ) {
    const sourcePlayer = Game.players.find((u) => u.id === idPlayerSource)
    const room = sourcePlayer?.room

    let isTargetGameObject = false
    let targetPlayer: Player | undefined
    let targetGameObject: IGameObject | undefined
    targetPlayer = room?.players.find((u) => u.id === idTarget)

    if (targetPlayer) {
      if (
        !Utils.isInSquare(targetPlayer.position, targetPlayer.size, position)
      ) {
        targetPlayer = undefined
      }
    }

    if (!sourcePlayer || sourcePlayer.hasAlteration(AlterationNames.SILENCED))
      return

    if (
      sourcePlayer.hasAlteration(AlterationNames.STEALTH) &&
      skillName !== SkillNames.TRAPPER_STEALTH &&
      skillName !== SkillNames.TRAPPER_SNIPER
    ) {
      sourcePlayer.removeAlteration(AlterationNames.STEALTH)
      const playerSkillData = sourcePlayer.getPlayerSkillData(
        SkillNames.TRAPPER_STEALTH
      )
      sourcePlayer.resetSkillOnTimeout(playerSkillData)
    }

    if (sourcePlayer && !targetPlayer) {
      // TODO perf (envoyer le type du gameobject depuis le front)
      targetGameObject = Game.getGameObjectById(idTarget)
      if (targetGameObject) {
        isTargetGameObject = true
      }
    }

    if (!(targetGameObject as IHasHealthPoint)?.takeRawDamage) {
      targetGameObject = undefined
    }

    const target =
      targetPlayer ||
      (targetGameObject as Player | IHasHealthPoint | undefined)

    skillsDef.get(skillName)?.trigger({
      playerSource: sourcePlayer,
      target: target,
      position: position,
      room: room,
    })

    if (isTargetGameObject) {
      if (target && 'hit' in target) target.hit(sourcePlayer as any, skillName)
      room?.emitGameEvent(GameEvents.ROOM_UPDATE, room.normalize())
    } else {
      if (target)
        room?.emitGameEvent(GameEvents.PLAYER_UPDATE, target?.normalize())
      if (sourcePlayer)
        room?.emitGameEvent(
          GameEvents.PLAYER_UPDATE,
          sourcePlayer?.normalize()
        )
    }
  }

  static revivePlayer(idTarget: string) {
    const targetPlayer = Game.players.find((u) => u.id === idTarget)
    targetPlayer?.revive()
  }

  static selectLvlUp(idTarget: string, className: ClassNames) {
    const targetPlayer = Game.players.find((u) => u.id === idTarget)
    targetPlayer?.levelUp(className)
  }

  static getGameObjectById(id: string): IGameObject | undefined {
    for (const room of Game.rooms) {
      for (const type of Object.values(GameObjectType)) {
        const gameObject = room.gameObjects.get(type)?.find((g) => g.id === id)
        if (gameObject) {
          return gameObject
        }
      }
    }
    return
  }

  static getBaseClass(currentClass: ClassNames) {
    const playerClass = classesDef.find((c) => c.name === currentClass)
    const lowClassName = playerClass?.archetype
    return lowClassName
  }

  static getPlayersResume() {
    const playerResumeArray = []

    for (const player of Game.players) {
      let playerBaseClass = Game.getBaseClass(player.currentClass)
      if (playerBaseClass === ClassNames.PECNO) {
        playerBaseClass = player.currentClass
      }
      const alterationTab = player.alterations.map(
        (alteration) => alteration.name
      )
      const playerResume = {
        id: player.id,
        name: player.name,
        roomId: player.roomId,
        class: playerBaseClass,
        score: player.hp,
        maxHp: player.maxHp,
        dead: player.dead,
        alterations: alterationTab,
        faction: Affinity.getAffinity(player.factionAffinity),
        ownedGold: player.ownedGold,
        goldStackMax: player.goldStackMax,
      } as PlayerResume

      playerResumeArray.push(playerResume)
    }
    return playerResumeArray
  }

  static updateAngle(id: string, angle: number) {
    const player = Game.players.find((u) => u.id === id)
    if (player) {
      player.angle = angle
    }
    player?.room.emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())
  }

  static respawnFlags() {
    const flags = Utils.findGameObjectByGameObjectType(
      GameObjectType.FLAG
    ) as Flag[]
    flags.forEach((flag) => flag.respawn())
  }

  static devolvePlayer(playerId: string) {
    const player = Game.players.find((p) => p.id === playerId)
    if (!player) return
    if (classesDef.find(classe => classe.name === player.currentClass)?.archetype != ClassNames.PECNO) {
      player.levelDown(player.currentClass)
      player.levelDown(player.currentClass)
    } else {
      player.levelDown(player.currentClass)
    }
    if (player.hp > ScoreLandings.PECNO_MAXSCORE) {
      player.hp = ScoreLandings.PECNO_MAXSCORE
    }
    player.room.emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())
  }

  static emote(playerId: string, emote: string) {
    const player = Game.players.find((p) => p.id === playerId)
    if (!player) return
    player.room.emitGameEvent(GameEvents.EMOTE, { playerId, emote })
  }
  
}
// ça va fix l'angle du canon c'est ça ? UI allez comment j'ai deviné làààà T'es fort et en plus beau mersi
// Par exemple  le canon maintenant il es ist mit dein schokolade dans le bon **richtung** 🥵
// Mais y'a aussi d'autres surprises ailleurs ... hâte de voir ça dans l'historique de commits, vu qu'évidemment le repo est tenu de manière exemplaire.
// Y'a des emojis c'est tout ce que je suis capable de garantir
// t'as accès au localhost ? => oui à une vitesse (phénoménale, le choc provoqué serait considérable etc... xD c l'inverse par contre mé vi ouais je crois bien) digne de 1998 par contre
// bien ouej. Je peux pas jouer avec vous, rien que l'image de bg met 10 ans à charger

// Dis toit que j'ai ouvert le liveshare pour Simon mais que t'es arrivé dessus avant (et qu'il est toutjours pas là je crois)
// c'est marrant, ça ne me surprend pas tant que ça :p c'est ça les devs windows (Popopo)
// allez, je vous laisse vous amuser, n'oublie pas de mettre en prod que je puisse tester ça demain :D
// Tu veux pas voir les modif ? C'est sur le serveur aussi si vraiment il faut. Et c'est bizarre parce que je me dis que tu vois pas ce que j'écris mais c'est en temps réèl c'est de la frappe.
// De ouf, j'ai l'impression de parler à ChatGPT sous stéroïdes x'D
// Je sens que ce bloc de commentaires va faire partie intégrale du projet

// STOP GENERATION
// azi j'me co sur le serveur. Y a quoi comme modifs à part Canon?
// Je suis désolé... Mais en tant qu'intelligence artificielle <3, Les (mauvais ?) tours...
// J'ai changé la manière dont est affichée les skill vite fait, grâce à mes compétences de développeur CSS :D
//On a rajouté des zgegs 8==D dans le sous sol ( :D ) j'ai pas vu les zgegs
// OH putain. Ca change de ouf. Stylé en vrai
