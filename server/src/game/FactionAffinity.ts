import { Colors } from './Colors'
import { FactionNames } from './FactionNames'
import { GameEvents } from './GameEventActions'
import { IFactionAffinity } from './interfaces/IFactionAffinity'
import { Player } from './player/Player'

export interface FactionAffinity {
  conservatist: number,
  punk: number,
  technophile: number
}

export const DEFAULT_FACTION_AFFINITY = {
  conservatist: 0,
  punk: 0,
  technophile: 0
}

export class Affinity {
  static updateAffinity(source: IFactionAffinity, faction: FactionNames, increase: boolean, quantity: number) {
    if (!source || source.disabled) {
      return
    }

    if (!source.factionAffinity) return
    const factionAffinity = source.factionAffinity
    const totalAmount = quantity * (increase ? 1 : -1)

    switch (faction) {
      case 'conservatist':
        factionAffinity.punk = Affinity.computeAffinity(
          source,
          FactionNames.PUNK,
          Math.floor(-totalAmount * Math.random() * 10),
          source.factionAffinity.punk
        )
        factionAffinity.technophile = Affinity.computeAffinity(
          source,
          FactionNames.TECHNOPHILE,
          Math.floor(-totalAmount * Math.random() * 10),
          factionAffinity.technophile
        )
        factionAffinity.conservatist = Affinity.computeAffinity(
          source,
          FactionNames.CONSERVATIST,
          Math.floor(totalAmount * 2 * Math.random() * 10),
          factionAffinity.conservatist
        )
        break

      case 'punk':
        factionAffinity.conservatist = Affinity.computeAffinity(
          source,
          FactionNames.CONSERVATIST,
          Math.floor(-totalAmount * Math.random() * 10),
          source.factionAffinity.conservatist
        )

        factionAffinity.technophile = Affinity.computeAffinity(
          source,
          FactionNames.TECHNOPHILE,
          Math.floor(-totalAmount * Math.random() * 10),
          factionAffinity.technophile
        )
        factionAffinity.punk = Affinity.computeAffinity(
          source,
          FactionNames.PUNK,
          Math.floor(totalAmount * 2 * Math.random() * 10),
          factionAffinity.punk
        )
        break

      case 'technophile':
        factionAffinity.conservatist = Affinity.computeAffinity(
          source,
          FactionNames.CONSERVATIST,
          Math.floor(-totalAmount * Math.random() * 10),
          source.factionAffinity.conservatist
        )

        factionAffinity.technophile = Affinity.computeAffinity(
          source,
          FactionNames.TECHNOPHILE,
          Math.floor(totalAmount * 2 * Math.random() * 10),
          factionAffinity.technophile
        )
        factionAffinity.punk = Affinity.computeAffinity(
          source,
          FactionNames.PUNK,
          Math.floor(-totalAmount * Math.random() * 10),
          factionAffinity.punk
        )
        break

      default:
        break

    }

    const sourceAffinity = Affinity.getAffinity(factionAffinity)
    source.faction = sourceAffinity
    this.getColorFromAffinity(source)
    source.room.emitGameEvent(GameEvents.PLAYER_UPDATE, source.normalize())
  }

  static getAffinity(factionAffinity: FactionAffinity | undefined): FactionNames {
    let max = 0
    let affinityName = '?'
    if (!factionAffinity) return FactionNames.CONSERVATIST

    for (const [affinity, affinityScore] of Object.entries(factionAffinity)) {
      if (parseInt(affinityScore) > max) {
        max = affinityScore
        affinityName = affinity
      }
    }

    return affinityName as FactionNames
  }

  static computeAffinity(player: IFactionAffinity, factionChanged: FactionNames, quantity: number, factionStat: number) {
    let effectiveChange = 0
    let max = 1000
    if (!player.factionAffinity) return 0
    const factionStats = player.factionAffinity
    if (
      (factionStats.conservatist >= 900 && factionChanged !== FactionNames.CONSERVATIST) ||
      (factionStats.punk >= 900 && factionChanged !== FactionNames.PUNK) ||
      (factionStats.technophile >= 900 && factionChanged !== FactionNames.TECHNOPHILE)
    ) {
      max = 750
    }

    if (factionStat + quantity <= max) {
      if (factionStat + quantity >= 0) {
        effectiveChange = factionStat + quantity
      } else {
        effectiveChange = 0
      }
    } else {
      effectiveChange = max
    }
    return effectiveChange
  }

  static getColorFromAffinity(player: IFactionAffinity) {
    const affinity = this.getAffinity(player.factionAffinity)
    switch (affinity) {
      case 'conservatist':
        player.color = Colors.CONSERVATISTS_COLOR
        break

      case 'punk':
        player.color = Colors.PUNKS_COLOR
        break

      case 'technophile':
        player.color = Colors.TECHNOPHILES_COLOR
        break
    }
  }
}

export const getAffinityFromColor = (color: Colors): FactionAffinity => {
  const factionAffinity: FactionAffinity = {
    conservatist: 0,
    technophile: 0,
    punk: 0,
  }

  switch (color) {
    case Colors.CONSERVATISTS_COLOR:
      factionAffinity.conservatist = 1000
      break
    case Colors.PUNKS_COLOR:
      factionAffinity.punk = 1000
      break
    case Colors.TECHNOPHILES_COLOR:
      factionAffinity.technophile = 1000
      break
  }

  return factionAffinity
}
