import { FactionNames } from './FactionNames'

export interface FactionData {
  name: FactionNames,
  goldAmount: number,
  playerCount: number
}

export interface GameSelectionData {
  factions: FactionData[]
  timer: number
}

export interface flagDataStats {
  faction : FactionNames;
  inBase : Boolean;
  baseFaction : FactionNames;
  freeOwnerFaction : FactionNames
}

export interface FactionAndFlagData {
  faction: FactionNames,
  goldAmount: number,
  playerCount: number,
  flagData?: flagDataStats
}
