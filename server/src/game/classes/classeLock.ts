import { ClassNames } from './ClassNames'
import { Classe } from './Classe'
import { classesDef } from './classesDef'

export interface ClassLock {
  key: ClassNames;
  order: number;
}

export interface ClassLockArchetype {
  key: ClassNames;
  locks: ClassLock[];
}

export const DEFAULT_CLASS_LOCK_ARCHETYPE = [
  {
    key: ClassNames.HEALER,
    locks: [
      {
        key : ClassNames.PRIEST,
        order : 1
      },
    ],
  },
  {
    key: ClassNames.DPS,
    locks: [
      {
        key : ClassNames.ROGUE,
        order : 1
      },
    ],
  },
  {
    key: ClassNames.TANK,
    locks: [
      {
        key : ClassNames.PESTIFEROUS,
        order : 1
      }
    ],
  },
]

export const getAvailableClasses = (
  currentClass: ClassNames,
  classesLocks: ClassLockArchetype[]
) => {
  const availableClasses: Classe[] = []
  let firstChoice: ClassNames
  let secondChoice: ClassNames

  if (currentClass === ClassNames.PECNO) {
    return classesDef.filter((classe) =>
      [ClassNames.DPS, ClassNames.TANK, ClassNames.HEALER].includes(classe.name)
    )
  }

  const concernedClassesLock = classesLocks.find(
    (classesLock) => classesLock.key === currentClass
  )
  const firstLock = concernedClassesLock?.locks.find(
    (lock) => lock.order === 1
  )
  const secondLock = concernedClassesLock?.locks.find(
    (lock) => lock.order === 2
  )
  if (firstLock && secondLock) {
    firstChoice = firstLock.key
    secondChoice = secondLock.key
  } else if (firstLock) {
    firstChoice = firstLock.key
    secondChoice = randomClassExcept1(currentClass, firstChoice)
  } else {
    firstChoice = randomClass(currentClass)
    secondChoice = randomClassExcept1(currentClass, firstChoice)
  }
  availableClasses.push(
    classesDef.find((classe) => classe.name === firstChoice) as Classe
  )
  availableClasses.push(
    classesDef.find((classe) => classe.name === secondChoice) as Classe
  )
  return availableClasses
}

export const randomClassExcept1 = (
  type: ClassNames,
  alreadyUsed: ClassNames | null
) => {
  const classArrayByArchetype = classesDef.filter((classe) => {
    return classe.archetype === type
  })
  let randomClass =
    classArrayByArchetype[
    Math.floor(Math.random() * classArrayByArchetype.length)
    ]
  let i = 0
  while (randomClass.name === alreadyUsed || i >= 100) {
    randomClass =
      classArrayByArchetype[
      Math.floor(Math.random() * classArrayByArchetype.length)
      ]
    i++
  }
  const randomClassName = randomClass.name
  return randomClassName as ClassNames
}

export const randomClass = (type: ClassNames) => {
  const classArrayByArchetype = classesDef.filter((classe) => {
    return classe.archetype === type
  })
  const randomClass =
    classArrayByArchetype[
    Math.floor(Math.random() * classArrayByArchetype.length)
    ]
  const randomClassName = randomClass.name
  return randomClassName
}
