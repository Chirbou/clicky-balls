import { ClassNames } from './ClassNames'
import { SkillNames } from '../skills/core/SkillNames'

export interface StatsShift {
  atk: number
  def: number
  heal: number
  zIndex: number
  size: number
  tpStackMax: number
}

export interface ClasseStats {
  maxHp: ScoreLandings
  scoreToEvolve?: ScoreLandings
  scoreToDevolve?: ScoreLandings
}

export interface Classe {
  name: ClassNames
  archetype: ClassNames
  goldStackMax:number
  statsShift: StatsShift
  stats: ClasseStats
  skills: SkillNames[]
}

export enum ScoreLandings {
  PECNO_MAXSCORE = 200,
  PECNO_EVOLVE = 100,
  PECNO_DEVOLVE = 0,
  FIRST_LEVEL_DEVOLVE = 100,
  SECOND_LEVEL_DEVOLVE = 200,
  DPS_EVOLVE = 300,
  DPS_MAXSCORE = 400,
  HEALER_EVOLVE = 350,
  HEALER_MAXSCORE = 450,
  TANK_EVOLVE = 400,
  TANK_MAXSCORE = 500,
}
