import { SkillNames } from '../skills/core/SkillNames'
import { Classe, ScoreLandings } from './Classe'
import { ClassNames } from './ClassNames'

const pecnoClasse = {
  name: ClassNames.PECNO,
  archetype: ClassNames.PECNO,
  goldStackMax: 1500,
  statsShift: {
    atk: 1,
    def: 0,
    heal: 1,
    zIndex: 2,
    size: 50,
    tpStackMax: 5
  },
  stats: {
    maxHp: ScoreLandings.PECNO_MAXSCORE,
    scoreToEvolve: ScoreLandings.PECNO_EVOLVE,
    scoreToDevolve: ScoreLandings.PECNO_DEVOLVE,
  },
  skills: [
    SkillNames.PECNO_TP,
    SkillNames.PECNO_ATK,
  ],
}

export const classesDef: Classe[] = [
  pecnoClasse,
  {
    name: ClassNames.DPS,
    archetype: ClassNames.PECNO,
    goldStackMax: 600,
    statsShift: {
      atk: 3,
      def: 1,
      heal: 1,
      zIndex: 4,
      size: 20,
      tpStackMax: -1
    },
    stats: {
      maxHp: ScoreLandings.DPS_MAXSCORE,
      scoreToEvolve: ScoreLandings.DPS_EVOLVE,
      scoreToDevolve: ScoreLandings.FIRST_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.DPS_ATK,
    ],
  },
  {
    name: ClassNames.ROGUE,
    archetype: ClassNames.DPS,
    goldStackMax: 1500,
    statsShift: {
      atk: 2,
      def: 0,
      heal: 0,
      zIndex: -5,
      size: -20,
      tpStackMax: 1
    },
    stats: {
      maxHp: ScoreLandings.DPS_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.DPS_ATK,
      SkillNames.ROGUE_DIVERSION,
      SkillNames.ROGUE_STAB,
      SkillNames.ROGUE_FURTIVE_HIT
    ],
  },
  {
    name: ClassNames.TRAPPER,
    archetype: ClassNames.DPS,
    goldStackMax: 1000,
    statsShift: {
      atk: 2,
      def: 0,
      heal: 0,
      zIndex: 1,
      size: 10,
      tpStackMax: 1
    },
    stats: {
      maxHp: ScoreLandings.DPS_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.DPS_ATK,
      SkillNames.TRAPPER_STEALTH,
      SkillNames.TRAPPER_SNIPER,
      SkillNames.TRAPPER_MINE,
    ],
  },
  {
    name: ClassNames.POISON,
    archetype: ClassNames.DPS,
    goldStackMax: 1000,
    statsShift: {
      atk: 0,
      def: 0,
      heal: 3,
      zIndex: 5,
      size: 10,
      tpStackMax: 0
    },
    stats: {
      maxHp: ScoreLandings.DPS_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.DPS_ATK,
      SkillNames.POISON_BOMBKIPU,
      SkillNames.POISON_VAMPIRISM,
      SkillNames.POISON_FANG
    ],
  },
  {
    name: ClassNames.HEALER,
    archetype: ClassNames.PECNO,
    goldStackMax: 600,
    statsShift: {
      atk: 1,
      def: 1,
      heal: 3,
      zIndex: 3, //5 abs
      size: 10,
      tpStackMax: -2
    },
    stats: {
      maxHp: ScoreLandings.HEALER_MAXSCORE,
      scoreToEvolve: ScoreLandings.HEALER_EVOLVE,
      scoreToDevolve: ScoreLandings.FIRST_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.HEALER_HEAL,
    ],
  },
  {
    name: ClassNames.PRIEST,
    archetype: ClassNames.HEALER,
    goldStackMax: 1550,
    statsShift: {
      atk: 1,
      def: 0,
      heal: 2,
      zIndex: 0, //5
      size: 10,
      tpStackMax: 0
    },
    stats: {
      maxHp: ScoreLandings.HEALER_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.HEALER_HEAL,
      SkillNames.PRIEST_HEALZONE,
      SkillNames.PRIEST_HANGON,
      SkillNames.PRIEST_DISPEL
    ],
  },
  {
    name: ClassNames.MECHANOTHERAPIST,
    archetype: ClassNames.HEALER,
    goldStackMax: 1000,
    statsShift: {
      atk: 1,
      def: 1,
      heal: 1,
      zIndex: 0, //5
      size: 10,
      tpStackMax: 0
    },
    stats: {
      maxHp: ScoreLandings.HEALER_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.HEALER_HEAL,
      SkillNames.MECHANOTHERAPIST_TOWER_BUILD,
      SkillNames.MECHANOTHERAPIST_TOWER_BUFF,
      SkillNames.MECHANOTHERAPIST_TOWER_HACK
    ],
  },
  {
    name: ClassNames.OCCULTIST,
    archetype: ClassNames.HEALER,
    goldStackMax: 1000,
    statsShift: {
      atk: 1,
      def: 0,
      heal: 2,
      zIndex: 1, //5
      size: 10,
      tpStackMax: 0
    },
    stats: {
      maxHp: ScoreLandings.HEALER_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.HEALER_HEAL,
      SkillNames.OCCULTIST_SUCKER,
      SkillNames.OCCULTIST_BLOW,
      SkillNames.OCCULTIST_FUCKER,
    ],
  },

  {
    name: ClassNames.DRUID,
    archetype: ClassNames.HEALER,
    goldStackMax: 1000,
    statsShift: {
      atk: 1,
      def: 1,
      heal: 0,
      zIndex: 0, //5
      size: 20,
      tpStackMax: 0
    },
    stats: {
      maxHp: ScoreLandings.HEALER_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.HEALER_HEAL,
      SkillNames.DRUID_HOTAOE,
      SkillNames.DPS_ATK,
      SkillNames.DRUID_STUN,
    ],
  },
  {
    name: ClassNames.TANK,
    archetype: ClassNames.PECNO,
    goldStackMax: 600,
    statsShift: {
      atk: 2,
      def: 3,
      heal: 1,
      zIndex: 6, //8
      size: 50,
      tpStackMax: -2
    },
    stats: {
      maxHp: ScoreLandings.TANK_MAXSCORE,
      scoreToEvolve: ScoreLandings.TANK_EVOLVE,
      scoreToDevolve: ScoreLandings.FIRST_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.TANK_INVINCIBILITY,
    ],
  },
  {
    name: ClassNames.BERSERK,
    archetype: ClassNames.TANK,
    goldStackMax: 1000,
    statsShift: {
      atk: 1,
      def: 0,
      heal: 0,
      zIndex: 3, //9
      size: 20,
      tpStackMax: 0
    },
    stats: {
      maxHp: ScoreLandings.TANK_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.TANK_INVINCIBILITY,
      SkillNames.BERSERK_SUFFER,
      SkillNames.BERSERK_BUBBLE,
      SkillNames.BERSERK_SHELL,
    ],
  },
  {
    name: ClassNames.PALADIN,
    archetype: ClassNames.TANK,
    goldStackMax: 700,
    statsShift: {
      atk: 0,
      def: 1,
      heal: 0,
      zIndex: 4, //10
      size: 30,
      tpStackMax: -1
    },
    stats: {
      maxHp: ScoreLandings.TANK_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.TANK_INVINCIBILITY,
      SkillNames.TANK_SILENCE,
      SkillNames.PALADIN_BUBBLE,
      SkillNames.PALADIN_GRAB,
    ],
  },
  {
    name: ClassNames.SHAMAN,
    archetype: ClassNames.TANK,
    goldStackMax: 1000,
    statsShift: {
      atk: 0,
      def: 0,
      heal: 1,
      zIndex: 4, //10
      size: 30,
      tpStackMax: -1
    },
    stats: {
      maxHp: ScoreLandings.TANK_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.TANK_INVINCIBILITY,
      SkillNames.SHAMAN_TOTEM,
      SkillNames.SHAMAN_UNDERPRESSURE,
      SkillNames.SHAMAN_THUNDERBOLT
    ],
  },
  {
    name: ClassNames.PESTIFEROUS,
    archetype: ClassNames.TANK,
    goldStackMax: 1000,
    statsShift: {
      atk: 0,
      def: 0,
      heal: 1,
      zIndex: 4, //10
      size: 30,
      tpStackMax: -1
    },
    stats: {
      maxHp: ScoreLandings.TANK_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.TANK_INVINCIBILITY,
      SkillNames.PESTIFEROUS_CONTAGION,
      SkillNames.PESTIFEROUS_DEFLAGRATION,
      SkillNames.PESTIFEROUS_DISPEL
    ],
  },
  {
    name: ClassNames.WINSLOW,
    archetype: ClassNames.PECNO,
    goldStackMax: 88888,
    statsShift: {
      atk: 42,
      def: 42,
      heal: 100,
      zIndex: 10, //10
      size: 80,
      tpStackMax: 42
    },
    stats: {
      maxHp: ScoreLandings.TANK_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.HEALER_HEAL,
      SkillNames.TANK_INVINCIBILITY,
      SkillNames.PALADIN_GRAB,
      SkillNames.TOGGLE_SILENCE,
      SkillNames.PESTIFEROUS_CONTAGION,
    ],
  },
  {
    name: ClassNames.YUMHAX,
    archetype: ClassNames.PECNO,
    goldStackMax: 188888,
    statsShift: {
      atk: 300,
      def: 300,
      heal: 300,
      zIndex: 4, //10
      size: 60,
      tpStackMax: 100
    },
    stats: {
      maxHp: ScoreLandings.TANK_MAXSCORE,
      scoreToDevolve: ScoreLandings.SECOND_LEVEL_DEVOLVE
    },
    skills: [
      SkillNames.PECNO_TP,
      SkillNames.PECNO_ATK,
      SkillNames.HEALER_HEAL,
      SkillNames.DPS_ATK,
      SkillNames.TANK_INVINCIBILITY,
      SkillNames.TANK_SILENCE,
      SkillNames.TOGGLE_SILENCE,
      SkillNames.BERSERK_BUBBLE,
      SkillNames.BERSERK_SUFFER,
      SkillNames.DRUID_STUN,
      SkillNames.DRUID_HOTAOE,
      SkillNames.PALADIN_BUBBLE,
      SkillNames.PALADIN_GRAB,
      SkillNames.POISON_BOMBKIPU,
      SkillNames.PESTIFEROUS_CONTAGION,
      SkillNames.POISON_VAMPIRISM,
      SkillNames.POISON_FANG,
      SkillNames.PRIEST_HANGON,
      SkillNames.PRIEST_HEALZONE,
      SkillNames.ROGUE_DIVERSION,
      SkillNames.ROGUE_FURTIVE_HIT,
      SkillNames.ROGUE_STAB,
      SkillNames.PECNO_TP,

    ],
  }
]

export const DEFAULT_CLASS = pecnoClasse
