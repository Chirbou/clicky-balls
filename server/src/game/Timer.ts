import { NetworkEventHandler } from '../core/NetworkEventHandler'
import { Utils } from '../core/Utils'
import { FactionNames } from './FactionNames'
import { Game, resetFactionTotalGold } from './Game'
import { GameEvents } from './GameEventActions'
import { GameObjectType } from './gameobjects/GameObjectType'
import { Gold } from './gameobjects/Gold'
import { GoldDeposit } from './gameobjects/GoldDeposit'
import { GoldMine } from './gameobjects/GoldMine'
import { Tower } from './gameobjects/Tower'
import { Portal } from './rooms/Portal'

export const TimerConstants = {
  OPENING_GATE_MESSAGE_DURATION: 5000,
  STARTING_PHASE_DURATION: 20000,
  GAME_DURATION: 600000,
  END_GAME_MESSAGE_DURATION: 10000
}

export class Timer {
  timeStampStart: number
  timeStampFinish: number
  globalTimeout: NodeJS.Timeout
  factionPortalTab: Portal[] = []

  //Temps de jeux :
  // 1) Accueil : tuto disponible, annonce de l'ouverture du portail et des enjeux
  // 2) Ouverture des portails
  // 3) Annonce : Il reste 5 minutes
  // 4) Annonce : Il reste 2 minutes
  // 5) FIN DE PARTIE :
  //Tout les joueurs dans une seule et même room,
  //room où ils peuvent changer de faction,
  //voir les résultats,
  //le timer change en décompte de relance de partie,
  //mais les joueurs ne peuvent pas s'attaquer.

  gameResult = resetFactionTotalGold()

  constructor(date: number) {

    this.timeStampStart = date
    this.timeStampFinish = this.timeStampStart + TimerConstants.GAME_DURATION

    // TODO les portails doivent s'activer durant la première partie du RESET serveur en fait, Amine.

    setInterval(() => {
      this.refresh()
    }, 1000)

    this.globalTimeout = setTimeout(() => {
      this.resetPlayers()
      this.emitEndAnnounces()
      Game.totalGoldInGame = 0
      Game.respawnFlags()
      this.saveAndResetGold()

      Game.timeStampStart = Date.now()
      Game.timer = new Timer(Game.timeStampStart)

    }, TimerConstants.GAME_DURATION)
  }

  refresh() {
    NetworkEventHandler.broadcast(GameEvents.TIMER_REFRESH, {
      // TODO phase:this.getPhase()
      timeStampStart: Game.timeStampStart,
      timerDuration: TimerConstants.GAME_DURATION
    })
  }

  saveAndResetGold() {
    //Stocker les scores de faction avant reset
//    Game.factionTotalGoldEver.punk += this.gameResult.punk
//    Game.factionTotalGoldEver.conservatist += this.gameResult.conservatist
//    Game.factionTotalGoldEver.technophile += this.gameResult.technophile

    Game.factionTotalGold = resetFactionTotalGold()

    // regrouper les goldDeposit et les mettre à Zero
    const goldDepositTab = []
    goldDepositTab.push(Utils.findGameObjectByTypeAndFaction(GameObjectType.GOLD_DEPOSIT, FactionNames.PUNK) as GoldDeposit)
    goldDepositTab.push(Utils.findGameObjectByTypeAndFaction(GameObjectType.GOLD_DEPOSIT, FactionNames.CONSERVATIST) as GoldDeposit)
    goldDepositTab.push(Utils.findGameObjectByTypeAndFaction(GameObjectType.GOLD_DEPOSIT, FactionNames.TECHNOPHILE) as GoldDeposit)
    goldDepositTab.forEach((gd) => gd.goldAmount = 0)

    const goldMinesTab = Utils.findGameObjectByGameObjectType(GameObjectType.GOLD_MINE) as GoldMine[]
    goldMinesTab.forEach((gm) => this.resetGoldMinesGold(gm))    

    const goldTab = Utils.findGameObjectByGameObjectType(GameObjectType.GOLD) as Gold[]
    goldTab.map(gold => gold.removeFromRoom())

    const towerTab = Utils.findGameObjectByGameObjectType(GameObjectType.TOWER) as Tower[]
    towerTab.forEach(tower => { 
      tower.hp = 0
      tower.takeHeal(750)
 })

  }
  emitEndAnnounces() {
    this.gameResult = Game.factionTotalGold
    NetworkEventHandler.broadcast(GameEvents.END_GAME, {
      eventName: GameEvents.END_GAME,
      messageDuration: TimerConstants.END_GAME_MESSAGE_DURATION,
      [FactionNames.PUNK]: this.gameResult.punk.gold,
      [FactionNames.CONSERVATIST]: this.gameResult.conservatist.gold,
      [FactionNames.TECHNOPHILE]: this.gameResult.technophile.gold,
    })

  }
  resetPlayers() {
    Game.players.forEach((player) => {
      player.reset()
    })
  }

  initFactionPortals() {
    this.factionPortalTab.push(Utils.findGameObjectByTypeAndFaction(GameObjectType.PORTAL, FactionNames.PUNK) as Portal)
    this.factionPortalTab.push(Utils.findGameObjectByTypeAndFaction(GameObjectType.PORTAL, FactionNames.CONSERVATIST) as Portal)
    this.factionPortalTab.push(Utils.findGameObjectByTypeAndFaction(GameObjectType.PORTAL, FactionNames.TECHNOPHILE) as Portal)
  }

  resetGoldMinesGold(gm:GoldMine){
    gm.hp = gm.maxHp;
    gm.goldAmount = 5000;
    Game.totalGoldInGame += 5000;
  }
}
