import { FactionNames } from '../FactionNames'
import { Game } from '../Game'
import { FlagBase } from '../gameobjects/FlagBase'
import { GoldDeposit } from '../gameobjects/GoldDeposit'
import { GoldMine } from '../gameobjects/GoldMine'
import { Tower, DEFAULT_TOWER_MAXHP, DEFAULT_SHOOT_RATE, DEFAULT_DAMAGE, Temperament } from '../gameobjects/Tower'
import { IGameObject } from '../interfaces/IGameObject'
import { FlagBasePositions } from './FlagBasePositions'
import { Portal } from './Portal'
import { PortalPositions } from './PortalPositions'
import { Room } from './Room'
import { RoomTypes } from './RoomTypes'

export class World {
  rooms: Room[] = []
  gameObjects: IGameObject[] = []
  factionGameObjects: IGameObject[] = []

  constructor() {
    const eastPopRoom = new Room(RoomTypes.PUNKZONE)
    const northPopRoom = new Room(RoomTypes.CONSERVATISTZONE)
    const westPopRoom = new Room(RoomTypes.TECHNOPHILEZONE)

    const eastInterRoom = new Room(RoomTypes.INTERPUNK)
    const northInterRoom = new Room(RoomTypes.INTERCONSERVATEUR)
    const westInterRoom = new Room(RoomTypes.INTERTECHNOPHILE)
    const southInterRoom = new Room(RoomTypes.BASE)

    const middleWarRoom = new Room(RoomTypes.SHOP)
    const southWarRoom = new Room(RoomTypes.GOLDZONE)

    const middleUndergroundRoom = new Room(RoomTypes.CHESTZONE)
    const eastUndergroundRoom = new Room(RoomTypes.CHESTZONE)
    const westUndergroundRoom = new Room(RoomTypes.CHESTZONE)
    const northUndergroundRoom = new Room(RoomTypes.CHESTZONE)
    const southUndergroundRoom = new Room(RoomTypes.CHESTZONE)

    new GoldDeposit(
      eastInterRoom,
      FactionNames.PUNK,
      FlagBasePositions.TO_CENTER
    )
    new GoldDeposit(
      northInterRoom,
      FactionNames.CONSERVATIST,
      FlagBasePositions.TO_CENTER
    )
    new GoldDeposit(
      westInterRoom,
      FactionNames.TECHNOPHILE,
      FlagBasePositions.TO_CENTER
    )

    new Portal(
      middleUndergroundRoom,
      eastUndergroundRoom,
      PortalPositions.TO_WEST
    )
    new Portal(
      middleUndergroundRoom,
      northUndergroundRoom,
      PortalPositions.TO_NORTH
    )
    new Portal(
      middleUndergroundRoom,
      westUndergroundRoom,
      PortalPositions.TO_EAST
    )
    new Portal(
      middleUndergroundRoom,
      southUndergroundRoom,
      PortalPositions.TO_SOUTH
    )
    new Portal(middleUndergroundRoom, middleWarRoom, PortalPositions.TO_CENTER)
    new Portal(
      eastUndergroundRoom,
      middleUndergroundRoom,
      PortalPositions.TO_EAST
    )
    new Portal(
      northUndergroundRoom,
      middleUndergroundRoom,
      PortalPositions.TO_SOUTH
    )
    new Portal(
      westUndergroundRoom,
      middleUndergroundRoom,
      PortalPositions.TO_WEST
    )
    new Portal(
      southUndergroundRoom,
      middleUndergroundRoom,
      PortalPositions.TO_NORTH
    )
    new Portal(
      eastPopRoom,
      eastInterRoom,
      PortalPositions.TO_EAST,
      FactionNames.PUNK,
      true
    )
    new Portal(
      northPopRoom,
      northInterRoom,
      PortalPositions.TO_SOUTH,
      FactionNames.CONSERVATIST,
      true
    )
    new Portal(
      westPopRoom,
      westInterRoom,
      PortalPositions.TO_WEST,
      FactionNames.TECHNOPHILE,
      true
    )
    new Portal(
      eastInterRoom,
      eastPopRoom,
      PortalPositions.TO_WEST,
      FactionNames.PUNK,
      true
    )
    new Portal(eastInterRoom, middleWarRoom, PortalPositions.TO_EAST)
    new Portal(
      northInterRoom,
      northPopRoom,
      PortalPositions.TO_NORTH,
      FactionNames.CONSERVATIST,
      true
    )
    new Portal(northInterRoom, middleWarRoom, PortalPositions.TO_SOUTH)
    new Portal(
      westInterRoom,
      westPopRoom,
      PortalPositions.TO_EAST,
      FactionNames.TECHNOPHILE,
      true
    )
    new Portal(westInterRoom, middleWarRoom, PortalPositions.TO_WEST)
    new Portal(southInterRoom, southWarRoom, PortalPositions.TO_SOUTH)
    new Portal(southInterRoom, middleWarRoom, PortalPositions.TO_NORTH)
    new Portal(
      middleWarRoom,
      eastInterRoom,
      PortalPositions.TO_WEST,
      FactionNames.PUNK,
      true
    )
    new Portal(
      middleWarRoom,
      northInterRoom,
      PortalPositions.TO_NORTH,
      FactionNames.CONSERVATIST,
      true
    )
    new Portal(
      middleWarRoom,
      westInterRoom,
      PortalPositions.TO_EAST,
      FactionNames.TECHNOPHILE,
      true
    )
    new Portal(middleWarRoom, southInterRoom, PortalPositions.TO_SOUTH)
    new Portal(
      middleWarRoom,
      middleUndergroundRoom,
      PortalPositions.TO_CENTER,
      FactionNames.NOFACTION,
      true
    )
    new Portal(southWarRoom, southInterRoom, PortalPositions.TO_NORTH)

    new GoldMine(southUndergroundRoom, 1350, FlagBasePositions.TO_CENTER)
    new GoldMine(southWarRoom, 1350, FlagBasePositions.TO_CENTER)
    new GoldMine(southWarRoom, 666, FlagBasePositions.TO_WEST)
    new GoldMine(southWarRoom, 999, FlagBasePositions.TO_EAST)
    new GoldMine(southInterRoom, 350, FlagBasePositions.TO_CENTER)

    new Tower(
      northPopRoom,
      FactionNames.CONSERVATIST,
      PortalPositions.TO_NORTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE,
      Temperament.GUARDIAN
    )
    new Tower(
      northPopRoom,
      FactionNames.CONSERVATIST,
      PortalPositions.TO_NORTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE,
      Temperament.GUARDIAN
    )

    new Tower(
      eastPopRoom,
      FactionNames.PUNK,
      PortalPositions.TO_NORTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE,
      Temperament.GUARDIAN
    )
    new Tower(
      eastPopRoom,
      FactionNames.PUNK,
      PortalPositions.TO_SOUTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE,
      Temperament.GUARDIAN
    )

    new Tower(
      westPopRoom,
      FactionNames.TECHNOPHILE,
      PortalPositions.TO_SOUTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE,
      Temperament.GUARDIAN
    )
    new Tower(
      westPopRoom,
      FactionNames.TECHNOPHILE,
      PortalPositions.TO_NORTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE,
      Temperament.GUARDIAN
    )

    new Tower(
      northInterRoom,
      FactionNames.CONSERVATIST,
      PortalPositions.TO_SOUTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      northInterRoom,
      FactionNames.CONSERVATIST,
      PortalPositions.TO_SOUTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )

    new Tower(
      eastInterRoom,
      FactionNames.PUNK,
      PortalPositions.TO_NORTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      eastInterRoom,
      FactionNames.PUNK,
      PortalPositions.TO_SOUTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )

    new Tower(
      westInterRoom,
      FactionNames.TECHNOPHILE,
      PortalPositions.TO_NORTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      westInterRoom,
      FactionNames.TECHNOPHILE,
      PortalPositions.TO_SOUTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )

    new Tower(
      southInterRoom,
      FactionNames.NOFACTION,
      PortalPositions.TO_SOUTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      southInterRoom,
      FactionNames.NOFACTION,
      PortalPositions.TO_SOUTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      southWarRoom,
      FactionNames.NOFACTION,
      PortalPositions.TO_NORTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      southWarRoom,
      FactionNames.NOFACTION,
      PortalPositions.TO_NORTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      southWarRoom,
      FactionNames.NOFACTION,
      PortalPositions.TO_SOUTH_EAST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )
    new Tower(
      southWarRoom,
      FactionNames.NOFACTION,
      PortalPositions.TO_SOUTH_WEST,
      DEFAULT_TOWER_MAXHP,
      DEFAULT_SHOOT_RATE,
      DEFAULT_DAMAGE
    )

    new FlagBase(
      eastPopRoom,
      FactionNames.PUNK,
      FlagBasePositions.TO_WEST,
      true
    )
    new FlagBase(
      eastPopRoom,
      FactionNames.PUNK,
      FlagBasePositions.TO_WEST_BOTTOM,
      false
    )
    new FlagBase(
      eastPopRoom,
      FactionNames.PUNK,
      FlagBasePositions.TO_WEST_TOP,
      false
    )
    new FlagBase(
      westPopRoom,
      FactionNames.TECHNOPHILE,
      FlagBasePositions.TO_EAST,
      true
    )
    new FlagBase(
      westPopRoom,
      FactionNames.TECHNOPHILE,
      FlagBasePositions.TO_EAST_BOTTOM,
      false
    )
    new FlagBase(
      westPopRoom,
      FactionNames.TECHNOPHILE,
      FlagBasePositions.TO_EAST_TOP,
      false
    )
    new FlagBase(
      northPopRoom,
      FactionNames.CONSERVATIST,
      FlagBasePositions.TO_NORTH,
      true
    )
    new FlagBase(
      northPopRoom,
      FactionNames.CONSERVATIST,
      FlagBasePositions.TO_NORTH_BOTTOM,
      false
    )
    new FlagBase(
      northPopRoom,
      FactionNames.CONSERVATIST,
      FlagBasePositions.TO_NORTH_TOP,
      false
    )
    Game.rooms = [
      middleWarRoom,
      eastPopRoom,
      eastInterRoom,
      northPopRoom,
      northInterRoom,
      westPopRoom,
      westInterRoom,
      southWarRoom,
      southInterRoom,
      southUndergroundRoom,
      eastUndergroundRoom,
      northUndergroundRoom,
      westUndergroundRoom,
      middleUndergroundRoom,
    ]
  }
}
