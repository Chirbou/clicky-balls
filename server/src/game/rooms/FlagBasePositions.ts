import { Coordinate } from '../../core/Utils'

export const toRand = () => {
  const xRnd = Math.floor(Math.random() * 1100)
  const yRnd = Math.floor(Math.random() * 700)
  return { x: xRnd, y: yRnd }
}

export const FlagBasePositions = {
  TO_WEST: { x: 100, y: 350 } as Coordinate,
  TO_WEST_TOP: { x: 100, y: 240 } as Coordinate,
  TO_WEST_BOTTOM: { x: 100, y: 460 } as Coordinate,
  TO_EAST: { x: 1100, y: 350 } as Coordinate,
  TO_EAST_TOP: { x: 1100, y: 240 } as Coordinate,
  TO_EAST_BOTTOM: { x: 1100, y: 460 } as Coordinate,
  TO_NORTH: { x: 600, y: 100 } as Coordinate,
  TO_NORTH_TOP: { x: 490, y: 100 } as Coordinate,
  TO_NORTH_BOTTOM: { x: 710, y: 100 } as Coordinate,
  TO_SOUTH: { x: 600, y: 650 } as Coordinate,
  TO_SOUTH_TOP: { x: 490, y: 650 } as Coordinate,
  TO_SOUTH_BOTTOM: { x: 710, y: 650 } as Coordinate,
  TO_CENTER: { x: 600, y: 350 } as Coordinate,
  TO_MIDDLE_TOP_LEFT: { x: 175, y: 75} as Coordinate
}

