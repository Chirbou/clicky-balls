import { Coordinate } from '../../core/Utils'

export const toRand = ()=>{
  const xRnd = Math.floor(Math.random() * 1100)
  const yRnd = Math.floor(Math.random() * 700)
  return { x: xRnd , y: yRnd }
}

export const PortalPositions = {
  TO_WEST :  { x: 100, y: 370 } as Coordinate,
  TO_EAST : { x: 1100, y: 370 } as Coordinate,
  TO_NORTH : { x: 620, y: 100 } as Coordinate,
  TO_SOUTH : { x: 620, y: 670 } as Coordinate,
  TO_CENTER : { x: 620, y: 370 } as Coordinate,
  TO_SOUTH_WEST : { x: 100, y: 650 } as Coordinate,
  TO_SOUTH_EAST : { x: 1100, y: 650 } as Coordinate,
  TO_NORTH_EAST : { x: 1100, y: 100 } as Coordinate,
  TO_NORTH_WEST : { x: 100, y: 100 } as Coordinate,
}

