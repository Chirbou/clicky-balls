import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { AlterationNames } from '../alterations/Alteration'
import { Player } from '../player/Player'
import { Room } from './Room'
import { PortalPositions } from './PortalPositions'
import { GameEvents } from '../GameEventActions'
import { ClassNames } from '../classes/ClassNames'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Game } from '../Game'
import { SkillNames } from '../skills/core/SkillNames'
import { defaultEmitGameEvent, IGameObject, IGameObjectData } from '../interfaces/IGameObject'
import { FactionNames } from '../FactionNames'
import { classesDef } from '../classes/classesDef'

export interface PortalData extends IGameObjectData {
  destination: Coordinate
  activated: boolean
  cooldown: number
  faction: FactionNames
}

export class Portal implements IGameObject {
  id: string = getUUID()
  position: Coordinate
  destination: Coordinate
  size = 100
  hitbox = 100
  activated: boolean
  room: Room
  roomId: string
  destinationRoom!: Room
  cooldownBeforeActivation = 3000
  type = GameObjectType.PORTAL
  name = ''
  disabled = false
  zIndex = 0
  faction: FactionNames = FactionNames.NOFACTION

  constructor(room: Room, destinationRoom: Room, position: Coordinate = { x: 0, y: 0 }, faction = FactionNames.NOFACTION, activated = true) {
    this.destinationRoom = destinationRoom
    this.room = room
    this.roomId = room.id
    this.position = position
    this.destination = this.seekDestinationByPosition(position)
    this.room.addGameObject(this.type, this)
    this.faction = faction
    this.activated = activated
  }

  removeFromRoom(): void {
    throw new Error('Method not implemented.')
  }
  emitGameEvent(gameEvent: GameEvents, data: unknown): void {
    defaultEmitGameEvent(this, gameEvent, data)
  }

  hit(player: Player, skillName: SkillNames): void {

  }

  // TODO Les portails ont 888 de vie, les pecnos ne peuvent que soiger les TP, les heals peuvent les soigner. a 0 de vie, il se desactive pendant 10 secondes, puis est invincible pendant 5 secondes.
  trigger(player: Player) {
    if (this.activated) {
      const allPortals = player.room.gameObjects.get(GameObjectType.PORTAL) as Portal[]
      allPortals?.forEach(p => p.cancel(player))
      for (const roomPlayer of this.room.players) {
        if (Utils.isColliding(roomPlayer, this) && roomPlayer.id !== player.id && this.hasBlockingPortalClass(roomPlayer) && roomPlayer.faction !== player.faction) {
          player.emitGameEvent(GameEvents.GAMEOBJECT_MISS, { type: GameObjectType.PORTAL, coords: this.position, id: this.id })
          return
        }
      }

      player.emitGameEvent(GameEvents.GAMEOBJECT_ON, { type: GameObjectType.PORTAL, id: this.id })
      player.applyAlteration(AlterationNames.TELEPORT, this.cooldownBeforeActivation)
      player.timeout = setTimeout(() => {
        if (Utils.isColliding(player, this) && this.activated) {
          this.room.playerLeaves(player)
          if (player.gluedPlayers) {
            for (let i = 0; i < player.gluedPlayers.length; i++) {
              const playerGlued = player.gluedPlayers[i]
              if (Utils.isColliding(playerGlued, this)) {
                this.room.playerLeaves(playerGlued)
                this.destinationRoom.playerEnters(playerGlued, this.destination)
              }
            }
          }
          this.destinationRoom.playerEnters(player, this.destination)
        }
      }, this.cooldownBeforeActivation)
    } else {
      player.emitGameEvent(GameEvents.GAMEOBJECT_MISS, { type: GameObjectType.PORTAL, coords: this.position, id: this.id })
    }
  }

  cancel(player: Player) {
    if (player.timeout) {
      clearTimeout(player.timeout)
      player.emitGameEvent(GameEvents.GAMEOBJECT_OFF, { type: GameObjectType.PORTAL, id: this.id })
    }
  }

  normalize(): PortalData {
    return {
      type: this.type,
      id: this.id,
      position: this.position,
      destination: this.destination,
      size: this.size,
      hitbox: this.hitbox,
      cooldown: this.cooldownBeforeActivation,
      name: this.name,
      disabled: this.disabled,
      roomId: this.roomId,
      zIndex: this.zIndex,
      activated: this.activated,
      faction: this.faction,
    }
  }

  remove(): void {

  }
  seekDestinationByPosition(position: Coordinate) {
    switch (position) {
      case PortalPositions.TO_EAST:
        return PortalPositions.TO_WEST
        break
      case PortalPositions.TO_WEST:
        return PortalPositions.TO_EAST
        break
      case PortalPositions.TO_NORTH:
        return PortalPositions.TO_SOUTH
        break
      case PortalPositions.TO_SOUTH:
        return PortalPositions.TO_NORTH
        break
      case PortalPositions.TO_CENTER:
        return PortalPositions.TO_CENTER
        break
      default:
        return PortalPositions.TO_SOUTH
    }
  }

  hasBlockingPortalClass(roomPlayer: Player) {
    const archetype = classesDef.find(classe => classe.name === roomPlayer.currentClass)?.archetype
    return archetype === ClassNames.TANK
  }

  desactivate() {
    this.activated = false
    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }
  activate() {
    this.activated = true
    this.room.emitGameEvent(GameEvents.ROOM_UPDATE, this.room.normalize())
  }
}
