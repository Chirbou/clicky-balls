import { Player } from '../player/Player'
import { RoomTypes } from './RoomTypes'
import { GameEvents } from '../GameEventActions'
import { Coordinate, Utils } from '../../core/Utils'
import { getUUID } from '../../core/GenerateId'
import { Game } from '../Game'
import { Colors } from '../Colors'
import { IGameObject, IGameObjectData } from '../interfaces/IGameObject'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Gold, GoldValue } from '../gameobjects/Gold'
import { PowerUp } from '../gameobjects/PowerUp'
import { GoldMine } from '../gameobjects/GoldMine'
import { PortalPositions } from './PortalPositions'
import { IPlayerData } from '../interfaces/IPlayer'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IFactionAffinity } from '../interfaces/IFactionAffinity'
import { IAlterable, isAlterable } from '../interfaces/IAlterable'

export interface RoomData {
  id: string
  players: IPlayerData[]
  gameObjects: IGameObjectData[]
  type: RoomTypes
}

export interface GameObjectMap {
  type: GameObjectType
  items: IGameObject[]
}

export class Room {
  id: string = getUUID()
  players: Player[] = []
  gameObjects: Map<GameObjectType, IGameObject[]> = new Map<GameObjectType, IGameObject[]>()
  type: RoomTypes = RoomTypes.BASE
  interval: NodeJS.Timeout
  chestIsLoading = false
  mineIsLoading = false
  minesRepop: NodeJS.Timeout | undefined

  constructor(type: RoomTypes = RoomTypes.BASE) {
    this.type = type
    this.interval = setInterval(() => { this._handleRoomEffect() }, 4000)
    this.gameObjects.set(GameObjectType.FLAG, [])
    this.gameObjects.set(GameObjectType.POWER_UP, [])
    this.gameObjects.set(GameObjectType.FLAGBASE, [])
    this.gameObjects.set(GameObjectType.GOLD, [])
    this.gameObjects.set(GameObjectType.GOLD_DEPOSIT, [])
    this.gameObjects.set(GameObjectType.GOLD_MINE, [])
    this.gameObjects.set(GameObjectType.MINE, [])
    this.gameObjects.set(GameObjectType.PORTAL, [])
    this.gameObjects.set(GameObjectType.TOTEM, [])
    this.gameObjects.set(GameObjectType.TOWER, [])
  }

  emitGameEvent(gameEvent: GameEvents, data: unknown) {
    for (const player of this.players) {
      player.emitGameEvent(gameEvent, data)
    }
  }

  addGameObject(type: GameObjectType, gameObject: IGameObject) {
    this.gameObjects.get(type)?.push(gameObject)
  }

  addGameObjects(type: GameObjectType, gameObjects: IGameObject[]) {
    gameObjects.forEach(p => this.gameObjects.get(type)?.push(p))
  }

  removeGameObject(type: GameObjectType, gameObject: IGameObject) {
    this.gameObjects.get(type)?.splice(this.gameObjects.get(type)?.indexOf(gameObject) || 0, 1)
  }
  normalize(): RoomData {
    const gameObjectsNormalized: IGameObjectData[] = []
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.FLAG)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.FLAGBASE)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.POWER_UP)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.GOLD)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.GOLD_DEPOSIT)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.GOLD_MINE)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.MINE)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.PORTAL)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.TOTEM)?.map(o => o.normalize()) || []))
    gameObjectsNormalized.push(...(this.gameObjects.get(GameObjectType.TOWER)?.map(o => o.normalize()) || []))

    return {
      id: this.id,
      players: this.players.map(p => p.normalize()),
      gameObjects: gameObjectsNormalized,
      type: this.type,
    }
  }

  playerEnters(player: Player, newPosition: Coordinate) {
    //ajouter le paramètre enterBy qui est un PortalPositions
    //ajouter une fonction qui réccupère des positions opposé selon le PortalPositions (TO_EAST en entrée signifie arriver en TO_WEST)
    //Attibuer la position d'arrivée au Player
    if (player && 'checkDeath' in player) {
      this.players.push(player)
    }
    player.position = newPosition
    player.room = this
    player.roomId = this.id
    this.emitGameEvent(GameEvents.ENTER_ROOM, player.normalize())
    this.emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())
    player.emitGameEvent(GameEvents.ROOM_UPDATE, this.normalize())
  }

  playerLeaves(player: Player) {
    this.players = this.players.filter((p) => player.id !== p.id)
    this.emitGameEvent(GameEvents.LEAVE_ROOM, player.normalize())
  }

  getNormalizedPlayers(): IPlayerData[] {
    return this.players.map((p) => p.normalize())
  }

  static getRoomFromColor(color: any) {
    let room = Game.rooms[0]
    switch (color) {
      case Colors.CONSERVATISTS_COLOR:
        room = Game.getRoomFromName(RoomTypes.CONSERVATISTZONE)
        break
      case Colors.PUNKS_COLOR:
        room = Game.getRoomFromName(RoomTypes.PUNKZONE)
        break
      case Colors.TECHNOPHILES_COLOR:
        room = Game.getRoomFromName(RoomTypes.TECHNOPHILEZONE)
        break
    }
    return room
  }

  _handleRoomEffect() {
    if (this.type === RoomTypes.CONSERVATISTZONE ||
      this.type === RoomTypes.PUNKZONE ||
      this.type === RoomTypes.TECHNOPHILEZONE) {
      this._popGold(16)
      // this._handleFactionEffect()
    } else if (this.type === RoomTypes.CHESTZONE) {
      this._handleChestLoading()
    }
  }

  getObjectsByType(type: GameObjectType) {
    return this.gameObjects.get(type)
  }

  getObjectsByTypes(types: GameObjectType[]): IGameObject[] {
    const res = []
    for (const type of types) {
      res.push(...this.gameObjects.get(type) || [])
    }
    return res
  }

  _handleChestLoading() {
    const hasChest = this.gameObjects.get(GameObjectType.POWER_UP)

    if (hasChest?.length === 0 && !this.chestIsLoading && hasChest.length < 2) {
      setTimeout(() => {
        new PowerUp(this, Utils.getRandomCoordinates())
        this.chestIsLoading = false
        this.emitGameEvent(GameEvents.ROOM_UPDATE, this.normalize())
      }, 30000)
      this.chestIsLoading = true
    }
  }

  // _handleFactionEffect() {
  //   let damage = 50

  //   for (const player of this.players) {
  //     const scoreShiftData = {
  //       idTarget: player.id,
  //       idSource: player.id,
  //       scoreSource: player.hp,
  //       scoreTarget: player.hp,
  //       value: damage,
  //       isCritic: false,
  //       isSuccessful: true,
  //       type: 'damage'
  //     }
  //     if (this.type !== Room.getRoomFromColor(player.color).type) {
  //       if (player.hp > 0) {
  //         damage = player.takeDamage(damage)
  //         scoreShiftData.value = damage
  //         scoreShiftData.type = 'damage'
  //       }
  //     } else {
  //       player.takeHeal(damage)
  //       scoreShiftData.type = 'heal'
  //     }
  //     player.room.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, scoreShiftData)
  //     player.room.emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())
  //   }
  // }

  _popGold(maxGold: number) {
    const MAX_GOLD_INITAL_ROOM = 10
    const position: Coordinate = { x: Utils.randomIntFromInterval(400, 900), y: Utils.randomIntFromInterval(150, 500) }
    const goldObjects = (this.gameObjects.get(GameObjectType.GOLD) || []) as Gold[]
    const goldValueInThisRoom = goldObjects.reduce((total, gold) => gold.value + total, 0) || 0
    const goldCountInThisRoom = goldObjects.length

    const random = Math.random()
    const goldValue = random < 0.4 ? GoldValue.TINY
      : random < 0.7 ? GoldValue.SMALL
        : random < 0.9 ? GoldValue.NORMAL
          : GoldValue.BIG

    if (goldValueInThisRoom + goldValue < maxGold && goldCountInThisRoom < MAX_GOLD_INITAL_ROOM) {
      new Gold(this, position, goldValue)
      Game.totalGoldInGame+=goldValue;
      this.emitGameEvent(GameEvents.ROOM_UPDATE, this.normalize())
    }
  }

  getHealthObjects() {
    const healthObjectsByType: IGameObject[][] = []
    const healthObjects = []
    healthObjectsByType.push(this.gameObjects.get(GameObjectType.GOLD_MINE) as IGameObject[])
    healthObjectsByType.push(this.gameObjects.get(GameObjectType.GOLD_DEPOSIT) as IGameObject[])
    healthObjectsByType.push(this.gameObjects.get(GameObjectType.TOTEM) as IGameObject[])
    healthObjectsByType.push(this.gameObjects.get(GameObjectType.TOWER) as IGameObject[])
    healthObjectsByType.push(this.gameObjects.get(GameObjectType.MINE) as IGameObject[])
    for (let i = 0; i < healthObjectsByType.length; i++) {
      for (let j = 0; j < healthObjectsByType[i].length; j++) {
        healthObjects.push(healthObjectsByType[i][j])
      }
    }
    return healthObjects as IHasHealthPoint[]
  }
 
}
