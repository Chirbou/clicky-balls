import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Martyr extends Alteration {
  stack = 0
  name = AlterationNames.MARTYR

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
