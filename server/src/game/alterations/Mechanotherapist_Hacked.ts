import { IAlterable } from '../interfaces/IAlterable'
import { IGameObject } from '../interfaces/IGameObject'
import { Alteration, AlterationNames } from './Alteration'
import { Tower } from '../gameobjects/Tower'
import { IPlayer } from '../interfaces/IPlayer'
import { FactionNames } from '../FactionNames'

export class Mechanotherapist_Hacked extends Alteration {
  stack = 0
  name = AlterationNames.MECHANOTHERAPIST_HACKED
  originFaction = FactionNames.NOFACTION

  handleEffectUp(target: IGameObject & IAlterable, source?: IGameObject): void {
    if (!source) { return }
    const targetedTower = target as Tower
    const playerSource = source as IPlayer
    this.originFaction = targetedTower.faction
    targetedTower.faction = playerSource.faction
  }

  handleEffectDown(target: IGameObject & IAlterable, source?: IGameObject): void {
    const targetedTower = target as Tower
    targetedTower.faction = this.originFaction as FactionNames
  }
}
