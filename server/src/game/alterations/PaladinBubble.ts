import { GameEvents } from '../GameEventActions'
import { Player } from '../player/Player'
import { SkillNames } from '../skills/core/SkillNames'
import { Alteration, AlterationNames } from './Alteration'

export class PaladinBubble extends Alteration {
  stack = 0
  name = AlterationNames.PALADIN_BUBBLE

  handleEffectUp(player: Player): void {
    player.heal = player.heal + 2
    const playerTargetTP = player.playerSkillData.get(SkillNames.PECNO_TP)
    if (playerTargetTP?.stackMax && playerTargetTP?.stack) {
      player.tpStackMax += 5
      playerTargetTP.stackMax += 5
      playerTargetTP.stack += 3
    }
    player.emitGameEvent(GameEvents.PLAYER_UPDATE,player.normalize())
  }

  handleEffectDown(player: Player): void {
    const playerTargetTP = player.playerSkillData.get(SkillNames.PECNO_TP)
    player.heal = player.heal - 2
    if (playerTargetTP?.stackMax && playerTargetTP?.stack) {
      player.tpStackMax -= 5
      playerTargetTP.stackMax -= 5
      if (playerTargetTP.stackMax < playerTargetTP.stack) {
        playerTargetTP.stack = playerTargetTP.stackMax
      }
      player.emitGameEvent(GameEvents.PLAYER_UPDATE,player.normalize())
    }
  }

}
