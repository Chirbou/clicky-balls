import { IAlterable } from '../interfaces/IAlterable'
import { IGameObject } from '../interfaces/IGameObject'
import { Alteration, AlterationNames } from './Alteration'
import { Tower } from '../gameobjects/Tower'

export class Mechanotherapist_Buffed extends Alteration {
  stack = 0
  name = AlterationNames.MECHANOTHERAPIST_BUFFED
  BUFF_DAMAGE_AMOUNT = 10

  handleEffectUp(target: IGameObject & IAlterable, source?: IGameObject): void {
    const targetedTower = target as Tower
    targetedTower.damage += this.BUFF_DAMAGE_AMOUNT
  }

  handleEffectDown(target: IGameObject & IAlterable, source?: IGameObject): void {
    const targetedTower = target as Tower
    targetedTower.damage -= this.BUFF_DAMAGE_AMOUNT
  }
}
