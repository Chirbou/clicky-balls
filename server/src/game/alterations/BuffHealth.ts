import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class BuffHealth extends Alteration {
  stack = 0
  name = AlterationNames.BUFF_HEALTH
  buff = 100

  handleEffectUp(player: Player): void {
    player.maxHp = player.maxHp + this.buff
  }

  handleEffectDown(player: Player): void {
    player.maxHp = player.maxHp - this.buff
    if (player.hp > player.maxHp) {
      player.hp = player.maxHp
    }
  }

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
