import { IAlterable } from '../interfaces/IAlterable'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Sucked extends Alteration {
  stack = 0
  name = AlterationNames.SUCKED

  handleEffectUp(player: IAlterable): void {
    
  }

  handleEffectDown(player: IAlterable): void {
    player.suckingPlayers = []
  }

  computeHeal(heal: number, alterable: IHasHealthPoint & IAlterable): number {
    if(alterable.suckingPlayers){
      for (const sucker of alterable.suckingPlayers) {
        sucker.takeHeal(heal)
      }
    }
    return 0
  }

}
