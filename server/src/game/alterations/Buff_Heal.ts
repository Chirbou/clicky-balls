import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Buff_Heal extends Alteration {
  stack = 0
  name = AlterationNames.BUFF_HEAL
  buff = 3

  handleEffectUp(player: Player): void {
    player.heal = player.heal + this.buff
  }
  
  handleEffectDown(player: Player): void {
    player.heal = player.heal - this.buff
  }

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
