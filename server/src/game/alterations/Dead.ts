import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Dead extends Alteration {
  stack = 0
  name = AlterationNames.DEAD

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
