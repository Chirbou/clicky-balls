import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Teleport extends Alteration {
  stack = 0
  name = AlterationNames.TELEPORT
  timeout?: NodeJS.Timeout = undefined

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
