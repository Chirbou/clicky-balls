import { Alteration, AlterationNames } from './Alteration'
import { Invincible } from './Invincible'
import { Martyr } from './Martyr'
import { Sick } from './Sick'
import { Silenced } from './Silenced'
import { Dead } from './Dead'
import { Grabbed } from './Grabbed'
import { Contagious } from './Contagious'
import { Suffer } from './Suffer'
import { Invisible } from './Invisible'
import { BerserkBubble } from './BerserkBubble'
import { PaladinBubble } from './PaladinBubble'
import { Stun } from './Stun'
import { Sucked } from './Sucked'
import { Fucked } from './Fucked'
import { Stealth } from './Stealth'
import { Buff_Atk } from './Buff_Atk'
import { Buff_Heal } from './Buff_Heal'
import { Mechanotherapist_Buffed } from './Mechanotherapist_Buffed'
import { Mechanotherapist_Hacked } from './Mechanotherapist_Hacked'
import { BuffHealth } from './BuffHealth'

export const alterationsDef = new Map<AlterationNames, Alteration>()

alterationsDef.set(AlterationNames.BERSERK_BUBBLE, new BerserkBubble())
alterationsDef.set(AlterationNames.PALADIN_BUBBLE, new PaladinBubble())
alterationsDef.set(AlterationNames.INVINCIBLE, new Invincible())
alterationsDef.set(AlterationNames.MARTYR, new Martyr())
alterationsDef.set(AlterationNames.SICK, new Sick())
alterationsDef.set(AlterationNames.SILENCED, new Silenced())
alterationsDef.set(AlterationNames.DEAD, new Dead())
alterationsDef.set(AlterationNames.STUN, new Stun())
alterationsDef.set(AlterationNames.GRABBED, new Grabbed())
alterationsDef.set(AlterationNames.CONTAGIOUS, new Contagious())
alterationsDef.set(AlterationNames.SUFFER, new Suffer())
alterationsDef.set(AlterationNames.SUCKED, new Sucked())
alterationsDef.set(AlterationNames.FUCKED, new Fucked())
alterationsDef.set(AlterationNames.INVISIBLE, new Invisible())
alterationsDef.set(AlterationNames.STEALTH, new Stealth())
alterationsDef.set(AlterationNames.BUFF_ATK, new Buff_Atk())
alterationsDef.set(AlterationNames.BUFF_HEAL, new Buff_Heal())
alterationsDef.set(AlterationNames.BUFF_HEALTH, new BuffHealth())
alterationsDef.set(AlterationNames.MECHANOTHERAPIST_BUFFED, new Mechanotherapist_Buffed())
alterationsDef.set(AlterationNames.MECHANOTHERAPIST_HACKED, new Mechanotherapist_Hacked())
