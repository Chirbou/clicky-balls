import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Invisible extends Alteration {
  stack = 0
  name = AlterationNames.INVISIBLE

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
