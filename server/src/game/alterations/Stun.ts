import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Stun extends Alteration {
  stack = 0
  name = AlterationNames.STUN

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
