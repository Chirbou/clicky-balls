import { Player } from '../player/Player'
import { GameEvents } from '../GameEventActions'
import { ScoreShiftType } from '../skills/core/Skill'
import { ScoreShiftData } from '../ScoreShiftData'
import { IAlterable } from '../interfaces/IAlterable'
import { IGameObject } from '../interfaces/IGameObject'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'

export interface AlterationData {
  stack: number,
  name: AlterationNames,
  cooldown?: number,
}
export abstract class Alteration {
  abstract stack: number
  abstract name: AlterationNames
  timeout?: NodeJS.Timeout
  cooldown?: number
  source?: IGameObject
  computeDamage(damage: number, player: IHasHealthPoint): number {
    return damage
  }

  computeHeal(heal: number, player: IHasHealthPoint): number {
    return heal
  }

  handleEffectUp(player: IGameObject & IAlterable, source?: IGameObject): void { }
  handleEffectDown(player: IGameObject & IAlterable, source?: IGameObject): void { }

  apply(player: IGameObject & IAlterable, timeoutDuration: number, source?: IGameObject): void {
    if (source) {
      this.source = source
    }

    if (player.disabled) return
    this.cooldown = timeoutDuration
    this.addAlteration(player)

    if (this.timeout) {
      clearTimeout(this.timeout)
    }

    this.timeout = setTimeout(() => {
      this.removeAlteration(player)
    }, timeoutDuration || 0)
  }

  applyForever(player: IGameObject & IAlterable): void {
    if (player.disabled || player.hasAlteration(this.name)) return
    this.addAlteration(player)
  }

  addAlteration(player: IGameObject & IAlterable) {
    const alteration = player.getAlteration(this.name)
    if (alteration && alteration.stack && alteration.stack > 0) {
      alteration.stack++
    } else {
      this.stack = 0
      player.alterations.push(this)
      this.handleEffectUp(player, this.source)
      const room = player.room
      room.emitGameEvent(GameEvents.ALTERATION_ON, { alteration: this.normalize(), playerId: player.id })
    }
  }

  removeAlteration(player: IGameObject & IAlterable) {
    const alteration = player.getAlteration(this.name)
    if (alteration && alteration.stack > 1) {
      alteration.stack--
    } else {
      player.alterations = player.alterations.filter(
        (a) => a.name !== this.name
      )
      this.stack = 0
      this.handleEffectDown(player, this.source)
      if (this.timeout) {
        clearTimeout(this.timeout)
      }
      const room = player.room
      room.emitGameEvent(GameEvents.ALTERATION_OFF, { alteration: this.normalize(), playerId: player.id })
    }
  }

  normalize(): AlterationData {
    return {
      stack: this.stack,
      name: this.name,
      cooldown: this.cooldown,
    }
  }

  handleScoreShiftData( // TODO MERGE WITH SKILL
    playerSource: Player,
    playerTarget: Player | undefined,
    value: number | undefined,
    scoreshiftType: ScoreShiftType,
    isCritic: boolean,
  ) {
    const roomSource = playerSource.room
    const roomTarget = playerTarget?.room

    const eventValue: ScoreShiftData = {
      idSource: playerSource.id,
      idTarget: playerTarget?.id,
      isCritic: isCritic,
      isSuccessful: true,
      scoreSource: playerSource.hp,
      scoreTarget: playerTarget?.hp,
      skill: this.name,
      type: scoreshiftType,
      value: value,
    }

    if (roomTarget && roomSource !== roomTarget) {
      roomSource.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
      roomTarget.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
    } else {
      roomSource.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
    }
  }
}

export enum AlterationNames {
  PALADIN_BUBBLE = 'paladin_bubble',
  BERSERK_BUBBLE = 'berserk_bubble',
  INVINCIBLE = 'invincible',
  SICK = 'sick',
  SILENCED = 'silenced',
  GRABBED = 'grabbed',
  MARTYR = 'martyr',
  SUFFER = 'suffer',
  INVISIBLE = 'invisible',
  TELEPORT = 'teleport',
  STUN = 'stun',
  DEAD = 'dead',
  CONTAGIOUS = 'contagious',
  SUCKER = 'sucker',
  SUCKED = 'sucked',
  FUCKED = 'fucked',
  STEALTH = 'stealth',
  BUFF_ATK = 'buff_atk',
  BUFF_HEAL = 'buff_heal',
  BUFF_HEALTH = 'buff_health',
  MECHANOTHERAPIST_HACKED = 'mechanotherapist_hacked',
  MECHANOTHERAPIST_BUFFED = 'mechanotherapist_buffed'
}
