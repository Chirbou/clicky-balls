import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Grabbed extends Alteration {
  stack = 0
  name = AlterationNames.GRABBED

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
