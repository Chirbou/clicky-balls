import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'
import { ScoreShiftType } from '../skills/core/Skill'

export class Contagious extends Alteration {
  stack = 0
  name = AlterationNames.CONTAGIOUS

  handleEffectUp(player: Player): void {
    player.takeRawDamage(30)
  }

  handleEffectDown(player: Player): void {

    if (player.contagiousPlayer) {
      player.contagiousPlayer.contaminedPlayers = player.contagiousPlayer?.contaminedPlayers.filter(contamined => contamined.id !== player.id)
      player.contagiousPlayer = undefined
    }

  }

  computeDamage(damage: number, player: Player) {
    return damage
  }

}
