import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Buff_Atk extends Alteration {
  stack = 0
  name = AlterationNames.BUFF_ATK
  buff = 3

  handleEffectUp(player: Player): void {
    player.atk = player.atk + this.buff
  }
  
  handleEffectDown(player: Player): void {
    player.atk = player.atk - this.buff
  }

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
