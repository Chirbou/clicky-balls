import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Silenced extends Alteration {
  stack = 0
  name = AlterationNames.SILENCED

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
