import { GameEvents } from '../GameEventActions'
import { NetworkEventHandler } from '../../core/NetworkEventHandler'
import { Player } from '../player/Player'
import { ScoreShiftData } from '../ScoreShiftData'
import { Alteration, AlterationNames } from './Alteration'

export class BerserkBubble extends Alteration {
  stack = 0
  name = AlterationNames.BERSERK_BUBBLE
  atkBuff = 2

  handleEffectUp(player: Player): void {
    player.atk = player.atk + this.atkBuff
  }
  
  handleEffectDown(player: Player): void {
    player.atk = player.atk - this.atkBuff
  }

  computeDamage(damage: number, player: Player) {
    if (player.martyrProtector !== null) {
      const scoreShiftData: ScoreShiftData = {}

      scoreShiftData.idTarget = player.martyrProtector?.id
      scoreShiftData.scoreSource = player?.hp
      scoreShiftData.scoreTarget = player?.martyrProtector?.hp
      scoreShiftData.type = 'damage'
      scoreShiftData.isCritic = false
      scoreShiftData.isSuccessful = true

      const effectiveDamage = player.martyrProtector?.takeRawDamage(Math.round(damage * 0.5))

      scoreShiftData.value = effectiveDamage

      NetworkEventHandler.broadcast(
        GameEvents.SCORE_SHIFT_DATA,
        scoreShiftData
      )
    }
    return Math.max(Math.round(damage * 0.25), 1)
  }
}
