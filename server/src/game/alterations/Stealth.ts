import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Stealth extends Alteration {
  stack = 0
  name = AlterationNames.STEALTH

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
