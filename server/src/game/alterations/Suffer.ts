import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Suffer extends Alteration {
  stack = 0
  name = AlterationNames.SUFFER
  timeout?: NodeJS.Timeout = undefined

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    player.sufferedDamage += damage
    return damage
  }
}
