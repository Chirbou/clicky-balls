import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Sick extends Alteration {
  stack = 0
  name = AlterationNames.SICK

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  apply(player: Player): void {
    throw new Error('Method not implemented.')
  }

  computeDamage(damage: number, player: Player) {
    return damage
  }
}
