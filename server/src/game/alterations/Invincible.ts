import { Player } from '../player/Player'
import { Alteration, AlterationNames } from './Alteration'

export class Invincible extends Alteration {
  stack = 0
  name = AlterationNames.INVINCIBLE

  handleEffectUp(): void {}
  handleEffectDown(): void {}

  computeDamage(damage: number, player: Player) {
    damage = 0
    return damage
  }
}
