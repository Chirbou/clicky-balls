import { AlterationNames } from '../alterations/Alteration'
import { AutoSkill } from './core/AutoSkill'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'

export class RogueDiversion extends AutoSkill {
  name = SkillNames.ROGUE_DIVERSION
  defaultCriticChance = 0.1
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 30000
  defaultTimeout = 8000
  
  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const timeoutDuration = playerSource?.playerSkillData.get(this.name)?.timeoutDuration

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable()
      playerSource?.applyAlteration(AlterationNames.INVISIBLE, timeoutDuration || 0, playerSource)
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
