import { SkillResults } from './core/SkillResults'
import { ScoreShiftType, Skill, SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'

export class PriestHealZone extends Skill {
  name = SkillNames.PRIEST_HEALZONE
  defaultCriticChance = 0.1
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 10000
  aoeRadius = 200

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillCooldown(this.name, () => {
      const playerTargets = this.getAOETargets(playerSource.room, skillInputData.position) as IHasHealthPoint[]

      for (const target of playerTargets) {
        this._handleHeal(playerSource, target)
      }

      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), SkillResults.HIT_PLAYER)
    })
  }

  _handleHeal(playerSource: Player, playerTarget: IHasHealthPoint){
    const heal = playerTarget?.takeHeal(50)
    this.handleScoreShiftData(playerSource, playerTarget, heal, ScoreShiftType.HEAL, false)
  }
}
