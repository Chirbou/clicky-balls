import { AlterationNames } from '../alterations/Alteration'
import { SkillInputProperties } from './core/Skill'
import { AutoSkill } from './core/AutoSkill'
import { SkillNames } from './core/SkillNames'

export class TankInvincibility extends AutoSkill {
  name = SkillNames.TANK_INVINCIBILITY
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 22000
  defaultTimeout = 6000
  defaultCriticChance = 0

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const timeoutDuration = playerSource?.playerSkillData.get(this.name)?.timeoutDuration

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable()
      playerSource?.applyAlteration(AlterationNames.INVINCIBLE, timeoutDuration || 0, playerSource)
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
