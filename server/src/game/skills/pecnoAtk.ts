import { TargetedSkill } from './core/TargetedSkill'
import { SkillInputProperties, ScoreShiftType } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { SkillResults } from './core/SkillResults'
import { Affinity } from '../FactionAffinity'

export class PecnoAtk extends TargetedSkill {
  name = SkillNames.PECNO_ATK
  defaultStack = 5
  defaultUsable = true
  defaultCooldown = 1000
  defaultCriticChance = 0

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const playerTarget = skillInputData.target

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, playerTarget)

      if (outcome.result === SkillResults.HIT_PLAYER) {
        const damage = playerTarget?.takeDamage(playerSource.atk)
        this.handleScoreShiftData(playerSource, playerTarget, damage, ScoreShiftType.DAMAGE, outcome.critic)
        Affinity.updateAffinity(playerSource, (playerTarget as any).faction, false, 1)
      } else if (outcome.result === SkillResults.MISS){
        const damage = playerSource.takeDamage(playerSource.atk)
        this.handleScoreShiftData(playerSource, playerSource, damage, ScoreShiftType.DAMAGE, outcome.critic)
      } else if (outcome.result === SkillResults.HIT_SELF) {
        const heal = playerSource.takeHeal(playerSource.heal)
        this.handleScoreShiftData(playerSource, playerSource, heal, ScoreShiftType.HEAL, outcome.critic)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
