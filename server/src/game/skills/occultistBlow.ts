import { SkillResults } from './core/SkillResults'
import { ScoreShiftType, SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { TargetedSkill } from './core/TargetedSkill'
import { Affinity } from '../FactionAffinity'
import { GameEvents } from '../GameEventActions'
import { IFactionAffinity } from '../interfaces/IFactionAffinity'
import { ClassNames } from '../classes/ClassNames'
import { isPlayer } from '../interfaces/IPlayer'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'

export class OccultistBlow extends TargetedSkill {
  name = SkillNames.OCCULTIST_BLOW
  defaultStack = 2
  defaultUsable = true
  defaultCooldown = 1000
  defaultTimeout = 5000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target) { return }
    const target = skillInputData.target as IHasHealthPoint

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        const amount = Math.round(playerSource.hp*0.20)
        const room = target?.room
        playerSource.takeRawDamage(amount)
        target?.takeHeal(amount)
        room?.emitGameEvent(GameEvents.ROOM_UPDATE, room.normalize())
        this.handleScoreShiftData(playerSource, target, amount, ScoreShiftType.HEAL, outcome.critic)
        this.handleScoreShiftData(playerSource, playerSource, amount, ScoreShiftType.DAMAGE, outcome.critic)
        if(isPlayer(target)){
          const playerTarget = target as Player
        OccultistBlow._handleHealAffinity(playerSource, playerTarget, 10)
        }
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }

  static _handleHealAffinity(playerSource:Player, target: IFactionAffinity, amount: number){
    if (playerSource.currentClass === ClassNames.PRIEST) {
      Affinity.updateAffinity(target, playerSource.faction, true, amount)
    } else {
      Affinity.updateAffinity(playerSource, target.faction, true, amount)
    }
  }
}
