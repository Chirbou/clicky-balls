import { GameEvents } from '../../game/GameEventActions'
import { AlterationNames } from '../alterations/Alteration'
import { IAlterable } from '../interfaces/IAlterable'
import { Skill, SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'

export class DruidStun extends Skill {
  name = SkillNames.DRUID_STUN
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 5000
  defaultTimeout = 3000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const target = skillInputData.target
    const feedback = this.initSkillFeedbackData(skillInputData)
    const timeoutDuration = playerSource?.playerSkillData.get(SkillNames.DRUID_STUN)?.timeoutDuration

    feedback.cooldown = timeoutDuration

    playerSource?.handleSkillCooldown(SkillNames.DRUID_STUN, () => {
      if (target) {
        if ('applyAlteration' in target){
          (target as IAlterable)?.applyAlteration(AlterationNames.SILENCED, timeoutDuration || 0, playerSource)
        }
        
        feedback.skillResults = SkillResults.HIT_PLAYER
        if ('emitGameEvent' in target){
          target?.emitGameEvent(GameEvents.SKILL_FEEDBACK_DATA, feedback)
        }
      } else {
        feedback.skillResults = SkillResults.MISS
      }
      playerSource?.emitGameEvent(GameEvents.SKILL_FEEDBACK_DATA, feedback)
    })
  }
}
