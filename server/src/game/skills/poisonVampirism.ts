import { SkillResults } from './core/SkillResults'
import { ScoreShiftType, SkillInputProperties, SkillOutcome } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Affinity } from '../FactionAffinity'
import { TargetedSkill } from './core/TargetedSkill'
import { Player } from '../player/Player'
import { isPlayer } from '../interfaces/IPlayer'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'

export class PoisonVampirism extends TargetedSkill {
  name = SkillNames.POISON_VAMPIRISM
  defaultCriticChance = 0
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 25000
  iterationCount = 10
  intervalDuration = 1000
  counter = 10

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target) { return }
    const target = skillInputData.target as IHasHealthPoint
    const outcome = this.checkIsAppliable(playerSource, target)

    playerSource?.handleSkillCooldown(this.name, () => {
      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        this._handleVampirismDot(playerSource, target, outcome)
        this.triggerPeriodicEvent(this.iterationCount, this.intervalDuration, () => {
          this._handleVampirismDot(playerSource, target, outcome)
        })
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }

  _handleVampirismDot(playerSource: Player, target: IHasHealthPoint, outcome: SkillOutcome){
    const damage = target?.takeDamage(10)
    playerSource.takeHeal(damage || 0)
    this.handleScoreShiftData(playerSource, target, damage, ScoreShiftType.DAMAGE, outcome.critic)
    this.handleScoreShiftData(playerSource, playerSource, damage, ScoreShiftType.HEAL, outcome.critic)
    if(isPlayer(target)){
      const playerTarget = target as Player
      Affinity.updateAffinity(playerSource, playerTarget.faction, false, 2)
      Affinity.updateAffinity(playerTarget, playerSource.faction, false, 2)
    }
  }
}
