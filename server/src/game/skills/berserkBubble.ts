import { AlterationNames } from '../alterations/Alteration'
import { Affinity } from '../FactionAffinity'
import { SkillInputProperties } from './core/Skill'
import { TargetedSkill } from './core/TargetedSkill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Player } from '../player/Player'
import { isPlayer } from '../interfaces/IPlayer'

export class BerserkBubble extends TargetedSkill {
  name = SkillNames.BERSERK_BUBBLE
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 30000
  defaultTimeout = 10000
  defaultCriticChance = 0.1
  atkBuff = 2

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isPlayer(skillInputData.target)) { return }
    const target = skillInputData.target as Player

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)
      const timeoutDuration = playerSource?.playerSkillData.get(this.name)?.timeoutDuration

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        if (!target.martyrProtector) {
          target?.applyAlteration(AlterationNames.BERSERK_BUBBLE, timeoutDuration || 0, playerSource)
          playerSource?.applyAlteration(AlterationNames.MARTYR, timeoutDuration || 0, playerSource)
          Affinity.updateAffinity(skillInputData.playerSource, target.faction, false, 1)
          target.martyrProtector = playerSource
          setTimeout(() => {
            target.martyrProtector = undefined
          }, timeoutDuration)
        }
        target.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
