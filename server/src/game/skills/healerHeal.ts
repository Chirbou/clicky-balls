import { Player } from '../player/Player'
import { ScoreShiftType, SkillInputProperties } from './core/Skill'
import { TargetedSkill } from './core/TargetedSkill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Affinity } from '../FactionAffinity'
import { IFactionAffinity } from '../interfaces/IFactionAffinity'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { ClassNames } from '../classes/ClassNames'
import { GameObjectType } from '../gameobjects/GameObjectType'

export class HealerHeal extends TargetedSkill {
  name = SkillNames.HEALER_HEAL
  tooltip = 'HEALER_HEAL'
  defaultStack = 3
  defaultUsable = true
  defaultCooldown = 750
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const target = skillInputData.target || playerSource

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)
      const healComputed = this.computeHeal(playerSource, target)
      if (outcome.result === SkillResults.HIT_PLAYER) {
        const heal = target?.takeHeal(outcome.critic ? healComputed * 3 : healComputed)
        this.handleScoreShiftData(playerSource, target, heal, ScoreShiftType.HEAL, outcome.critic)
        HealerHeal._handleHealAffinity(playerSource, target as Player, 1)
      } else if (outcome.result === SkillResults.MISS || outcome.result === SkillResults.HIT_SELF) {
        const heal = playerSource.takeHeal(outcome.critic ? healComputed * 3 : healComputed)
        this.handleScoreShiftData(playerSource, playerSource, heal, ScoreShiftType.HEAL, outcome.critic)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
  computeHeal(playerSource: Player, target: IHasHealthPoint) {
    if (playerSource.currentClass === ClassNames.MECHANOTHERAPIST && (target.type === GameObjectType.TOWER || target.type === GameObjectType.PLAYER_TOWER)) {
      return playerSource.heal + 1
    }
    return playerSource.heal
  }
  static _handleHealAffinity(playerSource: Player, target: IFactionAffinity, amount: number) {
    if (playerSource.currentClass === ClassNames.PRIEST) {
      Affinity.updateAffinity(target, playerSource.faction, true, amount)
    } else {
      Affinity.updateAffinity(playerSource, target.faction, true, amount)
    }
  }
}
