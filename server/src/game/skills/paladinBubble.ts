import { AlterationNames } from '../alterations/Alteration'
import { isPlayer } from '../interfaces/IPlayer'
import { Player } from '../player/Player'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { TargetedSkill } from './core/TargetedSkill'

export class PaladinBubble extends TargetedSkill {
  name = SkillNames.PALADIN_BUBBLE
  tooltip = 'PALADIN_BUBBLE'
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 20000
  defaultTimeout = 10000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isPlayer(skillInputData.target)) { return }
    const target = skillInputData.target as Player

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)
      const timeoutDuration = playerSource?.playerSkillData.get(this.name)?.timeoutDuration

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        target?.applyAlteration(AlterationNames.PALADIN_BUBBLE, timeoutDuration || 0, playerSource)
        target.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
