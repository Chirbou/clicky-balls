import { SkillInputProperties } from './core/Skill'
import { TargetedSkill } from './core/TargetedSkill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Player } from '../player/Player'
import { AlterationNames } from '../alterations/Alteration'
import { isPlayer } from '../interfaces/IPlayer'
export class PriestHangon extends TargetedSkill {
  name = SkillNames.PRIEST_HANGON
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 50000
  defaultTimeout = 10000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isPlayer(skillInputData.target)) { return }
    const target = skillInputData.target as Player

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)

      if (outcome.result === SkillResults.HIT_PLAYER) {
        playerSource?.applyAlteration(AlterationNames.GRABBED, this.defaultTimeout, playerSource)

        playerSource.position = target.position
        if (target.gluedPlayers){
          target.gluedPlayers.push(playerSource)
          target.zIndex+=10
          setTimeout(() => {
            target.gluedPlayers = target.gluedPlayers?.filter((player) => player !== playerSource)
            target.zIndex-=10
          }, this.defaultTimeout)
        }
      }

      playerSource?.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
      target?.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
