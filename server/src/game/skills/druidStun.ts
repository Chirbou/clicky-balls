import { GameEvents } from '../GameEventActions'
import { AlterationNames } from '../alterations/Alteration'
import { Skill, SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Player } from '../player/Player'

export class DruidStun extends Skill {
  name = SkillNames.DRUID_STUN
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 20000
  defaultTimeout = 8000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const target = skillInputData.target
    const feedback = this.initSkillFeedbackData(skillInputData)
    const timeoutDuration = playerSource?.playerSkillData.get(SkillNames.DRUID_STUN)?.timeoutDuration

    feedback.cooldown = timeoutDuration
    
    playerSource?.handleSkillCooldown(SkillNames.DRUID_STUN, () => {
      if (target) {
        feedback.skillResults = SkillResults.HIT_PLAYER

        if ('applyAlteration' in target) {
          (target as Player).applyAlteration(AlterationNames.STUN, timeoutDuration || 0, playerSource)
        }
        
        if ('emitGameEvent' in target){
          target?.emitGameEvent(GameEvents.SKILL_FEEDBACK_DATA, feedback)
        }
      } else {
        feedback.skillResults = SkillResults.MISS
      }
      playerSource?.emitGameEvent(GameEvents.SKILL_FEEDBACK_DATA, feedback)
    })
  }
}
