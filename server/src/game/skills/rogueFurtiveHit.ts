import { SkillInputProperties, ScoreShiftType } from './core/Skill'
import { Player } from '../player/Player'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Affinity } from '../FactionAffinity'
import { TargetedSkill } from './core/TargetedSkill'

export class RogueFurtiveHit extends TargetedSkill {
  name = SkillNames.ROGUE_FURTIVE_HIT
  defaultStack = 3
  defaultUsable = true
  defaultCooldown = 15000
  defaultCriticChance = 0.5

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const playerTarget = skillInputData.target

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, playerTarget)

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        const damage = playerTarget?.takeDamage(
          outcome.critic ? playerSource.atk * 5 : playerSource.atk + 3
        )
        this.handleScoreShiftData(playerSource, playerTarget, damage, ScoreShiftType.DAMAGE, outcome.critic)
        Affinity.updateAffinity(playerSource, (playerTarget as any).faction, false, 1)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
