import { Game } from '../Game'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Mine } from '../gameobjects/Mine'
import { Room } from '../rooms/Room'
import { AutoSkill } from './core/AutoSkill'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'

export class TrapperMine extends AutoSkill {
  name = SkillNames.TRAPPER_MINE
  defaultCriticChance = 0
  defaultStack = 3
  defaultUsable = true
  defaultCooldown = 15000
  defaultTimeout = Infinity

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillCooldown(this.name, () => {
      const position = playerSource.position
      const room = playerSource.room

      const newMine = new Mine(room,playerSource.faction,position,playerSource.id)
      playerSource.inventory.push(newMine)
      const minesOfPlayerSource = playerSource.inventory.filter((o)=> o.type === GameObjectType.MINE)

      if(minesOfPlayerSource.length > 3) {
        const mineToTrigger = minesOfPlayerSource.shift() as Mine
        if(mineToTrigger) {
          playerSource.inventory = playerSource.inventory.filter(o => o.id !== mineToTrigger.id)
          mineToTrigger.trigger(playerSource)
        }
      }
      room.emitGameEvent(GameEvents.ROOM_UPDATE,room.normalize())
      const outcome = this.checkIsAppliable()
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
