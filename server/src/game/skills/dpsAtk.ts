import { SkillInputProperties, ScoreShiftType } from './core/Skill'
import { Player } from '../player/Player'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Affinity } from '../FactionAffinity'
import { TargetedSkill } from './core/TargetedSkill'
import { IFactionAffinity } from '../interfaces/IFactionAffinity'

export class DpsAtk extends TargetedSkill {
  name = SkillNames.DPS_ATK
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 8000
  defaultCriticChance = 0.75

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const playerTarget = skillInputData.target

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, playerTarget)

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        const damage = playerTarget?.takeDamage(
          outcome.critic ? playerSource.atk * 15 : playerSource.atk * 5
        )
        this.handleScoreShiftData(playerSource, playerTarget, damage, ScoreShiftType.DAMAGE, outcome.critic)
        Affinity.updateAffinity(playerSource, (playerTarget as any).faction, false, 1)
      } else if (outcome.result === SkillResults.MISS){
        const damage = playerSource?.takeDamage(
          outcome.critic ? playerSource.atk * 10 : playerSource.atk * 3
        )
        this.handleScoreShiftData(playerSource, playerSource, damage, ScoreShiftType.DAMAGE, outcome.critic)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
