import { AlterationNames } from '../alterations/Alteration'
import { Affinity } from '../FactionAffinity'
import { SkillInputProperties } from './core/Skill'
import { TargetedSkill } from './core/TargetedSkill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { isAlterable, IAlterable } from '../interfaces/IAlterable'
import { IGameObject } from '../interfaces/IGameObject'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IFactionAffinity } from '../interfaces/IFactionAffinity'

export class ToggleSilence extends TargetedSkill {
  name = SkillNames.TOGGLE_SILENCE
  defaultStack = 42
  defaultUsable = true
  defaultCooldown = 15000
  defaultTimeout = 30000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isAlterable(skillInputData.target)) { return }
    const target = skillInputData.target as IGameObject & IHasHealthPoint & IAlterable & IFactionAffinity

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)
      const timeoutDuration = playerSource?.playerSkillData.get(this.name)?.timeoutDuration

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        if (playerSource !== target){
          Affinity.updateAffinity(playerSource, target.faction, false, 1)
          if(target.hasAlteration(AlterationNames.SILENCED)){
            target.removeAlteration(AlterationNames.SILENCED)
          }else{
            target?.applyAlteration(AlterationNames.SILENCED, timeoutDuration || 0)
          }
        }
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
