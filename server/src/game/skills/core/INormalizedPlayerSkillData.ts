import { SkillNames } from './SkillNames'

export interface INormalizedPlayerSkillData {
  skillName: SkillNames
  stack: number
  usable: boolean
  stackMax: number
  cooldownDuration: number
  timeoutDuration?: number
}
