import { Skill } from './Skill'
import { BerserkBubble } from '../berserkBubble'
import { DpsAtk } from '../dpsAtk'
import { HealerHeal } from '../healerHeal'
import { PecnoAtk } from '../pecnoAtk'
import { PecnoTp } from '../pecnoTp'
import { PaladinBubble } from '../paladinBubble'
import { PoisonVampirism } from '../poisonVampirism'
import { PoisonBombkipu } from '../poisonBomkipu'
import { PestiferousContagion } from '../pestiferousContagion'
import { PestiferousDeflagration } from '../pestiferousDeflagration'
import { PestiferousDispel } from '../pestiferousDispel'
import { SkillNames } from './SkillNames'
import { TankInvincibility } from '../tankInvincibility'
import { TankSilence } from '../tankSilence'
import { ToggleSilence } from '../toggleSilence'
import { PriestHealZone } from '../priestHealZone'
import { PriestDispel } from '../priestDispel'
import { PriestHangon } from '../priestHangon'
import { PaladinGrab } from '../paladinGrab'
import { DruidStun } from '../druidStun'
import { DruidHotaoe } from '../druidHotaoe'
import { BerserkSuffer } from '../berserkSuffer'
import { RogueStab } from '../rogueStab'
import { RogueDiversion } from '../rogueDiversion'
import { PoisonFang } from '../poisonFang'
import { RogueFurtiveHit } from '../rogueFurtiveHit'
import { OccultistSucker } from '../occultistSucker'
import { OccultistBlow } from '../occultistBlow'
import { OccultistFucker } from '../occultistFucker'
import { TrapperStealth } from '../trapperStealth'
import { TrapperSniper } from '../trapperSniper'
import { TrapperMine } from '../trapperMine'
import { ShamanTotem } from '../shamanTotem'
import { ShamanUnderpressure } from '../shamanUnderpressure'
import { MechanotherapistTowerBuild } from '../mechanotherapistTowerBuild'
import { MechanotherapistTowerBuff } from '../mechanotherapistTowerBuff'
import { MechanotherapistTowerHack } from '../mechanotherapistTowerHack'
import { BerserkShell } from '../berserkShell'
import { ShamanThunderbolt } from '../shamanThunderbolt'

export const skillsDef = new Map<SkillNames, Skill>()
skillsDef.set(SkillNames.PECNO_ATK, new PecnoAtk())
skillsDef.set(SkillNames.PECNO_TP, new PecnoTp())
skillsDef.set(SkillNames.POISON_VAMPIRISM, new PoisonVampirism())
skillsDef.set(SkillNames.POISON_FANG, new PoisonFang())
skillsDef.set(SkillNames.POISON_BOMBKIPU, new PoisonBombkipu())
skillsDef.set(SkillNames.PESTIFEROUS_CONTAGION, new PestiferousContagion())
skillsDef.set(SkillNames.PESTIFEROUS_DEFLAGRATION, new PestiferousDeflagration())
skillsDef.set(SkillNames.PESTIFEROUS_DISPEL, new PestiferousDispel())
skillsDef.set(SkillNames.DPS_ATK, new DpsAtk())
skillsDef.set(SkillNames.BERSERK_BUBBLE, new BerserkBubble())
skillsDef.set(SkillNames.BERSERK_SHELL, new BerserkShell())
skillsDef.set(SkillNames.PALADIN_BUBBLE, new PaladinBubble())
skillsDef.set(SkillNames.HEALER_HEAL, new HealerHeal())
skillsDef.set(SkillNames.TANK_INVINCIBILITY, new TankInvincibility())
skillsDef.set(SkillNames.MECHANOTHERAPIST_TOWER_BUFF, new MechanotherapistTowerBuff())
skillsDef.set(SkillNames.MECHANOTHERAPIST_TOWER_BUILD, new MechanotherapistTowerBuild())
skillsDef.set(SkillNames.MECHANOTHERAPIST_TOWER_HACK, new MechanotherapistTowerHack())
skillsDef.set(SkillNames.TANK_SILENCE, new TankSilence())
skillsDef.set(SkillNames.TOGGLE_SILENCE, new ToggleSilence())
skillsDef.set(SkillNames.PRIEST_HEALZONE, new PriestHealZone())
skillsDef.set(SkillNames.PRIEST_HANGON, new PriestHangon())
skillsDef.set(SkillNames.PRIEST_DISPEL, new PriestDispel())
skillsDef.set(SkillNames.PALADIN_GRAB, new PaladinGrab())
skillsDef.set(SkillNames.DRUID_STUN, new DruidStun())
skillsDef.set(SkillNames.DRUID_HOTAOE, new DruidHotaoe())
skillsDef.set(SkillNames.BERSERK_SUFFER, new BerserkSuffer())
skillsDef.set(SkillNames.ROGUE_DIVERSION, new RogueDiversion())
skillsDef.set(SkillNames.ROGUE_STAB, new RogueStab())
skillsDef.set(SkillNames.ROGUE_FURTIVE_HIT, new RogueFurtiveHit())
skillsDef.set(SkillNames.OCCULTIST_SUCKER, new OccultistSucker())
skillsDef.set(SkillNames.OCCULTIST_BLOW, new OccultistBlow())
skillsDef.set(SkillNames.OCCULTIST_FUCKER, new OccultistFucker())
skillsDef.set(SkillNames.TRAPPER_STEALTH, new TrapperStealth())
skillsDef.set(SkillNames.TRAPPER_SNIPER, new TrapperSniper())
skillsDef.set(SkillNames.TRAPPER_MINE, new TrapperMine())
skillsDef.set(SkillNames.SHAMAN_TOTEM, new ShamanTotem())
skillsDef.set(SkillNames.SHAMAN_UNDERPRESSURE, new ShamanUnderpressure())
skillsDef.set(SkillNames.SHAMAN_THUNDERBOLT, new ShamanThunderbolt())
