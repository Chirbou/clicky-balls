export enum SkillResults {
  HIT_SELF,
  HIT_PLAYER,
  MISS,
  CANNOT
}