import { SkillNames } from './SkillNames'
import { INormalizedPlayerSkillData } from './INormalizedPlayerSkillData'
export class PlayerSkillData {
  skillName: SkillNames
  stack: number
  usable: boolean
  stackMax = -1
  cooldownDuration = -1
  timeoutDuration?: number = -1
  timeout?: NodeJS.Timeout

  constructor(
    skillName: SkillNames,
    stack = 1,
    usable = true,
    cooldownDuration = -1,
    timeoutDuration = -1
  ) {
    this.stack = stack
    this.usable = usable
    this.cooldownDuration = cooldownDuration
    this.timeoutDuration = timeoutDuration
    this.stackMax = stack
    this.skillName = skillName
  }

  normalize(): INormalizedPlayerSkillData {
    return {
      skillName: this.skillName,
      stack: this.stack,
      usable: this.usable,
      stackMax: this.stackMax,
      cooldownDuration: this.cooldownDuration,
      timeoutDuration: this.timeoutDuration
    }
  }

  consumeStack() {
    this.stack--
  }

  restoreStack() {
    this.stack++
  }

  enable() {
    this.usable = true
  }

  disable() {
    this.usable = false
  }
}
