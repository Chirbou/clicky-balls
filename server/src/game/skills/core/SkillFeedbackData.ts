import { SkillNames } from './SkillNames'
import { SkillResults } from './SkillResults'
import { Coordinate } from '../../../core/Utils'
import { SkillOutputProperties } from './Skill'

export interface SkillFeedbackData {
  idSource?: string
  idTarget?: string
  cooldown?: number
  skillName: SkillNames
  skillResults?: SkillResults
  position?: Coordinate
  skillOutput?: SkillOutputProperties
  gameZoneWidth: number
  gameZoneHeight: number
  aoeRadius?: number
}
