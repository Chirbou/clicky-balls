import { Skill, SkillOutcome } from './Skill'
import { SkillResults } from './SkillResults'
import { Player } from '../../player/Player'
import { IGameObject } from '../../interfaces/IGameObject'
import { IHasHealthPoint } from '../../interfaces/IHasHealthPoint'

export abstract class TargetedSkill extends Skill {
  checkIsAppliable(playerSource: Player, target: IGameObject & IHasHealthPoint | undefined): SkillOutcome {
    if (!target || !playerSource) {
      return { critic: false, result: SkillResults.MISS }
    } else if (target === playerSource) {
      return { critic: this.isCritic(), result: SkillResults.HIT_SELF }
    }
    else {
      return { critic: this.isCritic(), result: SkillResults.HIT_PLAYER }
    }
  }
}