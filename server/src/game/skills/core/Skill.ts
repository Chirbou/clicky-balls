import { Player } from '../../player/Player'
import { Coordinate, Utils } from '../../../core/Utils'
import { SkillNames } from './SkillNames'
import { SkillFeedbackData } from './SkillFeedbackData'
import { ScoreShiftData } from '../../ScoreShiftData'
import { Room } from '../../rooms/Room'
import { Game } from '../../Game'
import { GameEvents } from '../../GameEventActions'
import { SkillResults } from './SkillResults'
import { IGameObject } from '../../interfaces/IGameObject'
import { IHasHealthPoint } from '../../interfaces/IHasHealthPoint'
import { IAlterable, isAlterable } from '../../interfaces/IAlterable'
import { IFactionAffinity, isFactionnable } from '../../interfaces/IFactionAffinity'

export interface SkillInputProperties {
  playerSource: Player
  target?: IGameObject & IHasHealthPoint
  position: Coordinate
  room?: Room
}

export interface SkillOutputProperties {
  position?: Coordinate
  aoeRadius?: number
  timeoutDuration?: number
}

export enum ScoreShiftType {
  HEAL = 'heal',
  DAMAGE = 'damage',
}

export interface SkillOutcome {
  result: SkillResults,
  critic: boolean,
}

export abstract class Skill {
  abstract name: SkillNames
  abstract defaultCriticChance: number
  abstract defaultStack: number
  abstract defaultUsable: boolean
  abstract defaultCooldown: number
  aoeRadius = 0
  defaultTimeout?: number

  abstract trigger(data: SkillInputProperties): void

  initSkillOutputData(skillInputProperties: SkillInputProperties): SkillOutputProperties {
    return {
      position: skillInputProperties.position,
    }
  }

  initSkillFeedbackData(skillInputProperties: SkillInputProperties): SkillFeedbackData {
    return {
      skillName: this.name,
      position: skillInputProperties.position,
      idTarget: skillInputProperties.target?.id,
      idSource: skillInputProperties.playerSource?.id,
      gameZoneHeight: Game.GAMEZONE_HEIGHT,
      gameZoneWidth: Game.GAMEZONE_WIDTH,
      cooldown: this.defaultTimeout,
      aoeRadius: this.aoeRadius,
    }
  }

  initScoreShiftData(skillInputProperties: SkillInputProperties): ScoreShiftData {
    return {
      skill: this.name,
      idTarget: skillInputProperties.target?.id,
      idSource: skillInputProperties.playerSource?.id,
    }
  }

  checkPlayerPosition(player: Player, position: Coordinate): boolean {
    return Utils.isInSquare(player.position, player.size, position)
  }

  isCritic(): boolean{
    return this.defaultCriticChance > Math.random()
  }

  handleScoreShiftData(
    playerSource: Player,
    target: IGameObject & IHasHealthPoint | undefined,
    value: number | undefined,
    scoreshiftType: ScoreShiftType,
    isCritic: boolean,
  ) {
    const roomSource = playerSource.room
    const roomTarget = target ? target.room : roomSource

    const eventValue: ScoreShiftData = {
      idSource: playerSource.id,
      idTarget: target?.id,
      isCritic: isCritic,
      isSuccessful: true,
      scoreSource: playerSource.hp,
      scoreTarget: target?.hp,
      skill: this.name,
      type: scoreshiftType,
      value: value,
    }

    if (roomTarget && roomSource !== roomTarget){
      roomSource.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
      roomTarget.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
    } else {
      roomSource.emitGameEvent(GameEvents.SCORE_SHIFT_DATA, eventValue)
    }
  }

  triggerPeriodicEvent(iterationCount: number, intervalDuration: number, callback: () => void) {
    let currentIteration = 0
    const interval = setInterval(() => {
      callback()
      currentIteration++
      if (currentIteration > iterationCount) {
        clearInterval(interval)
      }
    }, intervalDuration)
  }

  getAOETargets(room: Room, position: Coordinate): IGameObject[] {
    const aoeRadius = this.aoeRadius
    const players = room.players        
    const gameObjects = room.getHealthObjects()

    const hitByAOEGameObjects = []
    for (const player of players) {
      if (Utils.isInRadius(position, player.position, aoeRadius)) {
        hitByAOEGameObjects.push(player)
      }
    }
    for (const gameObject of gameObjects) {
      if (Utils.isInRadius(position, gameObject.position, aoeRadius)) {
        hitByAOEGameObjects.push(gameObject)
      }
    }
    return hitByAOEGameObjects
  }

  
  getAOEAlterablesTargets(room: Room, position: Coordinate): IAlterable[] {
    const aoeRadius = this.aoeRadius
    const players = room.players        
    const gameObjects = room.getHealthObjects()

    const hitByAOEGameObjects = []
    for (const player of players) {
      if (Utils.isInRadius(position, player.position, aoeRadius)) {
        hitByAOEGameObjects.push(player)
      }
    }
    for (const gameObject of gameObjects) {
      if (Utils.isInRadius(position, gameObject.position, aoeRadius) && isAlterable(gameObject)) {
        hitByAOEGameObjects.push(gameObject as IAlterable)
      }
    }
    return hitByAOEGameObjects
  }
  
}
