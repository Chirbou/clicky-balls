import { Skill, SkillOutcome } from './Skill'
import { SkillResults } from './SkillResults'

export abstract class AutoSkill extends Skill {
  checkIsAppliable(): SkillOutcome {
    return { critic: this.isCritic(), result: SkillResults.HIT_SELF }
  }
}