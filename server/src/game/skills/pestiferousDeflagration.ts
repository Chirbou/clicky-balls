import { SkillResults } from './core/SkillResults'
import {
  ScoreShiftType,
  Skill,
  SkillInputProperties,
} from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { AlterationNames } from '../alterations/Alteration'
import { AutoSkill } from './core/AutoSkill'

export class PestiferousDeflagration extends AutoSkill {
  name = SkillNames.PESTIFEROUS_DEFLAGRATION
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 7000
  defaultTimeout = 6000
  defaultCriticChance = 0

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const targets = playerSource.contaminedPlayers
    const damage = playerSource.atk * 20

    playerSource?.handleSkillCooldown(this.name, () => {
          targets.forEach(target => {
            target.takeDamage(damage)
            skillInputData.position = target.position
            this.handleScoreShiftData(playerSource, target, damage, ScoreShiftType.DAMAGE, true)
          })
      const outcome = this.checkIsAppliable()
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }

}
