import { SkillResults } from './core/SkillResults'
import {
  ScoreShiftType,
  Skill,
  SkillInputProperties,
} from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'

export class PoisonBombkipu extends Skill {
  name = SkillNames.POISON_BOMBKIPU
  defaultCriticChance = 0.1
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 15000
  aoeRadius = 200
  iterationCount = 12
  intervalDuration = 1000

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillCooldown(this.name, () => {
      const playerTargets = this.getAOETargets(playerSource.room, skillInputData.position) as IHasHealthPoint[]

      for (const target of playerTargets) {
        this._handleDamageDot(playerSource, target)
        this.triggerPeriodicEvent(this.iterationCount, this.intervalDuration, () => {
          this._handleDamageDot(playerSource, target)
        })
      }

      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), SkillResults.HIT_PLAYER)
    })
  }

  _handleDamageDot(playerSource: Player, target: IHasHealthPoint){
    const damage = target?.takeDamage(5)
    this.handleScoreShiftData(playerSource, target, damage, ScoreShiftType.DAMAGE, false)
  }
}
