import { Player } from '../player/Player'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { TargetedSkill } from './core/TargetedSkill'
import { AlterationNames } from '../alterations/Alteration'
import { isPlayer } from '../interfaces/IPlayer'

export class PaladinGrab extends TargetedSkill {
  name = SkillNames.PALADIN_GRAB
  defaultStack = 2
  defaultUsable = true
  defaultCooldown = 5000
  defaultTimeout = 12000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isPlayer(skillInputData.target)) { return }
    const target = skillInputData.target as Player

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)

      if (outcome.result === SkillResults.HIT_PLAYER) {
        target?.applyAlteration(AlterationNames.GRABBED, this.defaultTimeout, playerSource)
        target.position = playerSource.position
        if (playerSource.gluedPlayers){
          playerSource.gluedPlayers.push(target)
          setTimeout(() => {
            playerSource.gluedPlayers = playerSource.gluedPlayers?.filter((player: Player) => player !== target)
          }, this.defaultTimeout)
        }
      }

      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
      target?.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
