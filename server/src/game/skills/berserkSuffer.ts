import { AlterationNames } from '../alterations/Alteration'
import { ScoreShiftType, SkillInputProperties } from './core/Skill'
import { AutoSkill } from './core/AutoSkill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Game } from '../Game'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IPlayer } from '../interfaces/IPlayer'
import { Player } from '../player/Player'

export class BerserkSuffer extends AutoSkill {
  name = SkillNames.BERSERK_SUFFER
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 20000
  defaultTimeout = 10000
  SUFFER_MULTIPLICATOR = 1.2
  defaultCriticChance = 0.1
  aoeRadius = 400
  baseDamage = 60

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const timeoutDuration = playerSource?.playerSkillData.get(SkillNames.BERSERK_SUFFER)?.timeoutDuration

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable()

      playerSource?.applyAlteration(AlterationNames.SUFFER, timeoutDuration || 0, playerSource)

      setTimeout(() => {
        const sourcePlayerPosition = playerSource.position
        const targets = this.getAOETargets(playerSource.room, playerSource.position) as IHasHealthPoint[]
        let sufferReleasedDamage = ((playerSource.sufferedDamage || 0) * this.SUFFER_MULTIPLICATOR) + this.baseDamage
        playerSource.sufferedDamage = 0

        for (const target of targets) {
          if (target.id === playerSource?.id) {
            continue
          }
          sufferReleasedDamage = target.takeDamage(sufferReleasedDamage)
          if (target instanceof Player) {

            target.handleSkillFeedback({
              ...this.initSkillFeedbackData(skillInputData), position: sourcePlayerPosition
            }, SkillResults.HIT_PLAYER)
          }
          this.handleScoreShiftData(playerSource, target, sufferReleasedDamage, ScoreShiftType.DAMAGE, outcome.critic)
        }
        playerSource.handleSkillFeedback({
          ...this.initSkillFeedbackData(skillInputData), position: sourcePlayerPosition
        }, SkillResults.HIT_PLAYER)

      }, timeoutDuration || 0)

      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
