import { SkillInputProperties, ScoreShiftType } from './core/Skill'
import { Player } from '../player/Player'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { Affinity } from '../FactionAffinity'
import { TargetedSkill } from './core/TargetedSkill'
import { IFactionAffinity } from '../interfaces/IFactionAffinity'
import { Room } from '../rooms/Room'
import { AlterationNames } from '../alterations/Alteration'
import { Utils } from '../../core/Utils'
import { IGameObject } from '../interfaces/IGameObject'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { FactionNames } from '../FactionNames'
import { isPlayer } from '../interfaces/IPlayer'


export class ShamanThunderbolt extends TargetedSkill {
  name = SkillNames.SHAMAN_THUNDERBOLT
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 12000
  defaultCriticChance = 0.75
  maxTargetCount = 3


  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const playerTarget = skillInputData.target


    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, playerTarget)
      let sparkEmmiter = playerSource
      let sparkRecever: IHasHealthPoint | undefined = playerTarget as IHasHealthPoint
      let roomPlayers = this.getTargetableRoomPlayers(skillInputData.playerSource)

      //Si la cible n'est pas un player : alors lui burst la gueule et ne pas faire de rebond
      if(!isPlayer(sparkRecever)){
        for(let i = this.maxTargetCount; i > 0; i--){
          const damage = sparkRecever?.takeDamage(
            outcome.critic ? playerSource.atk * 20 : playerSource.atk * 15
          )
  
          this.handleScoreShiftData(sparkEmmiter, sparkRecever, damage, ScoreShiftType.DAMAGE, outcome.critic)
  
        }
        return
      }


      if (outcome.result === SkillResults.MISS) { sparkRecever = playerSource }

      for (let i = this.maxTargetCount; i > 0; i--) {

        const damage = sparkRecever?.takeDamage(
          outcome.critic ? playerSource.atk * 20 : playerSource.atk * 15
        )

        this.handleScoreShiftData(sparkEmmiter, sparkRecever, damage, ScoreShiftType.DAMAGE, outcome.critic)

        skillInputData.target = sparkRecever
        if (playerSource.id === playerTarget?.id) {
          outcome.result = SkillResults.HIT_SELF
        } else {
          outcome.result = SkillResults.HIT_PLAYER
        }
        sparkEmmiter.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)

        roomPlayers = roomPlayers.filter(p => p.id !== sparkRecever?.id)

        sparkEmmiter = sparkRecever as Player
        // on recherche la prochaine cible à Sparked ou on arrête
        if (roomPlayers.length === 0) {
          i = 0
        }
        if (roomPlayers.length >= 1) {
          sparkRecever = this.getNearestPlayerByFaction(playerSource.faction, roomPlayers, sparkEmmiter, false)
          if (!sparkRecever) {
            sparkRecever = this.getNearestPlayerByFaction(playerSource.faction, roomPlayers, sparkEmmiter, true)
          }
        }

      }
    })
  }
  getTargetableRoomPlayers(playerSource: Player) {
    const roomPlayers = playerSource.room.players.filter((roomPlayer) => {
      return (
        !roomPlayer.hasAlteration(AlterationNames.DEAD) &&
        roomPlayer.id !== playerSource.id
      )
    })
    return roomPlayers
  }
  getNearestPlayerByFaction(faction: FactionNames, roomPlayers: Player[], player: Player, lookingForAlly: boolean) {
    let nearestPlayer: Player | undefined = undefined
  let nearest: number | undefined = undefined
  for (const roomPlayer of roomPlayers) {

    const currentPlayerDistance = Utils.getDistance(player.position, roomPlayer.position)

    if (lookingForAlly) {
      if (
        (!nearest || currentPlayerDistance < nearest)
        && roomPlayer.faction === faction
      ) {
        nearest = currentPlayerDistance
        nearestPlayer = roomPlayer
      }
    } else {
      if ((!nearest || currentPlayerDistance < nearest) && roomPlayer.faction !== faction) {
        nearest = currentPlayerDistance
        nearestPlayer = roomPlayer
      }
    }
  }
  return nearestPlayer
}

}
