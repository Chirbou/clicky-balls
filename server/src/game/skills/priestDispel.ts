import { SkillResults } from './core/SkillResults'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { TargetedSkill } from './core/TargetedSkill'
import { AlterationNames } from '../alterations/Alteration'
import { IAlterable, isAlterable } from '../interfaces/IAlterable'
import { IGameObject } from '../interfaces/IGameObject'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'

export class PriestDispel extends TargetedSkill {
  name = SkillNames.PRIEST_DISPEL
  defaultStack = 2
  defaultUsable = true
  defaultCooldown = 1000
  defaultTimeout = 5000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isAlterable(skillInputData.target)) { return }
    const target = skillInputData.target as IGameObject & IHasHealthPoint & IAlterable

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        target?.alterations.map((alteration)=>{
          if(alteration.name!==AlterationNames.BERSERK_BUBBLE)
            alteration.removeAlteration(target)
          if(alteration.name===AlterationNames.INVINCIBLE){
            target.takeRawDamage(50)
          }
        })
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
