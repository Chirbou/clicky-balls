import { SkillResults } from './core/SkillResults'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { TargetedSkill } from './core/TargetedSkill'
import { AlterationNames } from '../alterations/Alteration'
import { isPlayer } from '../interfaces/IPlayer'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IAlterable, isAlterable } from '../interfaces/IAlterable'

export class OccultistSucker extends TargetedSkill {
  name = SkillNames.OCCULTIST_SUCKER
  defaultStack = 2
  defaultUsable = true
  defaultCooldown = 1000
  defaultTimeout = 5000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isAlterable(skillInputData.target)) { return }
    const target = skillInputData.target as IHasHealthPoint & IAlterable
    if (target.hasAlteration(AlterationNames.SUCKED)) { return }
    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)
      if (outcome.result === SkillResults.HIT_PLAYER) {
        target.suckingPlayers?.push(playerSource)
        target.applyAlteration(AlterationNames.SUCKED, 10000, playerSource)
        playerSource.applyAlteration(AlterationNames.SUCKER, 10000, playerSource)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
