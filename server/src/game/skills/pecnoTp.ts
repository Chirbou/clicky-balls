import { SkillInputProperties } from './core/Skill'
import { AutoSkill } from './core/AutoSkill'
import { SkillNames } from './core/SkillNames'
import { GameEvents } from '../GameEventActions'
import { Game } from '../Game'
import { AlterationNames } from '../alterations/Alteration'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Utils } from '../../core/Utils'
import { PortalPositions } from '../rooms/PortalPositions'

export const TRIGGERABLE_GAME_OBJECTS = [
  GameObjectType.TOWER,
  GameObjectType.POWER_UP,
  GameObjectType.FLAG,
  GameObjectType.PORTAL,
  GameObjectType.FLAGBASE,
  GameObjectType.GOLD,
  GameObjectType.GOLD_DEPOSIT,
  GameObjectType.GOLD_MINE,
  GameObjectType.MINE,
]

export class PecnoTp extends AutoSkill {
  name = SkillNames.PECNO_TP
  defaultCriticChance = 0.1
  defaultStack = 5
  defaultUsable = true
  defaultCooldown = 5000

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (playerSource.hasAlteration(AlterationNames.STUN)) {
      return
    }

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable()

      const gluedPlayers = skillInputData.playerSource?.gluedPlayers
      const position = skillInputData.position
      playerSource.position = position

      if (gluedPlayers && position) {
        for (const gluedPlayer of gluedPlayers) {
          //TODO : Si le gluedplayer est dans la même room
          if (gluedPlayer.roomId === playerSource.roomId) {
            gluedPlayer.position = (skillInputData.position)
            gluedPlayer.room.emitGameEvent(GameEvents.PLAYER_UPDATE, gluedPlayer.normalize())
          }
        }
      }
      if (skillInputData.playerSource && skillInputData.position) {
        skillInputData.playerSource.position = skillInputData.position
      }

      const room = playerSource.room

      for (const triggerableGameObjectType of TRIGGERABLE_GAME_OBJECTS) {
        const gameObjects = room.gameObjects.get(triggerableGameObjectType)
        gameObjects?.forEach(gameObject => {
          if (Utils.isColliding(gameObject, playerSource)) {
            gameObject.trigger(playerSource)
          }
        })
      }

      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)

    })
  }

}
