import { SkillResults } from './core/SkillResults'
import { SkillInputProperties, ScoreShiftType } from './core/Skill'
import { TargetedSkill } from './core/TargetedSkill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'

export class DruidHotaoe extends TargetedSkill {
  name = SkillNames.DRUID_HOTAOE
  defaultCriticChance = 0.1
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 10000
  aoeRadius = 200
  iterationCount = 10
  intervalDuration = 1000

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillCooldown(this.name, () => {
      const playerTargets = this.getAOETargets(playerSource.room, skillInputData.position) as IHasHealthPoint[]

      for (const target of playerTargets) {
        this._handleHealDot(playerSource, target)
        this.triggerPeriodicEvent(this.iterationCount, this.intervalDuration, () => {
          this._handleHealDot(playerSource, target)
        })
      }

      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), SkillResults.HIT_PLAYER)
    })
  }

  _handleHealDot(playerSource: Player, target: IHasHealthPoint){
    const heal = target?.takeHeal(5)
    this.handleScoreShiftData(playerSource, target, heal, ScoreShiftType.HEAL, false)
  }
}
