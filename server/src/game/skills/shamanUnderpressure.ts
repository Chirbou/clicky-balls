import { SkillResults } from './core/SkillResults'
import { ScoreShiftType, SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { AlterationNames } from '../alterations/Alteration'
import { GameEvents } from '../GameEventActions'
import { AutoSkill } from './core/AutoSkill'
import { Affinity } from '../FactionAffinity'
import { IAlterable, isAlterable } from '../interfaces/IAlterable'
import { IFactionAffinity, isFactionnable } from '../interfaces/IFactionAffinity'
import { isAnHealthGameObject } from '../interfaces/IHasHealthPoint'

export class ShamanUnderpressure extends AutoSkill {
  name = SkillNames.SHAMAN_UNDERPRESSURE
  defaultCriticChance = 0.1
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 10000
  aoeRadius = 200
  timeoutDuration = 10000
  damage = 25

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    playerSource?.handleSkillCooldown(this.name, () => {
      const targets = this.getAOEAlterablesTargets(playerSource.room, playerSource.position)
      // TODO : Degats si Health, Stun si alterable, Faction affinity si faction affinty

      for (const target of targets) {
        console.log(target.type)
        this.handleEffect(playerSource, target, skillInputData)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), SkillResults.HIT_PLAYER)
    })
  }

  handleEffect(playerSource: Player, target: IAlterable, skillInputData: SkillInputProperties) {

    if (isFactionnable(target) && isAlterable(target)) {
      if (playerSource.faction !== target.faction) {
        target.emitGameEvent(GameEvents.PLAYER_UPDATE, target.normalize())
        target.applyAlteration(AlterationNames.STUN, this.timeoutDuration || 0, playerSource)
        Affinity.updateAffinity(playerSource, target.faction, false, 1)
      }
    }

    target.takeDamage(this.damage)
    this.handleScoreShiftData(playerSource, target, this.damage, ScoreShiftType.DAMAGE, true)

    if ('emitGameEvent' in target) {
      const feedback = this.initSkillFeedbackData(skillInputData)
      feedback.skillResults = SkillResults.HIT_PLAYER
      target?.emitGameEvent(GameEvents.SKILL_FEEDBACK_DATA, feedback)
    }

  }
}
