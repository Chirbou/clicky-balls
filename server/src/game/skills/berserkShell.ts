import { AlterationNames } from '../alterations/Alteration'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { AutoSkill } from './core/AutoSkill'

export class BerserkShell extends AutoSkill {
  name = SkillNames.BERSERK_SHELL
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 30000
  defaultTimeout = 10000
  defaultCriticChance = 0.1
  atkBuff = 2

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const timeoutDuration = playerSource?.playerSkillData.get(this.name)?.timeoutDuration

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable()
      playerSource?.applyAlteration(AlterationNames.BUFF_HEALTH, timeoutDuration || 0, playerSource)
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })

  }
}
