import { SkillResults } from './core/SkillResults'
import { SkillInputProperties, ScoreShiftType } from './core/Skill'
import { TargetedSkill } from './core/TargetedSkill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IAlterable } from '../interfaces/IAlterable'
import { isAlterable } from '../interfaces/IAlterable'
import { IGameObject } from '../interfaces/IGameObject'

export class PestiferousDispel extends TargetedSkill {
  name = SkillNames.PESTIFEROUS_DISPEL
  defaultCriticChance = 0.1
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 10000
  aoeRadius = 200
  iterationCount = 10
  intervalDuration = 1000
  unitOfDamage = 30

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillCooldown(this.name, () => {
      const playerTargets = this.getAOEAlterablesTargets(playerSource.room, skillInputData.position)

      for (const target of playerTargets) {

        const numberOfAlteration = target.alterations.length
        const outcome = this.checkIsAppliable(playerSource, target)
        if(isAlterable(target)){
          const alterable = target
          this._handleDispel(playerSource, alterable)
        }
        const damage = this.unitOfDamage*numberOfAlteration
        target.takeDamage(damage)
        this.handleScoreShiftData(playerSource, target, damage, ScoreShiftType.DAMAGE, outcome.critic)

      }

      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), SkillResults.HIT_PLAYER)
    })
  }

  _handleDispel(playerSource: Player, target: IAlterable){
    if(isAlterable(target)){
      const alterable = target as IAlterable
      alterable.alterations.forEach(alteration => {    
        alterable.removeAlteration(alteration.name)
      });
    }
  }
}
