import { AlterationNames } from '../alterations/Alteration'
import { Game } from '../Game'
import { GameEvents } from '../GameEventActions'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Totem } from '../gameobjects/Totem'
import { Room } from '../rooms/Room'
import { AutoSkill } from './core/AutoSkill'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'

export class ShamanTotem extends AutoSkill {
  name = SkillNames.SHAMAN_TOTEM
  defaultCriticChance = 0
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 5000
  defaultTimeout = Infinity

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillCooldown(this.name, () => {

      const position = playerSource.position
      const room = playerSource.room

      const newTotem = new Totem(room,playerSource.faction,position,playerSource.id)
      playerSource.inventory.push(newTotem)
      //reccuperer les Totems déjà posées par ce joueur
      const TotemsOfPlayerSource = playerSource.inventory.filter((o)=> o.type === GameObjectType.TOTEM)

      if(TotemsOfPlayerSource.length>1){
        const TotemToTrigger = TotemsOfPlayerSource.shift() as Totem
        if(TotemToTrigger){
          TotemToTrigger.remove()
        }
      }
      room.emitGameEvent(GameEvents.ROOM_UPDATE,room.normalize())
      const outcome = this.checkIsAppliable()
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
