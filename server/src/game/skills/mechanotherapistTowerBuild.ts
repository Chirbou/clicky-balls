import { GameEvents } from '../GameEventActions'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { PlayerTower } from '../gameobjects/PlayerTower'
import { AutoSkill } from './core/AutoSkill'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'

export class MechanotherapistTowerBuild extends AutoSkill {
  name = SkillNames.MECHANOTHERAPIST_TOWER_BUILD
  defaultCriticChance = 0
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 20000
  defaultTimeout = Infinity
  SHOOT_RATE = 1000
  TOWER_DAMAGE = 5
  TOWER_LIFESPAN = 30000
  MAX_HP = 500
  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillCooldown(this.name, () => {
      const newPlayerTower = new PlayerTower(
        playerSource,
        this.SHOOT_RATE,
        this.TOWER_DAMAGE,
        this.TOWER_LIFESPAN,
        this.MAX_HP
      )
      playerSource.inventory.push(newPlayerTower)
    
      const towerOfPlayerSource = playerSource.inventory.filter(
        (o) => o.type === GameObjectType.PLAYER_TOWER
      )

      if (towerOfPlayerSource.length > 1) {
        const playerTowerToTrigger = towerOfPlayerSource.shift() as PlayerTower
        if (playerTowerToTrigger) {
          playerTowerToTrigger.destroy()
        }
      }
      playerSource.room.emitGameEvent(GameEvents.ROOM_UPDATE, playerSource.room.normalize())
      const outcome = this.checkIsAppliable()
      playerSource.handleSkillFeedback(
        this.initSkillFeedbackData(skillInputData),
        outcome.result
      )
    })
  }
}
