import { TargetedSkill } from './core/TargetedSkill'
import { SkillInputProperties, ScoreShiftType } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { SkillResults } from './core/SkillResults'
import { Affinity } from '../FactionAffinity'
import { AlterationNames } from '../alterations/Alteration'

export class TrapperSniper extends TargetedSkill {
  name = SkillNames.TRAPPER_SNIPER
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 1200
  defaultCriticChance = 0
  hitCount = 0
  maxHitCount = 5

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const playerTarget = skillInputData.target

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, playerTarget)

      if (outcome.result === SkillResults.HIT_PLAYER) {
        const damage = playerTarget?.takeDamage(playerSource.atk * this.getHitCountBonusRatio())
        this.handleScoreShiftData(playerSource, playerTarget, damage, ScoreShiftType.DAMAGE, outcome.critic)
        this.increaseHitCount()
        Affinity.updateAffinity(playerSource, (playerTarget as any).faction, false, 1)
      } else if (outcome.result === SkillResults.MISS){
        this.resetHitCount()
        if (playerSource.hasAlteration(AlterationNames.STEALTH)) {
          playerSource.removeAlteration(AlterationNames.STEALTH)
          const playerSkillData = playerSource.getPlayerSkillData(SkillNames.TRAPPER_STEALTH)
          playerSource.resetSkillOnTimeout(playerSkillData)
        }
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }

  increaseHitCount() {
    if (this.hitCount < this.maxHitCount) {
      this.hitCount++
    }
  }

  resetHitCount() {
    this.hitCount = 0
  }

  getHitCountBonusRatio() {
    return 1 + this.hitCount * 0.75
  }
}
