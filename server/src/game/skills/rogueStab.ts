
import { AlterationNames } from '../alterations/Alteration'
import { Player } from '../player/Player'
import { ScoreShiftType, SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { SkillResults } from './core/SkillResults'
import { TargetedSkill } from './core/TargetedSkill'

export class RogueStab extends TargetedSkill {
  name = SkillNames.ROGUE_STAB
  defaultCriticChance = 0.4
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 30000

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    const playerTarget = skillInputData.target

    if (!playerSource.hasAlteration(AlterationNames.INVISIBLE)) return

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, playerTarget)

      if (outcome.result === SkillResults.HIT_PLAYER) {
        const isCritic = this.defaultCriticChance > Math.random()
        const damage = playerTarget?.takeDamage(
          isCritic ? playerSource.atk * 25 : playerSource.atk * 15
        )
        this.handleScoreShiftData(playerSource, playerTarget, damage, ScoreShiftType.DAMAGE, outcome.critic)
        playerSource?.removeAlteration(AlterationNames.INVISIBLE)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
