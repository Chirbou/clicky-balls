import { SkillResults } from './core/SkillResults'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { TargetedSkill } from './core/TargetedSkill'
import { AlterationNames } from '../alterations/Alteration'
import { isPlayer } from '../interfaces/IPlayer'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IAlterable, isAlterable } from '../interfaces/IAlterable'

export class OccultistFucker extends TargetedSkill {
  name = SkillNames.OCCULTIST_FUCKER
  defaultStack = 2
  defaultUsable = true
  defaultCooldown = 10000
  defaultTimeout = 5000
  defaultCriticChance = 0.1

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target || !isAlterable(skillInputData.target)) return 
    const target = skillInputData.target as IHasHealthPoint & IAlterable
    if ( target.hasAlteration( AlterationNames.FUCKED )) return 

    playerSource?.handleSkillCooldown(this.name, () => {
      const outcome = this.checkIsAppliable(playerSource, target)

      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
          target.applyAlteration(AlterationNames.FUCKED, 10000, playerSource)
        }
      
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
