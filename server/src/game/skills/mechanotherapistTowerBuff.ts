import { GameEvents } from '../GameEventActions'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { TargetedSkill } from './core/TargetedSkill'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { isPlayer } from '../interfaces/IPlayer'
import { IGameObject } from '../interfaces/IGameObject'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IAlterable } from '../interfaces/IAlterable'
import { Tower } from '../gameobjects/Tower'
import { AlterationNames } from '../alterations/Alteration'

export class MechanotherapistTowerBuff extends TargetedSkill {
  name = SkillNames.MECHANOTHERAPIST_TOWER_BUFF
  defaultCriticChance = 0
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 15000
  defaultTimeout = Infinity

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!this.isTargetable(skillInputData.target)) {
      return
    }
    const target = skillInputData.target as Tower
    target.applyAlteration(AlterationNames.MECHANOTHERAPIST_BUFFED, this.defaultCooldown, skillInputData.playerSource)
    playerSource?.handleSkillCooldown(this.name, () => {
      playerSource.room.emitGameEvent(
        GameEvents.ROOM_UPDATE,
        playerSource.room.normalize()
      )
      const outcome = this.checkIsAppliable(playerSource, skillInputData.target)
      playerSource.handleSkillFeedback(
        this.initSkillFeedbackData(skillInputData),
        outcome.result
      )
    })
  }
  isTargetable(target: (IGameObject & IHasHealthPoint) | undefined) {
    return (
      target &&
      !isPlayer(target) &&
      (target.type === GameObjectType.TOWER ||
        target.type === GameObjectType.PLAYER_TOWER)
    )
  }
}
