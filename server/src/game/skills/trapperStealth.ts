import { AlterationNames } from '../alterations/Alteration'
import { AutoSkill } from './core/AutoSkill'
import { SkillInputProperties } from './core/Skill'
import { SkillNames } from './core/SkillNames'

export class TrapperStealth extends AutoSkill {
  name = SkillNames.TRAPPER_STEALTH
  defaultCriticChance = 0
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 15000
  defaultTimeout = Infinity

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource

    playerSource?.handleSkillForever(this.name, () => {
      const outcome = this.checkIsAppliable()
      playerSource.applyAlterationForever(AlterationNames.STEALTH)
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }
}
