import { SkillResults } from './core/SkillResults'
import { ScoreShiftType, SkillInputProperties, SkillOutcome } from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Affinity } from '../FactionAffinity'
import { TargetedSkill } from './core/TargetedSkill'
import { Player } from '../player/Player'
import { isPlayer } from '../interfaces/IPlayer'
import { IHasHealthPoint } from '../interfaces/IHasHealthPoint'
import { IFactionAffinity } from '../interfaces/IFactionAffinity'

export class PoisonFang extends TargetedSkill {
  name = SkillNames.POISON_FANG
  defaultCriticChance = .1
  defaultStack = 3
  defaultUsable = true
  defaultCooldown = 15000

  trigger(skillInputData: SkillInputProperties) {
    const playerSource = skillInputData.playerSource
    if (!skillInputData.target ) { return }
    const target = skillInputData.target as IHasHealthPoint
    const outcome = this.checkIsAppliable(playerSource, target)

    playerSource?.handleSkillCooldown(this.name, () => {
      if (outcome.result === SkillResults.HIT_PLAYER || outcome.result === SkillResults.HIT_SELF) {
        this._handleVampirism(playerSource, target, outcome)
      }
      playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), outcome.result)
    })
  }

  _handleVampirism(playerSource: Player, target: IHasHealthPoint , outcome: SkillOutcome){
    const absoluteDamage = outcome.critic ? (playerSource.atk + 3) * 3 : playerSource.atk + 3
    const damage = target?.takeDamage(absoluteDamage)
    playerSource.takeHeal(damage || 0)
    this.handleScoreShiftData(playerSource, target, damage, ScoreShiftType.DAMAGE, outcome.critic)
    this.handleScoreShiftData(playerSource, playerSource, damage, ScoreShiftType.HEAL, outcome.critic)
    if(isPlayer(target)){
      const playerTarget = target as Player
      Affinity.updateAffinity(playerSource, playerTarget.faction, false, 2)
      Affinity.updateAffinity(playerTarget, playerSource.faction, false, 2)
    }
  }
}
