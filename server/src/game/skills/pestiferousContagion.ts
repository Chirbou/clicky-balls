import { SkillResults } from './core/SkillResults'
import {
  ScoreShiftType,
  Skill,
  SkillInputProperties,
} from './core/Skill'
import { SkillNames } from './core/SkillNames'
import { Player } from '../player/Player'
import { Game } from '../Game'
import { AlterationNames } from '../alterations/Alteration'

export class PestiferousContagion extends Skill {
  name = SkillNames.PESTIFEROUS_CONTAGION
  defaultCriticChance = 0.1
  defaultStack = 1
  defaultUsable = true
  defaultCooldown = 12000
  aoeRadius = 100
  iterationCount = 12
  intervalDuration = 3000
  defaultTimeout = (this.iterationCount*this.intervalDuration)
  iteration = 0
  damages = 10
  //playerSourceOrigin:Player

  trigger(skillInputData: SkillInputProperties) {

    const playerSource = skillInputData.playerSource
    const targets = this.getAOETargets(playerSource.room, skillInputData.position)
    const playerTargets: Player[] = targets.filter(go => go instanceof Player) as Player[]

    // Un Pestiféré peut contaminer une aoe de joueur. Si ceux ci n'ont pas dejà de contagions, ils prennent la contagion du pestiféré. 
    //this.triggerPeriodicEvent(this.iterationCount, this.intervalDuration, () => {    this.handleContagion(playerSource) })
    playerSource?.handleSkillCooldown(this.name, () => {
      for (const target of playerTargets) {
        if(!target.hasAlteration(AlterationNames.CONTAGIOUS)){
        target.applyAlteration(AlterationNames.CONTAGIOUS, this.defaultTimeout, playerSource)
        target.contagiousPlayer = playerSource
        playerSource.contaminedPlayers?.push(target)
        }
      }
      if (playerSource.contaminedPlayers){

        const contagionOperation = setInterval(() => {

          const contaminatedArray:(Player[] | undefined) = playerSource.contaminedPlayers

          if(this.iteration<this.iterationCount && contaminatedArray.length>0){
            this.handleDamages(playerSource, contaminatedArray)
            this.handleContagion(playerSource, contaminatedArray, skillInputData)
            this.iteration++
          }else{
            if(contaminatedArray.length>0){
              for (const target of contaminatedArray){
                if(target.hasAlteration(AlterationNames.CONTAGIOUS)){
                  target.removeAlteration(AlterationNames.CONTAGIOUS)
                }
                playerSource.contaminedPlayers=[]
              }
            }
            this.iteration = 0
            clearTimeout(contagionOperation)
          }
          
        }, this.intervalDuration)
        playerSource.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), SkillResults.HIT_PLAYER)
      }

    })
  }

  handleContagion(playerSource:Player,actuallyContamined:Array<Player|undefined>, skillInputData: SkillInputProperties){
    if(actuallyContamined){
      for(const contamined of actuallyContamined){
        if(contamined){
          if(contamined.hasAlteration(AlterationNames.INVINCIBLE) || contamined.hasAlteration(AlterationNames.PALADIN_BUBBLE) ){
            return
          }
          skillInputData.playerSource = contamined
          skillInputData.position = contamined.position
          const newTargets = this.getAOETargets(contamined.room, contamined.position)
          const playerTargets: Player[] = newTargets.filter(go => go instanceof Player) as Player[]
          contamined.handleSkillFeedback(this.initSkillFeedbackData(skillInputData), SkillResults.HIT_PLAYER)

          for(const target of playerTargets){
            if(!target.hasAlteration(AlterationNames.CONTAGIOUS) && actuallyContamined.indexOf(target)===-1 && !target.hasAlteration(AlterationNames.INVINCIBLE)&& !target.hasAlteration(AlterationNames.PALADIN_BUBBLE)){
              target.applyAlteration(AlterationNames.CONTAGIOUS, this.defaultTimeout, playerSource)
              target.contagiousPlayer = playerSource
              playerSource.contaminedPlayers?.push(target)
            }
          }
        }
      }
    }
  }
  handleDamages(playerSource:Player,actuallyContamined:Array<Player>){
    for(const contamined of actuallyContamined){
      if(contamined){
        if(contamined.hasAlteration(AlterationNames.INVINCIBLE) || contamined.hasAlteration(AlterationNames.PALADIN_BUBBLE) ){
          return
        }
        contamined.takeDamage(this.damages)
        this.handleScoreShiftData(playerSource, contamined, this.damages, ScoreShiftType.DAMAGE, false)
      }
    }
  }
}
