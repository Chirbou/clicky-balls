import { Game } from '../Game'
import { IPlayerData } from '../interfaces/IPlayer'
import { Player } from './Player'

export const normalizePlayer = (player: Player): IPlayerData => {
  return {
    id: player.id,
    emoji: player.emoji,
    type: player.type,
    name: player.name,
    position: player.position,
    roomId: player.roomId,
    hp: player.hp,
    maxHp: player.maxHp,
    scoreToEvolve: player.scoreToEvolve,
    scoreToDevolve: player.scoreToDevolve,
    tpStackMax: player.tpStackMax,
    color: player.color,
    factionAffinity: player.factionAffinity,
    faction: player.faction,
    level: player.level,
    atk: player.atk,
    def: player.def,
    heal: player.heal,
    size: player.size,
    hitbox: player.hitbox,
    angle: player.angle,
    ownedGold: player.ownedGold,
    goldStackMax: player.goldStackMax,
    zIndex: player.zIndex,
    alterations: player.alterations.map(alteration => alteration.normalize()),
    inventory: player.inventory.map(gameObject => gameObject.normalize()),
    gluedPlayersIds: player.gluedPlayers?.map((player) => player.id),
    contaminedPlayersIds: player.contaminedPlayers?.map((player) => player.id),
    dead: player.dead,
    disabled: player.disabled,
    gameZoneWidth: Game.GAMEZONE_WIDTH,
    gameZoneHeight: Game.GAMEZONE_HEIGHT,
    allPlayerCounter: Game.players.length,
    playersResume: Game.getPlayersResume(),
    canEvolve: player.canEvolve,
    classesLocks: player.classesLocks,
    currentClass: player.currentClass,
    suckingPlayersIds: player.suckingPlayers.map(p => p.id),
    sufferedDamage: player.sufferedDamage,
    totalGold: player.totalGold,
    martyrProtectorId: player.martyrProtector?.id,
  }
}
