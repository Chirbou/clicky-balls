import { Socket } from 'socket.io'
import { classesDef } from '../classes/classesDef'
import { StatsShift, ClasseStats } from '../classes/Classe'
import { ClassNames } from '../classes/ClassNames'
import { GameEvents } from '../GameEventActions'
import { Coordinate, Utils } from '../../core/Utils'
import { Alteration, AlterationNames } from '../alterations/Alteration'
import { alterationsDef } from '../alterations/alterationsDefs'
import { SkillNames } from '../skills/core/SkillNames'
import { PlayerSkillData } from '../skills/core/PlayerSkillData'
import { skillsDef } from '../skills/core/skillsDef'
import { Game } from '../Game'
import {
  Affinity,
  DEFAULT_FACTION_AFFINITY,
  getAffinityFromColor,
} from '../FactionAffinity'
import { Colors } from '../Colors'
import { SkillFeedbackData } from '../skills/core/SkillFeedbackData'
import { SkillResults } from '../skills/core/SkillResults'
import { FactionNames } from '../FactionNames'
import { Room } from '../rooms/Room'
import { GameObjectType } from '../gameobjects/GameObjectType'
import { Classe, ScoreLandings } from '../classes/Classe'
import { ClassLockArchetype, DEFAULT_CLASS_LOCK_ARCHETYPE } from '../classes/classeLock'
import { normalizePlayer } from './PlayerData'
import {
  defaultGetAlteration,
  defaultHasAlteration,
} from '../interfaces/IAlterable'
import { defaultCheckLvlUp } from '../interfaces/IEvolveable'
import {
  BASE_LEVEL,
  BASE_SCORE,
  defaultDie,
  defaultDropExcessGold,
  defaultRevive,
  IPlayer,
} from '../interfaces/IPlayer'
import {
  defaultTakeHeal,
  IHasHealthPoint,
} from '../interfaces/IHasHealthPoint'
import { ScoreShiftType } from '../skills/core/Skill'
import { defaultUpdateFaction } from '../interfaces/IFactionAffinity'
import { defaultRemoveFromRoom, IGameObject } from '../interfaces/IGameObject'
import { IPlayerData } from '../interfaces/IPlayer'
import { toRand } from '../rooms/FlagBasePositions'

export class Player implements IPlayer {
  type = GameObjectType.PLAYER
  socket: Socket
  id: string
  name: string
  emoji: string
  classesLocks: ClassLockArchetype[] = DEFAULT_CLASS_LOCK_ARCHETYPE
  canEvolve = false
  position: Coordinate
  room!: Room
  roomId!: string
  // _score!: number
  maxHp = ScoreLandings.PECNO_MAXSCORE
  scoreToEvolve? = ScoreLandings.PECNO_EVOLVE
  scoreToDevolve? = ScoreLandings.PECNO_DEVOLVE
  color = 'grey'
  factionAffinity = DEFAULT_FACTION_AFFINITY
  faction: FactionNames
  currentClass = ClassNames.PECNO
  level = 1
  atk = 0
  def = 0
  heal = 0
  size = 0
  hitbox = 0
  zIndex = 0
  angle = 0
  tpStackMax = 0
  ownedGold = 0
  totalGold = 0
  goldStackMax = 100
  alterations: Alteration[] = []
  inventory: IGameObject[] = []
  dead = false
  disabled = false
  playerSkillData: Map<SkillNames, PlayerSkillData> = new Map<
    SkillNames,
    PlayerSkillData
  >()
  timeout?: NodeJS.Timeout
  // To Refactor
  martyrProtector?: Player
  contagiousPlayer?: Player
  gluedPlayers: Player[] = []
  contaminedPlayers: Player[] = []
  suckingPlayers: Player[] = []
  sufferedDamage = 0

  _hp = 0
  _rebornTimeout?: NodeJS.Timeout

  get hp(): number {
    return this._hp
  }

  set hp(score: number) {
    if (score <= this.maxHp) {
      this._hp = score
    } else if (score < 0) {
      this._hp = 0
    } else if (score >= this.maxHp) {
      this._hp = this.maxHp
    } else {
      this._hp = BASE_SCORE
    }

    this.checkLevelUp()
    this.checkLevelDown()
  }

  constructor(
    socket: Socket,
    position: Coordinate,
    name: string,
    emoji: string,
    faction: FactionNames,
    classesLocks: ClassLockArchetype[]
  ) {
    this.id = socket.id
    this.classesLocks = classesLocks
    this.name = (name || Utils.generateName()).slice(0, 30)
    this.socket = socket
    this.position = position
    this.color = Utils.getColorFromFaction(faction)
    this.initFactionAffinity()
    this.faction = faction
    this.level = BASE_LEVEL
    this.hp = BASE_SCORE
    const defaultClass = classesDef.find((c) => c.name === ClassNames.PECNO)
    this.initStats(defaultClass)
    this.setPlayerSkillData(defaultClass?.skills)
    this.emoji = emoji || Utils.getRandomEmoji()

    this.socket.on(GameEvents.PLAYER_READY, () => {
      this.syncPlayerSkillData()
      this.syncAllPlayers()
    })
  }

  enterStartRoom() {
    const room = Room.getRoomFromColor(this.color)
    room.playerEnters(this, toRand())
  }

  hit(player: IGameObject, skillName: SkillNames): void {
    throw new Error('Method not implemented.')
  }
  handleScoreShiftData(
    playerSource: IHasHealthPoint,
    playerTarget: IHasHealthPoint | undefined,
    value: number | undefined,
    scoreshiftType: ScoreShiftType,
    isCritic: boolean
  ): void {
    throw new Error('Method not implemented.')
  }

  removeFromRoom(): void {
    defaultRemoveFromRoom(this)
  }

  initStats(classe: Classe | undefined) {
    this.maxHp = classe?.stats.maxHp ?? ScoreLandings.PECNO_MAXSCORE
    this.scoreToEvolve =
      classe?.stats.scoreToEvolve ?? ScoreLandings.PECNO_EVOLVE
    this.scoreToDevolve =
      classe?.stats.scoreToDevolve ?? ScoreLandings.PECNO_DEVOLVE
    this.atk = classe?.statsShift.atk ?? -1
    this.def = classe?.statsShift.def ?? -1
    this.heal = classe?.statsShift.heal ?? -1
    this.size = classe?.statsShift.size ?? -1
    this.hitbox = classe?.statsShift.size ?? -1
    this.zIndex = classe?.statsShift.zIndex ?? -1
    this.tpStackMax = classe?.statsShift.tpStackMax ?? -1
    this.goldStackMax = classe?.goldStackMax ?? -1
  }

  setPlayerSkillData(skillNames: SkillNames[] = []) {
    for (const skillName of skillNames) {
      const skill = skillsDef.get(skillName)
      if (skill) {
        this.playerSkillData.set(
          skillName,
          new PlayerSkillData(
            skillName,
            skillName === SkillNames.PECNO_TP
              ? this.tpStackMax
              : skill.defaultStack,
            skill.defaultUsable,
            skill.defaultCooldown,
            skill.defaultTimeout
          )
        )
      }
    }
  }

  syncPlayerSkillData() {
    this.playerSkillData.forEach((playerSkillData) => {
      this.emitGameEvent(
        GameEvents.PLAYER_SKILL_UPDATE,
        playerSkillData.normalize()
      )
    })
  }

  syncAllPlayers() {
    for (const player of Game.players) {
      this.emitGameEvent(GameEvents.PLAYER_UPDATE, player.normalize())
    }
  }

  normalize(): IPlayerData {
    return normalizePlayer(this)
  }

  emitGameEvent(gameEvent: GameEvents, data: unknown) {
    this.socket.emit(gameEvent, data)
  }

  takeRawDamage(damage: number) {
    this.hp = this.hp - damage
    this.checkLevelDown()
    this.checkDeath()
    if (this.hp - damage < 0) {
      this.hp = 0
    }
    if (this.disabled || this.dead || this.hasAlteration(AlterationNames.DEAD))
      return 0
    return damage
  }

  updateFaction(faction: FactionNames): void {
    defaultUpdateFaction(this, faction)
  }

  takeDamage(damage: number): number {
    damage = Math.max(Utils.computeDamage(Math.abs(damage), this.def), 1)
    for (const alteration of this.alterations) {
      damage = alteration.computeDamage(damage, this)
    }

    return this.takeRawDamage(damage)
  }

  takeHeal(heal: number): number {
    heal = Math.abs(heal)
    if (this.disabled || this.dead || this.hasAlteration(AlterationNames.DEAD))
      return 0

    for (const alteration of this.alterations) {
      heal = alteration.computeHeal(heal, this)
    }

    this.hp += heal
    return heal
  }

  handleSkillForever(
    skillName: SkillNames,
    callback: (playerSkillData: PlayerSkillData) => void
  ) {
    if (this.dead || this.disabled) return

    const playerSkillData = this.getPlayerSkillData(skillName)
    if (playerSkillData?.usable) {
      if (playerSkillData.stack <= 0) {
        playerSkillData.disable()
        return
      }
      playerSkillData.consumeStack()
      callback(playerSkillData)
      this.emitGameEvent(
        GameEvents.PLAYER_SKILL_UPDATE,
        playerSkillData.normalize()
      )
    } else {
      return
    }
  }

  handleSkillCooldown(
    skillName: SkillNames,
    callback: (playerSkillData: PlayerSkillData) => void
  ) {
    if (this.dead || this.disabled) return

    const playerSkillData = this.getPlayerSkillData(skillName)
    if (playerSkillData?.usable) {
      if (playerSkillData.stack <= 0) {
        playerSkillData.disable()
        return
      }
      playerSkillData.consumeStack()

      callback(playerSkillData)
      this.emitGameEvent(
        GameEvents.PLAYER_SKILL_UPDATE,
        playerSkillData.normalize()
      )

      this.resetSkillOnTimeout(playerSkillData)
    } else {
      return
    }
  }

  resetSkillOnTimeout(playerSkillData?: PlayerSkillData) {
    if (!playerSkillData) return
    playerSkillData.timeout = setTimeout(() => {
      if (playerSkillData.stack < playerSkillData.stackMax) {
        playerSkillData.restoreStack()
        playerSkillData.enable()
        this.emitGameEvent(
          GameEvents.PLAYER_SKILL_UPDATE,
          playerSkillData.normalize()
        )
      }
    }, playerSkillData.cooldownDuration)
  }

  handleSkillFeedback(feedback: SkillFeedbackData, result: SkillResults) {
    this.room.emitGameEvent(GameEvents.SKILL_FEEDBACK_DATA, {
      ...feedback,
      skillResults: result,
    } as SkillFeedbackData)
  }

  checkLevelUp() {
    defaultCheckLvlUp(this, this.classesLocks)
  }

  dropExcessGold(newGoldStackMax: number) {
    defaultDropExcessGold(this, newGoldStackMax)
  }

  levelUp(className: ClassNames): void {
    const chosenClass = classesDef.find((c) => c.name === className)
    if (!chosenClass) return

    if (!this.canEvolve) {
      return
    }

    this.currentClass = chosenClass.name
    this.shiftStats(chosenClass.statsShift, 'add')
    this.setClasseStats(chosenClass.stats)
    this.level += 1
    const newGoldStackMax = chosenClass.goldStackMax
    this.goldStackMax = newGoldStackMax

    this.dropExcessGold(newGoldStackMax)

    this.canEvolve = false
    this.emitGameEvent(GameEvents.CANT_EVOLVE, this.normalize())
    this.setPlayerSkillData(chosenClass.skills)
    this.syncPlayerSkillData()

    this.room?.emitGameEvent(GameEvents.PLAYER_UPDATE, this?.normalize())
  }

  checkLevelDown() {
    const currentClass = classesDef.find((c) => c.name === this.currentClass)
    const scoreToLvlDown = currentClass?.stats.scoreToDevolve

    if (scoreToLvlDown && this.hp <= scoreToLvlDown) {
      if (
        (!currentClass?.archetype ||
          currentClass?.archetype === ClassNames.PECNO) &&
        this.currentClass === ClassNames.PECNO
      ) {
        if (this.canEvolve) {
          this.canEvolve = false
          this.emitGameEvent(GameEvents.CANT_EVOLVE, this.normalize())
        }
      } else {
        this.levelDown(currentClass?.name)
      }
    }
  }

  shiftStats(statsShift: StatsShift, type: 'add' | 'sub'): void {
    let ratio = 1
    if (type === 'sub') {
      ratio = -1
    }
    this.atk += statsShift.atk * ratio
    this.def += statsShift.def * ratio
    this.heal += statsShift.heal * ratio
    this.size += statsShift.size * ratio
    this.hitbox += statsShift.size * ratio
    this.zIndex += statsShift.zIndex * ratio
    this.tpStackMax += statsShift.tpStackMax * ratio
  }

  setClasseStats(classeStats: ClasseStats): void {
    this.maxHp = classeStats.maxHp
    this.scoreToEvolve = classeStats?.scoreToEvolve
    this.scoreToDevolve = classeStats?.scoreToDevolve
  }

  levelDown(className: ClassNames | undefined): void {
    const currentClass = classesDef.find((c) => c.name === className)
    const archetypeClass = classesDef.find(
      (c) => c.name === currentClass?.archetype
    )

    if (!currentClass || !archetypeClass) return

    this.currentClass = archetypeClass.name
    this.shiftStats(currentClass.statsShift, 'sub')
    this.setClasseStats(archetypeClass.stats)
    this.goldStackMax = archetypeClass?.goldStackMax
    this.level -= 1

    this.dropExcessGold(this.goldStackMax)

    this.playerSkillData.forEach((playerSkillData, skillName, self) => {
      if (!archetypeClass.skills.includes(skillName)) {
        self.delete(skillName)
      }
    })

    this.canEvolve = false
    this.emitGameEvent(GameEvents.CANT_EVOLVE, this.normalize())
    this.emitGameEvent(GameEvents.LVL_DOWN, archetypeClass)
    this.setPlayerSkillData(currentClass.skills)
    this.syncPlayerSkillData()
    this.checkLevelDown()
  }

  checkDeath() {
    if (this.hp <= 0 && !this.dead) {
      const rebornTimeout = 10000
      const room = this.room
      if (room) {
        const playerId = this.id
        this.applyAlteration(AlterationNames.DEAD, rebornTimeout)
        this.emitGameEvent(GameEvents.DEATH, {
          playerId,
        })
      }
      this.die()
      this._rebornTimeout = setTimeout(() => {
        this.reset()
      }, rebornTimeout)
    }
  }

  revive() {
    defaultRevive(this)
  }

  die() {
    defaultDie(this)
  }

  trigger(player: Player) {}

  getPlayerSkillData(skillName: SkillNames) {
    return this.playerSkillData.get(skillName)
  }

  removeAllAlterations() {
    for (const alteration of this.alterations) {
      this.removeAlteration(alteration.name)
    }
  }

  applyAlteration(
    alterationName: AlterationNames,
    cooldownDuration: number,
    source?: IGameObject
  ) {
    const alteration = alterationsDef.get(alterationName)
    alteration?.apply(this, cooldownDuration, source)
  }

  applyAlterationForever(alterationName: AlterationNames) {
    const alteration = alterationsDef.get(alterationName)
    alteration?.applyForever(this)
  }

  removeAlteration(alterationName: AlterationNames) {
    const alteration = alterationsDef.get(alterationName)
    alteration?.removeAlteration(this)
  }

  hasAlteration(alterationName: AlterationNames): boolean {
    return defaultHasAlteration(this, alterationName)
  }

  getAlteration(alterationName: AlterationNames): Alteration | undefined {
    return defaultGetAlteration(this, alterationName)
  }

  inventoryContains(type: GameObjectType) {
    return this.inventory.find((obj) => obj.type === type)
  }

  initFactionAffinity() {
    this.factionAffinity = getAffinityFromColor(this.color as Colors)
  }

  // TODO vérifier si c'est au bon endroit
  reset() {
    this.removeAllAlterations()

    Game.playerLeave(this.id)
    this.revive()
    const roomToPop = Room.getRoomFromColor(this.color)
    Game.players.push(this)
    this.enterStartRoom()
    roomToPop.emitGameEvent(GameEvents.PLAYER_UPDATE, this.normalize())
  }
}
