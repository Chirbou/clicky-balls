import { FactionNames } from '../FactionNames'
import { ClassNames } from '../classes/ClassNames'
import { AlterationNames } from '../alterations/Alteration'

export interface PlayerResume {
  id: string
  name: string
  roomId: string
  class: ClassNames
  score: number
  maxHp: number
  dead: boolean
  alterations: AlterationNames[]
  faction: FactionNames
  ownedGold:number
  goldStackMax:number
}