# Start

## Client

Go to the client : `cd client`
Install packages : `npm install`
Launch client with dev/watch mode : `npm run dev`

Go to http://localhost:8080

## Server

Go to the server : `cd server`
Install packages : `npm install`
Launch server with dev/watch mode : `npm run dev`

Go to http://localhost:3333

## Lint

Go to the server : `cd server`
Run the command : `npm run lint` to display all eslint errors
Run the command : `npm run lint:fix` to fix most of eslint errors

### Enable lint on save

- Go to préférences => Settings
- Search `editor onsave` and check the checkbox

## Git Branch

in order to work on a specific branch :
- If you have any, commit all your changes
  - `git add --all`
  - `git commit -m "My commit"`
- Pull git origin
  - `git pull`
- Create and go to your new branch
  - `git checkout -b my-branch-name`
- Do some stuff...
- Commit
  - `git add --all`
  - `git commit -m "My commit"`
- Push your change
  - `git push`
  - If the branch doesn't exists in the repo, which arrived when you just created it, just copy-paste the line provided by git in the error (something like `git push --set-upstream origin my-branch-name`)

- Login to GitLab
- Go on clicky project
- Go to the `Merge request` tab
- Click on "New merge request"
- Select your source branch `my-branch-name` and the destination branch `master`
- Click "Compare branches and continue"
- Fill forms to inform other users of your work
- Click on "Create merge request"
  - /!\ If title begin by `Draft: ...`, it inform you that there is no changes on your branch compare to `master`

- After that, you could merge your request yourself, or add somebody which can read and accept your changes.
- If the `merge` button displays : `There are conflicts`, give up and call a expert.
