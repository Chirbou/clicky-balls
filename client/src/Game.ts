import { GameConstants } from "./GameConstants";
import { GameEvents, GameActions } from "./GameEventActions";
import { MainPlayer } from "./MainPlayer";
import { NetworkEventHandler } from "./NetworkEventHandler";
import { RemotePlayer } from "./RemotePlayer";
import { SkillBar } from "./Skillbar";
import { AnimationGenerator } from "./AnimationGenerator";
import { SkillFactory } from "./SkillFactory";
import { ClassSelectionMenu } from "./ClassSelectionMenu";
import { ALL_SKILLS } from "./skill/SkillsDefs";
import { Stats } from "./Stats";
import { Display } from "./Display";
import { Player } from "./Player";
import { Portal } from "./gameobject/Portal";
import { RoomTypes } from "./RoomTypes.js";
import { GameObject } from "./GameObject.js";
import { GameObjectType } from "./GameObjectType.js";
import { Flag } from "./gameobject/Flag.js";
import { FlagBase } from "./gameobject/FlagBase.js";
import { ClassNames, FactionNames } from "./ClassNames.js";
import { KeyboardHandler } from "./KeyboardHandler.js";
import { Mine } from "./gameobject/Mine";
import { Totem } from "./gameobject/Totem";
import { Gold } from "./gameobject/Gold";
import { GoldDeposit } from "./gameobject/GoldDeposit";
import {
  ClasseData,
  EndGameData,
  PlayerData,
  RoomData,
  ScoreShiftData,
  SkillFeedbackData,
} from "./utils/InterfaceUtils";
import { GoldMine } from "./gameobject/GoldMine";
import { PowerUp } from "./gameobject/PowerUp";
import { Tower } from "./gameobject/Tower";
import { HealthBar } from "./ui/HealthBar";
import { Timer } from "./Timer";
import { GlobalAnnounce } from "./Announces";
import { AlterationList } from "./ui/AlterationList";
import { SelectionScreen } from "./ui/SelectionScreen";

export const getAngleDegrees = (fromX, fromY, toX, toY, force360 = true) => {
  const deltaX = fromX - toX;
  const deltaY = fromY - toY; // reverse
  const radians = Math.atan2(deltaY, deltaX);
  let degrees = (radians * 180) / Math.PI - 90; // rotate
  if (force360) {
    while (degrees >= 360) degrees -= 360;
    while (degrees < 0) degrees += 360;
  }
  return degrees;
};
export interface ClassLock {
  key: ClassNames;
  order: number;
}

export interface ClassLockArchetype {
  key: ClassNames;
  locks: ClassLock[];
}
export class Game {
  static mainPlayer: MainPlayer;
  static players: Player[] = [];
  static mainPlayerId = "";
  static gameObjects: GameObject[] = [];
  static gameZone: HTMLDivElement;
  static animationWrapper: HTMLDivElement;
  static selectionScreen: SelectionScreen;
  static alterationList: AlterationList;
  static timer: Timer;
  static healthBar: HealthBar;
  static skillbar: SkillBar;
  static display: Display;
  static classSelectionMenu: ClassSelectionMenu;
  static deathOverlayElt: HTMLDivElement;
  static skillbarWrapper: HTMLDivElement;
  static goldStatElt: HTMLDivElement;
  static isDeathTriggered = false;
  static keyboardHandler?: KeyboardHandler;
  static currentActiveCountdown?: Portal | GoldDeposit | FlagBase | Flag;
  static ready = false;
  static classesDef: ClasseData[] = []
  static classesLocks: ClassLockArchetype[]

  static showSelectionScreen() {
    Game.selectionScreen = new SelectionScreen()
  }

  static init() {
    Game.gameZone = document.getElementById(
      GameConstants.GAME_ZONE_ID
    ) as HTMLDivElement;
    Game.animationWrapper = document.getElementById(
      GameConstants.ANIMATION_WRAPPER_ID
    ) as HTMLDivElement;
    Game.deathOverlayElt = document.getElementById(
      GameConstants.DEATH_OVERLAY_ID
    ) as HTMLDivElement;
    Game.skillbarWrapper = document.getElementById(
      GameConstants.SKILLBAR_WRAPPER
    ) as HTMLDivElement;

    Game.skillbar = new SkillBar();
    Game.classSelectionMenu = new ClassSelectionMenu();
    Stats.init();
    Game.alterationList = new AlterationList();
    Game.display = new Display();
    // Game.keyboardHandler = new KeyboardHandler()
    SkillBar.addSkills([
      SkillFactory.create(GameActions.PECNO_TP),
      SkillFactory.create(GameActions.PECNO_ATK),
    ]);
    Game.gameZone.addEventListener("mousedown", (e: MouseEvent) => {
      if (e.button === 0 && Game.mainPlayer) {
        Game.mainPlayer.mouseTarget = e;
        Game.mainPlayer.leftClick();
        Game.mainPlayer.triggerAutoClick();
      }
    });

    window.addEventListener("mouseup", (e) => {
      if (e.button === 0 && Game.mainPlayer) {
        clearInterval(Game.mainPlayer.autoClickRate);
      }
    });
    Game.gameZone.addEventListener("mousemove", (e) => {
      if (Game.mainPlayer) {
        Game.mainPlayer.mouseTarget = e;
        Game.mainPlayer.angle =
          getAngleDegrees(
            Game.mainPlayer.position.x,
            Game.mainPlayer.position.y,
            e.pageX,
            e.pageY
          ) + 180;
        Game.mainPlayer.refresh();
      }
    });
    setInterval(() => {
      NetworkEventHandler.emit(GameEvents.UPDATE_CANON, Game.mainPlayer?.angle);
    }, 150);
    Game.skillbarWrapper.addEventListener("click", (e) => {
      const target = e.target as HTMLDivElement;
      if (target && target.classList.contains(GameConstants.SKILL_CLASS)) {
        SkillBar.selectSkill(target.dataset["position"]);
      }
    });
    Game.gameZone.addEventListener("contextmenu", (e) => {
      e.preventDefault();
      Game.mainPlayer?.rightClick(e);
    });
  }

  static endGame(endGameData: EndGameData) {
    const scores = [
      { faction: FactionNames.PUNK,
        score: endGameData[FactionNames.PUNK] 
      },
      {
        faction: FactionNames.CONSERVATIST,
        score: endGameData[FactionNames.CONSERVATIST],
      },
      {
        faction: FactionNames.TECHNOPHILE,
        score: endGameData[FactionNames.TECHNOPHILE],
      },
    ];

    scores.sort((a, b) => b.score - a.score);

    const winningFactions = scores
      .filter((score) => score.score === scores[0].score)
      .map((score) => score.faction);

    let endMessage = "";

    if (winningFactions.length === 1) {
      const winningFaction = winningFactions[0];
      if (winningFaction === Game.mainPlayer.faction) {
        endMessage = `Félicitations, votre équipe (${winningFaction}s) gagne ! Honte à vous, ${scores[2].faction}s, vous étiez vraiment mauvais !`;
      } else if (scores[1].faction === Game.mainPlayer.faction) {
        endMessage = `Votre équipe (${Game.mainPlayer.faction}s) a réussi à échapper à la défaite! Mais la première place vous est passée sous le nez. Faites mieux.`;
      } else {
        endMessage = `Malheureusement, votre équipe (${Game.mainPlayer.faction}s) a perdu. Les ${winningFaction}s sont victorieux, avec ${scores[0].score} gold. Faites mieux.`;
      }
    } else if (winningFactions.length === 2) {
      const secondPlaceFaction = scores.find(
        (score) => score.score === scores[1].score
      )?.faction;
      if (winningFactions.includes(Game.mainPlayer.faction)) {
        endMessage = `Votre équipe (${Game.mainPlayer.faction}s) a eu à la fois la première et la seconde place! Bravo pour cette double victoire !`;
      } else if (secondPlaceFaction === Game.mainPlayer.faction) {
        endMessage = `Votre équipe (${Game.mainPlayer.faction
          }) est deuxième, égalité avec les ${winningFactions
            .filter((f) => f !== this.mainPlayer.faction)
            .join("")}s !`;
      } else {
        endMessage = `Votre équipe (${Game.mainPlayer.faction
          }) n'a pas gagné, mais au moins vous êtes aussi deuxième, égalité avec les ${winningFactions
            .filter((f) => f !== this.mainPlayer.faction)
            .join("")}s !`;
      }
    } else if (winningFactions.length === 3) {
      endMessage = `Egalité parfaite ! Tout le monde a ${scores[0].score} points! Incroyable.`;
    }

    const message = scores
      .map(({ faction, score }) => `${faction}: ${score}`)
      .join(",\n  ");
    const announce = `Fin de partie!\n\nScores:\n  ${message}.\n\n${endMessage}`;

    new GlobalAnnounce(announce, endGameData.messageDuration);
  }

  static getPlayerById(id: string | undefined): Player | undefined {
    return Game.players.find((p) => p.id === id);
  }

  static chooseClass(chosenClass) {
    SkillBar.destroy();
    Game.skillbar = new SkillBar();
    SkillBar.addSkills(SkillFactory.createBulk(chosenClass.skills));
    Stats.levelInfo.updateCurrentLevel(chosenClass.name);
  }

  static start(data) {
    Game.keyboardHandler = new KeyboardHandler();
    if (Stats.timer === undefined) {
      Stats.createTimer(data.timeStampStart, data.timerDuration);
    }
    if (Game.players) {
      for (const player of Game.players) {
        player.remove();
      }
    }
    Game.players = [];
    Game.mainPlayerId = data.id;
    for (const user of data.users) {
      if (user.id === Game.mainPlayerId) {
        Game.mainPlayer = new MainPlayer(
          user.id,
          user.name,
          user.position,
          user.color,
          user.emoji
        );
        Game.mainPlayer.setScore(user.hp);
        Game.players.push(Game.mainPlayer);
        Game.listenPlayerEvent();
      }
    }
    NetworkEventHandler.emit(GameEvents.PLAYER_READY);

    if (this.goldStatElt === undefined) {
      this.goldStatElt = document.createElement("div");
      this.goldStatElt.classList.add(GameConstants.STAT_GOLD);
      this.skillbarWrapper.appendChild(this.goldStatElt);
    }

    if (this.healthBar === undefined) {
      this.healthBar = new HealthBar({
        hp: this.mainPlayer.score,
        maxHp: this.mainPlayer.maxHp,
        size: this.mainPlayer.size,
        hpToLvlUp: this.mainPlayer.scoreToEvolve,
        hpToLvlDown: this.mainPlayer.scoreToDevolve,
      });
      this.healthBar.addTo(this.skillbarWrapper);
    }
  }

  static listenPlayerEvent() {
    NetworkEventHandler.on(
      GameEvents.PLAYER_TAKE_DAMAGE,
      Game.mainPlayer.takeDamage
    );
    NetworkEventHandler.on(GameEvents.PLAYER_HEAL, Game.mainPlayer.healSelf);
  }

  static updatePlayer(playerData: PlayerData) {
    const player = Game.players.find((p) => p.id === playerData.id);
    if (!playerData || !player) return;
    player.update(playerData);

    const flag = playerData.inventory.find(
      (obj) => obj.type === GameObjectType.FLAG
    );
    if (flag) {
      switch (flag.faction) {
        case FactionNames.PUNK:
          AnimationGenerator.addClassAnimation(
            GameConstants.HAS_PUNK_FLAG,
            player.id
          );
          break;
        case FactionNames.CONSERVATIST:
          AnimationGenerator.addClassAnimation(
            GameConstants.HAS_CONSERVATIST_FLAG,
            player.id
          );
          break;
        case FactionNames.TECHNOPHILE:
          AnimationGenerator.addClassAnimation(
            GameConstants.HAS_TECHNOPHILE_FLAG,
            player.id
          );
          break;
        default:
          AnimationGenerator.addClassAnimation(
            GameConstants.HAS_FLAG,
            player.id
          );
      }
    } else {
      AnimationGenerator.removeClassAnimation(
        GameConstants.HAS_FLAG,
        player.id
      );
      AnimationGenerator.removeClassAnimation(
        GameConstants.HAS_PUNK_FLAG,
        player.id
      );
      AnimationGenerator.removeClassAnimation(
        GameConstants.HAS_CONSERVATIST_FLAG,
        player.id
      );
      AnimationGenerator.removeClassAnimation(
        GameConstants.HAS_TECHNOPHILE_FLAG,
        player.id
      );
    }

    Stats.updatePlayersResume(
      Game.mainPlayer.faction,
      playerData.playersResume
    );

    if (player.disabled) {
      player.btnElt.classList.add(GameConstants.ANIMATION_DISABLED_CLASS_NAME);
    } else {
      player.btnElt.classList.remove(
        GameConstants.ANIMATION_DISABLED_CLASS_NAME
      );
    }

    if (player.id === Game.mainPlayerId) {
      Stats.refresh(playerData)
    }
    Stats.allPlayersCounter = playerData.allPlayerCounter;

    this.healthBar.update({
      hp: this.mainPlayer.score,
      maxHp: this.mainPlayer.maxHp,
      size: this.mainPlayer.size,
      hpToLvlUp: this.mainPlayer.scoreToEvolve,
      hpToLvlDown: this.mainPlayer.scoreToDevolve,
    });
    player.refresh();
  }

  static updateSkillPlayer(playerSkillData) {
    const player = Game.mainPlayer;
    const skill = SkillBar.getSkillFromName(playerSkillData.skillName);
    const stack = playerSkillData.stack;

    if (skill) {
      if (skill.stack > stack) {
        skill.showCooldown(playerSkillData.cooldownDuration);
      }
      skill.stack = stack;
      skill.refresh();
    }

    if (playerSkillData.skillName === GameActions.PECNO_TP) {
      player.tpStack = playerSkillData?.stack;
    }

    player.refresh();
  }

  static onSkillFeedback(skillFeedbackData: SkillFeedbackData) {
    skillFeedbackData.sourcePlayer = Game.getPlayerById(
      skillFeedbackData.idSource
    );
    skillFeedbackData.targetPlayer = Game.getPlayerById(
      skillFeedbackData.idTarget
    );
    const skill = ALL_SKILLS.find(
      (s) => skillFeedbackData.skillName === s.event_name
    );
    skill?.onSkillFeedback(skillFeedbackData);
  }

  static lvlDown(classes) {
    AnimationGenerator.popAnimation(
      "Lvl down",
      Game.mainPlayer.position,
      "brown",
      false,
      AnimationGenerator.POP_SHIFT
    );
    Game.chooseClass(classes);
  }

  static scoreShift(scoreShiftData: ScoreShiftData) {
    let source: GameObject | Player | undefined;
    source = Game.getPlayerById(scoreShiftData.idSource) as
      | Player
      | GameObject
      | undefined;
    if (!source) {
      source = Game.getGameObjectById(scoreShiftData.idSource);
    }

    let target: GameObject | Player | undefined;
    target = Game.getPlayerById(scoreShiftData.idTarget);
    if (!target) {
      target = Game.getGameObjectById(scoreShiftData.idTarget);
    }
    const hitAnimationColor = AnimationGenerator.getScoreShiftColor(scoreShiftData)
    if (!source && !target) {
      return;
    }

    if (target) {
      if (!source) {
        source = target;
      }

      AnimationGenerator.handleCriticalAnimation(scoreShiftData, target);
      Game._handleScoreShift(
        source,
        target,
        scoreShiftData,
        hitAnimationColor
      );
    }
  }

  static _handleScoreShift(
    sourcePlayer: Player | GameObject,
    target: Player | GameObject,
    scoreShiftData: ScoreShiftData,
    hitAnimationColor: string
  ) {
    hitAnimationColor = AnimationGenerator.getScoreShiftColor(scoreShiftData);
    if (scoreShiftData.skill) {
      if (sourcePlayer !== target) {
        const skill = ALL_SKILLS.find(
          (s) => s.event_name === scoreShiftData.skill
        );
        skill?.handleAnimation(
          sourcePlayer,
          target,
          scoreShiftData,
          hitAnimationColor
        );
      } else {
        AnimationGenerator.handleScoreShiftAnimation(
          sourcePlayer,
          target,
          scoreShiftData,
          hitAnimationColor
        );
      }
    }

    if (target.setScore) {
      target?.setScore(scoreShiftData.scoreTarget);
    }
  }

  static triggerDeath(e) {
    if (Game.isDeathTriggered) {
      return;
    }
    Game.isDeathTriggered = true;
    const REPOP_WAIT_DURATION = 10000;
    const id = e.playerId;
    const player = Game.getPlayerById(id);
    if (!player) return;

    Game.deathOverlayElt.classList.add("visible");
    const line1 = document.createElement("p");
    const line2 = document.createElement("p");
    const line3 = document.createElement("p");
    const repopCounter = document.createElement("p");

    line1.innerText = "WHY ARE YOU DEAD?";
    line2.innerText = "WHO SAYS I'M DEAD?";
    line3.innerText = "YOU ARE DEAD.";
    repopCounter.innerText = "10";

    Game.deathOverlayElt.appendChild(line1);
    Game.deathOverlayElt.appendChild(line2);
    Game.deathOverlayElt.appendChild(line3);
    Game.deathOverlayElt.appendChild(repopCounter);
    repopCounter.innerText = "10";
    setInterval(() => {
      repopCounter.innerText = "" + (parseInt(repopCounter.innerText) - 1);
    }, 1000);
    setTimeout(() => {
      Game.deathOverlayElt.classList.remove("visible");
      Game.deathOverlayElt.removeChild(line2);
      Game.deathOverlayElt.removeChild(line3);
      Game.deathOverlayElt.removeChild(repopCounter);
      Game.deathOverlayElt.removeChild(line1);
      Game.isDeathTriggered = false;
    }, REPOP_WAIT_DURATION);
  }

  static enterRoom(playerData: PlayerData) {
    let player: Player | undefined = undefined;

    if (playerData.id !== Game.mainPlayerId) {
      player = new RemotePlayer(
        playerData.id,
        playerData.name,
        playerData.position,
        playerData.color,
        playerData.hp,
        playerData.maxHp,
        playerData.size,
        playerData.emoji,
        playerData.ownedGold
      );
      Game.players.push(player);
      playerData.alterations.forEach((a) => {
        Game.refreshAlteration({
          alteration: a,
          playerId: player?.id,
        });
      });
    } else {
      player = Game.mainPlayer;
    }

    player?.update(playerData);
  }

  static leaveRoom(playerData: any) {
    if (playerData.id !== Game.mainPlayerId) {
      Game.players = Game.players.filter((p) => {
        if (p.id === playerData.id) {
          p.remove();
          return false;
        } else {
          return true;
        }
      });
    }
  }

  static updatePlayers(playersData: PlayerData[]) {
    for (const playerData of playersData) {
      const player =
        playerData.id === Game.mainPlayerId
          ? Game.mainPlayer
          : Game.getPlayerById(playerData.id);

      if (player) {
        player.update(playerData);
      } else {
        const remotePlayer = new RemotePlayer(
          playerData.id,
          playerData.name,
          playerData.position,
          playerData.color,
          playerData.hp,
          playerData.maxHp,
          playerData.size,
          playerData.emoji,
          playerData.ownedGold
        );
        Game.players.push(remotePlayer);
      }
    }

    for (const remotePlayer of Game.players) {
      if (!playersData.find((p) => p.id === remotePlayer.id)) {
        if (remotePlayer.id !== Game.mainPlayerId) {
          remotePlayer.remove();
        }
      }
    }
  }

  static roomUpdate(roomData: RoomData) {
    Game.updatePlayers(roomData.players);
    for (const gameobject of Game.gameObjects) {
      if (!roomData.gameObjects.find((g) => g.id === gameobject.id)) {
        gameobject.remove();
      }
    }

    for (const gameobject of roomData.gameObjects) {
      const alreadyPresentGameObject = Game.gameObjects.find(
        (g) => g.id === gameobject.id
      );

      if (!alreadyPresentGameObject) {
        const type = gameobject.type
        const id = gameobject.id
        const position = gameobject.position
        const size = gameobject.size
        const cooldown = gameobject.cooldown
        const faction = gameobject?.faction
        const amount = gameobject?.goldAmount
        const hp = gameobject?.hp
        const maxHp = gameobject?.maxHp
        const activated = gameobject?.activated
        const value = gameobject?.value
        switch (type) {
          case GameObjectType.FLAG:
            Game.gameObjects.push(
              new Flag(id, position, size, faction, cooldown)
            );
            break;
          case GameObjectType.GOLD_MINE:
            Game.gameObjects.push(
              new GoldMine(id, position, size, faction, cooldown, amount, hp, maxHp)
            );
            break;
          case GameObjectType.TOWER:
            Game.gameObjects.push(new Tower(id, position, size, faction, cooldown, amount, hp, maxHp))
            break
          case GameObjectType.PLAYER_TOWER:
            Game.gameObjects.push(new Tower(id, position, size, faction, cooldown, amount, hp, maxHp))
            break
          case GameObjectType.FLAGBASE:
            Game.gameObjects.push(
              new FlagBase(id, position, size, faction, cooldown)
            );
            break;
          case GameObjectType.GOLD_DEPOSIT:
            Game.gameObjects.push(
              new GoldDeposit(id, position, size, faction, cooldown, amount, hp)
            );
            break;
          case GameObjectType.PORTAL:
            Game.gameObjects.push(
              new Portal(id, position, size, cooldown, activated, faction)
            );
            break;
          case GameObjectType.MINE:
            Game.gameObjects.push(
              new Mine(id, position, size, faction, cooldown)
            );
            break;
          case GameObjectType.TOTEM:
            Game.gameObjects.push(
              new Totem(id, position, size, faction, cooldown, hp)
            );
            break;
          case GameObjectType.POWER_UP:
            Game.gameObjects.push(new PowerUp(id, position, size, cooldown));
            break;
          case GameObjectType.GOLD:
            Game.gameObjects.push(
              new Gold(id, position, size, cooldown, value)
            );
            break;
        }
      } else {
        alreadyPresentGameObject.update(gameobject);
      }
    }
    roomData.players.forEach((p) => {
      p.alterations.forEach((a) => {
        Game.refreshAlteration({
          alteration: a,
          playerId: p?.id,
        });
      });
    });
    let bglink = "";
    let color = "";
    switch (roomData.type) {
      case RoomTypes.FACTION:
        bglink = "../assets/bg2.jpg";
        break;

      case RoomTypes.BASE:
      case RoomTypes.SHOP:
        bglink = "../assets/bg.png";
        break;
      case RoomTypes.GOLDZONE:
        bglink = "../assets/bg_goldZone.png";
        break;
      case RoomTypes.PUNKZONE:
        bglink = "../assets/texture.png";
        color = "#007700";
        break;
      case RoomTypes.CONSERVATISTZONE:
        bglink = "../assets/texture.png";
        color = "#990000";
        break;
      case RoomTypes.TECHNOPHILEZONE:
        bglink = "../assets/texture.png";
        color = "#000099";
        break;

      case RoomTypes.INTERTPUNK:
        bglink = "../assets/texture.png";
        color = "#229922";
        break;
      case RoomTypes.CHESTZONE:
        bglink = "../assets/bg_interpunk.png";
        break;

      case RoomTypes.INTERCONSERVATEUR:
        bglink = "../assets/bg_interconservatist.png";
        bglink = "../assets/texture.png";
        color = "#bb2222";
        break;
      case RoomTypes.INTERTECHNOPHILE:
        bglink = "../assets/bg_intertechnophile.png";
        bglink = "../assets/texture.png";
        color = "#5555ff";
        break;
    }
    Game.gameZone.style.backgroundImage = `url('${bglink}')`;
    Game.gameZone.style.backgroundColor = color;
  }

  static refreshAlteration(e) {
    const player = Game.getPlayerById(e.playerId);
    if (player) {
      AnimationGenerator.addClassAnimation(e.alteration.name, e.playerId);
    }
  }

  static alterationOn(e) {
    console.log(e);
    const player = Game.getPlayerById(e.playerId);
    if (player) {
      AnimationGenerator.addClassAnimation(e.alteration.name, e.playerId);
    }
    if (player && player.id === Game.mainPlayer.id) {
      Game.alterationList.addAlteration(e.alteration);
    }
  }

  static alterationOff(e) {
    const player = Game.getPlayerById(e.playerId);
    if (player) {
      AnimationGenerator.removeClassAnimation(e.alteration.name, e.playerId);
    }
    if (player && player.id === Game.mainPlayer.id) {
      Game.alterationList.removeAlteration(e.alteration);
    }
  }

  static getGameObjectsByType(gameObjectType: GameObjectType) {
    return Game.gameObjects.filter((g) => g.type === gameObjectType);
  }

  static getGameObjectById(gameObjectId) {
    return Game.gameObjects.find((g) => g.id === gameObjectId);
  }

  static emote(e) {
    AnimationGenerator.emote(e.emote, e.playerId);
  }
}
