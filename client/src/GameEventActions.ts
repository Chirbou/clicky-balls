export const GameActions = {
  CONNECT: 'connection',
  DISCONNECT: 'disconnect',
  HEAL_SELF: 'heal_self',
  MISS_CLICK: 'miss_click',
  SELECT_LVL_UP: 'select_lvl_up',
  PECNO_ATK: 'PECNO_ATK',
  PECNO_TP: 'PECNO_TP',
  TANK_INVINCIBILITY: 'TANK_INVINCIBILITY',
  TANK_SILENCE: 'TANK_SILENCE',
  TOGGLE_SILENCE: 'TOGGLE_SILENCE',
  DPS_ATK: 'DPS_ATK',
  HEALER_HEAL: 'HEALER_HEAL',
  BERSERK_SUFFER: 'BERSERK_SUFFER',
  BERSERK_BUBBLE: 'BERSERK_BUBBLE',
  BERSERK_SHELL: 'BERSERK_SHELL',
  PALADIN_GRAB: 'PALADIN_GRAB',
  PALADIN_BUBBLE: 'PALADIN_BUBBLE',
  ROGUE_DIVERSION: 'ROGUE_DIVERSION',
  ROGUE_FURTIVE_HIT: 'ROGUE_FURTIVE_HIT',
  ROGUE_STAB: 'ROGUE_STAB',
  POISON_BOMBKIPU: 'POISON_BOMBKIPU',
  POISON_VAMPIRISM: 'POISON_VAMPIRISM',
  POISON_FANG: 'POISON_FANG',
  PESTIFEROUS_CONTAGION: 'PESTIFEROUS_CONTAGION',
  PESTIFEROUS_DEFLAGRATION: 'PESTIFEROUS_DEFLAGRATION',
  PESTIFEROUS_DISPEL: 'PESTIFEROUS_DISPEL',
  DRUID_HOTAOE: 'DRUID_HOTAOE',
  DRUID_STUN: 'DRUID_STUN',
  PRIEST_HANGON: 'PRIEST_HANGON',
  PRIEST_HEALZONE: 'PRIEST_HEALZONE',
  PRIEST_DISPEL: 'PRIEST_DISPEL',
  OCCULTIST_SUCKER: 'OCCULTIST_SUCKER',
  OCCULTIST_BLOW: 'OCCULTIST_BLOW',
  OCCULTIST_FUCKER: 'OCCULTIST_FUCKER',
  TRAPPER_STEALTH: 'TRAPPER_STEALTH',
  TRAPPER_SNIPER: 'TRAPPER_SNIPER',
  TRAPPER_MINE: 'TRAPPER_MINE',
  SHAMAN_TOTEM: 'SHAMAN_TOTEM',
  SHAMAN_UNDERPRESSURE: 'SHAMAN_UNDERPRESSURE',
  SHAMAN_THUNDERBOLT: 'SHAMAN_THUNDERBOLT',
  MECHANOTHERAPIST_TOWER_BUFF: 'MECHANOTHERAPIST_TOWER_BUFF',
  MECHANOTHERAPIST_TOWER_BUILD: 'MECHANOTHERAPIST_TOWER_BUILD',
  MECHANOTHERAPIST_TOWER_HACK: 'MECHANOTHERAPIST_TOWER_HACK',
  AOE_DAMAGE: 'AOE_DAMAGE',
  GAMEOBJECT_INTERACTION: 'GAMEOBJECT_INTERACTION',
  DEVOLVE: 'DEVOLVE',
  EMOTE: 'EMOTE'
}

export const GameEvents = {
  START_GAME: 'start_game',
  PLAYER_READY: 'player_ready',
  PLAYER_SELECTION_READY: 'player_selection_ready',
  PLAYER_JOIN: 'player_join',
  PLAYER_LEAVE: 'player_leave',
  UPDATE: 'update',
  PLAYER_UPDATE: 'player_update',
  PLAYER_SKILL_UPDATE: 'player_skill_update',
  PLAYER_TAKE_DAMAGE: 'player_take_damage',
  PLAYER_HEAL: 'player_heal',
  PLAYER_TP: 'player_tp',
  DEATH: 'death',
  LVL_UP: 'lvl_up',
  LVL_DOWN: 'lvl_down',
  CAN_EVOLVE: 'CAN_EVOLVE',
  CANT_EVOLVE: 'CANT_EVOLVE',
  TIMER_REFRESH: 'TIMER_REFRESH',
  SCORE_SHIFT_DATA: 'score_shift_data',
  SKILL_FEEDBACK: 'cooldown_data',
  LEAVE_ROOM: 'LEAVE_ROOM',
  ENTER_ROOM: 'ENTER_ROOM',
  ROOM_UPDATE: 'ROOM_UPDATE',
  ALTERATION_ON: 'ALTERATION_ON',
  ALTERATION_OFF: 'ALTERATION_OFF',
  GAMEOBJECT_ON: 'GAMEOBJECT_ON',
  GAMEOBJECT_OFF: 'GAMEOBJECT_OFF',
  GAMEOBJECT_MISS: 'GAMEOBJECT_MISS',
  GAMEOBJECT_COMPLETE: 'GAMEOBJECT_COMPLETE',
  FLAG_ANNOUNCE: 'FLAG_ANNOUNCE',
  END_GAME: 'END_GAME',
  OPENING_GATE: 'OPENING_GATE',
  UPDATE_CANON: 'UPDATE_CANON',
  CONNECTED: 'connected',
  GAME_STATE_SELECTION: 'GAME_STATE_SELECTION',
  GAME_STATE_PANEL : 'GAME_STATE_PANEL',
  EMOTE: 'EMOTE',
}
