import { Game } from "./Game"
import { GameConstants } from "./GameConstants"
import { GameObject } from "./GameObject"
import { Player } from "./Player"
import { SkillNames } from "./SkillNames"
import { Util } from "./Util"
import { Coordinate } from "./utils/InterfaceUtils"

export class Bullet {
  targetId: string
  createAt: number
  duration: number
  bulletElt: HTMLDivElement

  constructor(source: GameObject | Player, target: GameObject | Player, skill: SkillNames, callback: () => void) {
    this.targetId = target.id
    this.createAt = new Date().getTime()
    const BULLET_SPEED = skill === SkillNames.HEALER_HEAL ? 600 : 300
    this.bulletElt = document.createElement('div')
    Game.animationWrapper.appendChild(this.bulletElt)

    const distance = Util.getDistance(source.position, target.position)
    this.duration = (distance / 1000) * BULLET_SPEED

    this.bulletElt.style.transitionDuration = `${this.duration}ms`
    this.bulletElt.classList.add(GameConstants.BULLET_CLASS)
    this.bulletElt.classList.add(`bullet--${skill}`)

    this.bulletElt.style.top = `${source.position.y}px`
    this.bulletElt.style.left = `${source.position.x}px`

    window.setTimeout(() => {
      this.bulletElt.style.top = `${target.position.y}px`
      this.bulletElt.style.left = `${target.position.x}px`
    }, 10)

    window.setTimeout(() => {
      this.bulletElt.remove()
      callback()
    }, this.duration)
  }

  update(position: Coordinate) {
    const updateAt = new Date().getTime()
    this.bulletElt.style.transitionDuration = `${this.duration - (updateAt - this.createAt)}ms`
    this.bulletElt.style.top = `${position.y}px`
    this.bulletElt.style.left = `${position.x}px`
  }
}
