import { GameConstants } from './GameConstants.js'
import { Game } from './Game.js'
import { Player } from './Player.js'
import { FactionNames } from './ClassNames.js'
import { Util } from './Util.js'
import { GameObject } from './GameObject.js'
import { SkillNames } from './SkillNames.js'
import { Coordinate } from './utils/InterfaceUtils.js'
import { Bullet } from './Bullet.js'

export class AnimationGenerator {
  static COMMON_SHIFT = 35
  static POP_SHIFT = 35 // todo : move in constants

  static popAnimation(text, coords, color = 'green', random = true, shift = 0) {
    if (text.length === 0 || text === '+0' || text === '-0' || !coords.x || !coords.y) {
      return
    }
    const animation = document.createElement('span')
    const randomDelta = (random ? Math.floor(Math.random() * 20 - 5) : 5) + AnimationGenerator.COMMON_SHIFT + shift
    animation.innerText = text
    animation.classList.add(GameConstants.ANIMATION_SCORE_ID)
    animation.classList.add(GameConstants.ANIMATION_NO_SELECT_CLASS)
    animation.style.left = coords.x + randomDelta + shift + 'px'
    animation.style.top = coords.y + randomDelta - shift + 'px'
    animation.style.color = color
    animation.style.fontSize = '3em'

    Game.animationWrapper.appendChild(animation)
    window.setTimeout(() => {
      Game.animationWrapper.removeChild(animation)
    }, 750)
  }

  static bullets: Bullet[] = []

  static handleBullet(source: GameObject | Player, target: GameObject | Player, skill: SkillNames, callback: any) {
    const bullet = new Bullet(source, target, skill, callback)
    this.bullets.push(bullet)
  }

  static updateBullet(targetId: string, position: Coordinate) {
    for (const bullet of AnimationGenerator.bullets) {
      if (bullet.targetId === targetId) {
        bullet.update(position)
      }
    }
  }

  static handleLaser(source: GameObject | Player, target: GameObject | Player, skill: SkillNames, callback: any) {
    const BULLET_SHOWN_DURATION = 300
    const bullet = document.createElement('div')
    Game.animationWrapper.appendChild(bullet)

    bullet.classList.add(GameConstants.LASER_CLASS)
    bullet.classList.add(`laser--${skill}`)

    bullet.style.top = `${source.position.y}px`
    bullet.style.left = `${source.position.x}px`
    bullet.style.transformOrigin = 'top left'
    bullet.style.transition = `all ${BULLET_SHOWN_DURATION / 2}ms linear`

    const length = Util.getDistance(source.position, target.position)
    let width = 70

    bullet.style.width = `${0}px`
    bullet.style.height = `${length}px`

    const angle = Util.getAngle(source.position, target.position)
    bullet.style.transform = `rotate(${angle}deg)`

    window.setTimeout(() => {
      bullet.style.width = `${width}px`
      const coeff = Math.abs(Util.degreesToRadians(angle) / (Math.PI) * 2)
      const coeff2 = Util.degreesToRadians(angle) / (Math.PI) * 2

      if (coeff < 1) {
        if (coeff2 < 0) {
          bullet.style.top = `${source.position.y + (width / 2) * coeff}px`
        } else {
          bullet.style.top = `${source.position.y - (width / 2) * coeff}px`
        }
      } else {
        if ((1 - coeff2) < 0) {
          bullet.style.top = `${source.position.y - (width / 2) * (1 - (coeff - 1))}px`
        } else {
          bullet.style.top = `${source.position.y + (width / 2) * (1 - (coeff - 1))}px`
        }
      }
      bullet.style.left = `${source.position.x - (width / 2) * (1 - coeff)}px`
    }, 10)

    window.setTimeout(() => {
      width = 0
      bullet.style.width = `${width}px`
      const coeff = Math.abs(Util.degreesToRadians(angle) / (Math.PI) * 2)
      const coeff2 = Util.degreesToRadians(angle) / (Math.PI) * 2

      if (coeff < 1) {
        if (coeff2 < 0) {
          bullet.style.top = `${source.position.y + (width / 2) * coeff}px`
        } else {
          bullet.style.top = `${source.position.y - (width / 2) * coeff}px`
        }
      } else {
        if ((1 - coeff2) < 0) {
          bullet.style.top = `${source.position.y - (width / 2) * (1 - (coeff - 1))}px`
        } else {
          bullet.style.top = `${source.position.y + (width / 2) * (1 - (coeff - 1))}px`
        }
      }
      bullet.style.left = `${source.position.x - (width / 2) * (1 - coeff)}px`
    }, BULLET_SHOWN_DURATION / 2)

    window.setTimeout(() => {
      bullet.remove()
      callback()
    }, BULLET_SHOWN_DURATION)

  }

  static popSlowAnimation(text, coords, color = 'green', random = true, shift = 0) {
    if (text.length === 0 || text === '+0' || text === '-0' || !coords.x || !coords.y) {
      return
    }

    const animation = document.createElement('span')
    const randomDelta = (random ? Math.floor(Math.random() * 20 - 5) : 5) + AnimationGenerator.COMMON_SHIFT + shift
    animation.innerText = text
    animation.classList.add(GameConstants.ANIMATION_CRITICAL_ID)
    animation.classList.add(GameConstants.ANIMATION_NO_SELECT_CLASS)
    animation.style.left = coords.x + randomDelta + shift + 'px'
    animation.style.top = coords.y + randomDelta - shift + 'px'
    animation.style.color = color
    animation.style.fontSize = '2vw'

    Game.animationWrapper.appendChild(animation)
    window.setTimeout(() => {
      Game.animationWrapper.removeChild(animation)
    }, 750)
  }

  static popCountdownAnimation(text, coords, color = 'green', random = true, shift = 0) {
    if (text.length === 0 || text === '+0' || text === '-0' || !coords.x || !coords.y) {
      return
    }

    const animation = document.createElement('span')
    const randomDelta = (random ? Math.floor(Math.random() * 20 - 5) : 5) + AnimationGenerator.COMMON_SHIFT + shift
    const txtWrapper = document.createElement('span')
    txtWrapper.classList.add('txt-wrapper')
    txtWrapper.innerText = text
    animation.appendChild(txtWrapper)
    animation.classList.add(GameConstants.ANIMATION_COUNTDOWN_ID)
    animation.classList.add(GameConstants.ANIMATION_NO_SELECT_CLASS)
    animation.style.left = coords.x + randomDelta + shift + 'px'
    animation.style.top = coords.y + randomDelta - shift + 'px'
    animation.style.color = color

    Game.animationWrapper.appendChild(animation)
    window.setTimeout(() => {
      Game.animationWrapper.removeChild(animation)
    }, 1000)
  }

  static popTextAnimation(text, coords, color = 'green', random = true, shift = 0, small = false) {
    if (text.length === 0 || text === '+0' || text === '-0' || Number(text) < 0 || !coords.x || !coords.y) {
      return
    }

    let finalCoordsX = coords.x
    const gameZoneWidth = parseInt(Game.gameZone.style.width.split('px')[0])

    if (coords.x + 600 >= gameZoneWidth) {
      finalCoordsX = gameZoneWidth - 600
    }

    const animation = document.createElement('span')
    const randomDelta = (random ? Math.floor(Math.random() * 20 - 5) : 5) + AnimationGenerator.COMMON_SHIFT + shift
    animation.innerText = text
    animation.classList.add(GameConstants.ANIMATION_TEXT_ID)
    animation.classList.add(GameConstants.ANIMATION_NO_SELECT_CLASS)
    animation.style.left = finalCoordsX + randomDelta + shift + 'px'
    animation.style.top = coords.y + randomDelta - shift + 'px'
    animation.style.color = color
    animation.style.fontSize = small ? '1.5vh' : '3.5vh'

    Game.animationWrapper.appendChild(animation)
    window.setTimeout(() => {
      Game.animationWrapper.removeChild(animation)
    }, 3000)
  }

  static addClassAnimation(className, playerId: string) {
    const player = Game.getPlayerById(playerId)
    AnimationGenerator._addClassAnimation(className, player)
  }

  static _addClassAnimation(className, player: Player | undefined) {
    if (player && player.btnElt && player.btnElt.classList) {
      player.btnElt.classList.add(className)
    }
  }
  static removeClassAnimation(className, playerId: string) {
    const player = Game.getPlayerById(playerId)
    AnimationGenerator._removeClassAnimation(className, player)
  }

  static _removeClassAnimation(className, player: Player | undefined) {
    if (player && player.btnElt && player.btnElt.classList) {
      player.btnElt.classList.remove(className)
    }
  }

  static popAoe(animationDuration: number, aoeRadius: number, epicenterPosition: Coordinate, className: string) {
    const aoeAnimation = document.createElement('span')
    aoeAnimation.classList.add(className)
    aoeAnimation.style.left = `${epicenterPosition.x - aoeRadius}px`
    aoeAnimation.style.top = `${epicenterPosition.y - aoeRadius}px`
    aoeAnimation.style.width = `${aoeRadius * 2}px`
    aoeAnimation.style.height = `${aoeRadius * 2}px`
    aoeAnimation.style.animationDuration = `${animationDuration}ms`

    Game.animationWrapper.appendChild(aoeAnimation)
    window.setTimeout(() => {
      aoeAnimation.remove()
    }, animationDuration)
  }

  static getScoreShiftColor(scoreShiftData) {
    if (scoreShiftData.idSource !== Game.mainPlayerId && scoreShiftData.idTarget !== Game.mainPlayerId) {
      return 'grey'
    }

    switch (scoreShiftData.type) {
      case 'heal':
        return 'green'
      case 'damage':
        return 'red'
      default:
        return 'white'
    }
  }

  static handleScoreShiftAnimation(sourcePlayer, targetPlayer, scoreShiftData, hitAnimationColor) {
    const sign = scoreShiftData.type === 'heal' ? '+' : '-'
    if (!scoreShiftData.isCritic) {
      AnimationGenerator
        .popAnimation(
          `${sign}${scoreShiftData.value}`,
          targetPlayer.position || sourcePlayer.position, hitAnimationColor
        )
    } else {
      AnimationGenerator.popSlowAnimation(
        `${sign}${scoreShiftData.value}`, targetPlayer.position || sourcePlayer.position,
        hitAnimationColor,
        false
      )
    }

    if (scoreShiftData.type === 'damage' && targetPlayer.id === Game.mainPlayerId && targetPlayer.score < 50) {
      AnimationGenerator.handleScreenDamageAnimation()
    }
  }

  static handleScreenDamageAnimation() {
    if (Game.animationWrapper.classList.contains(GameConstants.SCREEN_DAMAGE_ANIMATION_CLASS)) {
      return
    }
    Game.animationWrapper.classList.add(GameConstants.SCREEN_DAMAGE_ANIMATION_CLASS)
    setTimeout(() => {
      Game.animationWrapper.classList.remove(GameConstants.SCREEN_DAMAGE_ANIMATION_CLASS)
    }, 1000)
  }

  static addDeathAnimation(player) {
    if (player.score <= 0) {
      AnimationGenerator._addClassAnimation(GameConstants.IS_DEAD, player)
      setTimeout(() => {
        AnimationGenerator._removeClassAnimation(GameConstants.IS_DEAD, player)
      }, 10000)
    }
  }

  static handleCriticalAnimation(scoreShiftData, targetPlayer) {
    if (scoreShiftData.isCritic) {
      AnimationGenerator
        .popSlowAnimation('CRITICAL !', targetPlayer.position, 'multi', false, AnimationGenerator.POP_SHIFT)
    }
  }
  static handleAnnounceAnimation(player: Player, text, factionOfFlag: FactionNames) {
    const wrapper = document.createElement('div')
    const information = document.createElement('span')
    const name = document.createElement('span')
    const victim = document.createElement('span')
    name.style.backgroundColor = 'rgb(100, 118, 134)'
    name.style.color = player.color
    name.innerText = player.name
    name.style.fontWeight = 'bolder'
    information.innerText = text
    information.style.color = 'white'
    information.style.fontWeight = 'bold'
    const victimText = Util.getColorNameByFaction(factionOfFlag)
    victimText.toUpperCase()
    victim.innerText = victimText
    victim.style.color = Util.getColorFromFactionName(factionOfFlag)
    victim.style.fontWeight = 'bolder'

    wrapper.classList.add(GameConstants.ANOUNCE_ANIMATION)

    wrapper.appendChild(name)
    wrapper.appendChild(information)
    wrapper.appendChild(victim)
    Game.animationWrapper.appendChild(wrapper)
    window.setTimeout(() => {
      Game.animationWrapper.removeChild(wrapper)
    }, 10000)
  }

  static tooMuchGold() {
    Game.goldStatElt.classList.add('tooMuchGold')
    setTimeout(() => {
      Game.goldStatElt.classList.remove('tooMuchGold')
    }, 2000)
  }

  static emote(emote: string, playerId: string) {
    const player = Game.getPlayerById(playerId)
    if (!player) {
      return
    }
    const emoteElt = document.createElement('span')
    emoteElt.classList.add('emoteAnimation')
    emoteElt.innerText = emote

    player.btnElt.appendChild(emoteElt)
    setTimeout(() => {
      player.btnElt.removeChild(emoteElt)
    }, 3000)
  }
}
