import { GameConstants } from './GameConstants.js'
import { SkillBar } from './Skillbar.js'
import { GameActions } from './GameEventActions.js'
import { Player } from './Player.js'
import { translator } from './translations/i18n.js'
import { AnimationGenerator } from './AnimationGenerator.js'
import { GameObject } from './GameObject.js'
import { ScoreShiftData } from './utils/InterfaceUtils.js'
export class Skill {
  tooltip = 'Personne ne s\'est occupé de moi =('
  name = 'Je n\'ai pas de nom =('
  event_name: string
  isTrigger = false
  position = 0
  stack = 0
  autoskill = false

  imgElt!: HTMLImageElement
  skillElt!: HTMLDivElement
  stackElt!: HTMLSpanElement
  reloadWrapperElt!: HTMLDivElement
  reloadElts: HTMLDivElement[] = []

  constructor(event_name, addSkillToBar = true) {
    this.event_name = event_name
    this.tooltip = translator.t(`skills.${[this.event_name]}.tooltip`)
    this.name = translator.t(`skills.${[this.event_name]}.name`)

    if (addSkillToBar){
      this.skillElt = document.createElement('div')
      this.skillElt.classList.add(GameConstants.SKILL_CLASS)
      this.stackElt = document.createElement('span')
      this.stackElt.innerText = `${this.stack}`
      this.stackElt.classList.add(GameConstants.STACK_ELT_CLASS)
      this.imgElt = document.createElement('img')
      this.skillElt.appendChild(this.stackElt)

      this.skillElt.classList.add('tooltip')
      const tooltipTextElt = document.createElement('span')
      tooltipTextElt.classList.add('tooltiptext')
      tooltipTextElt.innerText = this.tooltip
      this.skillElt.appendChild(tooltipTextElt)

      this.skillElt.appendChild(this.imgElt)
      SkillBar.addSkill(this.event_name, this.skillElt)

      this.reloadWrapperElt = document.createElement('div')
      this.skillElt.appendChild(this.reloadWrapperElt)
    }

  }

  setPosition(index: number) {
    this.position = index
    this.skillElt.dataset.position = `${index}`
  }

  trigger(playerSource: Player, playerTarget: Player, coords) {
    console.log(`Undefined trigger for skill ${this.position} !`)
  }

  select = () => {
    this.skillElt.classList.add(GameConstants.SKILL_SELECTED_CLASS)
  }

  handleAnimation(sourcePlayer: Player|GameObject, target: Player|GameObject, scoreShiftData: ScoreShiftData, hitAnimationColor: string){
    if (
      scoreShiftData.skill === GameActions.DPS_ATK 
      || scoreShiftData.skill === GameActions.SHAMAN_THUNDERBOLT
      ) {
      AnimationGenerator.handleLaser(sourcePlayer, target, scoreShiftData.skill, () => {
        AnimationGenerator.handleScoreShiftAnimation(sourcePlayer, target, scoreShiftData, hitAnimationColor)
      })
    } else {
      if (scoreShiftData.skill){
        AnimationGenerator.handleBullet(sourcePlayer, target, scoreShiftData.skill, () => {
          AnimationGenerator.handleScoreShiftAnimation(sourcePlayer, target, scoreShiftData, hitAnimationColor)
        })
      }
    }
  }

  showCooldown(duration: number) {
    const reloadElt = document.createElement('div')
    reloadElt.classList.add('timer')
    const mask = document.createElement('div')
    mask.classList.add('mask')
    reloadElt.appendChild(mask)
    reloadElt.style.setProperty('--duration', `${duration}`)

    this.reloadElts.push(reloadElt)
    this.skillElt.appendChild(reloadElt)

    setTimeout(() => {
      this.reloadElts.shift()
      reloadElt.remove()
    }, duration)
  }

  refresh(){
    this.stackElt.innerText = `${this.stack}`
    if(this.stack === 0){
      if(this.event_name !== GameActions.PECNO_TP &&
        this.event_name !== GameActions.PECNO_ATK &&
        this.event_name !== GameActions.TRAPPER_SNIPER &&
        this.event_name !== GameActions.HEALER_HEAL) {
        SkillBar.selectSkill(0)
      }
    }
  }

  onSkillFeedback(feedbackSkillData){
    // pass
  }
}
