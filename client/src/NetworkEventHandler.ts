import { Game } from "./Game.js";
import { GameEvents } from "./GameEventActions.js";
import { Display } from "./Display.js";
import { GameObject } from "./GameObject.js";
import { AnimationGenerator } from "./AnimationGenerator.js";
import { Stats } from "./Stats.js";
import { GlobalAnnounce } from "./Announces.js";

export class NetworkEventHandler {
  static socket;

  static init(socket) {
    NetworkEventHandler.socket = socket;
    NetworkEventHandler.listenEvents();
  }

  static emit(eventName, data: unknown = null) {
    Display.scaleOutputEventValues(data);
    NetworkEventHandler.socket.emit(eventName, data);
  }

  static on(eventName, callback) {
    NetworkEventHandler.socket.on(eventName, callback);
  }

  static handleEvent(eventName, event, callback) {
    if (Game.ready || eventName === GameEvents.CONNECTED || eventName === GameEvents.GAME_STATE_SELECTION) {
      callback();
    }
  }

  static listenEvents() {
    NetworkEventHandler.on(GameEvents.CONNECTED, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.CONNECTED, event, () => {
        Game.classesDef = event.classesDef
        Game.classesLocks = event.classesLocks
        Game.showSelectionScreen();
      });
    });

    NetworkEventHandler.on(GameEvents.GAME_STATE_SELECTION, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.GAME_STATE_SELECTION, event, () => {
        Game.selectionScreen.update(event);
      });
    });

    NetworkEventHandler.on(GameEvents.GAME_STATE_PANEL, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.GAME_STATE_PANEL, event, () => {
        Stats.GameStatsPanel.generalUpdate(event)
      });
    });

    NetworkEventHandler.on(GameEvents.START_GAME, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.START_GAME, event, () => {
        const updatedEvent = Display.scaleInputEventValues(event);
        Game.start(updatedEvent);
      });
    });

    NetworkEventHandler.on(GameEvents.PLAYER_UPDATE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.PLAYER_UPDATE, event, () => {
        const updatedEvent = Display.scaleInputEventValues(event);
        Game.updatePlayer(updatedEvent);
      });
    });

    NetworkEventHandler.on(GameEvents.ALTERATION_OFF, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.ALTERATION_OFF, event, () => {
        Game.alterationOff(event);
      });
    });

    NetworkEventHandler.on(GameEvents.ALTERATION_ON, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.ALTERATION_ON, event, () => {
        Game.alterationOn(event);
      });
    });

    NetworkEventHandler.on(GameEvents.PLAYER_SKILL_UPDATE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.PLAYER_SKILL_UPDATE, event, () => {
        Game.updateSkillPlayer(event);
      });
    });

    NetworkEventHandler.on(GameEvents.CAN_EVOLVE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.CAN_EVOLVE, event, () => {
        Stats.showEvolveButton(event);
      });
    });

    NetworkEventHandler.on(GameEvents.CANT_EVOLVE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.CANT_EVOLVE, event, () => {
        Stats.hideEvolveButton();
      });
    });

    NetworkEventHandler.on(GameEvents.LVL_DOWN, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.LVL_DOWN, event, () => {
        Game.lvlDown(event);
      });
    });

    NetworkEventHandler.on(GameEvents.GAMEOBJECT_ON, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.GAMEOBJECT_ON, event, () => {
        GameObject.gameObjectOn(event);
      });
    });

    NetworkEventHandler.on(GameEvents.GAMEOBJECT_MISS, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.GAMEOBJECT_MISS, event, () => {
        GameObject.gameObjectMiss(event);
      });
    });

    NetworkEventHandler.on(GameEvents.GAMEOBJECT_OFF, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.GAMEOBJECT_OFF, event, () => {
        GameObject.gameObjectOff(event);
      });
    });

    NetworkEventHandler.on(GameEvents.GAMEOBJECT_COMPLETE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.GAMEOBJECT_COMPLETE, event, () => {
        GameObject.gameObjectComplete(event);
      });
    });

    NetworkEventHandler.on(GameEvents.FLAG_ANNOUNCE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.FLAG_ANNOUNCE, event, () => {
        AnimationGenerator.handleAnnounceAnimation(
          event.player,
          event.text,
          event.factionOfFlag
        );
      });
    });

    NetworkEventHandler.on(GameEvents.END_GAME, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.END_GAME, event, () => {
        Game.endGame(event);
      });
    });

    NetworkEventHandler.on(GameEvents.OPENING_GATE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.OPENING_GATE, event, () => {
        new GlobalAnnounce("Que la partie commence", event.messageDuration);
      });
    });

    NetworkEventHandler.on(GameEvents.TIMER_REFRESH, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.TIMER_REFRESH, event, () => {
        if (Stats.timer) {
          Stats.timer.timeStampStart = event.timeStampStart;
          Stats.timer.timerDuration = event.timerDuration;
          Stats.timer.refreshTimerElt();
        }
      });
    });

    NetworkEventHandler.on(GameEvents.SCORE_SHIFT_DATA, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.SCORE_SHIFT_DATA, event, () => {
        Game.scoreShift(event);
      });
    });

    NetworkEventHandler.on(GameEvents.DEATH, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.DEATH, event, () => {
        Game.triggerDeath(event);
      });
    });

    NetworkEventHandler.on(GameEvents.SKILL_FEEDBACK, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.SKILL_FEEDBACK, event, () => {
        const updatedEvent = Display.scaleInputEventValues(event);
        Game.onSkillFeedback(updatedEvent);
      });
    });

    NetworkEventHandler.on(GameEvents.ROOM_UPDATE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.ROOM_UPDATE, event, () => {
        const updatedEvent = Display.scaleInputEventValues(event);
        Game.roomUpdate(updatedEvent);
      });
    });

    NetworkEventHandler.on(GameEvents.LEAVE_ROOM, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.LEAVE_ROOM, event, () => {
        const updatedEvent = Display.scaleInputEventValues(event);
        Game.leaveRoom(updatedEvent);
      });
    });

    NetworkEventHandler.on(GameEvents.ENTER_ROOM, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.ENTER_ROOM, event, () => {
        const updatedEvent = Display.scaleInputEventValues(event);
        Game.enterRoom(updatedEvent);
      });
    });

    NetworkEventHandler.on(GameEvents.EMOTE, (event) => {
      NetworkEventHandler.handleEvent(GameEvents.EMOTE, event, () => {
        const updatedEvent = Display.scaleInputEventValues(event);
        Game.emote(updatedEvent);
      });
    })
  }
}
