import { io } from 'socket.io-client'
import { Game } from './Game.js'
import { NetworkEventHandler } from './NetworkEventHandler.js'

export const CORS_ORIGIN = 'localhost'
const PORT = 3333

let host = '37.187.75.123'

if (window.location.host.match('localhost')) {
  host = `http://localhost:${PORT}`
}

export class App {
  constructor() {
    document.addEventListener('DOMContentLoaded', () => {
      const server = io(`${host}`)
      Game.init()
      NetworkEventHandler.init(server)
    })
  }
}
