import { GameConstants } from './GameConstants.js'
import { Game } from './Game'
import { CanHandleKeyboard, KeyboardActions } from './KeyboardHandler.js'
import { GameActions } from './GameEventActions.js'

export class SkillBar implements CanHandleKeyboard {
  static skills = []
  static selectedIndex = 0
  static skillbarElt?: HTMLDivElement

  constructor() {
    SkillBar.skillbarElt = document.createElement('div')
    SkillBar.skillbarElt.id = GameConstants.SKILLBAR_ID
    document.getElementById(GameConstants.SKILLBAR_WRAPPER)?.appendChild(SkillBar.skillbarElt)
  }

  handleKeyboardNumber(index: number) {
    SkillBar.selectSkill(index - 1)
  }

  handleKeyboardActions(action: KeyboardActions) {
    //
  }

  static destroy() {
    SkillBar.skills = []
    SkillBar.selectedIndex = 0
    document.getElementById(GameConstants.SKILLBAR_ID)?.remove()
  }

  static getSkillFromIndex(i) {
    return SkillBar.skills[i]
  }

  static getSkillFromName(name): any {
    return SkillBar.skills.find((skill) => (skill as any).event_name === name)
  }

  static addSkill(skillName: string, skillElt: any) {
    if (skillName === GameActions.PECNO_TP) return
    SkillBar.skillbarElt?.appendChild(skillElt)
  }

  static addSkills(skillTab) {
    let skillIndex = SkillBar.skills.length
    for (const skill of skillTab) {
      if (skill.event_name === GameActions.PECNO_TP) continue
      (SkillBar.skills as any).push(skill)
      skill.setPosition(skillIndex)
      skillIndex++
    }
    if (SkillBar.skills.length) {
      (SkillBar.skills[0] as any).select()
    }
  }

  static getSelectedSkill() {
    return SkillBar.skills[SkillBar.selectedIndex]
  }

  static selectSkill(index) {
    if (SkillBar.skills.length <= index) {
      return
    }

    SkillBar.selectedIndex = index
    for (const skill of SkillBar.skills) {
      (skill as any).skillElt.classList.remove(GameConstants.SKILL_SELECTED_CLASS)
    }
    (SkillBar.skills[index] as any).select()
    const selectedSkill = SkillBar.skills[index]
    if ((selectedSkill as any).autoskill) {
      Game.mainPlayer.leftClick()
      this.selectSkill(0)
    }
  }
}
