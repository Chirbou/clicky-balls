import { Game } from './Game'
import { GameConstants } from './GameConstants'
import { Stats } from './Stats'


export class Timer {

  timerElt: HTMLElement
  sandglassElt: HTMLElement
  counterElt: HTMLElement

  timeStampStart:number
  clockTimeout: number
  timeStampFinish: number
  elapsedTimeSinceTimerStarted: number
  timerDuration = 0
  minutes = 0
  secondes = 0

  constructor(
    timeStampStart:number,
    timerDuration:number
  ){
    this.timerDuration = timerDuration
    this.timeStampStart = timeStampStart
    this.timeStampFinish = timeStampStart + this.timerDuration
    this.elapsedTimeSinceTimerStarted= Date.now() - this.timeStampStart
    this.clockTimeout = setInterval(()=>{
      if(this.elapsedTimeSinceTimerStarted<this.timeStampFinish){
        this.secondes++
        this.refreshTimerElt()
      } else {
        clearTimeout(this.clockTimeout)
      }
    },1000)
    this.timerElt = document.createElement('div')
    this.sandglassElt = document.createElement('div')
    this.counterElt = document.createElement('div')

    this.timerElt.classList.add('timerElt')
    this.sandglassElt.classList.add('countingTimerElt')
    this.counterElt.classList.add('showCountTimerElt')
    this.timerElt.classList.add(GameConstants.TIMER_ELT)

    this.timerElt.appendChild(this.sandglassElt)
    this.timerElt.appendChild(this.counterElt)
    Stats.statsPanelWrapperElt.appendChild(this.timerElt)
  }
    
  refreshTimerElt(){
    this.elapsedTimeSinceTimerStarted = Math.round((Date.now() - this.timeStampStart)/1000)
    const timerPourcent = Math.round(100 * (this.elapsedTimeSinceTimerStarted/ (this.timerDuration / 1000)))
    this.sandglassElt.style.width = `${timerPourcent}%`
    this.counterElt.innerText = `${this.getDecountInMinute(this.elapsedTimeSinceTimerStarted)}`
  }

  getDecountInMinute(instant){
    const totalGameDurationInSecond = this.timerDuration / 1000
    const e = totalGameDurationInSecond - instant
    const h = Math.floor(e / 3600).toString().padStart(2,'0'),
      m = Math.floor(e % 3600 / 60).toString().padStart(2,'0'),
      s = Math.floor(e % 60).toString().padStart(2,'0')
    
    return `${h}:${m}:${s}`
  }
}

