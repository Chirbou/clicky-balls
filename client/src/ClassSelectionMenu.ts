import { GameConstants } from './GameConstants.js'
import { GameActions } from './GameEventActions.js'
import { NetworkEventHandler } from './NetworkEventHandler.js'
import { Game } from './Game.js'
import { CanHandleKeyboard, KeyboardActions } from './KeyboardHandler.js'
import { ClasseData } from './utils/InterfaceUtils'
import { UIClassCard } from './ui/SkillCard.js'
import { Stats } from './Stats.js'

export class ClassSelectionMenu implements CanHandleKeyboard {
  levelMenuElt?: HTMLDivElement
  overlayElt?: HTMLDivElement
  isOpen = false
  classes: ClasseData[] = []
  canEvolve = false

  constructor() { }

  open() {
    if (!this.canEvolve) return
    if (this.isOpen) {
      return this.close()
    }

    if (this.classes && this.classes.length > 1) {
      this.isOpen = true
      Game.mainPlayer.isChoosingHisClasse = true
      this.levelMenuElt = document.createElement('div')
      this.levelMenuElt.id = GameConstants.LEVEL_MENU_ELT_ID
      this.overlayElt = document.createElement('div')
      this.overlayElt.classList.add('overlay')
      this.overlayElt.onclick = () => this.close()
      this.overlayElt.appendChild(this.levelMenuElt)
      document.getElementById(GameConstants.GAME_ZONE_ID)?.appendChild(this.overlayElt)

      this.levelMenuElt.style.display = 'grid'
      this.levelMenuElt.style.gap = '20px'
      this.levelMenuElt.style.gridTemplateColumns = this.classes.map(c => '1fr').join(' ')

      Stats.evolveButton?.wrapperElt.classList.add('open')

      for (let i = 0; i < this.classes.length; i++) {
        const currentClass = this.classes[i]

        const uiClassCard = new UIClassCard(currentClass, i, Game.mainPlayer.size)
        uiClassCard.addTo(this.levelMenuElt)

        uiClassCard.onSelected((classIndex: number) => {
          this.chooseClass(this.classes[classIndex])
        })
      }
    } else if (this.classes && this.classes.length === 1) {
      this.chooseClass(this.classes[0])
    } else {
      console.error('ERROR : empty classes object')
    }
  }

  close() {
    Game.mainPlayer.isChoosingHisClasse = false
    this.overlayElt?.remove()
    Stats.evolveButton.open()
    this.isOpen = false
  }

  chooseClass(chosenClass) {
    Game.chooseClass(chosenClass)
    NetworkEventHandler.emit(GameActions.SELECT_LVL_UP, chosenClass.name)
    this.close()
  }

  handleKeyboardNumber(index: number) {
    const selectedClass = this.classes[index - 1]
    if (selectedClass) {
      this.chooseClass(selectedClass)
    }
  }

  handleKeyboardActions(action: KeyboardActions) {
    if (action === KeyboardActions.SPACE) {
      this.open()
    }
  }
}
