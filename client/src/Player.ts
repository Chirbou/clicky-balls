import { AnimationGenerator } from "./AnimationGenerator.js";
import { ClassNames } from "./ClassNames.js";
import { Game } from "./Game.js";
import { GameConstants } from "./GameConstants.js";
import { Tp } from "./skill/Tp.js";
import { Stats } from "./Stats.js";
import { GoldBar } from "./ui/GoldBar.js";
import { HealthBar } from "./ui/HealthBar.js";
import { Util } from "./Util.js";
import { Coordinate, PlayerData } from "./utils/InterfaceUtils.js";

export class Player {
  id = "";
  emoji: string;
  score = 100;
  scoreToEvolve?: number = 250;
  scoreToDevolve?: number = 0;
  hp = 250;
  maxHp = 250;
  atk = 1;
  def = 1;
  heal = 1;
  disabled = true;
  tpStack = 0;
  tpStackMax = 5;
  level = 1;
  position: Coordinate = {
    x: 0,
    y: 0,
  };
  opacity = 1;
  color = "#000";
  size = 3;
  zIndex = 2;
  angle = 0;
  scoreColor = "#FFF";
  name = "";
  btnElt: any = null;
  emojiElt: any = null;
  classIcon: any = null;
  // alterationElt: HTMLDivElement
  healthBar: HealthBar | null = null;
  goldElt: GoldBar | null = null;
  goldStat: any = null;
  tpStackContainer: HTMLDivElement | null = null;
  nameElt: any = null;
  statsElt: HTMLDivElement;
  canonElt: HTMLDivElement;
  currentClass: ClassNames = ClassNames.PECNO;
  textColor;
  silenced;
  stun;
  playersResume;
  faction;
  ownedGold = 0;
  goldStackMax;
  alterations: any;
  static BTN_CLASS = "clickme";
  static SCORE_TEXT_CLASS = "btn-score";
  static GOLD_TEXT_CLASS = "btn-gold";
  static ANIMATION_WRAPPER_ID = "animationWrapper";
  static NAME_ELT_CLASS = "nameElt";
  static CLASS_ICON = "classIcon";

  constructor(id, name, startPosition, color, emoji) {
    this.id = id;
    this.name = name;
    this.emoji = emoji;
    this.position = startPosition;
    this.color = color;
    this.textColor = "#FFF";
    this.btnElt = document.createElement("button");
    this.btnElt.dataset.id = this.id;
    this.btnElt.classList.add(GameConstants.BTN_CLASS);
    this.btnElt.style.left = this.position.x + "px";
    this.btnElt.style.top = this.position.y + "px";
    this.btnElt.style.backgroundColor = this.color;
    this.btnElt.style.color = this.textColor;

    this.btnElt.style.width = this.size + "vw";
    this.btnElt.style.height = this.size + "vw";
    this.btnElt.style.zIndex = this.zIndex;

    this.statsElt = document.createElement("div");
    this.statsElt.classList.add("stats");

    this.emojiElt = document.createElement("span");
    this.emojiElt.classList.add(Player.SCORE_TEXT_CLASS);
    this.emojiElt.classList.add("no-select");
    // this.emojiElt.innerText = `${emoji}`

    this.classIcon = document.createElement("div");
    this.classIcon.classList.add(Player.CLASS_ICON);
    this.classIcon.classList.add("no-select");

    this.goldElt = new GoldBar(this.ownedGold, this.goldStackMax);

    if (this.id === Game.mainPlayerId) {
      if (this.tpStackContainer === null) {
        this.tpStackContainer = document.createElement("div");
        // TpStackUpdate()
        this.tpStackContainer.classList.add("tpStackContainer");
        Game.skillbarWrapper.appendChild(this.tpStackContainer);
      }
    }

    this.btnElt.appendChild(this.emojiElt);
    this.btnElt.appendChild(this.classIcon);
    this.btnElt.appendChild(this.statsElt);
    this.goldElt.addTo(this.btnElt);

    this.nameElt = document.createElement("div");
    this.nameElt.classList.add(Player.NAME_ELT_CLASS);
    this.nameElt.textContent = ` ${this.emoji} ${this.name}`;
    this.btnElt.appendChild(this.nameElt);

    this.canonElt = document.createElement("div");
    this.canonElt.classList.add("playerCanon");
    const canonImg = document.createElement("img");
    canonImg.src = "./assets/canon.png";
    this.canonElt.appendChild(canonImg);
    this.btnElt.appendChild(this.canonElt);
    const turretElement = document.createElement("div");
    turretElement.classList.add("turret");
    this.btnElt.appendChild(turretElement);
    Game.gameZone.appendChild(this.btnElt);

    this.healthBar = new HealthBar({
      hp: this.score,
      maxHp: this.maxHp,
      size: this.size,
    });
    this.healthBar.addTo(this.btnElt);

    // this.alterationElt = document.createElement('div')
    // this.alterationElt.classList.add('playerAlterationElt')
    // this.btnElt.appendChild(this.alterationElt)
  }

  getScore = () => {
    return this.score;
  };

  setScore = (value) => {
    if (this.score > value) {
      this.btnElt.animate(
        [{ outline: `1px solid white` }, { outline: `3px solid red` }],
        {
          duration: 50,
          easing: "linear",
        }
      );
    } else if (this.score < value) {
      this.btnElt.animate(
        [{ outline: `2px solid white` }, { outline: `2px solid green` }],
        {
          duration: 100,
          easing: "linear",
        }
      );
    }

    this.score = value;
    this.refresh();
  };

  refresh = () => {
    this.btnElt.style.backgroundColor = this.color;
    this.btnElt.style.backgroundImage = `url(${Util.getPlayerImageFromFaction(
      this.faction
    )})`;
    this.classIcon.style.backgroundImage = `url(${Util.getImagePathFromClassName(
      this.currentClass
    )})`;
    this.emojiElt.style.fontSize = `${Math.min(
      Math.max(this.score / 150, 1),
      10
    )}vw`;
    if (this.goldElt) this.goldElt.remove();

    this.canonElt.style.transform = `translate(-50%,-50%) rotate(${this.angle}deg)`;
    if (this.ownedGold > 0) {
      this.goldElt?.addTo(this.btnElt);
    }

    this.healthBar?.update({
      hp: this.score,
      maxHp: this.maxHp,
      size: this.size,
    });

    if (this.id === Game.mainPlayerId) {
      Game.healthBar?.update({
        hp: this.score,
        maxHp: this.maxHp,
        size: this.size,
      });
      if (Game.goldStatElt) {
        Game.goldStatElt.innerText = `💰 : [ ${this.ownedGold} / ${this.goldStackMax} ]`;
        if (this.ownedGold === this.goldStackMax) {
          Game.goldStatElt.classList.add(GameConstants.FULL);
        } else {
          Game.goldStatElt.classList.remove(GameConstants.FULL);
        }
      }

      this._refreshTpStack(this.tpStack);
    }

    this.btnElt.style.left =
      "calc(" + this.position.x + "px - " + this.size / 2 + "px)";
    this.btnElt.style.top =
      "calc(" + this.position.y + "px - " + this.size / 2 + "px)";
    this.btnElt.style.width = this.size + "px";
    this.btnElt.style.height = this.size + "px";
    this.btnElt.style.zIndex = this.zIndex;

    // this.alterationElt.innerHTML = ''
    // for (const alteration of this.alterations || []) {
    //   const line = document.createElement('div')
    //   const progression = document.createElement('div')
    //   progression.style.position = 'absolute'
    //   progression.innerText = alteration.cooldown
    //   line.innerText = alteration.name
    //   this.alterationElt.appendChild(line)
    //   this.alterationElt.appendChild(progression)
    // }

    // this.goldElt.classList.add(Player.GOLD_TEXT_CLASS)
    this.goldElt?.update(this.ownedGold, this.goldStackMax);

    Stats.playerByGameUpdate();
  };

  update(playerData: PlayerData) {
    this.score = playerData.hp;
    if (
      this.position.x !== playerData.position.x &&
      this.position.y !== playerData.position.y
    ) {
      AnimationGenerator.updateBullet(this.id, playerData.position);
    }
    window.setTimeout(() => {
      this.position = playerData.position;
      this.refresh();
    }, Tp.animationDuration - 50);
    this.color = playerData.color;
    this.level = playerData.level;
    this.atk = playerData.atk;
    this.def = playerData.def;
    this.heal = playerData.heal;
    this.disabled = playerData.disabled;
    this.faction = playerData.faction;
    this.ownedGold = playerData.ownedGold;
    this.goldStackMax = playerData.goldStackMax;
    this.scoreToEvolve = playerData.scoreToEvolve;
    this.playersResume = playerData.playersResume;
    this.zIndex = playerData.zIndex;
    if (Game.mainPlayerId !== this.id) {
      this.angle = playerData.angle;
    }
    this.size = playerData.size;
    this.scoreToDevolve = playerData.scoreToDevolve;
    this.maxHp = playerData.maxHp;
    this.tpStackMax = playerData.tpStackMax;
    this.alterations = playerData.alterations;
    this.currentClass = playerData.currentClass;

    this.refresh();
  }

  _refreshTpStack = (remainingTpCounter) => {
    const tpStackTab: HTMLElement[] = [];
    if (this.tpStackContainer !== null) {
      if (remainingTpCounter <= 0) {
        this.tpStackContainer.classList.add("empty");
      } else {
        this.tpStackContainer.classList.remove("empty");
      }
      this.tpStackContainer.innerHTML = "";
      for (let i = 0; i < this.tpStackMax; i++) {
        const tpStackElt = document.createElement("div");
        tpStackElt.classList.add("tpStackElt");
        tpStackElt.style.width = `${100 / this.tpStackMax}%`;
        tpStackTab.push(tpStackElt);
        this.tpStackContainer.appendChild(tpStackElt);
      }
      for (let j = 0; j < remainingTpCounter; j++) {
        tpStackTab[j]?.classList.add("tpStackEltVisible");
      }
    }
  };

  remove() {
    this.btnElt.remove();
    this.emojiElt.remove();
    this.classIcon.remove();
    if (this.goldElt) this.goldElt.remove();
    Game.players = Game.players.filter((p) => p.id !== this.id);
  }

  endTeleport(position) {
    this.position = position;
    this.btnElt.classList.remove("teleporting");
  }
}
