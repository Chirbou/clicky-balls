export enum GameObjectType{
    PORTAL = 'PORTAL',
    FLAGBASE = 'FLAGBASE',
    FLAG = 'FLAG',
    BUSH = 'BUSH',
    MINE = 'MINE',
    TOTEM = 'TOTEM',
    TOWER = 'TOWER',
    POWER_UP = 'POWER_UP',
    GOLD = 'GOLD',
    GOLD_DEPOSIT = 'GOLD_DEPOSIT',
    GOLD_MINE = 'GOLD_MINE',
    PLAYER_TOWER = 'PLAYER_TOWER',
  }