import { Game } from './Game.js'
import { GameObjectType } from './GameObjectType.js'
import { Stats } from './Stats.js'

export class Display {
  static GAME_ZONE_WIDTH_CLIENT = 0
  static GAME_ZONE_HEIGHT_CLIENT = 0
  static COMPUTED_CLIENT_WIDTH = 0
  static COMPUTED_CLIENT_HEIGHT = 0
  static GAME_ZONE_WIDTH_SERVER = 1200
  static GAME_ZONE_HEIGHT_SERVER = 800
  static CLIENT_GAME_ZONE_RATIO = 0
  static SERVER_GAME_RATIO = 0
  static SCALE_RATIO = 0
  static SCALE_RATIO_SAVE = 0
  constructor() {
    window.addEventListener('resize', (e) => {
      this.refresh()
    })
    this.refresh()
  }
  refresh() {
    Display.GAME_ZONE_WIDTH_CLIENT = document.getElementById('gameZone')?.clientWidth || 0
    Display.GAME_ZONE_HEIGHT_CLIENT = document.getElementById('gameZone')?.clientHeight || 0

    Display.CLIENT_GAME_ZONE_RATIO = Display.GAME_ZONE_WIDTH_CLIENT / Display.GAME_ZONE_HEIGHT_CLIENT
    Display.SERVER_GAME_RATIO = Display.GAME_ZONE_WIDTH_SERVER / Display.GAME_ZONE_HEIGHT_SERVER

    if (Display.CLIENT_GAME_ZONE_RATIO < Display.SERVER_GAME_RATIO) {
      Display.COMPUTED_CLIENT_WIDTH = Display.GAME_ZONE_WIDTH_CLIENT
      Display.COMPUTED_CLIENT_HEIGHT = Display.GAME_ZONE_WIDTH_CLIENT / Display.SERVER_GAME_RATIO
    } else {
      Display.COMPUTED_CLIENT_WIDTH = Display.GAME_ZONE_HEIGHT_CLIENT * Display.SERVER_GAME_RATIO
      Display.COMPUTED_CLIENT_HEIGHT = Display.GAME_ZONE_HEIGHT_CLIENT
    }

    Display.SCALE_RATIO = 1 / (Display.GAME_ZONE_WIDTH_SERVER / Display.COMPUTED_CLIENT_WIDTH)
    Game.gameZone.style.width = `${Display.COMPUTED_CLIENT_WIDTH}px`
    Game.gameZone.style.height = `${Display.COMPUTED_CLIENT_HEIGHT}px`

    Game.animationWrapper.style.width = Game.gameZone.style.width
    Game.animationWrapper.style.height = Game.gameZone.style.height

    Game.skillbarWrapper.style.height = `${Math.max(window.innerHeight - Display.COMPUTED_CLIENT_HEIGHT, 60)}px`
    Stats.statsPanelWrapperElt.style.width = `${Math.max(window.innerWidth - Display.COMPUTED_CLIENT_WIDTH, 60)}px`
    Stats.statsPanelWrapperElt.style.height = `calc(100% - ${Math.max(window.innerHeight - Display.COMPUTED_CLIENT_HEIGHT, 60)}px)`
    Stats.statsPanelWrapperElt.style.right = '0px'
    for (const player of Game.players){
      player.position.x = player.position.x * (Display.SCALE_RATIO / Display.SCALE_RATIO_SAVE)
      player.position.y = player.position.y * (Display.SCALE_RATIO / Display.SCALE_RATIO_SAVE)
      player.size = player.size * (Display.SCALE_RATIO / Display.SCALE_RATIO_SAVE)
      player.refresh()
    }

    for (const portal of Game.getGameObjectsByType(GameObjectType.PORTAL)){
      portal.position.x = portal.position.x * (Display.SCALE_RATIO / Display.SCALE_RATIO_SAVE)
      portal.position.y = portal.position.y * (Display.SCALE_RATIO / Display.SCALE_RATIO_SAVE)
      portal.size = portal.size * (Display.SCALE_RATIO / Display.SCALE_RATIO_SAVE)
      portal.refresh()
    }
    Display.SCALE_RATIO_SAVE = Display.SCALE_RATIO

  }

  static scaleInputEventValues(event) {
    if (event) {
      if (event.users) {
        for (const user of event.users) {
          this._scaleInputPosition(user)
          this._scaleInputSize(user)
          this._scaleAoeRadius(user)
        }
      } else {
        if (event.players){
          for (const player of event.players){
            this._scaleInputPosition(player)
            this._scaleInputSize(player)
          }
        } else {
          this._scaleInputPosition(event)
          this._scaleInputSize(event)
        }
        this._scaleAoeRadius(event)
        this._scalePortal(event)
      }
    }

    return event
  }

  static _scaleInputPosition(event) {
    if (event.position) {
      event.position.x *= Display.SCALE_RATIO
      event.position.y *= Display.SCALE_RATIO
    }
  }

  static _scaleInputSize(event) {
    if (event.size) {
      event.size *= Display.SCALE_RATIO
    }

  }

  static _scaleAoeRadius(event) {
    if (event.skillOutput && event.skillOutput.aoeRadius) {
      event.skillOutput.aoeRadius *= Display.SCALE_RATIO
    }
  }

  static _scalePortal(event) {
    if (event.gameObjects) {
      event.gameObjects = event.gameObjects.map(p => {
        return {
          ...p, size: p.size * Display.SCALE_RATIO, position: {
            x: p.position.x * Display.SCALE_RATIO,
            y: p.position.y * Display.SCALE_RATIO
          }
        }
      })
    }
  }

  static scaleOutputEventValues(event) {
    if (event && event.position) {
      if (event.skillOutput && event.skillOutput.position) {

        event.skillOutput.position.x /= Display.SCALE_RATIO
        event.skillOutput.position.y /= Display.SCALE_RATIO
      }
      event.position.x /= Display.SCALE_RATIO
      event.position.y /= Display.SCALE_RATIO

      return event
    }

  }
}
