import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Aoe } from './abstract/Aoe.js'

export class Pestiferous_Contagion extends Aoe {
  constructor() {
    super(GameActions.PESTIFEROUS_CONTAGION)
    this.imgElt.setAttribute('src', 'assets/skills/Pestiferous_Contagion.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } )
  }

  onSkillFeedback(skillFeedbackData){
    AnimationGenerator.popAnimation('☠️☠️', skillFeedbackData.position, 'purple', true, AnimationGenerator.POP_SHIFT)
    AnimationGenerator.popAoe(1500, skillFeedbackData.aoeRadius, skillFeedbackData.position, 'aoe-dot-animation')
  }
}
