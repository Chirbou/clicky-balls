import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { SkillResults } from './SkillResults.js'

export class Paladin_Grab extends Skill {
  constructor() {
    super(GameActions.PALADIN_GRAB)
    this.imgElt.setAttribute('src', 'assets/skills/Paladin_Grab.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } )
  }

  onSkillFeedback(skillFeedbackData){
    if (SkillResults.HIT_SELF === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('❌', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
    } else if (SkillResults.HIT_PLAYER === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('🧲🧲🧲', skillFeedbackData.position, 'orange', true, AnimationGenerator.POP_SHIFT)
    } else if(SkillResults.MISS === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('❌', skillFeedbackData.position, 'red', true, AnimationGenerator.POP_SHIFT)
    }
  }
}
