import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { Game } from '../Game.js'
import { SkillResults } from './SkillResults.js'
import { AnimationGenerator } from '../AnimationGenerator.js'

export class Rogue_Furtive_Hit extends Skill {
  constructor() {
    super(GameActions.ROGUE_FURTIVE_HIT)
    this.imgElt.setAttribute('src', 'assets/skills/Rogue_Furtive_Hit.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData){
    if (skillFeedbackData.sourcePlayer && skillFeedbackData.sourcePlayer.id === Game.mainPlayerId){
      if (SkillResults.HIT_SELF === skillFeedbackData.skillResults || SkillResults.HIT_PLAYER === skillFeedbackData.skillResults) {
        AnimationGenerator.popAnimation('🩸', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
      } else {
        AnimationGenerator.popAnimation('❌', skillFeedbackData.position, 'red', true, -AnimationGenerator.POP_SHIFT)
      }
    }
  }

}
