import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { Game } from '../Game.js'
import { Player } from '../Player.js'
export class Tp extends Skill {
  static animationDuration = 200

  constructor(tooltip = '', addSkillToBar = true) {
    super(GameActions.PECNO_TP, false)
  }

  trigger(playerSource: Player, playerTarget, coords) {
    if (playerSource.stun) {
      AnimationGenerator.popAnimation('Stun... 🔒🔒', coords, 'yellow', true, AnimationGenerator.POP_SHIFT)
      return
    }
    if (playerSource.tpStack <= 0) {
      AnimationGenerator.popAnimation('🚫', coords, 'yellow', true, AnimationGenerator.POP_SHIFT)
      return
    }
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData) { }
}
