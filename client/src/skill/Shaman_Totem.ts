import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'

export class Shaman_Totem extends Skill {
  autoskill = true
  constructor() {
    super(GameActions.SHAMAN_TOTEM)
    this.imgElt.setAttribute('src', 'assets/skills/Shaman_Totem.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData){
  //  AnimationGenerator.popAnimation(  'This MF Drop a Mine', skillFeedbackData.playerSource.position, 'grey', true, AnimationGenerator.POP_SHIFT )
  }
}
