import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'

export class Pestiferous_Deflagration extends Skill {
  autoskill = true
  constructor() {
    super(GameActions.PESTIFEROUS_DEFLAGRATION)
    this.imgElt.setAttribute('src', 'assets/skills/Pestiferous_Deflagration.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData) {
    AnimationGenerator.popAnimation('☠️☠️', skillFeedbackData.position, 'purple', true, AnimationGenerator.POP_SHIFT)
    AnimationGenerator.popAoe(1200, skillFeedbackData.aoeRadius, skillFeedbackData.position, 'aoe-damage-animation')
  }
}
