import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'

export class Rogue_Diversion extends Skill {
  autoskill = true

  constructor() {
    super(GameActions.ROGUE_DIVERSION)
    this.imgElt.setAttribute('src', 'assets/skills/Rogue_Diversion.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData){
    AnimationGenerator.popAnimation('This MF trigger invisibility', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
  }
}
