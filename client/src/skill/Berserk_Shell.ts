import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { SkillResults } from './SkillResults.js'

export class Berserk_Shell extends Skill {
  autoskill = true

  constructor() {
    super(GameActions.BERSERK_SHELL)
    this.imgElt.setAttribute('src', 'assets/skills/Berserk_Bubble.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', idSource: playerSource.id, position: coords })
  }

  onSkillFeedback(skillFeedbackData) {
    if (SkillResults.MISS === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('❌', skillFeedbackData.position, 'red', true, AnimationGenerator.POP_SHIFT)
    } else {
      AnimationGenerator.popAnimation('Berserk Shell', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
    }
  }
}
