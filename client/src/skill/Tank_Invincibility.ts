import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
export class Tank_Invincibility extends Skill {
  autoskill = true
  constructor() {
    super(GameActions.TANK_INVINCIBILITY)
    this.imgElt.setAttribute('src', 'assets/skills/Tank_Invincibility.jpg')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerSource.id, position: coords })
  }

}
