import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { Aoe } from './abstract/Aoe.js'

export class DRUID_HOTAOE extends Aoe {
  constructor() {
    super(GameActions.DRUID_HOTAOE)
    this.imgElt.setAttribute('src', 'assets/skills/Druid_Hotaoe.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } )
  }

  onSkillFeedback(skillFeedbackData){
    AnimationGenerator.popAoe(1000, skillFeedbackData.aoeRadius, skillFeedbackData.position, 'aoe-heal-animation')
  }
}
