import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { SkillResults } from './SkillResults.js'
import { Game } from '../Game.js'

export class Trapper_Sniper extends Skill {
  constructor() {
    super(GameActions.TRAPPER_SNIPER)
    this.imgElt.setAttribute('src', 'assets/skills/Trapper_Sniper.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } as any)
  }

  onSkillFeedback(skillFeedbackData){
    if (skillFeedbackData.sourcePlayer && skillFeedbackData.sourcePlayer.id === Game.mainPlayerId){
      if (SkillResults.HIT_PLAYER === skillFeedbackData.skillResults) {
        AnimationGenerator.popAnimation('⚔️', skillFeedbackData.position, 'orange', true, AnimationGenerator.POP_SHIFT)
      } else if(SkillResults.MISS === skillFeedbackData.skillResults) {
        AnimationGenerator.popAnimation('❌', skillFeedbackData.position, 'red', true, -AnimationGenerator.POP_SHIFT)
      }
    }
  }
}
