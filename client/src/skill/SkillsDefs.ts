import { Attack } from './Attack.js'
import { DPS_Focus } from './DPS_Focus.js'
import { Healer_Heal } from './Healer_Heal.js'
import { Berserk_Suffer } from './Berserk_Suffer.js'
import { Berserk_Bubble } from './Berserk_Bubble.js'
import { Berserk_Shell } from './Berserk_Shell.js'
import { Paladin_Bubble } from './Paladin_Bubble.js'
import { Paladin_Grab } from './Paladin_Grab.js'
import { Priest_Hangon } from './Priest_Hangon.js'
import { Priest_Healzone } from './Priest_Healzone.js'
import { Priest_Dispel } from './Priest_Dispel.js'
import { Pestiferous_Dispel } from './Pestiferous_Dispel.js'
import { DRUID_HOTAOE } from './Druid_Hotaoe.js'
import { Druid_Stun } from './Druid_Stun.js'
import { Rogue_Diversion } from './Rogue_Diversion.js'
import { Rogue_Furtive_Hit } from './Rogue_Furtive_Hit.js'
import { Rogue_Stab } from './Rogue_Stab.js'
import { Poison_Bombkipu } from './Poison_Bombkipu.js'
import { Pestiferous_Contagion } from './Pestiferous_Contagion.js'
import { Pestiferous_Deflagration } from './Pestiferous_Deflagration.js'
import { Poison_Vampirism } from './Poison_Vampirism.js'
import { Poison_Fang } from './Poison_Fang.js'
import { Tank_Invincibility } from './Tank_Invincibility.js'
import { Tank_Silence } from './Tank_Silence.js'
import { Toggle_Silence } from './Toggle_Silence.js'
import { Tp } from './Tp.js'
import { Occultist_Sucker } from './Occultist_Sucker.js'
import { Occultist_Blow } from './Occultist_Blow.js'
import { Occultist_Fucker } from './Occultist_Fucker.js'
import { Trapper_Stealth } from './Trapper_Stealth.js'
import { Trapper_Mine } from './Trapper_Mine.js'
import { Trapper_Sniper } from './Trapper_Sniper.js'
import { Shaman_Totem } from './Shaman_Totem.js'
import { Shaman_Underpressure } from './Shaman_Underpressure.js'
import { Shaman_Thunderbolt } from './Shaman_Thunderbolt.js'
import { Mechanotherapist_Tower_Build } from './Mechanotherapist_Tower_Build.js'
import { Mechanotherapist_Tower_Hack } from './Mechanotherapist_Tower_Hack.js'
import { Mechanotherapist_Tower_Buff } from './Mechanotherapist_Tower_Buff.js'

export const ALL_SKILLS = [
  new Attack(),
  new Tp(),
  new Tank_Invincibility(),
  new DPS_Focus(),
  new Healer_Heal(),
  new Tank_Silence(),
  new Toggle_Silence(),
  new Berserk_Suffer(),
  new Berserk_Bubble(),
  new Berserk_Shell(),
  new Paladin_Bubble(),
  new Paladin_Grab(),
  new Rogue_Diversion(),
  new Rogue_Furtive_Hit(),
  new Rogue_Stab(),
  new Poison_Bombkipu(),
  new Pestiferous_Contagion(),
  new Pestiferous_Deflagration(),
  new Pestiferous_Dispel(),
  new Poison_Vampirism(),
  new Poison_Fang(),
  new Priest_Hangon(),
  new Priest_Healzone(),
  new Priest_Dispel(),
  new Occultist_Sucker(),
  new Occultist_Blow(),
  new Occultist_Fucker(),
  new DRUID_HOTAOE(),
  new Druid_Stun(),
  new Trapper_Stealth(),
  new Trapper_Mine(),
  new Trapper_Sniper(),
  new Shaman_Totem(),
  new Shaman_Underpressure(),
  new Shaman_Thunderbolt(),
  new Mechanotherapist_Tower_Build(),
  new Mechanotherapist_Tower_Hack(),
  new Mechanotherapist_Tower_Buff(),
]
