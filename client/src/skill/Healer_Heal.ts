import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { Skill } from '../Skill.js'
import { Game } from '../Game.js'

export class Healer_Heal extends Skill {
  constructor() {
    super(GameActions.HEALER_HEAL)
    this.imgElt.setAttribute('src', 'assets/skills/Healer_Heal_logo.jpg')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idSource: playerSource.id, idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData) {
    // pass
  }
}
