import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { Aoe } from './abstract/Aoe.js'

export class Priest_Healzone extends Aoe {
  constructor() {
    super(GameActions.PRIEST_HEALZONE)
    this.imgElt.setAttribute('src', 'assets/skills/Priest_Healzone.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData){
    AnimationGenerator.popAoe(2000, skillFeedbackData.aoeRadius, skillFeedbackData.position, 'aoe-heal-animation')
  }
}
