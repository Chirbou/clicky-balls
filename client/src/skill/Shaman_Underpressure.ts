import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { Game } from '../Game.js'
import { Aoe } from './abstract/Aoe.js'
import { SkillResults } from './SkillResults.js'

export class Shaman_Underpressure extends Aoe {
  autoskill = true
  aoeRadius = 200

  constructor() {
    super(GameActions.SHAMAN_UNDERPRESSURE)
    this.imgElt.setAttribute('src', 'assets/skills/Shaman_Underpressure.png')
  }

  trigger(playerSource, playerTarget, coords) {
    const copyOfPosition = { x: playerSource.position.x, y: playerSource.position.y}
    NetworkEventHandler.emit(this.event_name, { idTarget: playerSource ? playerSource.id : '', position: copyOfPosition })
  }

  onSkillFeedback(skillFeedbackData){
    if (SkillResults.HIT_SELF === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('Suffer! 😈', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
    } else if (SkillResults.HIT_PLAYER === skillFeedbackData.skillResults) {
      AnimationGenerator.popAoe(1000, skillFeedbackData.aoeRadius, skillFeedbackData.position, 'aoe-damage-animation')
    }
  }
}
