import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { SkillResults } from './SkillResults.js'
export class Occultist_Sucker extends Skill {
  /*
   1ère alternative : (passif) tous les x dégâts faire une somme fixe de dégâts
   2ème alternative : encaisse dégâts et release d'un seul coup en AOE autour de soi
  */
  constructor() {
    super(GameActions.OCCULTIST_SUCKER)
    this.imgElt.setAttribute('src', 'assets/skills/Occultist_Sucker.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } )
  }

  onSkillFeedback(skillFeedbackData){
    if (SkillResults.HIT_SELF === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('Sucker! 😈', skillFeedbackData.position, 'purple', true, AnimationGenerator.POP_SHIFT)
    }
  }

}
