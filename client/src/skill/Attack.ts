import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { SkillResults } from './SkillResults.js'
import { Game } from '../Game.js'

export class Attack extends Skill {
  constructor() {
    super(GameActions.PECNO_ATK)
    this.imgElt.setAttribute('src', 'assets/skills/Atk_logo.jpg')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } as any)
  }

  onSkillFeedback(skillFeedbackData) {
    if (skillFeedbackData.sourcePlayer && skillFeedbackData.sourcePlayer.id === Game.mainPlayerId) {
      if (SkillResults.HIT_SELF === skillFeedbackData.skillResults) {
        // AnimationGenerator.popAnimation('🍔', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
      } else if (SkillResults.MISS === skillFeedbackData.skillResults) {
        AnimationGenerator.popAnimation('❌', skillFeedbackData.position, 'red', true, -AnimationGenerator.POP_SHIFT)
      }
    }
  }
}
