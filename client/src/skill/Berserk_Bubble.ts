import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { SkillResults } from './SkillResults.js'

export class Berserk_Bubble extends Skill {
  constructor() {
    super(GameActions.BERSERK_BUBBLE)
    this.imgElt.setAttribute('src', 'assets/skills/Berserk_Bubble.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', idSource : playerSource.id, position: coords } )
  }

  onSkillFeedback(skillFeedbackData){
    if(SkillResults.MISS === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('❌', skillFeedbackData.position, 'red', true, AnimationGenerator.POP_SHIFT)
    } else {
      AnimationGenerator.popAnimation('Berserk Bubble ♋️', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
    }

  }
}

//Est ce que plusieurs personnes peuvent bubble berserk ? auquel cas dégat subits divisés par berserks ? Max de deux bulles ?
//Si le Berserk tape sur bouclier, il fait plus de dégats à la cible.
//ALTERNATIVE : Marque du chasseur, la cible prend plus de dégats et surtout par le berserk.
