import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'

export class Trapper_Mine extends Skill {
  autoskill = true
  constructor() {
    super(GameActions.TRAPPER_MINE)
    this.imgElt.setAttribute('src', 'assets/skills/Trapper_Mine.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData){
    AnimationGenerator.popAnimation(
      'This MF Drop a Mine', this.position, 'grey', true, AnimationGenerator.POP_SHIFT
    )
  }
}
