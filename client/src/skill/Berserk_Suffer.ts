import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { SkillResults } from './SkillResults.js'
import { Aoe } from './abstract/Aoe.js'
export class Berserk_Suffer extends Aoe {
  autoskill = true
  /*
   1ère alternative : (passif) tous les x dégâts faire une somme fixe de dégâts
   2ème alternative : encaisse dégâts et release d'un seul coup en AOE autour de soi
  */
  constructor() {
    super(GameActions.BERSERK_SUFFER)
    this.imgElt.setAttribute('src', 'assets/skills/Berserk_Suffer.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } )
  }

  onSkillFeedback(skillFeedbackData){
    if (SkillResults.HIT_SELF === skillFeedbackData.skillResults) {
      AnimationGenerator.popAnimation('Suffer! 😈', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
    } else if (SkillResults.HIT_PLAYER === skillFeedbackData.skillResults) {
      AnimationGenerator.popAoe(1000, skillFeedbackData.aoeRadius, skillFeedbackData.position, 'aoe-damage-animation')
    }
  }

}
