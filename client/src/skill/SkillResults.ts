export const SkillResults = {
  HIT_SELF: 0,
  HIT_PLAYER: 1,
  MISS: 2,
  CANNOT: 3
}