import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { AnimationGenerator } from '../AnimationGenerator.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'
import { Aoe } from './abstract/Aoe.js'

export class Poison_Bombkipu extends Aoe {
  constructor() {
    super(GameActions.POISON_BOMBKIPU)
    this.imgElt.setAttribute('src', 'assets/skills/Poison_Bombkipu.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords } )
  }

  onSkillFeedback(skillFeedbackData){
    AnimationGenerator.popAnimation('☠️☠️', skillFeedbackData.position, 'green', true, AnimationGenerator.POP_SHIFT)
    AnimationGenerator.popAoe(1500, skillFeedbackData.aoeRadius, skillFeedbackData.position, 'aoe-dot-animation')
  }
}
