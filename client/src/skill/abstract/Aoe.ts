import { GameObject } from '../../GameObject'
import { Player } from '../../Player'
import { Skill } from '../../Skill.js'

import { ScoreShiftData } from '../../utils/InterfaceUtils'

export class Aoe extends Skill {
  constructor(type){
    super(type)
  }
  handleAnimation(sourcePlayer: Player|GameObject, target: Player|GameObject, scoreShiftData: ScoreShiftData, hitAnimationColor: string){
    // no bullet
  }
}
