import { NetworkEventHandler } from '../NetworkEventHandler.js'
import { GameActions } from '../GameEventActions.js'
import { Skill } from '../Skill.js'

export class Mechanotherapist_Tower_Build extends Skill {
  autoskill = true
  constructor() {
    super(GameActions.MECHANOTHERAPIST_TOWER_BUILD)
    this.imgElt.setAttribute('src', 'assets/skills/Mechanotherapist_Tower_Build.png')
  }

  trigger(playerSource, playerTarget, coords) {
    NetworkEventHandler.emit(this.event_name, { idTarget: playerTarget ? playerTarget.id : '', position: coords })
  }

  onSkillFeedback(skillFeedbackData) {
    //  AnimationGenerator.popAnimation(  'This MF Drop a Mine', skillFeedbackData.playerSource.position, 'grey', true, AnimationGenerator.POP_SHIFT )
  }
}
