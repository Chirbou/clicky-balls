export enum AlterationNames {
  PALADIN_BUBBLE = 'paladin_bubble',
  BERSERK_BUBBLE = 'berserk_bubble',
  INVINCIBLE = 'invincible',
  SICK = 'sick',
  SILENCED = 'silenced',
  SUCKED = 'sucked',
  FUCKED = 'fucked',
  MARTYR = 'martyr',
  SUFFER = 'suffer',
  INVISIBLE = 'invisible',
  STEALTH = 'stealth',
  TELEPORT = 'teleport',
  GRABBED = 'grabbed',
  CONTAGIOUS = 'contagious',
  DEAD = 'dead',
  STUN = 'stun',
  BUFF_ATK = 'buff_atk',
  BUFF_HEAL = 'buff_heal',
  BUFF_HEALTH = 'buff_health',
  MECHANOTHERAPIST_HACKED = 'mechanotherapist_hacked',
  MECHANOTHERAPIST_BUFFED = 'mechanotherapist_buffed'
}
