import { AlterationNames } from "./AlterationNames";

export interface AlterationData {
  stack: number,
  name: AlterationNames,
  cooldown?: number,
}
