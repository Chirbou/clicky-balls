import { Player } from './Player.js'
import { GameConstants } from './GameConstants.js'
import { Game } from './Game.js'
import { SkillBar } from './Skillbar.js'
import { GameActions } from './GameEventActions.js'
import { ALL_SKILLS } from './skill/SkillsDefs.js'
import { AnimationGenerator } from './AnimationGenerator.js'
import { GameObject } from './GameObject.js'

export class MainPlayer extends Player {
  atkStat = 1
  healStat = 1
  defStat = 1
  autoClickRate?: number
  mouseTarget: any
  canChangePosition = true
  skillsInCooldown = []
  btnElt!: HTMLButtonElement
  isChoosingHisClasse = false

  static DAMAGE_OUTSIDE = 2
  static INITIAL_MAIN_PLAYER_OPACITY = 1
  constructor(id: string, name: string, startPosition, color, emoji) {
    super(id, name, startPosition, color, emoji)
    this.btnElt.classList.add(GameConstants.MAIN_PLAYER_CLASS)
  }

  leftClick() {
    if (this.isChoosingHisClasse) return

    const clickedCoords = { x: this.mouseTarget.pageX, y: this.mouseTarget.pageY }
    const targetId = this.mouseTarget.target.dataset['id'] || this.mouseTarget.target.parentNode.dataset['id']
    let target: Player | GameObject | undefined

    if (this.silenced) {
      AnimationGenerator.popTextAnimation('Silenced... 🤫🤫', clickedCoords, 'yellow', true, AnimationGenerator.POP_SHIFT, true)
      return
    }

    target = Game.getPlayerById(targetId)
    if (!target) {
      target = Game.getGameObjectById(targetId)
    }

    if (this.mouseTarget.target.classList.contains(GameConstants.SKILL_CLASS)) {
      SkillBar.selectSkill(this.mouseTarget.target.dataset['position'])
    } else {
      this.triggerSkillUse(
        this,
        target,
        clickedCoords
      )
    }
  }

  triggerAutoClick() {
    this.autoClickRate = setInterval(() => {
      this.leftClick()
    }, 150)
  }

  triggerSkillUse(source, target, position) {
    const usedSkill = SkillBar.getSelectedSkill() as any
    usedSkill.trigger(source, target, position)
  }

  takeDamage = (value) => {
    // this.score -= value
    // this.refresh()
  }

  healSelf = (value) => {
    // this.score += value
    // this.refresh()
  }

  rightClick(event) {
    //peut posserder un Cooldown
    if (this.isChoosingHisClasse) return false

    Game.currentActiveCountdown?.cancelCountdown()

    if ((event.target.classList.contains(GameConstants.SKILL_CLASS)
      || event.target.parentNode.classList.contains(GameConstants.SKILL_CLASS))) {
      return false
    } else {
      const clickedCoords = { x: event.pageX, y: event.pageY }

      const skill = ALL_SKILLS.find((s) => s.event_name === GameActions.PECNO_TP)
      skill?.trigger(this, this, clickedCoords)
    }

    return false // empêcher le menu contextuel de s'afficher
  }

  destroy() {
    //quand le score est en deça de 0
    //le personnage se désintègre
    //ferme sa session de jeu
  }

}
