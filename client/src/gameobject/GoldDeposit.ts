import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject, GameObjectData } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'
import { HealthBar } from '../ui/HealthBar'
import { IHoverable, makeHoverable } from '../ui/stats/IHoverable'
import { Util } from '../Util'

export class GoldDeposit extends GameObject implements IHoverable {
  type: GameObjectType = GameObjectType.GOLD_DEPOSIT
  goldDepositElt: HTMLDivElement
  goldDepositTextElt: HTMLDivElement
  healthBar: HealthBar
  counter = 0
  countdownInterval: any
  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public faction: FactionNames,
    public cooldownBeforeActivation: number,
    public goldAmount: number,
    public hp: number
  ) {
    super()
    this.goldDepositElt = document.createElement('div')
    this.goldDepositElt.classList.add(GameConstants.GOLD_DEPOSIT_ELT_CLASS_NAME)
    this.goldDepositElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.goldDepositElt.style.top = `${position.y - this.size / 2}px`
    this.goldDepositElt.style.left = `${position.x - this.size / 2}px`
    this.goldDepositElt.style.width = `${size}px`
    this.goldDepositElt.style.height = `${size}px`
    this.goldDepositElt.dataset.id = id
    this.goldDepositElt.style.boxShadow = `inset 0px 0px 20px 0px ${Util.getColorFromFactionName(faction)}`
    this.goldDepositTextElt = document.createElement('div')
    this.goldDepositTextElt.classList.add(GameConstants.SCORE_TEXT_CLASS)
    this.healthBar = new HealthBar({ hp: this.hp, maxHp: 3000, size: this.size, faction: this.faction })
    this.healthBar.addTo(this.goldDepositElt)
    this.goldDepositElt.appendChild(this.goldDepositTextElt)

    Game.gameZone.appendChild(this.goldDepositElt)
    makeHoverable(this, this.goldDepositElt)
    this.refresh()
  }
  hoverableName: string = 'Banque' 
  hoverableDescription: string = 'A gold deposit that can be mined for gold'
  hoverableLeftClickDescription: string = 'Attaquer'
  hoverableRightClickDescription: string = 'Déposer or'

  popCountdown() {
    Game.currentActiveCountdown = this
    this.counter = Math.round((+(this.goldDepositElt.dataset?.cooldown || '0')) / 1000)
    if (this.countdownInterval) {
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    Game.currentActiveCountdown = undefined
    clearInterval(this.countdownInterval)
  }

  remove() {
    this.goldDepositElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }

  refresh() {
    this.goldDepositElt.style.top = `${this.position.y - this.size / 2}px`
    this.goldDepositElt.style.left = `${this.position.x - this.size / 2}px`
    this.goldDepositElt.style.width = `${this.size}px`
    this.goldDepositElt.style.height = `${this.size}px`
    this.goldDepositTextElt.innerText = `${this.goldAmount}💰`
    this.healthBar.update({ hp: this.hp, maxHp: 3000, size: this.size })
  }

  update(gameObjectData: GameObjectData) {
    this.type = gameObjectData.type
    this.size = gameObjectData.size
    this.position = gameObjectData.position
    this.cooldownBeforeActivation = gameObjectData.cooldownBeforeActivation
    this.amount = gameObjectData?.amount
    this.hp = gameObjectData?.hp || 0
    this.activated = gameObjectData?.activated || false
    this.goldAmount = gameObjectData?.goldAmount || 0
    this.refresh()
  }

  on(e) {
    this.popCountdown()
  }

  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popAnimation('Cassé 🤫', clickedCoords, 'yellow', true, AnimationGenerator.POP_SHIFT)
  }

  off(e) {

  }

  complete(e) {
    //TODO : Faire des points qui se stockent dans une stat sur le côté, direction Collection.
    
    this.goldAmount += e.goldAmount
    this.refresh()
    AnimationGenerator.popTextAnimation(`💰 Put ${e.goldAmount} in the bank ! 💰`, this.position, 'yellow', true, AnimationGenerator.POP_SHIFT)
  }

}
