import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'

export class FlagBase extends GameObject{
  type:GameObjectType = GameObjectType.FLAGBASE
  flagBaseElt: HTMLDivElement
  counter = 0
  countdownInterval: any

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public faction: FactionNames,
    public cooldownBeforeActivation: number
  ) {
    super()
    this.flagBaseElt = document.createElement('div')
    this.flagBaseElt.classList.add(GameConstants.FLAGBASE_ELT_CLASS_NAME)
    this.flagBaseElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.flagBaseElt.style.top = `${position.y - this.size/2}px`
    this.flagBaseElt.style.left = `${position.x - this.size/2}px`
    this.flagBaseElt.style.width = `${size}px`
    this.flagBaseElt.style.height = `${size}px`
    this.flagBaseElt.dataset.id = id
    Game.gameZone.appendChild(this.flagBaseElt)
  }

  popCountdown() {
    Game.currentActiveCountdown = this
    this.counter = Math.round((+(this.flagBaseElt.dataset?.cooldown || '0')) / 1000)
    if (this.countdownInterval){
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    Game.currentActiveCountdown = undefined
    clearInterval(this.countdownInterval)
  }

  getFlagClassFromFaction(faction:FactionNames){
    switch(faction){
    case FactionNames.PUNK:
      return GameConstants.FLAG_PUNK
    case FactionNames.CONSERVATIST:
      return GameConstants.FLAG_CONSERVATIST
    case FactionNames.TECHNOPHILE:
      return GameConstants.FLAG_TECHNOPHILE
    default:
      return ''
    }
  }

  remove() {
    this.flagBaseElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }

  refresh(){
    this.flagBaseElt.style.top = `${this.position.y - this.size/2}px`
    this.flagBaseElt.style.left = `${this.position.x - this.size/2}px`
    this.flagBaseElt.style.width = `${this.size}px`
    this.flagBaseElt.style.height = `${this.size}px`
  }

  on(e) {
    this.popCountdown()
  }

  miss(e) {
    const clickedCoords = e.coords
  }

  off(e) {

  }
  complete(e){

  }
}
