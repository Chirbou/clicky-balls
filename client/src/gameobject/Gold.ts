import { AnimationGenerator } from '../AnimationGenerator'
import { Game } from '../Game'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'

export enum GoldValue {
  MASSIVE = 100,
  CHONKY = 50,
  BIG = 20,
  NORMAL = 10,
  SMALL = 5,
  TINY = 1,
}

export class Gold extends GameObject {
  type: GameObjectType = GameObjectType.GOLD
  coinElt: HTMLDivElement
  counter = 0
  countdownInterval: any
  static tooMuchGoldNotificationDelay = 7500
  static recentlyNotified = false

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public cooldownBeforeActivation: number,
    public value: GoldValue
  ) {
    super()
    this.value = value
    this.position = position
    this.size = this.getSizeByGoldValue()
    this.coinElt = document.createElement('div')
    this.coinElt.classList.add(this.getClassByGoldValue())
    this.coinElt.classList.add('coin')
    this.coinElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.coinElt.dataset.id = id
    this.coinElt.style.animationDelay = `${Math.floor(Math.random() * 1500)}ms`
    this.coinElt.innerText = `${this.value}`

    this.coinElt.style.setProperty('--coinSize', `${this.size}px`)
    this.coinElt.style.setProperty('--coinFontSize', `calc(var(--coinSize) * 0.75)`)
    this.coinElt.style.setProperty('--coinTranslate', `calc(var(--coinSize) * -0.075)`)
    this.coinElt.style.setProperty('--coinTranslateDelta', `calc(var(--coinSize) * -0.0005)`)
    this.coinElt.style.setProperty('--coinLeft', `calc(var(--coinSize) * 0.425)`)
    this.coinElt.style.setProperty('--coinWidth', `calc(var(--coinSize) * 0.075)`)
    this.coinElt.style.top = `${this.position.y - this.size / 2}px`
    this.coinElt.style.left = `${this.position.x - this.size / 2}px`

    Game.gameZone.appendChild(this.coinElt)
  }

  popCountdown() {
    this.counter = Math.round((+(this.coinElt.dataset?.cooldown || '0')) / 1000)
    this._popCountdown()
    if (this.countdownInterval) {
      clearInterval(this.countdownInterval)
    }
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter} `, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    clearInterval(this.countdownInterval)
  }

  remove() {
    this.coinElt.remove()
    Game.gameObjects = Game.gameObjects.filter(o => o.id !== this.id)
  }

  refresh() {

  }

  on(e) {
    AnimationGenerator.popAnimation('💰', this.position, 'yellow')
  }

  miss(e) {
    if (!Gold.recentlyNotified) {
      const clickedCoords = e.coords
      Gold.recentlyNotified = true
      window.setTimeout(() => {
        Gold.recentlyNotified = false
      }, Gold.tooMuchGoldNotificationDelay)
      AnimationGenerator.popTextAnimation('⛔💰', clickedCoords, 'yellow', true, AnimationGenerator.POP_SHIFT, true)
    }
    AnimationGenerator.tooMuchGold()
  }

  off(e) {

  }
  complete(e) {

  }

  getClassByGoldValue(): string {
    switch (this.value) {
      case GoldValue.TINY:
        return 'coinValueTiny'
      case GoldValue.SMALL:
        return 'coinValueSmall'
      case GoldValue.NORMAL:
        return 'coinValueNormal'
      case GoldValue.BIG:
        return 'coinValueBig'
      case GoldValue.CHONKY:
        return 'coinValueChonky'
      case GoldValue.MASSIVE:
        return 'coinValueMassive'
      default:
        return 'coinValueTiny'
    }
  }

  getSizeByGoldValue(): number {
    switch (this.value) {
      case GoldValue.TINY:
        return this.size * 1
      case GoldValue.SMALL:
        return this.size * 1.1
      case GoldValue.NORMAL:
        return this.size * 1.2
      case GoldValue.BIG:
        return this.size * 1.3
      case GoldValue.CHONKY:
        return this.size * 1.4
      case GoldValue.MASSIVE:
        return this.size * 1.5
      default:
        return this.size * 1.1
    }
  }
}
