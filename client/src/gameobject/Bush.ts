import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'

export class Bush extends GameObject{
  type:GameObjectType = GameObjectType.BUSH
  BushElt: HTMLDivElement
  counter = 0
  countdownInterval: any

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public faction: FactionNames,
    public cooldownBeforeActivation: number
  ) {
    super()
    this.BushElt = document.createElement('div')
    this.BushElt.classList.add(GameConstants.BUSH_ELT_CLASS_NAME)
    this.BushElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.BushElt.style.top = `${position.y - this.size/2}px`
    this.BushElt.style.left = `${position.x - this.size/2}px`
    this.BushElt.style.width = `${size}px`
    this.BushElt.style.height = `${size}px`
    this.BushElt.dataset.id = id
    Game.gameZone.appendChild(this.BushElt)
  }

  popCountdown() {

  }

  _popCountdown = () => {

  }

  cancelCountdown = () => {
  }

 
  remove() {
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
    this.BushElt.remove()
  }

  refresh(){
    this.BushElt.style.top = `${this.position.y - this.size/2}px`
    this.BushElt.style.left = `${this.position.x - this.size/2}px`
    this.BushElt.style.width = `${this.size}px`
    this.BushElt.style.height = `${this.size}px`
  }

  on(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popAnimation('🤫hiden🤫', clickedCoords, 'yellow', true, AnimationGenerator.POP_SHIFT)
  }
  
  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popAnimation('Nope', clickedCoords, 'yellow', true, AnimationGenerator.POP_SHIFT)
  }

  off(e) {
    
  }
  complete(e){
    
  }
}
