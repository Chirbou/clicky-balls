import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'
import { HealthBar } from '../ui/HealthBar'
import { IHoverable, makeHoverable } from '../ui/stats/IHoverable'
import { Util } from '../Util'

export class GoldMine extends GameObject implements IHoverable{
  type:GameObjectType = GameObjectType.GOLD_MINE
  goldMineElt: HTMLDivElement
  goldMineTextElt: HTMLDivElement
  counter = 0
  countdownInterval: any
  healthBar: HealthBar

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public faction: FactionNames,
    public cooldownBeforeActivation: number,
    public amount:number,
    public hp: number,
    public maxHp: number = 3000

  ) {
    super()
    this.goldMineElt = document.createElement('div')
    this.goldMineElt.classList.add(GameConstants.GOLD_MINE_ELT_CLASS_NAME)
    this.goldMineElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.goldMineElt.style.top = `${position.y - this.size/2}px`
    this.goldMineElt.style.left = `${position.x - this.size/2}px`
    this.goldMineElt.style.width = `${size}px`
    this.goldMineElt.style.height = `${size}px`
    this.goldMineElt.dataset.id = id
    this.goldMineElt.style.boxShadow = `0 0 18px 12px ${Util.getColorFromFactionName(faction)}`
    this.goldMineTextElt = document.createElement('div')
    this.goldMineTextElt.classList.add(GameConstants.SCORE_TEXT_CLASS)
    this.goldMineElt.appendChild(this.goldMineTextElt)

    this.healthBar = new HealthBar({ hp : this.hp, maxHp:this.maxHp, size: this.size })
    this.healthBar.addTo(this.goldMineElt)

    Game.gameZone.appendChild(this.goldMineElt)
    makeHoverable(this, this.goldMineElt)
    this.refresh()
  }
  hoverableName: string = 'Mine d\'or'
  hoverableDescription: string = 'Une mine d\'or qui rapporte de l\'or'
  hoverableLeftClickDescription: string = 'Attaquer'
  hoverableRightClickDescription: string = 'Se téléporter'

  popCountdown() {
    this.counter = Math.round((+(this.goldMineElt.dataset?.cooldown || '0')) / 1000)
    if (this.countdownInterval){
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    clearInterval(this.countdownInterval)
  }

  remove() {
    this.goldMineElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }

  refresh(){
    this.goldMineElt.style.top = `${this.position.y - this.size/2}px`
    this.goldMineElt.style.left = `${this.position.x - this.size/2}px`
    this.goldMineElt.style.width = `${this.size}px`
    this.goldMineElt.style.height = `${this.size}px`
    this.healthBar.update({ hp : this.hp, maxHp : this.maxHp, size: this.size })
  }

  on(e) {
    this.popCountdown()
  }

  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popAnimation('Bloqué 🤫🤫', clickedCoords, 'yellow', true, AnimationGenerator.POP_SHIFT)
  }

  off(e) {

  }
  complete(e){
    this.amount += e.amount
    this.refresh()
  }

}
