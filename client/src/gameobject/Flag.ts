import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'
import { IHoverable, makeHoverable } from '../ui/stats/IHoverable'

export class Flag extends GameObject implements IHoverable{
  type:GameObjectType = GameObjectType.FLAG
  flagElt: HTMLDivElement
  textWrapper: HTMLDivElement
  counter = 0
  countdownInterval: any
  
  hoverableName: string = 'Drapeau'
  hoverableDescription: string = 'Un drapeau qui rapporte périodiquement des golds à sa faction.'
  hoverableLeftClickDescription: string = 'Ø' 
  hoverableRightClickDescription: string = 'Prendre'

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public faction: FactionNames,
    public cooldownBeforeActivation: number
  ) {
    super()
    this.flagElt = document.createElement('div')
    this.textWrapper = document.createElement('div')
    this.textWrapper.classList.add('flagTextWrapper')
    // this.textWrapper.innerText = '🏳️'
    this.textWrapper.style.fontSize='25px'
    this.textWrapper.style.width = `${size}px`
    this.textWrapper.style.height = `${size}px`
    this.flagElt.classList.add(GameConstants.FLAG_ELT_CLASS_NAME)
    this.flagElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.flagElt.style.top = `${position.y - this.size/2}px`
    this.flagElt.style.left = `${position.x - this.size/2}px`
    this.flagElt.style.width = `${size}px`
    this.flagElt.style.height = `${size}px`
    this.flagElt.dataset.id = id
    this.flagElt.classList.add(Flag.getFlagClassFromFaction(faction))
    this.flagElt.appendChild(this.textWrapper)
    Game.gameZone.appendChild(this.flagElt)
    makeHoverable(this, this.flagElt)
  }

  popCountdown() {
    Game.currentActiveCountdown = this
    this.counter = Math.round((+(this.flagElt.dataset?.cooldown || '0')) / 1000)
    if (this.countdownInterval){
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    Game.currentActiveCountdown = undefined
    clearInterval(this.countdownInterval)
  }

  static getFlagClassFromFaction(faction:FactionNames){
    switch(faction){
    case FactionNames.PUNK:
      return GameConstants.FLAG_PUNK
    case FactionNames.CONSERVATIST:
      return GameConstants.FLAG_CONSERVATIST
    case FactionNames.TECHNOPHILE:
      return GameConstants.FLAG_TECHNOPHILE
    default:
      return ''
    }
  }

  remove() {
    this.flagElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }

  refresh(){
    this.flagElt.style.top = `${this.position.y - this.size/2}px`
    this.flagElt.style.left = `${this.position.x - this.size/2}px`
    this.flagElt.style.width = `${this.size}px`
    this.flagElt.style.height = `${this.size}px`
  }

  on(e) {
    this.popCountdown()
  }

  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popTextAnimation('Cannot take this flag ! 🤫🤫', clickedCoords, 'red', true, AnimationGenerator.POP_SHIFT)
  }

  off(e) {

  }
  complete(e){

  }

}
