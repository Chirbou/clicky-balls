import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'

export class PowerUp extends GameObject{
  
  type:GameObjectType = GameObjectType.POWER_UP
  powerUpElt: HTMLDivElement
  textWrapper: HTMLDivElement
  counter = 0
  countdownInterval: any
  aoeRadius = 200
  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public cooldownBeforeActivation: number
  ) {
    super()
    this.powerUpElt = document.createElement('div')
    this.textWrapper = document.createElement('div')
    this.textWrapper.innerText = '🎱'
    this.textWrapper.style.fontSize='25px'
    this.textWrapper.style.width = `${size}px`
    this.textWrapper.style.height = `${size}px`
    this.powerUpElt.classList.add(GameConstants.POWERUP_ELT_CLASS_NAME)
    this.powerUpElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.powerUpElt.style.top = `${position.y - this.size/2}px`
    this.powerUpElt.style.left = `${position.x - this.size/2}px`
    this.powerUpElt.style.width = `${size}px`
    this.powerUpElt.style.height = `${size}px`
    this.powerUpElt.dataset.id = id
    this.powerUpElt.style.backgroundColor = 'grey'
    this.powerUpElt.appendChild(this.textWrapper)
    Game.gameZone.appendChild(this.powerUpElt)
  }

  popCountdown() {
    this.counter = Math.round((+(this.powerUpElt.dataset?.cooldown || '0')) / 1000)
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    clearInterval(this.countdownInterval)
  }

  static getFlagClassFromFaction(faction:FactionNames){
    switch(faction){
    case FactionNames.PUNK:
      return GameConstants.FLAG_PUNK
    case FactionNames.CONSERVATIST:
      return GameConstants.FLAG_CONSERVATIST
    case FactionNames.TECHNOPHILE:
      return GameConstants.FLAG_TECHNOPHILE
    default:
      return ''
    }
  }

  remove() {
    this.powerUpElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }
  
  refresh(){
    this.powerUpElt.style.top = `${this.position.y - this.size/2}px`
    this.powerUpElt.style.left = `${this.position.x - this.size/2}px`
    this.powerUpElt.style.width = `${this.size}px`
    this.powerUpElt.style.height = `${this.size}px`
  }

  on(e) {
    AnimationGenerator.popAoe(1500, this.aoeRadius, this.position, 'aoe-both-animation')
  }
  
  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popTextAnimation('Cannot take the flag 🤫🤫', clickedCoords, 'red', true, AnimationGenerator.POP_SHIFT)
  }
  
  off(e: any) {
    throw new Error('Method not implemented.')
  }
  complete(e: any) {
    throw new Error('Method not implemented.')
  }
}
