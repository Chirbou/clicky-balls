import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'
import { IHoverable, makeHoverable } from '../ui/stats/IHoverable'

export const PortalClassConstants = {
  NOFACTION_PORTAL : 'portalNoFaction',
  PUNK_PORTAL : 'portalPunk',
  CONSERVATIST_PORTAL : 'portalConservatist',
  TECHNOPHILE_PORTAL : 'portalTechnophile'
}

export class Portal extends GameObject implements IHoverable{
  type:GameObjectType = GameObjectType.PORTAL
  portalElt: HTMLDivElement
  subPortalElt: HTMLDivElement
  counter = 0
  countdownInterval: any

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public cooldownBeforeActivation: number,
    public activated:boolean,
    public faction: FactionNames = FactionNames.NOFACTION
  ) {
    super()
    this.portalElt = document.createElement('div')
    this.portalElt.classList.add(GameConstants.PORTAL_ELT_CLASS_NAME)
    this.setupPortalClass()
    this.portalElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.portalElt.style.top = `${position.y - this.size/2}px`
    this.portalElt.style.left = `${position.x - this.size/2}px`
    this.portalElt.style.width = `${size}px`
    this.portalElt.style.height = `${size}px`
    this.portalElt.dataset.id = id

    this.subPortalElt = document.createElement('div')

    this.applyActivationClass()

    this.subPortalElt.style.top = `${position.y - size/2}px`
    this.subPortalElt.style.left = `${position.x - size/2}px`
    this.subPortalElt.style.width = `${size}px`
    this.subPortalElt.style.height = `${size}px`

    const imageElt = document.createElement('img') as HTMLImageElement
    imageElt.src = '../assets/portal/portal_spiral.png'

    this.subPortalElt.appendChild(imageElt)

    Game.gameZone.appendChild(this.subPortalElt)

    Game.gameZone.appendChild(this.portalElt)
   // makeHoverable(this, this.portalElt)
  }
  hoverableName: string = 'Portail'
  hoverableDescription: string = 'Portail'
  hoverableLeftClickDescription: string = 'Ø'
  hoverableRightClickDescription: string = 'Se téléporter'

  popCountdown() {
    Game.currentActiveCountdown = this
    this.counter = Math.round((+(this.portalElt.dataset?.cooldown || '0')) / 1000)
    if (this.countdownInterval){
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    Game.currentActiveCountdown = undefined
    clearInterval(this.countdownInterval)
  }

  remove() {
    this.portalElt.remove()
    this.subPortalElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }

  refresh(){
    this.portalElt.style.top = `${this.position.y - this.size/2}px`
    this.portalElt.style.left = `${this.position.x - this.size/2}px`
    this.portalElt.style.width = `${this.size}px`
    this.portalElt.style.height = `${this.size}px`
    this.applyActivationClass()
    this.setupPortalClass()
    
    //this.subPortalElt.classList.add(this.activated ? GameConstants.PORTAL_SPIRAL_ELT_CLASS_NAME : GameConstants.PORTAL_SPIRAL_DESACTIVATED_ELT_CLASS_NAME)

  }

  on(e) {
    const portal = Game.getGameObjectById(e.id) as Portal
    portal?.popCountdown()
  }

  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popTextAnimation('Vous ne pouvez pas passer ! 🤫🤫', clickedCoords, 'yellow', true, AnimationGenerator.POP_SHIFT)
  }

  off(e) {
    const portal = Game.getGameObjectById(e.id) as Portal
    portal?.cancelCountdown()
  }

  complete(e){

  }

  applyActivationClass(){
    if(this.activated){
      this.portalElt.classList.add(GameConstants.PORTAL_ELT_CLASS_NAME)
      this.portalElt.classList.remove(GameConstants.PORTAL_DESACTIVATED_ELT_CLASS_NAME)

      this.subPortalElt.classList.add(GameConstants.PORTAL_SPIRAL_ELT_CLASS_NAME)
      this.subPortalElt.classList.remove(GameConstants.PORTAL_SPIRAL_DESACTIVATED_ELT_CLASS_NAME)
    }else{
      this.portalElt.classList.remove(GameConstants.PORTAL_ELT_CLASS_NAME)
      this.portalElt.classList.add(GameConstants.PORTAL_DESACTIVATED_ELT_CLASS_NAME)

      this.subPortalElt.classList.remove(GameConstants.PORTAL_SPIRAL_ELT_CLASS_NAME)
      this.subPortalElt.classList.add(GameConstants.PORTAL_SPIRAL_DESACTIVATED_ELT_CLASS_NAME)
    }
  }

  setupPortalClass(){
    if (this.activated){
      switch(this.faction){
      case FactionNames.CONSERVATIST:
        this.portalElt.classList.add(PortalClassConstants.CONSERVATIST_PORTAL)
        break
      case FactionNames.PUNK:
        this.portalElt.classList.add(PortalClassConstants.PUNK_PORTAL)
        break
      case FactionNames.TECHNOPHILE:
        this.portalElt.classList.add(PortalClassConstants.TECHNOPHILE_PORTAL)
        break
      default:
        this.portalElt.classList.add(PortalClassConstants.NOFACTION_PORTAL)
        break
      }
    }
  }

}
