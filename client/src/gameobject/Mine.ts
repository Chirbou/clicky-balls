import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'

export class Mine extends GameObject {
  type: GameObjectType = GameObjectType.MINE
  mineElt: HTMLDivElement
  textWrapper: HTMLDivElement
  counter = 0
  countdownInterval: any
  aoeRadius = 200

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public faction: FactionNames,
    public cooldownBeforeActivation: number
  ) {
    super()
    this.mineElt = document.createElement('div')
    this.textWrapper = document.createElement('div')
    this.textWrapper.classList.add('flagTextWrapper')
    this.textWrapper.innerText = ''
    this.textWrapper.style.fontSize = '25px'
    this.textWrapper.style.width = `${size}px`
    this.textWrapper.style.height = `${size}px`
    this.mineElt.classList.add(GameConstants.MINE_ELT_CLASS_NAME)
    this.mineElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.mineElt.style.top = `${position.y - this.size / 2}px`
    this.mineElt.style.left = `${position.x - this.size / 2}px`
    this.mineElt.style.width = `${size}px`
    this.mineElt.style.height = `${size}px`
    if (Game.mainPlayer.faction === this.faction) {
      this.mineElt.style.opacity = '0.15'
    } else {
      this.mineElt.style.animation = 'concealed 5s infinite'
      this.mineElt.style.opacity = '0'
    }
    this.mineElt.dataset.id = id
    this.mineElt.classList.add(Mine.getFlagClassFromFaction(faction))
    this.mineElt.appendChild(this.textWrapper)
    Game.gameZone.appendChild(this.mineElt)
  }

  popCountdown() {
    this.counter = Math.round((+(this.mineElt.dataset?.cooldown || '0')) / 1000)
    if (this.countdownInterval) {
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    clearInterval(this.countdownInterval)
  }

  static getFlagClassFromFaction(faction: FactionNames) {
    switch (faction) {
      case FactionNames.PUNK:
        return GameConstants.FLAG_PUNK
      case FactionNames.CONSERVATIST:
        return GameConstants.FLAG_CONSERVATIST
      case FactionNames.TECHNOPHILE:
        return GameConstants.FLAG_TECHNOPHILE
      default:
        return ''
    }
  }

  remove() {
    this.mineElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }
  explode() {
    AnimationGenerator.popAoe(1500, this.aoeRadius, this.position, 'aoe-damage-animation')
  }
  refresh() {
    this.mineElt.style.top = `${this.position.y - this.size / 2}px`
    this.mineElt.style.left = `${this.position.x - this.size / 2}px`
    this.mineElt.style.width = `${this.size}px`
    this.mineElt.style.height = `${this.size}px`
  }

  on(e) {
    this.explode()
  }

  miss(e) {

  }

  off(e) {

  }

  complete(e) {

  }
}
