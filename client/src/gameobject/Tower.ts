import { AlterationNames } from '../alteration/AlterationNames'
import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'
import { HealthBar } from '../ui/HealthBar'
import { IHoverable, makeHoverable } from '../ui/stats/IHoverable'
import { Util } from '../Util'

export const DEFAULT_TOWER_MAXHP = 1000
export enum Attitude {
  HEAL = 'HEAL',
  ATTACK = 'ATTACK',
}
interface TowerData extends GameObject {
  orientation: number
  alterations: any[]
  attitude: Attitude
}

export class Tower extends GameObject implements IHoverable {
  type: GameObjectType = GameObjectType.TOWER
  towerElt: HTMLDivElement
  towerTextElt: HTMLDivElement
  towerImageElt: HTMLDivElement
  healthBar: HealthBar | null = null
  counter = 0
  countdownInterval: any
  orientation = 0
  attitude: Attitude = Attitude.HEAL
  alterations: any[] = []

  constructor(
    public id: string,
    public position: { x: number; y: number },
    public size: number = 10,
    public faction: FactionNames,
    public cooldownBeforeActivation: number,
    public amount: number,
    public hp: number,
    public maxHp: number = DEFAULT_TOWER_MAXHP
  ) {
    super()
    this.towerElt = document.createElement('div')
    this.towerElt.classList.add(GameConstants.TOWER_ELT_CLASS_NAME)
    // use the correct FactionColor and apply a filter to the tower of that color
    this.towerElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.towerElt.style.top = `${position.y - this.size / 2}px`
    this.towerElt.style.left = `${position.x - this.size / 2}px`
    this.towerElt.style.width = `${this.size}px`
    this.towerElt.style.height = `${this.size}px`
    this.towerElt.style.transition = 'transform .1s !important'
    this.towerElt.dataset.id = id
    this.towerImageElt = document.createElement('div')
    this.towerImageElt.classList.add('towerImage')
    switch (this.attitude) {
      case Attitude.HEAL:
        this.towerImageElt.style.backgroundImage = `url('../assets/towerHeal.png')`
        break
      case Attitude.ATTACK:
        this.towerImageElt.style.backgroundImage = `url('../assets/towerAttack.png')`
        break
      default:
        this.towerImageElt.style.backgroundImage = `url('../assets/towerHeal.png')`
        break
    }

    this.towerTextElt = document.createElement('div')
    this.towerTextElt.classList.add(GameConstants.SCORE_TEXT_CLASS)
    this.towerElt.appendChild(this.towerTextElt)
    this.towerElt.appendChild(this.towerImageElt)
    const healthStatus = { hp: this.hp, maxHp: this.maxHp, size: this.size, faction: this.faction }
    this.healthBar = new HealthBar(healthStatus)
    this.healthBar.addTo(this.towerElt)
    Game.gameZone.appendChild(this.towerElt)
    this.hoverableName += this.faction === Game.mainPlayer?.faction ? ' alliée' : ' ennemie'
    makeHoverable(this, this.towerElt)

    this.refresh()
  }
  hoverableName: string = 'Tour'
  hoverableDescription: string = 'A tower that heals or damages enemies'
  hoverableLeftClickDescription: string = 'Attaquer'
  hoverableRightClickDescription: string = 'Se téléporter'

  popCountdown() {
    this.counter = Math.round(+(this.towerElt.dataset?.cooldown || '0') / 1000)
    if (this.countdownInterval) {
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter} `, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    clearInterval(this.countdownInterval)
  }

  remove() {
    this.towerElt.remove()
    Game.gameObjects = Game.gameObjects.filter((g) => g.id !== this.id)
  }

  refresh() {
    this.towerElt.style.top = `${this.position.y - this.size / 2} px`
    this.towerElt.style.left = `${this.position.x - this.size / 2} px`
    this.towerElt.style.width = `${this.size} px`
    this.towerElt.style.height = `${this.size} px`
    this.towerImageElt.style.transform = `rotate(${this.orientation - 90}deg)`
    switch (this.attitude) {
      case Attitude.HEAL:
        this.towerImageElt.style.backgroundImage = `url('../assets/towerHeal.png')`
        break
      case Attitude.ATTACK:
        this.towerImageElt.style.backgroundImage = `url('../assets/towerAttack.png')`
        break
      default:
        this.towerImageElt.style.backgroundImage = `url('../assets/towerHeal.png')`
        break
    }
    this.healthBar?.update({
      hp: this.hp,
      maxHp: this.maxHp,
      size: this.size,
      faction: this.faction,
    })

    if (this.alterations.find((a) => a.name === AlterationNames.MECHANOTHERAPIST_BUFFED)) {
      this.towerElt.style.outline = '4px solid blue'
    } else {
      this.towerElt.style.outline = 'none'
    }
  }

  on(e) {
    this.popCountdown()
  }

  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popAnimation(
      'Bloqué 🤫🤫',
      clickedCoords,
      'yellow',
      true,
      AnimationGenerator.POP_SHIFT
    )
  }

  off(e) { }

  complete(e) {
    this.amount += e.amount
    this.refresh()
  }

  update(gameObjectData: TowerData) {
    this.type = gameObjectData.type
    this.size = gameObjectData.size
    this.position = gameObjectData.position
    this.faction = gameObjectData?.faction as any
    this.cooldownBeforeActivation = gameObjectData.cooldownBeforeActivation
    this.amount = gameObjectData?.amount as any
    this.hp = gameObjectData?.hp || 0
    this.activated = gameObjectData?.activated || false
    this.orientation = gameObjectData?.orientation
    this.alterations = gameObjectData?.alterations || []
    this.attitude = gameObjectData?.attitude || Attitude.HEAL

    this.refresh()
  }
}
