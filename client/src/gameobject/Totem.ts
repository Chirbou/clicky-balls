import { AnimationGenerator } from '../AnimationGenerator'
import { FactionNames } from '../ClassNames'
import { Game } from '../Game'
import { GameConstants } from '../GameConstants'
import { GameObject } from '../GameObject.js'
import { GameObjectType } from '../GameObjectType'
import { HealthBar } from '../ui/HealthBar'

export class Totem extends GameObject{
  type:GameObjectType = GameObjectType.TOTEM
  totemElt: HTMLDivElement
  textWrapper: HTMLDivElement
  counter = 0
  countdownInterval: any
  aoeRadius = 200
  healthBar:HealthBar

  constructor(
    public id: string,
    public position: { x: number, y: number },
    public size: number,
    public faction: FactionNames,
    public cooldownBeforeActivation: number,
    public hp: number
  ) {
    super()
    this.totemElt = document.createElement('div')
    this.textWrapper = document.createElement('div')
    this.textWrapper.classList.add('flagTextWrapper')
    this.textWrapper.style.fontSize='25px'
    this.textWrapper.style.width = `${size}px`
    this.textWrapper.style.height = `${size}px`
    this.totemElt.classList.add(GameConstants.TOTEM_ELT_CLASS_NAME)
    this.totemElt.dataset.cooldown = `${cooldownBeforeActivation}`
    this.totemElt.style.top = `${position.y - this.size/2}px`
    this.totemElt.style.left = `${position.x - this.size/2}px`
    this.totemElt.style.width = `${size}px`
    this.totemElt.style.height = `${size}px`
    this.totemElt.dataset.id = id
    this.totemElt.classList.add(Totem.getTotemClassFromFaction(faction))
    this.totemElt.appendChild(this.textWrapper)
    this.healthBar = new HealthBar({ hp : this.hp, maxHp : 100, size: this.size, faction : this.faction })
    this.healthBar.addTo(this.totemElt)
    Game.gameZone.appendChild(this.totemElt)
  }

  popCountdown() {
    this.counter = Math.round((+(this.totemElt.dataset?.cooldown || '0')) / 1000)
    if (this.countdownInterval){
      clearInterval(this.countdownInterval)
    }
    this._popCountdown()
    this.countdownInterval = window.setInterval(this._popCountdown, 1000)
  }

  _popCountdown = () => {
    AnimationGenerator.popCountdownAnimation(`${this.counter}`, this.position, 'black')
    this.counter -= 1
    if (this.counter <= 0) {
      clearInterval(this.countdownInterval)
    }
  }

  cancelCountdown = () => {
    clearInterval(this.countdownInterval)
  }

  static getTotemClassFromFaction(faction:FactionNames){
    switch(faction){
    case FactionNames.PUNK:
      return GameConstants.FLAG_PUNK
    case FactionNames.CONSERVATIST:
      return GameConstants.FLAG_CONSERVATIST
    case FactionNames.TECHNOPHILE:
      return GameConstants.FLAG_TECHNOPHILE
    default:
      return ''
    }
  }

  remove() {
    this.totemElt.remove()
    Game.gameObjects = Game.gameObjects.filter(g => g.id !== this.id)
  }
  radiate() {
    AnimationGenerator.popAoe(1500, this.aoeRadius, this.position, 'aoe-both-animation')
  }
  refresh(){
    this.totemElt.style.top = `${this.position.y - this.size/2}px`
    this.totemElt.style.left = `${this.position.x - this.size/2}px`
    this.totemElt.style.width = `${this.size}px`
    this.totemElt.style.height = `${this.size}px`
    this.healthBar.update({ hp : this.hp, maxHp : 100, size: this.size })
  }

  on(e) {
    this.radiate()
  }

  miss(e) {
    const clickedCoords = e.coords
    AnimationGenerator.popTextAnimation('Cannot take the flag 🤫🤫', clickedCoords, 'red', true, AnimationGenerator.POP_SHIFT)
  }

  off(e) {

  }

  complete(e){

  }
}
