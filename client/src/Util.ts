import { ClassNames, FactionNames } from './ClassNames'
import { allAdjectives } from './resources/adjectives'
import { Colors } from './resources/Colors'
import { allNouns } from './resources/nouns'
import { Coordinate } from './utils/InterfaceUtils'

export class Util {
  static getImagePathFromClassName(className) {
    switch (className) {
      case ClassNames.PECNO:
        return '/assets/skills/Atk_logo.jpg'
      case ClassNames.DPS:
        return '/assets/skills/DPS_Focus_logo.jpg'
      case ClassNames.HEALER:
        return '/assets/skills/Healer_Heal_logo.jpg'
      case ClassNames.TANK:
        return '/assets/skills/Tank_Invincibility.jpg'
      case ClassNames.PALADIN:
        return '/assets/skills/Paladin_Bubble.png'
      case ClassNames.POISON:
        return '/assets/skills/Poison_Vampirism.png'
      case ClassNames.BERSERK:
        return '/assets/skills/Berserk_Suffer.png'
      case ClassNames.PESTIFEROUS:
        return '/assets/skills/Pestiferous_Contagion.png'
      case ClassNames.ROGUE:
        return '/assets/skills/Rogue_Diversion.png'
      case ClassNames.DRUID:
        return '/assets/skills/Druid_Hotaoe.png'
      case ClassNames.PRIEST:
        return '/assets/skills/Priest_Hangon.png'
      case ClassNames.OCCULTIST:
        return '/assets/skills/Occultist_Curse.png'
      case ClassNames.SHAMAN:
        return '/assets/skills/Shaman_Totem.png'
      case ClassNames.TRAPPER:
        return '/assets/skills/Trapper_Sniper.png'
      case ClassNames.YUMHAX:
        return '/assets/skills/Yumhax_Godlike.png'
      default:
        return '/assets/skills/Atk_logo.jpg' // TODO : use ? img
    }
  }

  static getPlayerImageFromFaction(factionName: FactionNames) {
    switch (factionName) {
      case FactionNames.PUNK:
        return '/assets/green_player.png'
      case FactionNames.TECHNOPHILE:
        return '/assets/blue_player.png'
      case FactionNames.CONSERVATIST:
        return '/assets/red_player.png'
      default:
        return ''
    }
  }

  static getColorFromClassName(className: ClassNames) {
    switch (className) {
      case ClassNames.PECNO:
        return 'rgb(50, 50, 50)'
      case ClassNames.DPS:
        return 'rgb(180, 20, 30)'
      case ClassNames.HEALER:
        return 'rgb(10, 120, 20)'
      case ClassNames.TANK:
        return 'rgb(100, 100, 100)'
      case ClassNames.PALADIN:
        return 'rgb(100, 100, 100)'
      case ClassNames.POISON:
        return 'rgb(50, 100, 50)'
      case ClassNames.BERSERK:
        return 'rgb(100, 100, 100)'
      case ClassNames.ROGUE:
        return 'rgb(180, 20, 30)'
      case ClassNames.DRUID:
        return 'rgb(120, 70, 20)'
      case ClassNames.PRIEST:
        return 'rgb(10, 100, 80)'
      case ClassNames.MECHANOTHERAPIST:
        return 'rgb(10, 60, 80)'
      case ClassNames.TRAPPER:
        return 'rgb(10, 10, 10)'
      case ClassNames.OCCULTIST:
        return 'rgb(40, 10, 40)'
      case ClassNames.YUMHAX:
        return 'chartreuse'
      default:
        return 'black'
    }
  }
  static getColorNameByFaction(factionOfFlag) {
    switch (factionOfFlag) {
      case FactionNames.PUNK:
        return 'Verts'
      case FactionNames.TECHNOPHILE:
        return 'Bleus'
      case FactionNames.CONSERVATIST:
        return 'Rouges'
      default:
        return 'BUG'
    }
  }
  static isLight(color): boolean {
    let r, g, b

    // Check the format of the color, HEX or RGB?
    if (color.match(/^rgb/)) {
      // If HEX --> store the red, green, blue values in separate variables
      const colors = color.match(
        /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
      )

      r = colors[1]
      g = colors[2]
      b = colors[3]
    } else {
      // If RGB --> Convert it to HEX: http://gist.github.com/983661
      const colors = +(
        '0x' + color.slice(1).replace((color.length < 5 && /./g), '$&$&')
      )

      r = colors >> 16
      g = (colors >> 8) & 255
      b = colors & 255
    }
    return true
  }

  static generateName() {
    return `${allAdjectives[Math.ceil(Math.random() * (allAdjectives.length - 1))]
      } ${allNouns[Math.ceil(Math.random() * (allNouns.length - 1))]}`
  }

  static getColorFromFactionName(factionName: FactionNames) {
    switch (factionName) {
      case FactionNames.CONSERVATIST:
        return Colors.CONSERVATISTS_COLOR
      case FactionNames.PUNK:
        return Colors.PUNKS_COLOR
      case FactionNames.TECHNOPHILE:
        return Colors.TECHNOPHILES_COLOR
      default:
        return 'black'
    }
  }

  static getDistance(position1: Coordinate, position2: Coordinate) {
    const x1 = position1.x
    const x2 = position2.x
    const y1 = position1.y
    const y2 = position2.y
    const res = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
    return res
  }

  static getAngle(p1: Coordinate, p2: Coordinate) {
    return Util.getAngleRaw(p1.x, p1.y, p2.x, p2.y)
  }

  static getAngleRaw(cx: number, cy: number, ex: number, ey: number) {
    const dy = ey - cy
    const dx = cx - ex
    return Math.atan2(dx, dy) * 180 / Math.PI
  }

  static degreesToRadians(degrees: number): number {
    const pi = Math.PI
    return degrees * (pi / 180)
  }

  static getHueFromFactionName(factionName: FactionNames) {
    const color = Util.getColorFromFactionName(factionName)
    const rgb = Util.hexToRgb(color)
    const hsl = Util.rgbToHsl(rgb.r, rgb.g, rgb.b)
    return hsl.h
  }

  static hexToRgb(hex: string) {
    return {
      r: parseInt(hex.slice(1, 3), 16),
      g: parseInt(hex.slice(3, 5), 16),
      b: parseInt(hex.slice(5, 7), 16),
    }
  }

  static rgbToHsl(r: number, g: number, b: number) {
    (r /= 255), (g /= 255), (b /= 255)
    const max = Math.max(r, g, b),
      min = Math.min(r, g, b)
    let h,
      s,
      l = (max + min) / 2

    if (max == min) {
      h = s = 0 // achromatic
    } else {
      const d = max - min
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min)
      switch (max) {
        case r:
          h = (g - b) / d + (g < b ? 6 : 0)
          break
        case g:
          h = (b - r) / d + 2
          break
        case b:
          h = (r - g) / d + 4
          break
      }
      h /= 6
    }

    return { h, s, l }
  }
  static capitalizeFirstLetter(input: string): string {
    if (input.length === 0) {
      return input; // Ne rien faire si la chaîne est vide
    }
  
    return input.charAt(0).toUpperCase() + input.slice(1);
  }
}
