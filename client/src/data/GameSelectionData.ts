import { FactionNames } from "../ClassNames"

export interface FactionData {
  name: FactionNames
  goldAmount: number,
  playerCount: number
}

export interface GameSelectionData {
  factions: FactionData[]
  timer: number
}
