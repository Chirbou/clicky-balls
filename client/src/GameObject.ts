import { Game } from './Game'
import { GameObjectType } from './GameObjectType'
import { Coordinate } from './utils/InterfaceUtils'

export interface GameObjectData {
  type:GameObjectType
  position: Coordinate
  size: number
  cooldownBeforeActivation: number
  faction?: string
  amount?: number
  hp?: number
  activated?: boolean
  goldAmount?: number // TODO: remove and use amount (goldAmount is only used for gold deposit)
}

export abstract class GameObject {
  abstract type:GameObjectType
  abstract id:string
  abstract position
  abstract size: number
  abstract cooldownBeforeActivation: number
  amount?: number
  activated?: boolean

  faction?: string
  hp = 0
  setScore = (value) => {
    this.hp = value
    this.refresh()
  }
  abstract on(e)

  abstract off(e)

  abstract miss(e)

  abstract complete(e)

  static gameObjectOn(e){
    const gameObject = GameObject.getGameObjectFromId(e.id)
    gameObject?.on(e)
  }
  static gameObjectOff(e){
    const gameObject = GameObject.getGameObjectFromId(e.id)
    gameObject?.off(e)
  }
  static gameObjectComplete(e){
    const gameObject = GameObject.getGameObjectFromId(e.id)
    gameObject?.complete(e)
  }
  static gameObjectMiss(e){
    const gameObject = GameObject.getGameObjectFromId(e.id)
    gameObject?.miss(e)
  }
  static getGameObjectFromId(id){
    return Game.gameObjects.find(g => g.id === id)
  }
  abstract remove()
  abstract refresh()

  update(gameObjectData: GameObjectData) {
    this.type = gameObjectData.type
    this.size = gameObjectData.size
    this.position = gameObjectData.position
    this.faction = gameObjectData?.faction
    this.cooldownBeforeActivation = gameObjectData.cooldownBeforeActivation
    this.amount = gameObjectData?.amount
    this.hp = gameObjectData?.hp || 0
    this.activated = gameObjectData?.activated || false

    this.refresh()
  }

}
