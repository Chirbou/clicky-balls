export enum Colors {
    CONSERVATISTS_COLOR = '#c23616',
    PUNKS_COLOR = '#44bd32',
    TECHNOPHILES_COLOR = '#192a56'
}