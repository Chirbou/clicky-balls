import { ClassNames, FactionNames } from '../ClassNames'
import { GameActions } from '../GameEventActions'
import { Player } from '../Player'
import { SkillNames } from '../SkillNames'

export interface Coordinate {
  x: number
  y: number
}

export interface ScoreShiftData {
  idSource?: string
  idTarget?: string
  scoreSource?: number,
  scoreTarget?: number,
  type?: 'damage' | 'heal'
  value?: number
  isCritic?: boolean
  isSuccessful?: boolean
  skill?: SkillNames
}

export interface SkillFeedbackData {
  idSource?: string
  idTarget?: string
  sourcePlayer?: Player
  targetPlayer?: Player
  cooldown?: number
  skillName: any
  skillResults?: any
  position?: any
  skillOutput?: any
  gameZoneWidth: number
  gameZoneHeight: number
  aoeRadius?: number
}

export interface PlayerData {
  id: string
  name: string
  emoji: string
  position: Coordinate
  roomId: string
  hp: number
  maxHp: number
  currentClass: ClassNames
  scoreToEvolve: number | undefined
  scoreToDevolve: number | undefined
  color: string
  level: number
  atk: number
  def: number
  heal: number
  size: number
  ownedGold: number
  goldStackMax: number
  zIndex: number
  tpStackMax: number
  sickStack: number
  dead: boolean
  angle: number
  disabled: boolean
  alterations: any[]
  inventory: any[]
  martyrProtectorId: string | undefined
  gluedPlayersIds: string[] | undefined
  contaminedPlayersIds: string[] | undefined
  gameZoneWidth: number
  gameZoneHeight: number
  factionAffinity: any
  faction: any
  allPlayerCounter: number
  playersResume: any[] | undefined
}

export interface RoomData {
  id: string
  players: PlayerData[]
  gameObjects: any[]
  type: any
}

export interface StatsShift {
  atk: number
  def: number
  heal: number
  zIndex: number
  size: number
  tpStackMax: number
}

export interface ClasseStats {
  maxHp: number
  scoreToEvolve?: number
  scoreToDevolve?: number
}

export interface ClasseData {
  name: ClassNames
  archetype: ClassNames
  goldStackMax: number
  statsShift: StatsShift
  stats: ClasseStats
  skills: string[]
}

export interface EndGameData {
  eventName: string
  messageDuration: number
  [FactionNames.PUNK]: number
  [FactionNames.CONSERVATIST]: number
  [FactionNames.TECHNOPHILE]: number
}
