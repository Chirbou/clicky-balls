import { FactionNames } from "../../ClassNames"
import { translator } from "../../translations/i18n"
import { HealthBar } from "../HealthBar"
import { AffinityLine } from "./AffinityLine"

export class AffinitiesPanel {
    wrapperElt: HTMLDivElement
    punkAffinityBar!: HealthBar
    conservatistAffinityBar!: HealthBar
    technophileAffinityBar!: HealthBar
    affinitiesTextElt!: HTMLDivElement
    punkAffinityLine!: AffinityLine
    conservatistAffinityLine!: AffinityLine
    technophileAffinityLine!: AffinityLine

    constructor() {
        this.wrapperElt = document.createElement('div')
        this.wrapperElt.id = 'affinitiesPanel'
        this.createAffinitiesPanel()
    }

    createAffinitiesPanel() {
        this.createAffinitiesHeader()
        this.createAffinityBars()
    }

    createAffinitiesHeader() {
        this.affinitiesTextElt = document.createElement('div')
        this.affinitiesTextElt.classList.add('affinitiesText')
        this.affinitiesTextElt.innerText = 'Affinities'
        this.wrapperElt.appendChild(this.affinitiesTextElt)

        const separatorElt = document.createElement('div')
        separatorElt.classList.add('separator')
        this.wrapperElt.appendChild(separatorElt)
    }

    createAffinityBars() {
        this.punkAffinityBar = new HealthBar({ hp: 0, maxHp: 1000, })
        this.conservatistAffinityBar = new HealthBar({ hp: 0, maxHp: 1000, })
        this.technophileAffinityBar = new HealthBar({ hp: 0, maxHp: 1000, })
        this.punkAffinityLine = new AffinityLine(FactionNames.PUNK)
        this.conservatistAffinityLine = new AffinityLine(FactionNames.CONSERVATIST)
        this.technophileAffinityLine = new AffinityLine(FactionNames.TECHNOPHILE)

        this.punkAffinityLine.wrapperElt.appendChild(this.punkAffinityBar.healthBar)
        this.conservatistAffinityLine.wrapperElt.appendChild(this.conservatistAffinityBar.healthBar)
        this.technophileAffinityLine.wrapperElt.appendChild(this.technophileAffinityBar.healthBar)

        this.wrapperElt.appendChild(this.punkAffinityLine.wrapperElt)
        this.wrapperElt.appendChild(this.conservatistAffinityLine.wrapperElt)
        this.wrapperElt.appendChild(this.technophileAffinityLine.wrapperElt)

        //     this.createAffinityBar(this.punkAffinityBar, FactionNames.PUNK)
        //     this.createAffinityBar(this.conservatistAffinityBar, FactionNames.CONSERVATIST)
        //     this.createAffinityBar(this.technophileAffinityBar, FactionNames.TECHNOPHILE)
    }

    // createAffinityBar(affinityBar: HealthBar, factionName: FactionNames) {
    //     const affinityBarWrapperElt = document.createElement('div')
    //     affinityBarWrapperElt.classList.add('affinityBarWrapper')
    //     this.affinityTextElt = document.createElement('div')
    //     this.affinityTextElt.classList.add('affinityText')
    //     this.affinityTextElt.innerText = translator.t(`factions.${factionName}`)
    //     this.affinityTextElt.classList.add(factionName)
    //     affinityBarWrapperElt.appendChild(this.affinityTextElt)
    //     affinityBarWrapperElt.appendChild(affinityBar.healthBar)
    //     this.wrapperElt.appendChild(affinityBarWrapperElt)
    // }

    updateMainAffinity(factionName: FactionNames) {
        if (factionName === FactionNames.PUNK) {
            this.punkAffinityLine.activate()
            this.conservatistAffinityLine.deactivate()
            this.technophileAffinityLine.deactivate()
        } else if (factionName === FactionNames.CONSERVATIST) {
            this.punkAffinityLine.deactivate()
            this.conservatistAffinityLine.activate()
            this.technophileAffinityLine.deactivate()
        } else if (factionName === FactionNames.TECHNOPHILE) {
            this.punkAffinityLine.deactivate()
            this.conservatistAffinityLine.deactivate()
            this.technophileAffinityLine.activate()
        }
    }
}
