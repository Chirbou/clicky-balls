import { Game } from '../../Game'

export class GameStatsPaneler {
  GameStatsPaneler: HTMLDivElement
  GameStatsPanelerRoom: HTMLSpanElement
  GameStatsPanelerGame: HTMLSpanElement
  factionStatus: HTMLDivElement
  teamMatesClassIcon: HTMLSpanElement
  teamMatesName: HTMLSpanElement
  wrapperElt: HTMLDivElement
  headerElt: HTMLDivElement
  separatorElt: HTMLDivElement

  constructor() {

    //Todo refactor this AWFUL SHIT.
    //Le user counter doit permettre à l'utilisateur de savoir qui est connecté : 
    //Combien de personnes sont dans sa room
    //Combien de personne par faction
    //Etat des flags (qui possède quel flag, quel flag est "libre",quel flag est porté, si porté par qui ?)
    //Etats des goldDeposits par faction

    this.wrapperElt = document.createElement('div')
    this.wrapperElt.classList.add('GameStatsPanelerWrapper')
    this.headerElt = document.createElement('div')
    this.headerElt.classList.add('statHeaderText')
    this.headerElt.innerText = 'Teammates'
    this.wrapperElt.appendChild(this.headerElt)
    this.separatorElt = document.createElement('div')
    this.separatorElt.classList.add('separator')
    this.wrapperElt.appendChild(this.separatorElt)
    this.GameStatsPaneler = document.createElement('div')
    this.GameStatsPaneler.id = 'GameStatsPaneler'
    this.GameStatsPaneler.innerText = `🧑🏿‍🤝BEBOU : `
    this.GameStatsPanelerRoom = document.createElement('span')
    this.GameStatsPanelerRoom.id = 'GameStatsPanelerRoom'
    this.GameStatsPanelerRoom.innerText = 'Room'
    this.GameStatsPanelerGame = document.createElement('span')
    this.GameStatsPanelerGame.id = 'GameStatsPanelerGame'
    this.GameStatsPanelerGame.innerText = 'Game'
    this.GameStatsPaneler.appendChild(this.GameStatsPanelerRoom)
    this.GameStatsPaneler.appendChild(document.createTextNode(' / '))
    this.GameStatsPaneler.appendChild(this.GameStatsPanelerGame)
    this.factionStatus = document.createElement('div')
    this.factionStatus.id = 'factionStatus'
    this.factionStatus.classList.add('hidden')
    this.teamMatesClassIcon = document.createElement('span')
    this.teamMatesClassIcon.classList.add('teamMatesClassIcon')
    this.teamMatesClassIcon.innerText = 'Icon'
    this.teamMatesName = document.createElement('span')
    this.teamMatesName.classList.add('teamMatesName')
    this.teamMatesName.innerText = 'Name'
    this.factionStatus.appendChild(this.teamMatesClassIcon)
    this.factionStatus.appendChild(this.teamMatesName)
    this.GameStatsPaneler.appendChild(this.factionStatus)
    this.GameStatsPaneler.addEventListener('mousedown', (e: MouseEvent) => {
      if (e.button === 0 && Game.mainPlayer) {
        const factionStatus = document.getElementById('factionStatus')
        if (factionStatus) {
          factionStatus.classList.toggle('hidden')
        }
      }
    })
    this.wrapperElt.appendChild(this.GameStatsPaneler)
  }


  
}
