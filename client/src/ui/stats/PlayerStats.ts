import { GameConstants } from "../../GameConstants"
import { Stats } from "../../Stats"

export class PlayerStats {
  wrapperElt: HTMLDivElement

  static STATS_EMOJI_MAP = {
    atk: '⚔️',
    def: '🛡️',
    heal: '🍔',
    size: '↕️',
    maxHp: '💚',
    scoreToEvolve: '🧬↑',
    scoreToDevolve: '🧬↓',
    ownedGold: '💰',
    tpStackMax: '✨',
    punkAffinity: '🥦',
    conservatistAffinity: '🧱',
    technophileAffinity: '📱',
  }

  constructor() {
    this.wrapperElt = document.createElement('div')
    this.createStatsHeader(this.wrapperElt)
    this.wrapperElt.id = GameConstants.STATS_WRAPPER_ELT_ID
    this.createStat('atk', PlayerStats.STATS_EMOJI_MAP.atk, '?', 'Attack power')
    this.createStat('def', PlayerStats.STATS_EMOJI_MAP.def, '?', 'Defense power')
    this.createStat('heal', PlayerStats.STATS_EMOJI_MAP.heal, '?', 'Heal power')
    this.createStat('tpStackMax', PlayerStats.STATS_EMOJI_MAP.tpStackMax, '?', 'Teleport power')
  }

  createStatsHeader(wrapperElt: HTMLDivElement) {
    const header = document.createElement('div')
    const className = document.createElement('div')
    header.id = 'statsHeader'
    className.id = 'className'
    header.appendChild(className)
    wrapperElt.appendChild(header)
    const separatorElt = document.createElement('div')
    separatorElt.classList.add('separator')
    this.wrapperElt.appendChild(separatorElt)
  }

  createStat(statType, label, number, tooltip?) {
    const stat = document.createElement('div')
    stat.classList.add('stat')
    stat.dataset.statType = statType
    stat.innerHTML = `${this._getStatText(label, number)}`
    const tooltipTextElt = document.createElement('span')
    tooltipTextElt.classList.add('tooltiptext')
    stat.classList.add('tooltip')
    tooltipTextElt.innerText = tooltip
    stat.appendChild(tooltipTextElt)
    this.wrapperElt.appendChild(stat)
  }

  _getStatText(label: string, number: number) {
    if (!number && number !== 0) {
      return ''
    }
    return `${label} ${number}`
  }

  updateStat(statType, value) {
    const statToUpdate = document.querySelector(`#${GameConstants.STATS_PANEL_WRAPPER_ELT_ID} [data-stat-type=${statType}]`)
    if (statToUpdate) {
      const tooltipText = statToUpdate.querySelector('.tooltiptext')?.innerHTML

      let label = statType
      if (statType in PlayerStats.STATS_EMOJI_MAP) {
        label = PlayerStats.STATS_EMOJI_MAP[statType]
      }
      statToUpdate.innerHTML = this._getStatText(label, value)
      const tooltipTextElt = document.createElement('span')
      tooltipTextElt.classList.add('tooltiptext')
      if (tooltipText) tooltipTextElt.innerText = tooltipText
      statToUpdate.appendChild(tooltipTextElt)
    }
  }
  updateClass(classNameInfo) {
    const className = document.getElementById('className')
    if(className){
      className.innerText = `Classe : ${classNameInfo}`
    }
  }
}
