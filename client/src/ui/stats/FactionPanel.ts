import { FactionNames } from "../../ClassNames"
import { Util } from "../../Util"
import { FactionAndFlagData } from "./GameStatsPanel"

export class FactionPanel {
    classDisplayed = 0

    faction:FactionNames
    flagsContainerFaction: HTMLElement = document.createElement('div')
    factionPlayers: HTMLElement = document.createElement('span')
    factionGolds: HTMLElement = document.createElement('span')

    goldStatFaction: HTMLElement = document.createElement('div')

    factionGoldsPercent: HTMLElement = document.createElement('div')

    factionCard: HTMLElement = document.createElement('div')

    constructor(faction :FactionNames){
        this.faction = faction
        this.flagsContainerFaction.id = 'flagsContainer' + Util.capitalizeFirstLetter(faction)
        this.flagsContainerFaction.classList.add('flagsContainer')

        this.factionPlayers.id = `${faction}Players` 

        this.factionGoldsPercent.id = `${faction}GoldsPercent`

        this.factionCard.id = `${faction}FactionCard`
        this.factionCard.classList.add(`factionCard`)
        this.factionCard.classList.add(`${faction}BG`)

        this.goldStatFaction.id = `${faction}GoldsPercent`
        this.goldStatFaction.classList.add(`${faction}BG`)

        this.factionCard.appendChild(this.flagsContainerFaction)
        this.factionCard.appendChild(this.factionPlayers)
        this.factionCard.appendChild(this.factionGolds)
    
    
    
        this.factionCard.addEventListener('mouseup', (e: MouseEvent) => {
          this.togglePlayersResumeMenu()
        })
    }

    togglePlayersResumeMenu() {
        if (this.isOn(this.faction)) {
          this.classDisplayed--
          this.setOff(this.faction)
          if (this.classDisplayed == 0) {
            this.showPanels()
          }
        } else {
          this.classDisplayed++
          if (this.classDisplayed != 0) {
            this.hidePanels()
          }
          this.setOn(this.faction)
        }
      }
    isOn(faction) {
        let res = false
      
        const id = faction + 'FactionCard'
        const btn = document.getElementById(id)
      
        if (btn?.classList.contains('selected')) {
          res = true
        }
        return res
      }
      
    setOff(faction) {
        const id = faction + 'FactionCard'
        const btn = document.getElementById(id)
      
        btn?.classList.remove('selected')
      }
    setOn(faction) {
        const id = faction + 'FactionCard'
        const btn = document.getElementById(id)
      
        btn?.classList.add('selected')
      }
      
    showPanels() {
        const statsWrapper = document.getElementById('statsWrapper')
        const affinitiesPanel = document.getElementById('affinitiesPanel')
        const playerResumes = document.getElementById('playerResumes')
      
        statsWrapper?.classList.remove('displayNone')
        affinitiesPanel?.classList.remove('displayNone')
        playerResumes?.classList.add('displayNone')
      }
    hidePanels() {
        const statsWrapper = document.getElementById('statsWrapper')
        const affinitiesPanel = document.getElementById('affinitiesPanel')
        const playerResumes = document.getElementById('playerResumes')
      
        statsWrapper?.classList.add('displayNone')
        affinitiesPanel?.classList.add('displayNone')
        playerResumes?.classList.remove('displayNone')
      }
    update(FactionAndFlagDatas : FactionAndFlagData[]){
        const generalResume = FactionAndFlagDatas.find(f => f.faction === FactionNames.NOFACTION)
        const factionResume = FactionAndFlagDatas.find(f => f.faction === this.faction)
        if(!factionResume || !generalResume) return
            this.factionPlayers.innerText = `${factionResume.playerCount}`
            const factionGolds = factionResume.goldAmount
            this.factionGolds.innerText = `${factionGolds}`
            const totalGolds = generalResume.goldAmount || 1
            const factionPourcentage = (100 * factionGolds) / totalGolds
            this.goldStatFaction.style.width = factionPourcentage + "%"  
    }
    isSelected(){
        return this.factionCard.classList.contains('selected')
    }
}
