import { FactionNames } from "../../ClassNames"

export class FactionFlag{
    faction:FactionNames
    flagElt: HTMLElement = document.createElement('div')

    constructor(faction:FactionNames){
        this.faction = faction
        this.flagElt.id = `${this.faction}Flag`
    }
    
  updateFlagSituations(container) {
        this.flagElt.remove()
        this.flagElt = document.createElement('div')
        this.flagElt.id = this.faction + "Flag"
        container.appendChild(this.flagElt)
  }


}