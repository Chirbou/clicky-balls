import { Game } from "../../Game"
import { translator } from "../../translations/i18n"

export class EvolveButton {
  wrapperElt: HTMLDivElement

  constructor() {
    this.wrapperElt = document.createElement('div')
    this.wrapperElt.classList.add('evolutionButton')
    this.wrapperElt.innerText = translator.t(`levelUp`)

    this.wrapperElt.addEventListener('click', () => {
      Game.classSelectionMenu.open()
    })
  }

  show() {
    this.wrapperElt.classList.add('evolutionButton-on')
  }

  hide() {
    this.wrapperElt.classList.remove('evolutionButton-on')
  }

  open() {
    this.wrapperElt.classList.add('open')
  }
}
