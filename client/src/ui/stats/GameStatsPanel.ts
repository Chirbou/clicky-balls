import { FactionNames } from '../../ClassNames'
import { FactionFlag } from './FactionFlag';
import { FactionPanel } from './FactionPanel';

export interface FactionAndFlagData {
  faction: FactionNames,
  goldAmount: number,
  playerCount: number,
  flagData?: FlagDataStats
}
export interface FlagDataStats {
  faction: FactionNames;
  inBase: Boolean;
  baseFaction: FactionNames;
  freeOwnerFaction: FactionNames
}

export class GameStatsPanel {
  wrapperElt: HTMLElement = document.createElement('div')

  title: HTMLElement = document.createElement('div')
  globalPlayerStat: HTMLElement = document.createElement('span')

  whitebar: HTMLElement = document.createElement('div')

  bigContainer: HTMLElement = document.createElement('div')

  flagsContainerNeutral: HTMLElement = document.createElement('div')

  cardContainer: HTMLElement = document.createElement('div')

  goldStatContainer: HTMLElement = document.createElement('div')

  punkFlag = new FactionFlag(FactionNames.PUNK)
  conservatistFlag = new FactionFlag(FactionNames.CONSERVATIST)
  technophileFlag = new FactionFlag(FactionNames.TECHNOPHILE)

  goldStats: HTMLElement = document.createElement('div')

  punkFactionPanel = new FactionPanel(FactionNames.PUNK)
  conservatistFactionPanel = new FactionPanel(FactionNames.CONSERVATIST)
  technophileFactionPanel = new FactionPanel(FactionNames.TECHNOPHILE);

  constructor() {
    this.wrapperElt.id = 'wrapperFactionStats' // 

    this.title.id = 'titleFactionStats' //
    this.globalPlayerStat.id = 'globalPlayerStat' // Joueurs totaux

    this.whitebar.id = 'whitebar'
    this.bigContainer.id = 'bigContainer'
    this.cardContainer.id = 'cardContainer'

    this.flagsContainerNeutral.id = 'flagsContainerNeutral' // Flag unset
    this.flagsContainerNeutral.classList.add('flagsContainer')

    this.goldStats.id = 'goldStats'

    this.wrapperElt.innerHTML = ''

    this.title.appendChild(this.globalPlayerStat)

    this.wrapperElt.appendChild(this.title)
    this.wrapperElt.appendChild(this.whitebar)
    this.wrapperElt.appendChild(this.bigContainer)

    this.bigContainer.appendChild(this.flagsContainerNeutral)
    this.bigContainer.appendChild(this.cardContainer)
    this.bigContainer.appendChild(this.goldStats)

    this.goldStats.appendChild(this.punkFactionPanel.goldStatFaction)
    this.goldStats.appendChild(this.conservatistFactionPanel.goldStatFaction)
    this.goldStats.appendChild(this.technophileFactionPanel.goldStatFaction)

    this.cardContainer.appendChild(this.punkFactionPanel.factionCard)
    this.cardContainer.appendChild(this.conservatistFactionPanel.factionCard)
    this.cardContainer.appendChild(this.technophileFactionPanel.factionCard)
  }

  generalUpdate(factionAndFlagDatas : FactionAndFlagData[]) {
    const generalResume = factionAndFlagDatas.find(f => f.faction === FactionNames.NOFACTION)
    this.punkFactionPanel.update(factionAndFlagDatas)
    this.conservatistFactionPanel.update(factionAndFlagDatas)
    this.technophileFactionPanel.update(factionAndFlagDatas)
    this.globalPlayerStat.innerText = `Joueurs : ${generalResume?.playerCount}`
    this.checkOnFlag(factionAndFlagDatas)
  }

  updatePlayers(players) {
    let res = []
    const isPunkSearched = this.punkFactionPanel.isSelected()
    const isConservatistSearched = this.conservatistFactionPanel.isSelected()
    const isTechnophileSearched = this.technophileFactionPanel.isSelected()

    if (isPunkSearched) {
      res = res.concat(players.filter(p => p.faction === FactionNames.PUNK))
    }
    if (isConservatistSearched) {
      res = res.concat(players.filter(p => p.faction === FactionNames.CONSERVATIST))
    }
    if (isTechnophileSearched) {
      res = res.concat(players.filter(p => p.faction === FactionNames.TECHNOPHILE))
    }
    return res
  }

  updateFlagSituations(flagFaction, container) {
    switch (flagFaction) {
      case FactionNames.CONSERVATIST:
        this.conservatistFlag.updateFlagSituations(container)
        break;
      case FactionNames.PUNK:
        this.punkFlag.updateFlagSituations(container)
        break;
      case FactionNames.TECHNOPHILE:
        this.technophileFlag.updateFlagSituations(container)
        break;
      default:
        break;
    }
  }

  checkOnFlag(FactionAndFlagDatas) {
    FactionAndFlagDatas.forEach(Datas => {
      const flagId = Datas.faction + "Flag"
      if (Datas.flagData) {
        if (!Datas.flagData.inBase) {
          if (this.flagsContainerNeutral.innerHTML.includes(flagId)) {
            //vérifier le porteur
          } else {
            this.updateFlagSituations(Datas.faction, this.flagsContainerNeutral)
          }
        } else {
          switch (Datas.flagData.baseFaction) {
            case FactionNames.PUNK:
              if (!this.punkFactionPanel.flagsContainerFaction.innerHTML.includes(flagId)) {
                this.updateFlagSituations(Datas.faction, this.punkFactionPanel.flagsContainerFaction)
              }
              break;
            case FactionNames.CONSERVATIST:
              if (!this.conservatistFactionPanel.flagsContainerFaction.innerHTML.includes(flagId)) {
                this.updateFlagSituations(Datas.faction, this.conservatistFactionPanel.flagsContainerFaction)
              } break;
            case FactionNames.TECHNOPHILE:
              if (!this.technophileFactionPanel.flagsContainerFaction.innerHTML.includes(flagId)) {
                this.updateFlagSituations(Datas.faction, this.technophileFactionPanel.flagsContainerFaction)
              } break;
            default:
              break;
          }
        }
      }
    });
  }
}


