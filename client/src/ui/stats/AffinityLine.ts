import { FactionNames } from "../../ClassNames"
import { Stats } from "../../Stats"
import { translator } from "../../translations/i18n"

export class AffinityLine {
  wrapperElt: HTMLDivElement
  affinityTextElt: HTMLDivElement

  constructor(factionName: FactionNames) {
    this.wrapperElt = document.createElement('div')
    this.wrapperElt.classList.add('affinityBarWrapper')
    this.affinityTextElt = document.createElement('div')
    this.affinityTextElt.classList.add('affinityText')
    this.affinityTextElt.innerText = translator.t(`factions.${factionName}`)
    this.affinityTextElt.classList.add(factionName)
    this.wrapperElt.appendChild(this.affinityTextElt)
  }

  activate() {
    this.affinityTextElt.classList.add('active')
  }

  deactivate() {
    this.affinityTextElt.classList.remove('active')
  }
}
