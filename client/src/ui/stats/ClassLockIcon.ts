import { Game } from "../../Game";
import { Util } from "../../Util";
import { ClasseData } from "../../utils/InterfaceUtils";
import { UIClassCard } from "../SkillCard";
import { ClassLock } from "./ClassLock";

export class ClassLockIcon{
    isLocked?:boolean = false
    lock:HTMLElement = document.createElement('div')
    classeDef: ClasseData
    btnElt:HTMLElement = document.createElement('div')
    classLock
    classCardConteneur
    constructor(classeDef:ClasseData, conteneur, classCardConteneur, locked:boolean, classLock:ClassLock){
        this.classCardConteneur = classCardConteneur
        this.classLock = classLock
        this.isLocked=locked
        this.classeDef = classeDef
        this.lock.classList.add('lock')
        this.btnElt.style.backgroundImage = `url(${Util.getImagePathFromClassName(this.classeDef.name)})`
        this.btnElt.classList.add('classIconMenu')
        this.btnElt.appendChild(this.lock)

        conteneur.appendChild(this.btnElt);
        this.btnElt.onclick = () => this.displayClassCard()
        this.lock.onclick = () => this.isLocked ? this.unlock() : this.lockInClassLock()
        if(this.isLocked){
            this.lock.classList.add('locked')
            this.lock.classList.remove('lock')
        }

    }
    displayClassCard(){
        this.classCardConteneur.innerHTML = ''
        const classCard = new UIClassCard(this.classeDef,1,70)
        classCard.addTo(this.classCardConteneur)
    }
    lockInClassLock(){
        this.isLocked = true
        this.lock.classList.add('locked')
        this.lock.classList.remove('lock')

        const classesLockByArchetype = Game.classesLocks.find(cl => cl.key === this.classeDef.archetype)
        if(!classesLockByArchetype)return
        classesLockByArchetype.locks[0].key = this.classeDef.name

        this.classLock.iconClassMenu.forEach(i => {
            if(i.classeDef.archetype === this.classeDef.archetype && i.isLocked && i.classeDef!==this.classeDef){
                i.unlock()
            }
        });
    }
    unlock(){
        this.isLocked = false
        this.lock.classList.add('lock')
        this.lock.classList.remove('locked')
    }
}