import { ClassNames } from "../../ClassNames"
import { Game } from "../../Game"
import { NetworkEventHandler } from "../../NetworkEventHandler"
import { Util } from "../../Util"
import { ClasseData } from "../../utils/InterfaceUtils"
import { ClassLockIcon } from "./ClassLockIcon"

export class ClassLock {
    wrapper: HTMLElement = document.createElement('div')
    menuElt: HTMLElement = document.createElement('div')
    rightColumn: HTMLElement = document.createElement('div')

    dpsElt: HTMLElement = document.createElement('div')
    tankElt: HTMLElement = document.createElement('div')
    healElt: HTMLElement = document.createElement('div')

    classDef: ClasseData[] = Game.classesDef
    dpsMenu: HTMLElement = document.createElement('div')
    tankMenu: HTMLElement = document.createElement('div')
    healMenu: HTMLElement = document.createElement('div')

    iconClassMenu:ClassLockIcon[]= []

    classCardContainer: HTMLElement = document.createElement('div')

    constructor(conteneur) {
        this.dpsElt.classList.add('classIconMenu')
        this.tankElt.classList.add('classIconMenu')
        this.healElt.classList.add('classIconMenu')

        this.dpsElt.style.backgroundImage = `url(${Util.getImagePathFromClassName(ClassNames.DPS)})`
        this.tankElt.style.backgroundImage = `url(${Util.getImagePathFromClassName(ClassNames.TANK)})`
        this.healElt.style.backgroundImage = `url(${Util.getImagePathFromClassName(ClassNames.HEALER)})`

        this.rightColumn.id = 'rightColumn'
        this.wrapper.id = 'classLockWrapper'
        this.menuElt.id = 'classLockMenuElt'

        this.dpsMenu.classList.add('hidden', 'originClassMenu')
        this.tankMenu.classList.add('hidden', 'originClassMenu')
        this.healMenu.classList.add('hidden', 'originClassMenu')

        this.classCardContainer.classList.add('classCardContainer')

        for (let i = 0; i < this.classDef.length; i++) {
            let isLocked = false
            switch (this.classDef[i].archetype) {
                case ClassNames.DPS:
                    isLocked = this.isItLocked(this.classDef[i].archetype, this.classDef[i].name)
                    this.iconClassMenu.push(new ClassLockIcon(this.classDef[i], this.dpsMenu,this.classCardContainer,isLocked, this))
                    break;

                case ClassNames.TANK:
                    isLocked = this.isItLocked(this.classDef[i].archetype, this.classDef[i].name)
                    this.iconClassMenu.push(new ClassLockIcon(this.classDef[i], this.tankMenu,this.classCardContainer,isLocked,this))
                    break;

                case ClassNames.HEALER:
                    isLocked = this.isItLocked(this.classDef[i].archetype, this.classDef[i].name)
                    this.iconClassMenu.push(new ClassLockIcon(this.classDef[i], this.healMenu,this.classCardContainer,isLocked,this))
                    break;

                default:
                    break;
            }
        }

        this.wrapper.appendChild(this.rightColumn);
        this.wrapper.appendChild(this.menuElt);

        this.rightColumn.appendChild(this.dpsElt);
        this.rightColumn.appendChild(this.tankElt);
        this.rightColumn.appendChild(this.healElt);

        this.menuElt.appendChild(this.dpsMenu);
        this.menuElt.appendChild(this.tankMenu);
        this.menuElt.appendChild(this.healMenu);
        this.menuElt.appendChild(this.classCardContainer)

        conteneur.appendChild(this.wrapper);
        this.dpsElt.onclick = () => this.changeOriginClassMenu(ClassNames.DPS)
        this.tankElt.onclick = () => this.changeOriginClassMenu(ClassNames.TANK)
        this.healElt.onclick = () => this.changeOriginClassMenu(ClassNames.HEALER) 
        this.openFirstLockedClass()
        
        }

    changeOriginClassMenu(className: ClassNames) {
        switch (className) {
            case ClassNames.DPS:
                this.dpsElt.classList.add('selected')
                this.tankElt.classList.remove('selected')
                this.healElt.classList.remove('selected')

                this.dpsMenu.classList.remove('hidden')
                this.tankMenu.classList.add('hidden')
                this.healMenu.classList.add('hidden')
                break;
            case ClassNames.TANK:
                this.dpsElt.classList.remove('selected')
                this.tankElt.classList.add('selected')
                this.healElt.classList.remove('selected')

                this.dpsMenu.classList.add('hidden')
                this.tankMenu.classList.remove('hidden')
                this.healMenu.classList.add('hidden')
                break;
            case ClassNames.HEALER:
                this.dpsElt.classList.remove('selected')
                this.tankElt.classList.remove('selected')
                this.healElt.classList.add('selected')

                this.dpsMenu.classList.add('hidden')
                this.tankMenu.classList.add('hidden')
                this.healMenu.classList.remove('hidden')
                break;

            default:
                break;
        }
    }
    isItLocked(archetype:ClassNames, className:ClassNames){
        const classesLocks = Game.classesLocks.find(cl => cl.key === archetype)

        for (let index = 0; index < (classesLocks?.locks.length || 0); index++) {
            if(classesLocks?.locks[index].key === className){
                return true
            }
        }
        return false
    }

    openFirstLockedClass(){
        const classLockArchetype = Game.classesLocks.find(cl => cl.locks.length > 0)
        if(!classLockArchetype){
            this.changeOriginClassMenu(ClassNames.DPS)
            const iconClassMenuToOpen = this.iconClassMenu[0]
            if(iconClassMenuToOpen)
            iconClassMenuToOpen.displayClassCard()
        }else{
            const firstLockedClass = classLockArchetype.locks[0]
            if(classLockArchetype)
            this.changeOriginClassMenu(classLockArchetype.key)
            const iconClassMenuToOpen = this.iconClassMenu.find(cl => cl.classeDef.name === firstLockedClass.key)
            if(iconClassMenuToOpen)
            iconClassMenuToOpen.displayClassCard()
        }
    }
}


//TODO : 
// Pour savoir quel classe est en selection actuelle : L'entourer
// Savoir quelle classe est classe lock
// Pouvoir lock ou unlock des classes
// Pouvoir lock qu'une seule classe par archetype