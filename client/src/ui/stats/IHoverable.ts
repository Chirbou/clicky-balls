export interface IHoverable {
    hoverableName: string
    hoverableDescription: string
    hoverableLeftClickDescription: string
    hoverableRightClickDescription: string
}

export const makeHoverable = (hoverable: IHoverable, wrapper: HTMLElement ) => {
    const hoverableElt = document.createElement('div')
    hoverableElt.classList.add('hoverable')
    const hoverableName = document.createElement('div')
    hoverableName.classList.add('hoverableName')
    hoverableName.innerText = hoverable.hoverableName
    hoverableElt.appendChild(hoverableName)
    const hoverableDescription = document.createElement('div')
    hoverableDescription.classList.add('hoverableDescription')
    hoverableDescription.innerText = hoverable.hoverableDescription
    hoverableElt.appendChild(hoverableDescription)
    const hoverableLeftClickDescription = document.createElement('div')
    hoverableLeftClickDescription.classList.add('hoverableLeftClickDescription')
    hoverableLeftClickDescription.innerText = hoverable.hoverableLeftClickDescription
    hoverableElt.appendChild(hoverableLeftClickDescription)
    const hoverableRightClickDescription = document.createElement('div')
    hoverableRightClickDescription.classList.add('hoverableRightClickDescription')
    hoverableRightClickDescription.innerText = hoverable.hoverableRightClickDescription
    hoverableElt.appendChild(hoverableRightClickDescription)
    const hoverableActionsWrapper = document.createElement('div')
    hoverableActionsWrapper.classList.add('hoverableActionsWrapper')
    hoverableActionsWrapper.appendChild(hoverableLeftClickDescription)
    hoverableActionsWrapper.appendChild(hoverableRightClickDescription)
    hoverableElt.appendChild(hoverableActionsWrapper)

    console.log('hoverableElt', hoverableElt)
    wrapper.addEventListener('mouseenter', () => {
        console.log('mouseenter')
        hoverableElt.classList.add('hoverableActive')
    })
    wrapper.addEventListener('mouseleave', () => {
        hoverableElt.classList.remove('hoverableActive')
        console.log('mouseleave')
    })

    wrapper.appendChild(hoverableElt)


}

     
