import { FactionNames } from "../ClassNames";
import { GameSelectionData } from "../data/GameSelectionData";
import { Display } from "../Display";
import { Game } from "../Game";
import { GameConstants } from "../GameConstants";
import { GameEvents } from "../GameEventActions";
import { NetworkEventHandler } from "../NetworkEventHandler";
import { translator } from "../translations/i18n";
import { ClassLock } from "./stats/ClassLock";

export class SelectionScreen {
  selectionScreenElt!: HTMLDivElement
  nameInput!: HTMLInputElement
  emojiSelectionMenu!: HTMLDivElement
  factionSelectionMenu!: HTMLDivElement
  readyBtn!: HTMLButtonElement
  timerElt!: HTMLDivElement
  emojiNameWrapper: HTMLDivElement = document.createElement('div')
  topEmojiNameClassLockWrapper: HTMLDivElement = document.createElement('div')
  classLockElt

  constructor() {
    this.selectionScreenElt = document.getElementById(
      GameConstants.SELECTION_SCREEN_ID
    ) as HTMLDivElement;

    // Create name input
    this.nameInput = document.createElement("input");
    this.nameInput.type = "text";
    this.nameInput.placeholder = translator.t(`inputName`);
    this.nameInput.classList.add("nameInput");
    this.nameInput.maxLength = 30;
    this.nameInput.focus()

    // Create faction selection menu
    this.factionSelectionMenu = document.createElement("div");
    this.factionSelectionMenu.classList.add("factionSelectionMenu");

    const factionNames = Object.values(FactionNames);
    const random = Math.random()
    const randomFactionSelected = random < (1 / 3) ? FactionNames.CONSERVATIST : random < (2 / 3) ? FactionNames.PUNK : FactionNames.TECHNOPHILE
    factionNames.forEach((factionName) => {
      if (factionName === FactionNames.NOFACTION) return;
      const factionBtn = document.createElement("button");
      factionBtn.dataset.faction = factionName
      factionBtn.classList.add("factionBtn");
      factionBtn.classList.add(factionName);
      factionBtn.addEventListener("click", () => {
        this.factionSelectionMenu
          .querySelectorAll(".selected")
          .forEach((elt) => elt.classList.remove("selected"));
        factionBtn.classList.add("selected");
      });
      const factionGold = document.createElement("div");
      factionGold.classList.add('factionGold')
      const factionTitle = document.createElement("div");
      factionTitle.innerText = translator.t(`factions.${factionName}`);
      factionTitle.classList.add('factionTitle')
      const factionPlayers = document.createElement("div");
      factionPlayers.classList.add('factionPlayers')
      factionBtn.appendChild(factionGold)
      factionBtn.appendChild(factionTitle)
      factionBtn.appendChild(factionPlayers)
      if (factionName === randomFactionSelected) {
        factionBtn.classList.add("selected");
      }
      this.factionSelectionMenu.appendChild(factionBtn);
    });

    // Create emoji selection menu with pre-defined emojis
    const onEmojiBtnClick = (emojiBtn: HTMLButtonElement) => {
      this.emojiSelectionMenu
        .querySelectorAll(".selected")
        .forEach((elt) => elt.classList.remove("selected"));
      emojiBtn.classList.add("selected");
    };

    this.emojiSelectionMenu = document.createElement("div");
    this.emojiSelectionMenu.classList.add("emojiSelectionMenu");



    const emojis = [
      "👽",
      "👾",
      "🤖",
      "🧠",
      "🐱",
      "🐶",
      "🐭",
      "🦊",
      "🐻",
      "🐼",
      "🐯",
      "🦁",
      "🐵",
      "🐧",
      "🐦",
      "🐣",
      "🦆",
      "🦅",
      "🦉",
      "🦇",
      "🐺",
      "🐗",
      "🐝",
      "🐜",
      "🕷",
      "🦂",
      "😈",
      "👿",
      "👹",
      "👺",
      "🤡",
      "💩",
      "👻",
      "💀",
      "☠️",
      "🦝",
      "🦨",
      "🦦",
      "🐲",
      "🐛",
      "🍆",
      "🥒",
      "🍑",
      "🧂",
      "🐸",
      "🐢",
      "🐍",
      "🦀",
      "🦐",
      "🦞",
      "🐟",
      "🐬",
      "🐋",
      "🦈",
      "🦟",
    ];
    // Add a '?' button to select a random emoji
    const randomEmojiBtn = document.createElement("button");
    randomEmojiBtn.innerText = "?";
    randomEmojiBtn.classList.add("emojiBtn");
    randomEmojiBtn.addEventListener("click", () => {
      onEmojiBtnClick(randomEmojiBtn);
    });

    // Add all emojis to the menu
    this.emojiSelectionMenu.appendChild(randomEmojiBtn);
    emojis.forEach((emoji) => {
      const emojiBtn = document.createElement("button");
      emojiBtn.innerText = emoji;
      emojiBtn.classList.add("emojiBtn");
      emojiBtn.addEventListener("click", () => {
        onEmojiBtnClick(emojiBtn);
      });
      this.emojiSelectionMenu.appendChild(emojiBtn);
    });
    this.emojiSelectionMenu.querySelector(".emojiBtn")?.classList.add("selected");


    // Create ready button
    this.readyBtn = document.createElement("button");
    this.readyBtn.innerText = translator.t('startButton');
    this.readyBtn.classList.add("readyBtn");
    this.readyBtn.addEventListener("click", () => {
      this.validateSelection()
    });
    this.selectionScreenElt.addEventListener("keydown", (e) => {
      console.log(e)
      if (e.code === "Enter") {
        this.validateSelection()
      }
    })

    // Create timer element
    this.timerElt = document.createElement("div");
    this.timerElt.classList.add(`selectionTimerElt`)

    // Add Class to my shit
    this.emojiNameWrapper.classList.add('emojiNameWrapper')
    this.topEmojiNameClassLockWrapper.classList.add('topEmojiNameClassLockWrapper')
    
    // Add everything to the selection screen
    this.emojiNameWrapper.appendChild(this.nameInput);
    this.emojiNameWrapper.appendChild(this.emojiSelectionMenu);
    this.emojiNameWrapper.appendChild(this.factionSelectionMenu);
    this.topEmojiNameClassLockWrapper.appendChild(this.emojiNameWrapper);
    this.classLockElt = new ClassLock(this.topEmojiNameClassLockWrapper)
    this.selectionScreenElt.appendChild(this.topEmojiNameClassLockWrapper)
    this.selectionScreenElt.appendChild(this.readyBtn);
    this.selectionScreenElt.appendChild(this.timerElt);
  }

  validateSelection() {
    let selectedEmoji = this.emojiSelectionMenu.querySelector(".selected")?.innerHTML
    if (selectedEmoji === "?") {
      selectedEmoji = '' // leave blank (will be replaced by a random emoji on the server)
    }
    const faction = (this.factionSelectionMenu.querySelector(".selected") as HTMLElement).dataset.faction
    NetworkEventHandler.emit(GameEvents.PLAYER_SELECTION_READY, {
      name: this.nameInput.value,
      emoji: selectedEmoji,
      faction: faction,
      classesLocks : Game.classesLocks
    });
    this.selectionScreenElt.classList.add("hidden");
    Game.ready = true;
  }

  update(gameSelectionData: GameSelectionData) {
    for (const faction of gameSelectionData.factions) {
      const gold = document.querySelector(`.${faction.name} .factionGold`) as HTMLElement
      gold.innerText = `${faction.goldAmount} 💰`
      const player = document.querySelector(`.${faction.name} .factionPlayers`) as HTMLElement
      player.innerText = `${faction.playerCount} 🤖`
    }
    this.nameInput.focus()
  }
}
