import { FactionNames } from '../ClassNames'
import { translator } from '../translations/i18n'

export interface HealthStatus {
  hp: number
  maxHp: number
  size?: number
  hpToLvlUp?: number
  hpToLvlDown?: number
  lowColor?: string
  highColor?: string
  faction?: FactionNames
}

export const getShadeOfHealthColor = (currentHp: number, maxHp: number): string => {
  // From red to green
  const red = Math.round(255 * (1 - currentHp / maxHp))
  const green = Math.round(255 * (currentHp / maxHp))
  return `rgb(${red}, ${green}, 0)`
}

export const getShadeOfColor = (
  currentHp: number,
  maxHp: number,
  lowColor = 'rgb(150, 0, 0)',
  hightColor = 'rgb(0, 255, 0)',
): string => {
  // Return the shade of colors from lowColor to highColor from currentHp on maxHp
  const ratio = currentHp / maxHp
  const lowFormatted = lowColor.replace('rgb(', '').replace(')', '')
  const lowRed = +lowFormatted.split(',')[0]
  const lowGreen = +lowFormatted.split(',')[1]
  const lowBlue = +lowFormatted.split(',')[2]
  const highFormatted = hightColor.replace('rgb(', '').replace(')', '')
  const highRed = +highFormatted.split(',')[0]
  const highGreen = +highFormatted.split(',')[1]
  const highBlue = +highFormatted.split(',')[2]

  const red = Math.round(lowRed + (highRed - lowRed) * ratio)
  const green = Math.round(lowGreen + (highGreen - lowGreen) * ratio)
  const blue = Math.round(lowBlue + (highBlue - lowBlue) * ratio)

  return `linear-gradient(to right, rgb(${red - 100}, ${green - 100}, ${blue - 100}), rgb(${red}, ${green}, ${blue}))`
  // return `rgb(${red}, ${green}, ${blue})`
}

export class HealthBar {
  healthBar: HTMLElement
  healthGraph: HTMLElement
  healthScore: HTMLElement
  markerToLvlUp: HTMLElement
  markerToLvlDown: HTMLElement
  hp: number = 0

  constructor(source: HealthStatus) {
    this.healthBar = document.createElement('div')
    this.healthGraph = document.createElement('div')
    this.healthScore = document.createElement('div')
    this.markerToLvlUp = document.createElement('div')
    this.markerToLvlDown = document.createElement('div')
    this.healthBar.classList.add('healthBar')
    this.healthScore.classList.add('healthScore')
    this.healthGraph.classList.add('healthGraph')
    this.markerToLvlUp.classList.add('markerToLvl', 'up', 'tooltip')
    this.markerToLvlDown.classList.add('markerToLvl', 'down', 'tooltip')
    this.markerToLvlUp.innerText = '▼'
    this.markerToLvlDown.innerText = '▼'

    const tooltipLvlUpTextElt = document.createElement('span')
    tooltipLvlUpTextElt.classList.add('tooltiptext')
    tooltipLvlUpTextElt.innerText = translator.t('healthBar.lvlUp')
    this.markerToLvlUp.appendChild(tooltipLvlUpTextElt)

    const tooltipTextDownElt = document.createElement('span')
    tooltipTextDownElt.classList.add('tooltiptext')
    tooltipTextDownElt.innerText = translator.t('healthBar.lvlDown')
    this.markerToLvlDown.appendChild(tooltipTextDownElt)

    if (this.healthBar)
      this.healthBar.remove()

    this.healthBar.appendChild(this.healthGraph)
    this.healthBar.appendChild(this.healthScore)
    this.healthBar.appendChild(this.markerToLvlUp)
    this.healthBar.appendChild(this.markerToLvlDown)

    this.update(source)
  }

  addTo(element: HTMLElement) {
    element.appendChild(this.healthBar)
  }

  update(source: HealthStatus) {
    this.getBorderByFaction(source.faction)
    this.healthScore.innerHTML = `${source?.hp}`
    // this.healthBar.style.right = `${source.size/2 + 20}px`
    if (this.hp > source.hp) {
      this.healthScore.animate([
        { textShadow: `1px 1px 3px white` },
        { textShadow: `1px 1px 3px red` },
        { textShadow: `1px 1px 3px red` },
      ], {
        duration: 150,
        easing: 'linear',
      })
    } else if (this.hp < source.hp) {
      this.healthScore.animate([
        { textShadow: `1px 1px 3px white` },
        { textShadow: `1px 1px 3px green` },
        { textShadow: `1px 1px 3px green` },
      ], {
        duration: 150,
        easing: 'linear',
      })
    }
    this.hp = source?.hp
    const healthPourcent = Math.round(100 * (source?.hp / source?.maxHp))
    // this.healthGraph.style.backgroundColor = getShadeOfHealthColor(source?.hp, source?.maxHp)
    this.healthGraph.style.background = getShadeOfColor(
      source?.hp, source?.maxHp, source?.lowColor, source?.highColor
    )
    this.healthGraph.style.width = `${healthPourcent}%`

    if (source.hpToLvlUp) {
      const leftPosition = Math.round(100 * (source.hpToLvlUp / source.maxHp))
      this.markerToLvlUp.style.left = `${leftPosition}%`
      this.markerToLvlUp.style.visibility = 'visible'
    }

    if (source.hpToLvlDown) {
      const leftPosition = Math.round(100 * (source.hpToLvlDown / source.maxHp))
      this.markerToLvlDown.style.left = `${leftPosition}%`
      this.markerToLvlDown.style.visibility = 'visible'
    }
  }

  remove() {
    this.healthBar.remove()
  }
  getBorderByFaction(faction: any) {
    switch (faction) {
      case !faction:
        this.healthBar.classList.remove('punkPins')
        this.healthBar.classList.remove('conservatistPins')
        this.healthBar.classList.remove('technophilePins')
        this.healthBar.classList.add('noFactionPins')
        break;
      case FactionNames.NOFACTION:
        this.healthBar.classList.remove('punkPins')
        this.healthBar.classList.remove('conservatistPins')
        this.healthBar.classList.remove('technophilePins')
        this.healthBar.classList.add('noFactionPins')
        break;
      case FactionNames.PUNK:
        this.healthBar.classList.add('punkPins')
        this.healthBar.classList.remove('conservatistPins')
        this.healthBar.classList.remove('technophilePins')
        this.healthBar.classList.remove('noFactionPins')

        break;
      case FactionNames.CONSERVATIST:
        this.healthBar.classList.remove('punkPins')
        this.healthBar.classList.add('conservatistPins')
        this.healthBar.classList.remove('technophilePins')
        this.healthBar.classList.remove('noFactionPins')

        break;
      case FactionNames.TECHNOPHILE:
        this.healthBar.classList.remove('punkPins')
        this.healthBar.classList.remove('conservatistPins')
        this.healthBar.classList.add('technophilePins')
        this.healthBar.classList.remove('noFactionPins')

        break;
    }
  }
}
