
export class GoldBar {
    goldBar: HTMLElement
    goldGraph: HTMLElement
    maxGold: number
    constructor(ownedGold:number, maxGold:number) {
      this.maxGold = maxGold;
      this.goldBar = document.createElement('div')
      this.goldGraph = document.createElement('div')
      this.goldBar.classList.add('goldConteneur')
      this.goldGraph.classList.add('goldBar')
      this.goldBar.appendChild(this.goldGraph)


      if (this.goldBar)
        this.goldBar.remove()
  
      this.goldBar.appendChild(this.goldGraph)
  
      this.update(ownedGold, maxGold)
    }
  
    addTo(element: HTMLElement) {
      element.appendChild(this.goldBar)
    }
  
    update(ownedGold:number, maxGold:number) {
        this.goldGraph.style.width = (ownedGold/maxGold) * 100 + "%"
    }
  
    remove() {
      this.goldBar.remove()
    }
  }
  