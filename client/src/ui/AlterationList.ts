import { AlterationData } from '../alteration/AlterationData'
import { Stats } from '../Stats'
import { translator } from '../translations/i18n'

export class AlterationList {
  alterationListElt!: HTMLDivElement
  timeout!:number
  constructor() {
    this.alterationListElt = document.createElement('div')
    this.alterationListElt.classList.add('alterationListElt')
    Stats.topStatsElt.appendChild(this.alterationListElt)
  }

  addAlteration(alteration: AlterationData) {
    const alterationElt = document.createElement('div')
    alterationElt.classList.add('alteration')
    alterationElt.classList.add('tooltip')
    const alterationTextElt = document.createElement('div')
    alterationTextElt.classList.add('text')
    const alterationProgressionElt = document.createElement('div')
    alterationProgressionElt.classList.add('progression')
    alterationTextElt.innerText = translator.t(`alterations.${alteration.name}.name`)
    alterationElt.appendChild(alterationTextElt)
    alterationElt.appendChild(alterationProgressionElt)
    const tooltipTextElt = document.createElement('span')
    tooltipTextElt.innerText = translator.t(`alterations.${alteration.name}.tooltip`)
    tooltipTextElt.classList.add('tooltiptext')
    alterationElt.appendChild(tooltipTextElt)
    this.alterationListElt.appendChild(alterationElt)

    alterationProgressionElt.animate([
      { width: '100%' },
      { width: '0%' },
    ], {
      duration: alteration.cooldown,
      easing: 'linear',
    })

    this.timeout = setTimeout(() => {
      alterationElt.remove()
    }, alteration.cooldown)
  }
  removeAlteration(alteration: AlterationData){
    //todo
  }
}
