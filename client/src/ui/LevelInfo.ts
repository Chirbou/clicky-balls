import { ClassNames } from '../ClassNames'
import { Game } from '../Game'
import { GameActions } from '../GameEventActions'
import { NetworkEventHandler } from '../NetworkEventHandler'
import { Stats } from '../Stats'
import { translator } from '../translations/i18n'

export class LevelInfo {
  devolveButtonElt!: HTMLDivElement
  timeout: any

  constructor() {
    //this.levelNameElt.innerText = `Classe : ` + translator.t(`classes.${ClassNames.PECNO}.name`)

    this.devolveButtonElt = document.createElement('div')
    this.devolveButtonElt.classList.add('devolveButtonElt')
    this.devolveButtonElt.classList.add('disabled')
    this.devolveButtonElt.innerText = translator.t(`devolve`)

    this.devolveButtonElt.addEventListener('click', () => {
      if (this.devolveButtonElt.classList.contains('confirm')) {
        NetworkEventHandler.emit(GameActions.DEVOLVE, Game.mainPlayer.id)
        this.devolveButtonElt.innerText = translator.t(`devolve`)
      } else {
        this.devolveButtonElt.innerText = translator.t(`Are you sure ?`)
      }
      this.devolveButtonElt.classList.toggle('confirm')
      clearTimeout(this.timeout)
      this.timeout = setTimeout(() => {
        this.devolveButtonElt.innerText = translator.t(`devolve`)
        this.devolveButtonElt.classList.remove('confirm')
      }, 3000)
    })

    const statsHeader = document.getElementById('statsHeader')
    if(statsHeader){
      statsHeader.appendChild(this.devolveButtonElt)
    }
  }

  updateCurrentLevel(className: ClassNames) {
    if (className === ClassNames.PECNO) {
      this.devolveButtonElt.classList.add('disabled')
    } else {
      this.devolveButtonElt.classList.remove('disabled')
    }
    //this.levelNameElt.innerText = `Classe : ` + translator.t(`classes.${className}.name`)
    const statsHeader = document.getElementById('statsHeader')
    if(statsHeader){
      statsHeader.appendChild(this.devolveButtonElt)
    }
  }
  
}
