// EmoteTool : when clicked, the box opens and shows a list of emoji that are organized in a grid.
// When clicked, the EMOTE event is sent to the server.

import { AnimationGenerator } from "../AnimationGenerator"
import { NetworkEventHandler } from "../NetworkEventHandler"
import { Stats } from "../Stats"

export class EmoteTool {
    emoteToolElt!: HTMLDivElement
    emoteToolContentElt!: HTMLDivElement
    emoteToolHeaderElt!: HTMLDivElement
    emoteToolTextElt!: HTMLDivElement
    emoteToolSeparatorElt!: HTMLDivElement

    emotes = [
        '👍',
        '👎',
        '👌',
        '👊',
        '🤛',
        '🤜',
        '🤞',
        '🤟',
        '🤘',
        '🤙',
        '👋',
        '👏',
        '🙌',
        '👈',
        '👉',
        '👆',
        '👇',
    ]

    constructor() {
        this.emoteToolElt = document.createElement('div')
        this.emoteToolElt.classList.add('emoteToolElt')
        this.emoteToolElt.addEventListener('click', () => {
            this.toggleEmoteWindow()
        })

        this.emoteToolHeaderElt = document.createElement('div')
        this.emoteToolHeaderElt.classList.add('emoteToolHeaderElt')
        this.emoteToolHeaderElt.innerText = 'Emotes'
        this.emoteToolElt.appendChild(this.emoteToolHeaderElt)

        this.emoteToolSeparatorElt = document.createElement('div')
        this.emoteToolSeparatorElt.classList.add('separator')
        this.emoteToolElt.appendChild(this.emoteToolSeparatorElt)

        this.emoteToolTextElt = document.createElement('div')
        this.emoteToolTextElt.classList.add('emoteToolTextElt')
        this.emoteToolTextElt.innerText = '👍'
        this.emoteToolElt.appendChild(this.emoteToolTextElt)

        this.emoteToolContentElt = document.createElement('div')
        this.emoteToolContentElt.classList.add('emoteToolContentElt')
        this.emoteToolContentElt.classList.add('hidden')
        this.emoteToolContentElt.addEventListener('mouseleave', () => {
            this.emoteToolContentElt.classList.add('hidden')
        })
        this.emotes.forEach(emote => {
            const emoteElt = document.createElement('div')
            emoteElt.classList.add('emoteElt')
            emoteElt.innerText = emote
            emoteElt.addEventListener('click', (e) => {
                NetworkEventHandler.emit('EMOTE', emote)
            })
            this.emoteToolContentElt.appendChild(emoteElt)
        })

        this.emoteToolElt.appendChild(this.emoteToolContentElt)

        Stats.statsPanelWrapperElt.appendChild(this.emoteToolElt)
    }

    toggleEmoteWindow() {
        console.log('toggleEmoteWindow')
        this.emoteToolContentElt.classList.toggle('hidden')
    }

}
