import { ClassNames } from '../ClassNames'
import { Game } from '../Game'
import { GameActions } from '../GameEventActions'
import { ALL_SKILLS } from '../skill/SkillsDefs'
import { Stats } from '../Stats'
import { translator } from '../translations/i18n'
import { Util } from '../Util'
import { ClasseData } from '../utils/InterfaceUtils'
import { PlayerStats } from './stats/PlayerStats'

export const BASE_SKILLS = [GameActions.PECNO_ATK, GameActions.PECNO_TP]

export class UIClassCard {
  index: number
  cardElement: HTMLDivElement
  titleElement: HTMLDivElement
  headerElement: HTMLDivElement
  imageElement: HTMLDivElement
  skillsElement: HTMLDivElement
  statsElement: HTMLDivElement
  statsShiftWrapperElt: HTMLDivElement

  constructor(classeData: ClasseData, index: number, previousSize: number) {
    // const color = Util.getColorFromClassName(classeData.name)
    this.index = index
    this.cardElement = document.createElement('div')
    this.cardElement.classList.add('classe-card')
    this.cardElement.classList.add('glow-on-hover')
    //this.cardElement.style.backgroundColor = `${color}`
    this.titleElement = document.createElement('div')
    this.titleElement.classList.add('classe-card-title')
    //this.titleElement.style.backgroundColor = `${color}`
    this.headerElement = document.createElement('div')
    this.headerElement.classList.add('classe-card-header')
    this.imageElement = document.createElement('div')
    this.imageElement.classList.add('classe-card-image')
    this.skillsElement = document.createElement('div')
    this.skillsElement.classList.add('classe-card-skills')
    this.statsElement = document.createElement('div')
    this.statsElement.classList.add('classe-card-stats')

    this.titleElement.textContent = translator.t(`classes.${classeData.name}.name`)

    this.imageElement.style.display = 'flex'
    this.imageElement.style.gap = '10px'
    this.imageElement.style.alignItems = 'center'
    this.imageElement.style.justifyContent = 'center'

    const imageCurrentClasse = document.createElement('div')
    const imageNextClasse = document.createElement('div')

    imageNextClasse.style.backgroundImage = `url(${Util.getImagePathFromClassName(classeData.name)})`

    const borderWith = 2

    const oldSize = previousSize - borderWith / 2
    const newSize = oldSize + classeData.statsShift.size
    const sizeRatio = newSize / oldSize

    imageCurrentClasse.style.position = 'absolute'
    imageCurrentClasse.style.zIndex = '100000'
    imageCurrentClasse.style.outline = `${borderWith}px dashed rgb(80, 80, 80)`
    imageCurrentClasse.style.backgroundSize = 'cover'
    imageCurrentClasse.classList.add('breath')
    imageCurrentClasse.style.width = `${oldSize}px`
    imageCurrentClasse.style.height = `${oldSize}px`
    imageCurrentClasse.animate([
      { transform: 'scale(1)' },
      { transform: `scale(${sizeRatio})` },
      { transform: 'scale(1)' },
    ], {
      duration: 1000 * Math.abs(sizeRatio),
      iterations: Infinity,
      easing: 'ease-in-out',
    })

    imageNextClasse.style.backgroundSize = 'cover'
    imageNextClasse.style.width = `${oldSize}px`
    imageNextClasse.style.height = `${oldSize}px`
    imageNextClasse.style.opacity = '0.8'

    this.imageElement.appendChild(imageCurrentClasse)
    this.imageElement.appendChild(imageNextClasse)

    for (const skillName of classeData.skills) {
      const skill = ALL_SKILLS.find(s => s.event_name === skillName)
      if (!skill || BASE_SKILLS.some((s) => s === skill.event_name)) continue
      const skillElement = document.createElement('div')
      const skillElementTitle = document.createElement('div')
      const skillElementDescription = document.createElement('div')
      const skillElementImage = document.createElement('img')
      skillElementImage.src = skill.imgElt.getAttribute('src') || ''
      skillElement.classList.add('classe-card-skill')
      //skillElement.style.backgroundColor = `${color}`
      skillElementTitle.classList.add('classe-card-skill-title')
      skillElementDescription.classList.add('classe-card-skill-description')
      skillElementTitle.textContent = skill.name
      skillElementDescription.textContent = skill.tooltip

      skillElementTitle.prepend(skillElementImage)
      skillElement.appendChild(skillElementTitle)
      skillElement.appendChild(skillElementDescription)

      this.skillsElement.appendChild(skillElement)
    }

    this.statsShiftWrapperElt = document.createElement('div')
    this.statsShiftWrapperElt.classList.add('classe-card-stat-shift-wrapper')
    for (const key in classeData.statsShift) {
      if (key === 'size') {
        continue
      }
      const statShiftElement = document.createElement('div')

      const classDefArchetype = Game.classesDef.find(classe => classe.name === classeData.archetype)
      const classeDefPecno = Game.classesDef.find(classe => classe.name === ClassNames.PECNO)
      if (!classeDefPecno || !classDefArchetype) return
      const tpStackMaxArchetype = classeDefPecno.statsShift.tpStackMax + classDefArchetype.statsShift.tpStackMax

      statShiftElement.classList.add('classe-card-stat-shift')
      const emoji = PlayerStats.STATS_EMOJI_MAP[key]
      const shift = classeData.statsShift[key]
      const shiftText = `${classeData.statsShift[key] > 0 ? '+' : ''}${classeData.statsShift[key]}`
      const current = key === 'tpStackMax' ? tpStackMaxArchetype : tpStackMaxArchetype
      const total = current + shift
      const type = shift > 0 ? 'add' : shift === 0 ? 'null' : 'sub'

      if (emoji && shiftText && !isNaN(total)) {
        const statShiftEmojiElement = document.createElement('span')
        statShiftEmojiElement.classList.add('classe-card-stat-shift-emoji')
        statShiftEmojiElement.textContent = `${emoji}`
        const statShiftTotalElement = document.createElement('span')
        statShiftTotalElement.classList.add('classe-card-stat-shift-total')
        statShiftTotalElement.textContent = `${total}`
        const statShiftColorElement = document.createElement('span')
        statShiftColorElement.classList.add('classe-card-stat-shift-color')
        statShiftColorElement.classList.add(type)
        statShiftColorElement.textContent = `${shiftText}`

        statShiftElement.appendChild(statShiftEmojiElement)
        statShiftElement.appendChild(statShiftTotalElement)
        statShiftElement.appendChild(statShiftColorElement)
        this.statsShiftWrapperElt.appendChild(statShiftElement)
      }
    }

    for (const key in classeData.stats) {
      if (key === 'scoreToDevolve') {
        continue
      }

      const statShiftElement = document.createElement('div')

      const classDefArchetype = Game.classesDef.find(classe => classe.name === classeData.archetype)
      const classeDefPecno = Game.classesDef.find(classe => classe.name === ClassNames.PECNO)
      if (!classeDefPecno || !classDefArchetype) return

      statShiftElement.classList.add('classe-card-stat-shift')
      const emoji = PlayerStats.STATS_EMOJI_MAP[key]
      const shift = classeData.stats[key] - (classeDefPecno.stats[key] + classDefArchetype.stats[key])
      const shiftText = `${shift > 0 ? '+' : ''}${shift}`
      const current = Math.floor((classeDefPecno.stats[key] + classDefArchetype.stats[key]))
      const total = shift + current
      const type = shift > 0 ? 'add' : shift === 0 ? 'null' : 'sub'

      if (emoji) {

        const statShiftEmojiElement = document.createElement('span')
        statShiftEmojiElement.classList.add('classe-card-stat-shift-emoji')
        statShiftEmojiElement.textContent = `${emoji}`
        const statShiftTotalElement = document.createElement('span')
        statShiftTotalElement.classList.add('classe-card-stat-shift-total')
        statShiftTotalElement.textContent = `${total}`
        const statShiftColorElement = document.createElement('span')
        statShiftColorElement.classList.add('classe-card-stat-shift-color')
        statShiftColorElement.classList.add(type)
        statShiftColorElement.textContent = `${shiftText}`

        statShiftElement.appendChild(statShiftEmojiElement)
        statShiftElement.appendChild(statShiftTotalElement)
        statShiftElement.appendChild(statShiftColorElement)
      }

      this.statsShiftWrapperElt.appendChild(statShiftElement)
    }
    this.addGoldStackMaxStat(classeData.goldStackMax)
    this.headerElement.appendChild(this.imageElement)
    this.headerElement.appendChild(this.statsShiftWrapperElt)

    this.cardElement.appendChild(this.titleElement)
    this.cardElement.appendChild(this.headerElement)
    this.cardElement.appendChild(this.skillsElement)
  }

  addTo(element: HTMLElement) {
    element.appendChild(this.cardElement)
  }

  remove() {
    this.cardElement.remove()
  }

  onSelected(callback: (index: number) => void) {
    this.cardElement.addEventListener('click', (event) => {
      event.stopPropagation()
      event.preventDefault()
      callback(this.index)
    })
  }
  addGoldStackMaxStat(goldStackMax: number) {
    const goldStackMaxStat = document.createElement('div')
    goldStackMaxStat.classList.add('classe-card-stat-shift')
    const goldStackMaxNumber = document.createElement('span')
    goldStackMaxNumber.classList.add('classe-card-stat-shift-total')
    goldStackMaxNumber.textContent = `${goldStackMax}`
    const rightElt = document.createElement('span')
    rightElt.classList.add('classe-card-stat-shift-color')
    rightElt.textContent = `max`    
    const leftElt = document.createElement('span')
    leftElt.classList.add('classe-card-stat-shift-emoji')
    leftElt.textContent = `💰`

    goldStackMaxStat.appendChild(leftElt)
    goldStackMaxStat.appendChild(goldStackMaxNumber)
    goldStackMaxStat.appendChild(rightElt)
    this.statsShiftWrapperElt.appendChild(goldStackMaxStat)
  }
}
