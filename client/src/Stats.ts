import { GameConstants } from './GameConstants'
import { Game } from './Game'
import { ClassNames, FactionNames } from './ClassNames'
import { AnimationGenerator } from './AnimationGenerator'
import { ClasseData, PlayerData } from './utils/InterfaceUtils'
import { HealthBar } from './ui/HealthBar'
import { EvolveButton } from './ui/stats/EvolveButton'
import { AffinitiesPanel } from './ui/stats/AffinitiesPanel'
import { PlayerStats } from './ui/stats/PlayerStats'
import { LevelInfo } from './ui/LevelInfo'
import { EmoteTool } from './ui/EmoteTool'
import { Timer } from '../src/Timer'
import { GameStatsPanel } from './ui/stats/GameStatsPanel'
export class Stats {
  static statsPanelWrapperElt: HTMLDivElement
  static topStatsElt: HTMLDivElement
  static bottomStatsElt: HTMLDivElement
  static playerStats: PlayerStats
  static allPlayersCounter = 0
  static evolveButton: EvolveButton
  static affinitiesPanel: AffinitiesPanel
  static levelInfo: LevelInfo;
  static emoteTool: EmoteTool
  static timer: Timer
  static GameStatsPanel: GameStatsPanel
  static buttonBarElt: HTMLDivElement
  static playerResumes: HTMLDivElement

  static init() {
    const statsPanelWrapperElt = document.getElementById(GameConstants.STATS_PANEL_WRAPPER_ELT_ID)
    if (statsPanelWrapperElt) {
      Stats.statsPanelWrapperElt = statsPanelWrapperElt as HTMLDivElement
    }

    Stats.createTopStats()
    Stats.createBottomStats()

    Stats.statsPanelWrapperElt.appendChild(Stats.topStatsElt)
    Stats.statsPanelWrapperElt.appendChild(Stats.bottomStatsElt)
  }

  static createTopStats() {
    Stats.topStatsElt = document.createElement('div')
    Stats.topStatsElt.classList.add('topStats')

    Stats.affinitiesPanel = new AffinitiesPanel()
    Stats.topStatsElt.appendChild(Stats.affinitiesPanel.wrapperElt)

    Stats.playerStats = new PlayerStats()
    Stats.statsPanelWrapperElt.innerHTML = '' // clear stats panel in case of server refresh
    Stats.topStatsElt.appendChild(Stats.playerStats.wrapperElt)

    Stats.levelInfo = new LevelInfo();

    Stats.evolveButton = new EvolveButton()
    Stats.topStatsElt.appendChild(Stats.evolveButton.wrapperElt)

    Stats.playerResumes = document.createElement('div')
    Stats.playerResumes.classList.add('levelInfoElt')
    Stats.playerResumes.id = 'playerResumes'
    Stats.playerResumes.classList.add('displayNone')
    Stats.topStatsElt.appendChild(Stats.playerResumes)
  }

  static createBottomStats() {
    Stats.bottomStatsElt = document.createElement('div')
    Stats.bottomStatsElt.classList.add('bottomStats')

    Stats.buttonBarElt = document.createElement('div')
    Stats.buttonBarElt.classList.add('buttonBar')

    Stats.emoteTool = new EmoteTool()
    Stats.buttonBarElt.appendChild(Stats.emoteTool.emoteToolElt)

    Stats.GameStatsPanel = new GameStatsPanel()
    Stats.buttonBarElt.appendChild(Stats.GameStatsPanel.wrapperElt)

    Stats.bottomStatsElt.appendChild(Stats.buttonBarElt)
  }

  static createTimer(timeStampStart, timerDuration) {
    Stats.timer = new Timer(timeStampStart, timerDuration)
    Stats.bottomStatsElt.appendChild(Stats.timer.timerElt)
  }

  static refresh(playerData: PlayerData) {
    Stats.playerStats.updateClass(playerData.currentClass)
    Stats.playerStats.updateStat("atk", playerData.atk);
    Stats.playerStats.updateStat("def", playerData.def);
    Stats.playerStats.updateStat("heal", playerData.heal);
    Stats.playerStats.updateStat("score", playerData.hp);
    Stats.playerStats.updateStat("tpStackMax", playerData.tpStackMax);
    Stats.playerStats.updateStat("ownedGold", playerData.ownedGold);
    Stats.playerStats.updateStat("goldStackMax", playerData.goldStackMax);

    Stats.updateGauges(
      playerData.factionAffinity.punk,
      playerData.factionAffinity.conservatist,
      playerData.factionAffinity.technophile
    );
    Stats.affinitiesPanel.updateMainAffinity(playerData.faction)
  }


  static updateGauges(punk: number, conservatist: number, technophile: number) {
    const lowColorPunk = 'rgb(80, 100, 80)'
    const highColorPunk = 'rgb(0, 255, 0)'
    const lowColorConservatist = 'rgb(100, 80, 80)'
    const highColorConservatist = 'rgb(255, 0, 0)'
    const lowColorTechnophile = 'rgb(80, 80, 100)'
    const highColorTechnophile = 'rgb(0, 0, 255)'

    Stats.affinitiesPanel.punkAffinityBar?.update({
      hp: punk,
      maxHp: 1000,
      lowColor: lowColorPunk,
      highColor: highColorPunk,
    })
    Stats.affinitiesPanel.conservatistAffinityBar?.update({
      hp: conservatist,
      maxHp: 1000,
      lowColor: lowColorConservatist,
      highColor: highColorConservatist,
    })
    Stats.affinitiesPanel.technophileAffinityBar?.update({
      hp: technophile,
      maxHp: 1000,
      lowColor: lowColorTechnophile,
      highColor: highColorTechnophile,
    })
  }

  static playerByGameUpdate() {
    const GameStatsPanelerGame = document.getElementById('GameStatsPanelerGame')
    if (GameStatsPanelerGame) {
      GameStatsPanelerGame.innerText = this.allPlayersCounter.toLocaleString()
    }
  }

  static updatePlayersResume(mainPlayerFaction, playersResume) {
    let factionPlayers = playersResume.filter((p) => (p.id !== Game.mainPlayerId))

    factionPlayers = Stats.GameStatsPanel.updatePlayers(factionPlayers)

    const playerResumes = document.getElementById('playerResumes')

    if (playerResumes) {
      playerResumes.innerHTML = 'TEAMMATES'
    }
    if (factionPlayers.length > 0) {

      for (const factionPlayer of factionPlayers) {
        const resume = document.createElement('div')
        const icon = document.createElement('div')
        const name = document.createElement('span')

        resume.classList.add('teamMatesWrapper')
        icon.classList.add('teamMatesClassIcon')
        name.classList.add('teamMatesName')
        switch (factionPlayer.class) {
          case ClassNames.DPS:
            icon.style.backgroundImage = 'url(\'../assets/skills/DPS_Focus_logo.jpg\')'
            break
          case ClassNames.HEALER:
            icon.style.backgroundImage = 'url(\'../assets/skills/Healer_Heal_logo.jpg\')'
            break
          case ClassNames.TANK:
            icon.style.backgroundImage = 'url(\'../assets/skills/Tank_Invincibility.jpg\')'
            break
          case ClassNames.PECNO:
            icon.style.backgroundImage = 'url(\'../assets/skills/Atk_logo.jpg\')'
            break
        }
        switch (factionPlayer.faction) {
          case FactionNames.PUNK:
            icon.classList.add('punkMatesNames')
            break
          case FactionNames.CONSERVATIST:
            icon.classList.add('conservatistMatesNames')
            break
          case FactionNames.TECHNOPHILE:
            icon.classList.add('technophileMatesNames')
            break
          default:
            name.style.color = 'white'
            break
        }
        name.innerHTML = factionPlayer.name

        const healthBar = new HealthBar({
          hp: factionPlayer.score,
          maxHp: factionPlayer.maxHp,
          size: factionPlayer.size,
        })
        healthBar.addTo(resume)
        resume.appendChild(icon)
        resume.appendChild(name)
        playerResumes?.appendChild(resume)
      }

    }
  }

  static showEvolveButton(classes: ClasseData[]) {
    if (Game.mainPlayer)
      AnimationGenerator.popSlowAnimation('LVL UP!', Game.mainPlayer.position, 'red', false, AnimationGenerator.POP_SHIFT)
    Game.classSelectionMenu.canEvolve = true
    Game.classSelectionMenu.classes = classes
    Stats.evolveButton.show()
  }

  static hideEvolveButton() {
    Game.classSelectionMenu.canEvolve = false
    Stats.evolveButton.hide()
  }
}
