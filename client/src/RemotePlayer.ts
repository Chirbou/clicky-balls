import { Player } from './Player.js'
import { GameConstants } from './GameConstants.js'

export class RemotePlayer extends Player {
  static INITIAL_REMOTE_PLAYER_OPACITY = .6
// TODO typer les strings pour éviter les conflits
  constructor(
    id, name, position, color, 
    hp, maxHp, size, 
    emoji, ownedGold) {
    super(id, name, position, color, emoji)
    this.emoji=emoji
    this.score = hp
    this.maxHp = maxHp
    this.size = size
    this.ownedGold = ownedGold
    this.btnElt.classList.add(GameConstants.REMOTE_PLAYER_CLASS)
    this.refresh()
  }

}
