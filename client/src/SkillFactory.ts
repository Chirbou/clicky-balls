import { GameActions } from './GameEventActions.js'
import { Attack } from './skill/Attack.js'
import { DPS_Focus } from './skill/DPS_Focus.js'
import { Healer_Heal } from './skill/Healer_Heal.js'
import { Berserk_Suffer } from './skill/Berserk_Suffer.js'
import { Berserk_Bubble } from './skill/Berserk_Bubble.js'
import { Paladin_Bubble } from './skill/Paladin_Bubble.js'
import { Paladin_Grab } from './skill/Paladin_Grab.js'
import { Priest_Hangon } from './skill/Priest_Hangon.js'
import { Priest_Healzone } from './skill/Priest_Healzone.js'
import { Priest_Dispel } from './skill/Priest_Dispel.js'
import { DRUID_HOTAOE } from './skill/Druid_Hotaoe.js'
import { Druid_Stun } from './skill/Druid_Stun.js'
import { Rogue_Diversion } from './skill/Rogue_Diversion.js'
import { Rogue_Furtive_Hit } from './skill/Rogue_Furtive_Hit.js'
import { Rogue_Stab } from './skill/Rogue_Stab.js'
import { Poison_Bombkipu } from './skill/Poison_Bombkipu.js'
import { Pestiferous_Contagion } from './skill/Pestiferous_Contagion'
import { Pestiferous_Deflagration } from './skill/Pestiferous_Deflagration'
import { Pestiferous_Dispel } from './skill/Pestiferous_Dispel.js'
import { Poison_Vampirism } from './skill/Poison_Vampirism.js'
import { Poison_Fang } from './skill/Poison_Fang.js'
import { Tank_Invincibility } from './skill/Tank_Invincibility.js'
import { Tank_Silence } from './skill/Tank_Silence.js'
import { Occultist_Sucker } from './skill/Occultist_Sucker.js'
import { Occultist_Blow } from './skill/Occultist_Blow.js'
import { Occultist_Fucker } from './skill/Occultist_Fucker.js'
import { Toggle_Silence } from './skill/Toggle_Silence.js'
import { Tp } from './skill/Tp.js'
import { Trapper_Stealth } from './skill/Trapper_Stealth.js'
import { Trapper_Sniper } from './skill/Trapper_Sniper.js'
import { Trapper_Mine } from './skill/Trapper_Mine.js'
import { Shaman_Totem } from './skill/Shaman_Totem.js'
import { Shaman_Underpressure } from './skill/Shaman_Underpressure.js'
import { Shaman_Thunderbolt } from './skill/Shaman_Thunderbolt.js'
import { Mechanotherapist_Tower_Buff } from './skill/Mechanotherapist_Tower_Buff.js'
import { Mechanotherapist_Tower_Build } from './skill/Mechanotherapist_Tower_Build.js'
import { Mechanotherapist_Tower_Hack } from './skill/Mechanotherapist_Tower_Hack.js'
import { Berserk_Shell } from './skill/Berserk_Shell.js'

export class SkillFactory {
  static create(id) {
    switch (id) {
      case GameActions.PECNO_ATK:
        return new Attack()
      case GameActions.PECNO_TP:
        return new Tp()
      case GameActions.TANK_INVINCIBILITY:
        return new Tank_Invincibility()
      case GameActions.DPS_ATK:
        return new DPS_Focus()
      case GameActions.HEALER_HEAL:
        return new Healer_Heal()
      case GameActions.MECHANOTHERAPIST_TOWER_BUFF:
        return new Mechanotherapist_Tower_Buff()
      case GameActions.MECHANOTHERAPIST_TOWER_BUILD:
        return new Mechanotherapist_Tower_Build()
      case GameActions.MECHANOTHERAPIST_TOWER_HACK:
        return new Mechanotherapist_Tower_Hack()
      case GameActions.TANK_SILENCE:
        return new Tank_Silence()
      case GameActions.TOGGLE_SILENCE:
        return new Toggle_Silence()
      case GameActions.BERSERK_SUFFER:
        return new Berserk_Suffer()
      case GameActions.BERSERK_BUBBLE:
        return new Berserk_Bubble()
      case GameActions.BERSERK_SHELL:
        return new Berserk_Shell()
      case GameActions.PALADIN_BUBBLE:
        return new Paladin_Bubble()
      case GameActions.PALADIN_GRAB:
        return new Paladin_Grab()
      case GameActions.ROGUE_DIVERSION:
        return new Rogue_Diversion()
      case GameActions.ROGUE_FURTIVE_HIT:
        return new Rogue_Furtive_Hit()
      case GameActions.ROGUE_STAB:
        return new Rogue_Stab()
      case GameActions.PESTIFEROUS_CONTAGION:
        return new Pestiferous_Contagion()
      case GameActions.PESTIFEROUS_DEFLAGRATION:
        return new Pestiferous_Deflagration()
      case GameActions.PESTIFEROUS_DISPEL:
        return new Pestiferous_Dispel()
      case GameActions.POISON_BOMBKIPU:
        return new Poison_Bombkipu()
      case GameActions.POISON_VAMPIRISM:
        return new Poison_Vampirism()
      case GameActions.POISON_FANG:
        return new Poison_Fang()
      case GameActions.PRIEST_HANGON:
        return new Priest_Hangon()
      case GameActions.PRIEST_HEALZONE:
        return new Priest_Healzone()
      case GameActions.PRIEST_DISPEL:
        return new Priest_Dispel()
      case GameActions.OCCULTIST_SUCKER:
        return new Occultist_Sucker()
      case GameActions.OCCULTIST_BLOW:
        return new Occultist_Blow()
      case GameActions.OCCULTIST_FUCKER:
        return new Occultist_Fucker()
      case GameActions.DRUID_HOTAOE:
        return new DRUID_HOTAOE()
      case GameActions.DRUID_STUN:
        return new Druid_Stun()
      case GameActions.TRAPPER_STEALTH:
        return new Trapper_Stealth()
      case GameActions.TRAPPER_SNIPER:
        return new Trapper_Sniper()
      case GameActions.TRAPPER_MINE:
        return new Trapper_Mine()
      case GameActions.SHAMAN_TOTEM:
        return new Shaman_Totem()
      case GameActions.SHAMAN_UNDERPRESSURE:
        return new Shaman_Underpressure()
      case GameActions.SHAMAN_THUNDERBOLT:
        return new Shaman_Thunderbolt()
      default:
        throw new Error(`skill ${id} doesn't exist`)
    }
  }

  static createBulk(skillArray) {
    const res = []
    for (const skill of skillArray) {
      res.push(SkillFactory.create(skill) as never)
    }
    return res
  }
}
