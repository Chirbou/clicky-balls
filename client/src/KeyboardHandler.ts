import { Game } from './Game'

export enum KeyboardActions {
  'SPACE' = 'SPACE'
}

export interface CanHandleKeyboard {
  handleKeyboardNumber(index: number): void
  handleKeyboardActions(action: KeyboardActions): void
}

export class KeyboardHandler {
  constructor() {
    document.addEventListener('keydown', this.handleKeyboardEvent)
  }

  handleKeyboardEvent = (event: KeyboardEvent) => {
    let activeMenu = Game.skillbar
    if (Game.classSelectionMenu.isOpen) {
      activeMenu = Game.classSelectionMenu
    }
    if (event.key === '&' || event.key === '1' || event.key === 'v' ) {
      event.preventDefault(); activeMenu.handleKeyboardNumber(1)
    }
    if (event.key === 'É' || event.key === 'é' || event.key === '2' || event.key === 'b') {
      event.preventDefault(); activeMenu.handleKeyboardNumber(2)
    }
    if (event.key === '"' || event.key === '3' || event.key === 'n') {
      event.preventDefault(); activeMenu.handleKeyboardNumber(3)
    }
    if (event.key === '\'' || event.key === '4' || event.key === ',' || event.key === '?') {
      event.preventDefault(); activeMenu.handleKeyboardNumber(4)
    }
    if (event.key === '(' || event.key === '5' || event.key === ';' || event.key === '.') {
      event.preventDefault(); activeMenu.handleKeyboardNumber(5)
    }
    if (event.key === ' ') {
      event.preventDefault(); Game.classSelectionMenu.handleKeyboardActions(KeyboardActions.SPACE)
    }
  }
}
