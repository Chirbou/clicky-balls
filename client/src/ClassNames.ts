export enum ClassNames {
  PECNO = 'PECNO',
  TANK = 'TANK',
  HEALER = 'HEALER',
  DPS = 'DPS',
  PRIEST = 'PRIEST',
  PALADIN = 'PALADIN',
  BERSERK = 'BERSERK',
  PESTIFEROUS = 'PESTIFEROUS',
  ROGUE = 'ROGUE',
  TRAPPER = 'TRAPPER',
  OCCULTIST = 'OCCULTIST',
  SHAMAN = 'SHAMAN',
  POISON = 'POISON',
  DRUID = 'DRUID',
  MECHANOTHERAPIST = 'MECHANOTHERAPIST',
  YUMHAX = 'YUMHAX',
  WINSLOW = 'WINSLOW'
}

export enum FactionNames {
  PUNK = 'punk',
  CONSERVATIST = 'conservatist',
  TECHNOPHILE = 'technophile',
  NOFACTION = 'nofaction'
}
