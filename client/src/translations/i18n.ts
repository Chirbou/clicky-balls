import i18next from 'i18next'
import { en } from './locales/en'
import { fr } from './locales/fr'

i18next.init({
  lng: 'fr',
  debug: true,
  resources: {
    en: en,
    fr: fr,
  }
})

export const toggleLanguage = () => {
  i18next.changeLanguage(i18next.language === 'en' ? 'fr' : 'en')
}

export const translator = i18next
