/* eslint-disable max-len */
import { GameActions } from '../../GameEventActions'
import { ClassNames } from '../../ClassNames'
import { AlterationNames } from '../../alteration/AlterationNames'

export const fr = {
  translation: {
    classes: {
      [ClassNames.DPS]: {
        name: 'DPS'
      },
      [ClassNames.PECNO]: {
        name: 'PECNO'
      },
      [ClassNames.HEALER]: {
        name: 'HEALER'
      },
      [ClassNames.TANK]: {
        name: 'TANK'
      },
      [ClassNames.ROGUE]: {
        name: 'ROGUE'
      },
      [ClassNames.POISON]: {
        name: 'POISON'
      },
      [ClassNames.TRAPPER]: {
        name: 'TRAPPEUR'
      },
      [ClassNames.BERSERK]: {
        name: 'BERSERK'
      },
      [ClassNames.PALADIN]: {
        name: 'PALADIN'
      },
      [ClassNames.SHAMAN]: {
        name: 'SHAMAN'
      },
      [ClassNames.PESTIFEROUS]: {
        name: 'PESTIFÉRÉ'
      },
      [ClassNames.OCCULTIST]: {
        name: 'OCCULTIST'
      },
      [ClassNames.WINSLOW]: {
        name: 'WINSLOW'
      },
      [ClassNames.YUMHAX]: {
        name: 'YUMHAX'
      },
      [ClassNames.DRUID]: {
        name: 'DRUIDE'
      },
      [ClassNames.PRIEST]: {
        name: 'PRETRE'
      },
      [ClassNames.MECHANOTHERAPIST]: {
        name: 'MECANOTHERAPEUTE'
      },
    },
    skills: {
      [GameActions.PECNO_ATK]: {
        tooltip: 'Inflige des dégâts de base ou te soigne si tu es la cible !',
        name: 'Attaque',
      },
      [GameActions.BERSERK_BUBBLE]: {
        tooltip: 'Votre cible subit moins de dégâts mais tu en subis une certaine partie. Augmente aussi l\'attaque de ta cible.',
        name: 'Bulle Berserk',
      },
      [GameActions.ROGUE_DIVERSION]: {
        tooltip: 'Devenez invisible !',
        name: 'Diversion',
      },
      [GameActions.BERSERK_SUFFER]: {
        tooltip: 'Concentre tous les dégâts subits. Libère les dégâts concentrés autour de toi en une onde de choc après un certain temps.',
        name: 'Souffrance Berserk',
      },
      [GameActions.BERSERK_SHELL]: {
        tooltip: 'Augmente tes points de vie de manière temporaire',
        name: 'Coquille Berserk',
      },
      [GameActions.DPS_ATK]: {
        tooltip: 'Inflige de lourds dégâts à ta cible avec une probabilité élevée de critique.',
        name: 'Tir de canon',
      },
      [GameActions.DRUID_HOTAOE]: {
        tooltip: 'Soigne toutes les cibles dans la zone d\'effet de façon périodique.',
        name: 'Pluie de Soins',
      },
      [GameActions.DRUID_STUN]: {
        tooltip: 'Immobilise ta cible pendant un certain temps.',
        name: 'Étourdir',
      },
      [GameActions.HEALER_HEAL]: {
        tooltip: 'Un soin polyvalent, à la fois rapide et relativement puissant.',
        name: 'Soin',
      },
      [GameActions.OCCULTIST_BLOW]: {
        tooltip: 'Transfert 20% de ta vie à la cible',
        name: 'Aspiration',
      },
      [GameActions.OCCULTIST_FUCKER]: {
        tooltip: 'Tous les soins reçu par la cible sont transformés en dégat',
        name: 'Malédiction',
      },
      [GameActions.OCCULTIST_SUCKER]: {
        tooltip: 'Détourne les soins de la cible pendant un certain temps',
        name: 'Détournement',
      },
      [GameActions.PALADIN_BUBBLE]: {
        tooltip: 'Ta cible subit moins de dégâts pendant un certain temps. De plus, elle récupère plus de soins et davantage de téléportations.',
        name: 'Boule Paladin',
      },
      [GameActions.PALADIN_GRAB]: {
        tooltip: 'Garde ta cible à proximité de toi, où que tu ailles, pendant un certain temps.',
        name: 'Attraper',
      },
      [GameActions.POISON_BOMBKIPU]: {
        tooltip: 'Inflige des dégâts périodiques à toutes les cibles dans la zone d\'effet.',
        name: 'Nuage toxique',
      },
      [GameActions.PESTIFEROUS_CONTAGION]: {
        tooltip: 'Inflige des dégâts périodiques à toutes les cibles dans la zone et peut rendre malade tout autre joueur se trouvant dans la zone.',
        name: 'Contagion',
      },
      [GameActions.PESTIFEROUS_DEFLAGRATION]: {
        tooltip: 'Inflige de lourds dégats à tout les joueurs que vous avez rendu malade.',
        name: 'Deflagration',
      },
      [GameActions.PESTIFEROUS_DISPEL]: {
        tooltip: 'Dissipe toutes les altérations des joueurs présents dans la zone ciblée.',
        name: 'Dissipation de masse',
      },
      [GameActions.POISON_FANG]: {
        tooltip: 'Inflige une petite quantité de dégâts à ta cible. Soigne aussi la même quantité de dégâts.',
        name: 'Crochet',
      },
      [GameActions.POISON_VAMPIRISM]: {
        tooltip: 'Inflige des dégâts périodiques à ta cible. Soigne aussi la même quantité de dégâts.',
        name: 'Vampirisme',
      },
      [GameActions.PRIEST_HEALZONE]: {
        tooltip: 'Soigne toutes les cibles dans la zone d\'effet.',
        name: 'Zone de soins',
      },
      [GameActions.PRIEST_DISPEL]: {
        tooltip: 'La cible perd tout ses effets',
        name: 'Dissiper',
      },
      [GameActions.PRIEST_HANGON]: {
        tooltip: 'Accroche-toi sous ta cible et sois téléporté où qu\'elle aille pendant un certain temps.',
        name: 'S\'accrocher',
      },
      [GameActions.ROGUE_FURTIVE_HIT]: {
        tooltip: 'Une attaque qui a 1 chance sur 2 d\'être un coup critique.',
        name: 'Coup Furtif',
      },
      [GameActions.ROGUE_STAB]: {
        tooltip: 'Si tu es invisible, frappe ta cible pour une grande quantité de dégâts.',
        name: 'Poignarder',
      },
      [GameActions.SHAMAN_TOTEM]: {
        tooltip: 'Dépose un totem sur le sol. Le totem soigne tous les alliés dans la zone d\'effet et fait des dommages aux ennemis.',
        name: 'Totem',
      },
      [GameActions.SHAMAN_UNDERPRESSURE]: {
        tooltip: 'Inflige des dégâts à tous les joueurs autour de toi, y compris toi-même, et étourdit les joueurs qui ne sont pas dans ta faction.',
        name: 'Sous Pression',
      },
      [GameActions.SHAMAN_THUNDERBOLT]: {
        tooltip: 'Chaine de kiff, inflige des dommages.',
        name: 'Sous Pression',
      },
      [GameActions.TANK_INVINCIBILITY]: {
        tooltip: 'Deviens invulnérable pendant une courte période de temps.',
        name: 'Invulnérable',
      },
      [GameActions.TANK_SILENCE]: {
        tooltip: 'Rend ta cible silencieuse, l\'empêchant ainsi d\'agir pendant un certain temps.',
        name: 'Silence Tank',
      },
      [GameActions.TOGGLE_SILENCE]: {
        tooltip: 'Silence ta cible, on/off ! Muhahah',
        name: 'Silence',
      },
      [GameActions.PECNO_TP]: {
        tooltip: 'TP',
        name: 'TP',
      },
      [GameActions.TRAPPER_MINE]: {
        tooltip: 'Pose jusqu\'à 3 mines, actives au bout de quelques secondes, puis BOUM si quelqu\'un passe dans la zone autour de la mine !',
        name: 'Mine',
      },
      [GameActions.TRAPPER_SNIPER]: {
        tooltip: 'Ne rate jamais pour un maximum de dégâts ! Garde l\'invisibilité si le tir est réussi.',
        name: 'Sniper',
      },
      [GameActions.TRAPPER_STEALTH]: {
        tooltip: 'Deviens invisible de manière permanente jusqu\'à ta prochaine action.',
        name: 'Invisibilité',
      },
      [GameActions.MECHANOTHERAPIST_TOWER_BUILD]: {
        tooltip: 'Construit une tour qui inflige des dégâts à la cible la plus proche',
        name: 'Tower Build',
      },
      [GameActions.MECHANOTHERAPIST_TOWER_BUFF]: {
        tooltip: 'Augmente les dégâts de la tour pour un certain temps',
        name: 'Tower Damage Buff',
      },
      [GameActions.MECHANOTHERAPIST_TOWER_HACK]: {
        tooltip: 'Hack la tour ennemie pour la rendre amie',
        name: 'Tower Faction Hack',
      },
    },
    healthBar: {
      lvlUp: 'Atteint ce niveau pour lvl up',
      lvlDown: 'Attention tu pourrais perdre ton niveau ',
    },
    factions: {
      punk: `Punks`,
      conservatist: `Conservatistes`,
      technophile: `Technophiles`,
      nofaction: `Aucune faction`,
    },
    inputName: `Nom`,
    startButton: `Commencer !`,
    levelUp: `🧬 ÉVOLUER 🧬`,
    alterations: {
      [AlterationNames.BERSERK_BUBBLE]: {
        name: `Bubulle Berserk`,
        tooltip: `Bulle de rage berserk : dégâts augmentés !`,
      },
      [AlterationNames.BUFF_HEALTH]: {
        name: `Boost de vie`,
        tooltip: `Vos PV max sont augmentés`,
      },
      [AlterationNames.BUFF_ATK]: {
        name: `Boost ATK`,
        tooltip: `Plus d'attaque`,
      },
      [AlterationNames.BUFF_HEAL]: {
        name: `Boost Heal`,
        tooltip: `Plus de soin`,
      },
      [AlterationNames.CONTAGIOUS]: {
        name: `Contagieux`,
        tooltip: `Attention tu es contagieux`,
      },
      [AlterationNames.DEAD]: {
        name: `Mort...`,
        tooltip: `T'es mort, crevé, dead, cané, 6 pieds sous terre en train de bouffer le pissenlit par la racine.`,
      },
      [AlterationNames.FUCKED]: {
        name: `Baisé`,
        tooltip: `Avec un préservatif`,
      },
      [AlterationNames.GRABBED]: {
        name: `Attrapé`,
        tooltip: `Tu t'es solidement aggripé à quelqu'un`,
      },
      [AlterationNames.INVINCIBLE]: {
        name: `Invincible`,
        tooltip: `Personne ne peut te faire de dégat`,
      },
      [AlterationNames.INVISIBLE]: {
        name: `Invisible`,
        tooltip: `Personne ne peut te voir`,
      },
      [AlterationNames.MARTYR]: {
        name: `Martyr`,
        tooltip: `Tu subis une partie des dégats de ta cible`,
      },
      [AlterationNames.MECHANOTHERAPIST_BUFFED]: {
        name: `Tour buffée`,
        tooltip: `Tour buffée`,
      },
      [AlterationNames.MECHANOTHERAPIST_HACKED]: {
        name: `Tour hackée`,
        tooltip: `Tour hackée`,
      },
      [AlterationNames.PALADIN_BUBBLE]: {
        name: `Bubulle Palouf`,
        tooltip: `Protection divine : heal et téléportation augmentée !`,
      },
      [AlterationNames.SICK]: {
        name: `Malade`,
        tooltip: `Tu es malades et tu rends les autres malades`,
      },
      [AlterationNames.SILENCED]: {
        name: `Silence`,
        tooltip: `Tu ne peux plus bouger. Ni attaquer. Ni soigner. Ni rien.`,
      },
      [AlterationNames.STEALTH]: {
        name: `Camouflé`,
        tooltip: `Camouflé, personne ne peut te voir, mais attention, leurs visions pourraient être basé sur le mouvement`,
      },
      [AlterationNames.STUN]: {
        name: `Étourdi`,
        tooltip: `Gueule de boius prononcée`,
      },
      [AlterationNames.SUCKED]: {
        name: `Sucé`,
        tooltip: `Ça devient chaud ...`,
      },
      [AlterationNames.SUFFER]: {
        name: `Souffrant`,
        tooltip: `Souffre en enmagasinant le plus de dégat possible, que tu relâchera d'un seul coup.`,
      },
      [AlterationNames.TELEPORT]: {
        name: `Téléporté`,
        tooltip: `Téléporté`,
      },

    }
  }
}
