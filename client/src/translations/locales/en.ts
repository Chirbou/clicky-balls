/* eslint-disable max-len */
import { GameActions } from '../../GameEventActions'
import { ClassNames } from '../../ClassNames'
export const en = {
  translation: {
    classes: {
      [ClassNames.DPS]: {
        name: 'DPS'
      },
      [ClassNames.PECNO]: {
        name: 'PECNO'
      },
      [ClassNames.HEALER]: {
        name: 'HEALER'
      },
      [ClassNames.TANK]: {
        name: 'TANK'
      },
      [ClassNames.ROGUE]: {
        name: 'ROGUE'
      },
      [ClassNames.POISON]: {
        name: 'POISON'
      },
      [ClassNames.TRAPPER]: {
        name: 'TRAPPER'
      },
      [ClassNames.BERSERK]: {
        name: 'BERSERK'
      },
      [ClassNames.PALADIN]: {
        name: 'PALADIN'
      },
      [ClassNames.SHAMAN]: {
        name: 'SHAMAN'
      },
      [ClassNames.PESTIFEROUS]: {
        name: 'PESTIFEROUS'
      },
      [ClassNames.OCCULTIST]: {
        name: 'OCCULTIST'
      },
      [ClassNames.WINSLOW]: {
        name: 'WINSLOW'
      },
      [ClassNames.YUMHAX]:{
        name: 'YUMHAX'
      },
      [ClassNames.DRUID]:{
        name: 'DRUID'
      },
      [ClassNames.PRIEST]:{
        name: 'PRIEST'
      },
      [ClassNames.MECHANOTHERAPIST]:{
        name: 'MECHANOTHERAPIST'
      },
    },
    skills: {
      [GameActions.PECNO_ATK]: {
        tooltip: 'Deal basic damage or heal yourself if the target is you.',
        name: 'Attack',
      },
      [GameActions.ROGUE_DIVERSION]: {
        tooltip: 'Become invisible !',
        name: 'Diversion',
      },
      [GameActions.BERSERK_BUBBLE]: {
        tooltip: 'Your target gets less damage but you take a certain amount of damage it takes. Also boosts the attack of the target.',
        name: 'Berserk Bubble',
      },
      [GameActions.BERSERK_SUFFER]: {
        tooltip: 'Concentrate every damage you take. Release the concentrated damage in a shockwave around you after a certain amount of time.',
        name: 'Berserk Suffer',
      },
      [GameActions.DPS_ATK]: {
        tooltip: 'Deals damage to your target. High critical chance.',
        name: 'Cannon Shot',
      },
      [GameActions.DRUID_HOTAOE]: {
        tooltip: 'All the targets within the range of the area of effect are healed periodically',
        name: 'Healing Rain',
      },
      [GameActions.DRUID_STUN]: {
        tooltip: 'Immobilize your target for a certain amount of time.',
        name: 'Stun',
      },
      [GameActions.HEALER_HEAL]: {
        tooltip: 'Basic heal. Click on a target to use it!',
        name: 'Heal Zone',
      },
      [GameActions.OCCULTIST_BLOW]: {
        tooltip: 'Transfert 20% of your health to the target',
        name: 'Aspiration',
      },
      [GameActions.OCCULTIST_FUCKER]: {
        tooltip: 'Embezzle heals from the target during 10s',
        name: 'Curse',
      },
      [GameActions.OCCULTIST_SUCKER]: {
        tooltip: 'Embezzle heals from the target during 10s',
        name: 'Embezzlement',
      },
      [GameActions.PALADIN_BUBBLE]: {
        tooltip: 'Your target gets reduced damage for a certain amount of time. It also gets a more heal power and a teleport boost.',
        name: 'Paladin Bubble',
      },
      [GameActions.PALADIN_GRAB]: {
        tooltip: 'Keeps your target around you, wherever you go, for a certain amount of time.',
        name: 'Grab',
      },
      [GameActions.POISON_BOMBKIPU]: {
        tooltip: 'Deals periodical damage to all the targets within the area of effect.',
        name: 'Sick Zone',
      },
      [GameActions.PESTIFEROUS_CONTAGION]: {
        tooltip: 'Deals periodical damage to all the targets within the area and each target can make others players sicked.',
        name: 'Contagion',
      },
      [GameActions.PESTIFEROUS_DEFLAGRATION]: {
        tooltip: 'Deals large amount of damage to all the player you get sick.',
        name: 'Deflagration',
      },
      [GameActions.PESTIFEROUS_DISPEL]: {
        tooltip: 'Dispel alteration from every player in AOE.',
        name: 'Dispel AOE',
      },
      [GameActions.POISON_FANG]: {
        tooltip: 'Deal a small amount of damage to your target. Also heal yourself that damage.',
        name: 'Fang',
      },
      [GameActions.POISON_VAMPIRISM]: {
        tooltip: 'Deal periodical damage to your target. Heal yourself for the same amount of damage.',
        name: 'Vampirism',
      },
      [GameActions.PRIEST_HEALZONE]: {
        tooltip: 'Heal each target in a range around you.',
        name: 'Heal Zone',
      },
      [GameActions.PRIEST_DISPEL]: {
        tooltip: 'Removes the effect of the target.',
        name: 'Dispel',
      },
      [GameActions.PRIEST_HANGON]: {
        tooltip: 'Hang under your target and get teleported wherever it goes for a certain amount of time.',
        name: 'Hang On',
      },
      [GameActions.ROGUE_FURTIVE_HIT]: {
        tooltip: 'An attack that has 1 chance out of 2 to be critical',
        name: 'Furtive Hit',
      },
      [GameActions.ROGUE_STAB]: {
        tooltip: 'If you are invisible, hit your target for a large amount of damage.',
        name: 'Stab',
      },
      [GameActions.SHAMAN_TOTEM]: {
        tooltip: 'Drop a totem',
        name: 'Totem',
      },
      [GameActions.SHAMAN_UNDERPRESSURE]: {
        tooltip: 'Damage everyone around you including yourself, and stun players that do not belong to your faction.',
        name: 'Under Pressure',
      },
      [GameActions.SHAMAN_THUNDERBOLT]: {
        tooltip: 'Chain of kiff, deal damages.',
        name: 'Under Pressure',
      },
      [GameActions.TANK_INVINCIBILITY]: {
        tooltip: 'Get invincible for a certain amount of time.',
        name: 'Invincibility',
      },
      [GameActions.TANK_SILENCE]: {
        tooltip: 'Silence your target for a certain amount of time.',
        name: 'Tank Silence',
      },
      [GameActions.TOGGLE_SILENCE]: {
        tooltip: 'Silence your target, on and off ! Muhahah',
        name: 'Silence',
      },
      [GameActions.PECNO_TP]: {
        tooltip: 'TP',
        name: 'TP',
      },
      [GameActions.TRAPPER_MINE]: {
        tooltip: 'Drop until 3 mines, stun when dropped, activate within 6 seconds, then mega boum when in mine-zone!',
        name: 'Mine',
      },
      [GameActions.TRAPPER_SNIPER]: {
        tooltip: 'Do not miss for a maximum of damage ! Keeps stealth on hit.',
        name: 'Sniper',
      },
      [GameActions.TRAPPER_STEALTH]: {
        tooltip: 'Become invisible until your next action',
        name: 'Stealth',
      },
      [GameActions.MECHANOTHERAPIST_TOWER_BUILD]: {
        tooltip: 'Build a defense tower that will attack the closest enemy',
        name: 'Tower Build',
      },
      [GameActions.MECHANOTHERAPIST_TOWER_BUFF]: {
        tooltip: 'Increase the damage of the tower for a certain amount of time',
        name: 'Tower Damage Buff',
      },
      [GameActions.MECHANOTHERAPIST_TOWER_HACK]: {
        tooltip: 'Hack the tower to make it belong to your faction for a certain amount of time',
        name: 'Tower Faction Hack',
      },
    },
    healthBar: {
      lvlUp: 'Reach this to level up',
      lvlDown: 'Careful, you will lose your level',
    }
  }
}
