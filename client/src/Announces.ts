import { Game } from './Game'
import { GameEvents } from './GameEventActions'

export class GlobalAnnounce {
  announceElt: HTMLElement = document.createElement('div')
  closeBtnElt: HTMLElement = document.createElement('div')

  constructor(message = 'Je ne sais pas quoi vous dire', messageDuration = 60000) {
    this.announceElt.classList.add('announceElt')
    this.announceElt.style.animationDuration = `${messageDuration}ms`
    this.announceElt.innerText = message
    this.closeBtnElt.classList.add('closeBtnElt')
    this.closeBtnElt.innerText = '❌'
    this.initListeners()
    
    this.announceElt.appendChild(this.closeBtnElt)
    Game.gameZone.appendChild(this.announceElt)

    setTimeout(() => {
      this.announceElt.remove()
      this.closeBtnElt.remove()
    }, messageDuration)
  }

  initListeners() {
    this.announceElt.addEventListener('mousedown', (e: Event) => {
      e.stopPropagation()
      e.preventDefault()
    })
    this.announceElt.addEventListener('contextmenu', (e: Event) => {
      e.stopPropagation()
      e.preventDefault()
    })
    this.closeBtnElt.addEventListener('mousedown', (e: Event) => {
      e.stopPropagation()
      e.preventDefault()
      this.announceElt.remove()
      this.closeBtnElt.remove()
    })
  }
}
